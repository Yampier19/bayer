<nav class="main-header navbar navbar-expand" style="background-color: #e9ecef;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="index.php" class="nav-link">Panel principal</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link logout" href="auth/logout.php">
                <i class="fas fa-door-open"></i> Cerrar Sesión
            </a>
            <style>
                .logout:hover{
                    color: red !important;
                }
            </style>
        </li>
    </ul>
</nav>