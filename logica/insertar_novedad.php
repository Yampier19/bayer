<?php
require_once('session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>insertar</title>
	<style>
		.aviso3 {
			font-size: 130%;
			font-weight: bold;
			color: #11a9e3;
			text-transform: uppercase;
			/*font-family: "Trebuchet MS";
	font-family:"Gill Sans MT";
	border-radius:10px;
	background: #11a9e3;*/
			background-color: transparent;
			text-align: center;
			padding: 10px;
		}
		.error {
			font-size: 130%;
			font-weight: bold;
			color: #fb8305;
			text-transform: uppercase;
			background-color: transparent;
			text-align: center;
			padding: 10px;
		}
		.btn_continuar {
			padding-top: 7px;
			width: 152px;
			height: 37px;
			color: transparent;
			background-color: transparent;
			border-radius: 5px;
			border: 1px solid transparent;
		}
		.btn_continuar:active {
			box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
			box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.3),
				inset 0px 0px 20px #EEECEC;
		}
		.btn_continuar:hover {
			box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.2);
			box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.3),
				inset 0px 0px 20px #EEECEC;
		}
	</style>
</head>
<body>
	<?PHP
require('../datos/parse_str.php');
	require_once("../datos/conex.php");
	if (isset($_POST['registrar'])) {
		$PAP = $_POST['PAP'];
		$ASUNTO = $_POST['ASUNTO'];
		$PRODUCTO = $_POST['PRODUCTO'];
		$NOVEDAD = $_POST['NOVEDAD'];
		$FECHA_REPORTE = $_POST['FECHA_REPORTE'];
		$OBSERVACION = $_POST['OBSERVACION'];
		$FECHA_RESPUESTA = $_POST['FECHA_RESPUESTA'];
		//$OBSERVACION_RESPUESTA = $_POST['OBSERVACION_RESPUESTA'];
		//$telefono3=$_POST['telefono3'];
		//mysql_query("SET NAMES utf8");
		$insertar = mysqli_query($conex,"INSERT INTO bayer_novedades (PAP, ASUNTO, PRODUCTO, NOVEDADES, OBSERVACIONES, FECHA_REPORTE, FECHA_RESPUESTA, OBSERVACION_RESPUESTA, ESTADO)
		VALUES ('" . $PAP . "', '" . $ASUNTO . "', '" . $PRODUCTO . "', '" . $NOVEDAD . "', '" . $OBSERVACION . "', '" . $FECHA_REPORTE . "', '" . $FECHA_RESPUESTA . "', '', 'NUEVO')");
		echo mysqli_error($conex);
		if ($insertar) {
	?>
			<span style="margin-top:5%;">
				<center>
					<img src="../presentacion/imagenes/chulo.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;" />
				</center>
			</span>
			<p class="aviso3" style=" width:68.9%; margin:auto auto;">HA REGISTRADO LA NOVEDAD CORRECTAMENTE.</p>
			<br />
			<br />
			<center>
				<a href="../presentacion/novedades_registro.php" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
			</center>
		<?php
		} else {
		?>
			<span style="margin-top:5%;">
				<center>
					<img src="../presentacion/imagenes/advertencia.png" style="width:50px; margin-top:100px;margin-top:5%;" />
				</center>
			</span>
			<p class="error" style=" width:68.9%; margin:auto auto;">
				<span style="border-left-color:#fff">ERROR EN EL REGISTRO DE NOVEDAD.</span>
			</p>
			<br />
			<br />
			<center>
				<a href="../presentacion/novedades_registro.php" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA.png" style="width:152px; height:37px" /></a>
			</center>
	<?php
		}
	}
	?>
</body>
</html>