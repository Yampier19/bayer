<?php

class reporte_causal_conteo_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function reporte_causal_conteo_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_reporte_causal_conteo";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "reporte_causal_conteo.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['reporte_causal_conteo']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['reporte_causal_conteo']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['reporte_causal_conteo']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->pap = $Busca_temp['pap']; 
          $tmp_pos = strpos($this->pap, "##@@");
          if ($tmp_pos !== false)
          {
              $this->pap = substr($this->pap, 0, $tmp_pos);
          }
          $this->pap_2 = $Busca_temp['pap_input_2']; 
          $this->producto_tratamiento = $Busca_temp['producto_tratamiento']; 
          $tmp_pos = strpos($this->producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->producto_tratamiento = substr($this->producto_tratamiento, 0, $tmp_pos);
          }
          $this->c_conteo = $Busca_temp['c_conteo']; 
          $tmp_pos = strpos($this->c_conteo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->c_conteo = substr($this->c_conteo, 0, $tmp_pos);
          }
          $this->c_conteo_2 = $Busca_temp['c_conteo_input_2']; 
          $this->c_causal_no_visita = $Busca_temp['c_causal_no_visita']; 
          $tmp_pos = strpos($this->c_causal_no_visita, "##@@");
          if ($tmp_pos !== false)
          {
              $this->c_causal_no_visita = substr($this->c_causal_no_visita, 0, $tmp_pos);
          }
          $this->c_fecha_ultimo_registro = $Busca_temp['c_fecha_ultimo_registro']; 
          $tmp_pos = strpos($this->c_fecha_ultimo_registro, "##@@");
          if ($tmp_pos !== false)
          {
              $this->c_fecha_ultimo_registro = substr($this->c_fecha_ultimo_registro, 0, $tmp_pos);
          }
          $this->c_fecha_ultimo_registro_2 = $Busca_temp['c_fecha_ultimo_registro_input_2']; 
          $this->t_asegurador_tratamiento = $Busca_temp['t_asegurador_tratamiento']; 
          $tmp_pos = strpos($this->t_asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_asegurador_tratamiento = substr($this->t_asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->t_operador_logistico_tratamiento = $Busca_temp['t_operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->t_operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_operador_logistico_tratamiento = substr($this->t_operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->t_medico_tratamiento = $Busca_temp['t_medico_tratamiento']; 
          $tmp_pos = strpos($this->t_medico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_medico_tratamiento = substr($this->t_medico_tratamiento, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT P.ID_PACIENTE as pap, T.PRODUCTO_TRATAMIENTO as producto_tratamiento, C.CONTEO as c_conteo, C.CAUSAL_NO_VISITA as c_causal_no_visita, T.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, T.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, T.MEDICO_TRATAMIENTO as t_medico_tratamiento, str_replace (convert(char(10),C.FECHA_ULTIMO_REGISTRO,102), '.', '-') + ' ' + convert(char(8),C.FECHA_ULTIMO_REGISTRO,20) as c_fecha_ultimo_registro from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT P.ID_PACIENTE as pap, T.PRODUCTO_TRATAMIENTO as producto_tratamiento, C.CONTEO as c_conteo, C.CAUSAL_NO_VISITA as c_causal_no_visita, T.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, T.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, T.MEDICO_TRATAMIENTO as t_medico_tratamiento, C.FECHA_ULTIMO_REGISTRO as c_fecha_ultimo_registro from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT P.ID_PACIENTE as pap, T.PRODUCTO_TRATAMIENTO as producto_tratamiento, C.CONTEO as c_conteo, C.CAUSAL_NO_VISITA as c_causal_no_visita, T.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, T.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, T.MEDICO_TRATAMIENTO as t_medico_tratamiento, convert(char(23),C.FECHA_ULTIMO_REGISTRO,121) as c_fecha_ultimo_registro from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT P.ID_PACIENTE as pap, T.PRODUCTO_TRATAMIENTO as producto_tratamiento, C.CONTEO as c_conteo, C.CAUSAL_NO_VISITA as c_causal_no_visita, T.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, T.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, T.MEDICO_TRATAMIENTO as t_medico_tratamiento, C.FECHA_ULTIMO_REGISTRO as c_fecha_ultimo_registro from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT P.ID_PACIENTE as pap, T.PRODUCTO_TRATAMIENTO as producto_tratamiento, C.CONTEO as c_conteo, C.CAUSAL_NO_VISITA as c_causal_no_visita, T.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, T.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, T.MEDICO_TRATAMIENTO as t_medico_tratamiento, EXTEND(C.FECHA_ULTIMO_REGISTRO, YEAR TO FRACTION) as c_fecha_ultimo_registro from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT P.ID_PACIENTE as pap, T.PRODUCTO_TRATAMIENTO as producto_tratamiento, C.CONTEO as c_conteo, C.CAUSAL_NO_VISITA as c_causal_no_visita, T.ASEGURADOR_TRATAMIENTO as t_asegurador_tratamiento, T.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_1, T.MEDICO_TRATAMIENTO as t_medico_tratamiento, C.FECHA_ULTIMO_REGISTRO as c_fecha_ultimo_registro from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['pap'])) ? $this->New_label['pap'] : "PAP"; 
          if ($Cada_col == "pap" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['c_conteo'])) ? $this->New_label['c_conteo'] : "CONTEO"; 
          if ($Cada_col == "c_conteo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['c_causal_no_visita'])) ? $this->New_label['c_causal_no_visita'] : "CAUSAL NO VISITA"; 
          if ($Cada_col == "c_causal_no_visita" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_asegurador_tratamiento'])) ? $this->New_label['t_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "t_asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_operador_logistico_tratamiento'])) ? $this->New_label['t_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "t_operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_medico_tratamiento'])) ? $this->New_label['t_medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          if ($Cada_col == "t_medico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['c_fecha_ultimo_registro'])) ? $this->New_label['c_fecha_ultimo_registro'] : "FECHA ULTIMO REGISTRO"; 
          if ($Cada_col == "c_fecha_ultimo_registro" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->pap = $rs->fields[0] ;  
         $this->pap = (string)$this->pap;
         $this->producto_tratamiento = $rs->fields[1] ;  
         $this->c_conteo = $rs->fields[2] ;  
         $this->c_conteo = (string)$this->c_conteo;
         $this->c_causal_no_visita = $rs->fields[3] ;  
         $this->t_asegurador_tratamiento = $rs->fields[4] ;  
         $this->t_operador_logistico_tratamiento = $rs->fields[5] ;  
         $this->t_medico_tratamiento = $rs->fields[6] ;  
         $this->c_fecha_ultimo_registro = $rs->fields[7] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- pap
   function NM_export_pap()
   {
         $this->pap = html_entity_decode($this->pap, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->pap = strip_tags($this->pap);
         if (!NM_is_utf8($this->pap))
         {
             $this->pap = sc_convert_encoding($this->pap, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->pap = str_replace('<', '&lt;', $this->pap);
         $this->pap = str_replace('>', '&gt;', $this->pap);
         $this->Texto_tag .= "<td>" . $this->pap . "</td>\r\n";
   }
   //----- producto_tratamiento
   function NM_export_producto_tratamiento()
   {
         $this->producto_tratamiento = html_entity_decode($this->producto_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->producto_tratamiento = strip_tags($this->producto_tratamiento);
         if (!NM_is_utf8($this->producto_tratamiento))
         {
             $this->producto_tratamiento = sc_convert_encoding($this->producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->producto_tratamiento = str_replace('<', '&lt;', $this->producto_tratamiento);
         $this->producto_tratamiento = str_replace('>', '&gt;', $this->producto_tratamiento);
         $this->Texto_tag .= "<td>" . $this->producto_tratamiento . "</td>\r\n";
   }
   //----- c_conteo
   function NM_export_c_conteo()
   {
         nmgp_Form_Num_Val($this->c_conteo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->c_conteo))
         {
             $this->c_conteo = sc_convert_encoding($this->c_conteo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->c_conteo = str_replace('<', '&lt;', $this->c_conteo);
         $this->c_conteo = str_replace('>', '&gt;', $this->c_conteo);
         $this->Texto_tag .= "<td>" . $this->c_conteo . "</td>\r\n";
   }
   //----- c_causal_no_visita
   function NM_export_c_causal_no_visita()
   {
         $this->c_causal_no_visita = html_entity_decode($this->c_causal_no_visita, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->c_causal_no_visita = strip_tags($this->c_causal_no_visita);
         if (!NM_is_utf8($this->c_causal_no_visita))
         {
             $this->c_causal_no_visita = sc_convert_encoding($this->c_causal_no_visita, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->c_causal_no_visita = str_replace('<', '&lt;', $this->c_causal_no_visita);
         $this->c_causal_no_visita = str_replace('>', '&gt;', $this->c_causal_no_visita);
         $this->Texto_tag .= "<td>" . $this->c_causal_no_visita . "</td>\r\n";
   }
   //----- t_asegurador_tratamiento
   function NM_export_t_asegurador_tratamiento()
   {
         $this->t_asegurador_tratamiento = html_entity_decode($this->t_asegurador_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_asegurador_tratamiento = strip_tags($this->t_asegurador_tratamiento);
         if (!NM_is_utf8($this->t_asegurador_tratamiento))
         {
             $this->t_asegurador_tratamiento = sc_convert_encoding($this->t_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_asegurador_tratamiento = str_replace('<', '&lt;', $this->t_asegurador_tratamiento);
         $this->t_asegurador_tratamiento = str_replace('>', '&gt;', $this->t_asegurador_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_asegurador_tratamiento . "</td>\r\n";
   }
   //----- t_operador_logistico_tratamiento
   function NM_export_t_operador_logistico_tratamiento()
   {
         $this->t_operador_logistico_tratamiento = html_entity_decode($this->t_operador_logistico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_operador_logistico_tratamiento = strip_tags($this->t_operador_logistico_tratamiento);
         if (!NM_is_utf8($this->t_operador_logistico_tratamiento))
         {
             $this->t_operador_logistico_tratamiento = sc_convert_encoding($this->t_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_operador_logistico_tratamiento = str_replace('<', '&lt;', $this->t_operador_logistico_tratamiento);
         $this->t_operador_logistico_tratamiento = str_replace('>', '&gt;', $this->t_operador_logistico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_operador_logistico_tratamiento . "</td>\r\n";
   }
   //----- t_medico_tratamiento
   function NM_export_t_medico_tratamiento()
   {
         $this->t_medico_tratamiento = html_entity_decode($this->t_medico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_medico_tratamiento = strip_tags($this->t_medico_tratamiento);
         if (!NM_is_utf8($this->t_medico_tratamiento))
         {
             $this->t_medico_tratamiento = sc_convert_encoding($this->t_medico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_medico_tratamiento = str_replace('<', '&lt;', $this->t_medico_tratamiento);
         $this->t_medico_tratamiento = str_replace('>', '&gt;', $this->t_medico_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_medico_tratamiento . "</td>\r\n";
   }
   //----- c_fecha_ultimo_registro
   function NM_export_c_fecha_ultimo_registro()
   {
         $conteudo_x = $this->c_fecha_ultimo_registro;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->c_fecha_ultimo_registro, "YYYY-MM-DD");
             $this->c_fecha_ultimo_registro = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa;hhiiss"));
         } 
         if (!NM_is_utf8($this->c_fecha_ultimo_registro))
         {
             $this->c_fecha_ultimo_registro = sc_convert_encoding($this->c_fecha_ultimo_registro, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->c_fecha_ultimo_registro = str_replace('<', '&lt;', $this->c_fecha_ultimo_registro);
         $this->c_fecha_ultimo_registro = str_replace('>', '&gt;', $this->c_fecha_ultimo_registro);
         $this->Texto_tag .= "<td>" . $this->c_fecha_ultimo_registro . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['reporte_causal_conteo'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Causal Conteo :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="reporte_causal_conteo_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="reporte_causal_conteo"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
