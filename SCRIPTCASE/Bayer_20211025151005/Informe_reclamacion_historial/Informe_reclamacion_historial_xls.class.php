<?php

class Informe_reclamacion_historial_xls
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Xls_dados;
   var $Xls_workbook;
   var $Xls_col;
   var $Xls_row;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();
   var $Arquivo;
   var $Tit_doc;
   //---- 
   function Informe_reclamacion_historial_xls()
   {
   }

   //---- 
   function monta_xls()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $this->Xls_row = 1;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      set_include_path(get_include_path() . PATH_SEPARATOR . $this->Ini->path_third . '/phpexcel/');
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel.php';
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel/IOFactory.php';
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel/Cell/AdvancedValueBinder.php';
      $orig_form_dt = strtoupper($_SESSION['scriptcase']['reg_conf']['date_format']);
      $this->SC_date_conf_region = "";
      for ($i = 0; $i < 8; $i++)
      {
          if ($i > 0 && substr($orig_form_dt, $i, 1) != substr($this->SC_date_conf_region, -1, 1)) {
              $this->SC_date_conf_region .= $_SESSION['scriptcase']['reg_conf']['date_sep'];
          }
          $this->SC_date_conf_region .= substr($orig_form_dt, $i, 1);
      }
      $this->Xls_col    = 0;
      $this->nm_data    = new nm_data("es");
      $this->Arquivo    = "sc_xls";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_Informe_reclamacion_historial";
      $this->Arquivo   .= ".xls";
      $this->Tit_doc    = "Informe_reclamacion_historial.xls";
      $this->Xls_f = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );;
      $this->Xls_dados = new PHPExcel();
      $this->Xls_dados->setActiveSheetIndex(0);
      $this->Nm_ActiveSheet = $this->Xls_dados->getActiveSheet();
      if ($_SESSION['scriptcase']['reg_conf']['css_dir'] == "RTL")
      {
          $this->Nm_ActiveSheet->setRightToLeft(true);
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_paciente = $Busca_temp['id_paciente']; 
          $tmp_pos = strpos($this->id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
          }
          $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
          $this->logro_comunicacion_gestion = $Busca_temp['logro_comunicacion_gestion']; 
          $tmp_pos = strpos($this->logro_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->logro_comunicacion_gestion = substr($this->logro_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->fecha_comunicacion = $Busca_temp['fecha_comunicacion']; 
          $tmp_pos = strpos($this->fecha_comunicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_comunicacion = substr($this->fecha_comunicacion, 0, $tmp_pos);
          }
          $this->autor_gestion = $Busca_temp['autor_gestion']; 
          $tmp_pos = strpos($this->autor_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->autor_gestion = substr($this->autor_gestion, 0, $tmp_pos);
          }
          $this->departamento_paciente = $Busca_temp['departamento_paciente']; 
          $tmp_pos = strpos($this->departamento_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->departamento_paciente = substr($this->departamento_paciente, 0, $tmp_pos);
          }
          $this->ciudad_paciente = $Busca_temp['ciudad_paciente']; 
          $tmp_pos = strpos($this->ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ciudad_paciente = substr($this->ciudad_paciente, 0, $tmp_pos);
          }
          $this->estado_paciente = $Busca_temp['estado_paciente']; 
          $tmp_pos = strpos($this->estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->estado_paciente = substr($this->estado_paciente, 0, $tmp_pos);
          }
          $this->status_paciente = $Busca_temp['status_paciente']; 
          $tmp_pos = strpos($this->status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->status_paciente = substr($this->status_paciente, 0, $tmp_pos);
          }
          $this->fecha_activacion_paciente = $Busca_temp['fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_activacion_paciente = substr($this->fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->codigo_xofigo = $Busca_temp['codigo_xofigo']; 
          $tmp_pos = strpos($this->codigo_xofigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->codigo_xofigo = substr($this->codigo_xofigo, 0, $tmp_pos);
          }
          $this->codigo_xofigo_2 = $Busca_temp['codigo_xofigo_input_2']; 
          $this->producto_tratamiento = $Busca_temp['producto_tratamiento']; 
          $tmp_pos = strpos($this->producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->producto_tratamiento = substr($this->producto_tratamiento, 0, $tmp_pos);
          }
          $this->nombre_referencia = $Busca_temp['nombre_referencia']; 
          $tmp_pos = strpos($this->nombre_referencia, "##@@");
          if ($tmp_pos !== false)
          {
              $this->nombre_referencia = substr($this->nombre_referencia, 0, $tmp_pos);
          }
          $this->dosis_tratamiento = $Busca_temp['dosis_tratamiento']; 
          $tmp_pos = strpos($this->dosis_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->dosis_tratamiento = substr($this->dosis_tratamiento, 0, $tmp_pos);
          }
          $this->numero_cajas = $Busca_temp['numero_cajas']; 
          $tmp_pos = strpos($this->numero_cajas, "##@@");
          if ($tmp_pos !== false)
          {
              $this->numero_cajas = substr($this->numero_cajas, 0, $tmp_pos);
          }
          $this->asegurador_tratamiento = $Busca_temp['asegurador_tratamiento']; 
          $tmp_pos = strpos($this->asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->asegurador_tratamiento = substr($this->asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->operador_logistico_tratamiento = $Busca_temp['operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->operador_logistico_tratamiento = substr($this->operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->punto_entrega = $Busca_temp['punto_entrega']; 
          $tmp_pos = strpos($this->punto_entrega, "##@@");
          if ($tmp_pos !== false)
          {
              $this->punto_entrega = substr($this->punto_entrega, 0, $tmp_pos);
          }
          $this->reclamo_gestion = $Busca_temp['reclamo_gestion']; 
          $tmp_pos = strpos($this->reclamo_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->reclamo_gestion = substr($this->reclamo_gestion, 0, $tmp_pos);
          }
          $this->fecha_reclamacion_gestion = $Busca_temp['fecha_reclamacion_gestion']; 
          $tmp_pos = strpos($this->fecha_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_reclamacion_gestion = substr($this->fecha_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->causa_no_reclamacion_gestion = $Busca_temp['causa_no_reclamacion_gestion']; 
          $tmp_pos = strpos($this->causa_no_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->causa_no_reclamacion_gestion = substr($this->causa_no_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->id_historial_reclamacion = $Busca_temp['id_historial_reclamacion']; 
          $tmp_pos = strpos($this->id_historial_reclamacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_historial_reclamacion = substr($this->id_historial_reclamacion, 0, $tmp_pos);
          }
          $this->id_historial_reclamacion_2 = $Busca_temp['id_historial_reclamacion_input_2']; 
          $this->anio_historial_reclamacion = $Busca_temp['anio_historial_reclamacion']; 
          $tmp_pos = strpos($this->anio_historial_reclamacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->anio_historial_reclamacion = substr($this->anio_historial_reclamacion, 0, $tmp_pos);
          }
          $this->anio_historial_reclamacion_2 = $Busca_temp['anio_historial_reclamacion_input_2']; 
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xls_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xls_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xls_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xls_name']);
          $this->Xls_f = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
       } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
       } 
      else 
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['logro_comunicacion_gestion'])) ? $this->New_label['logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          if ($Cada_col == "logro_comunicacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_comunicacion'])) ? $this->New_label['fecha_comunicacion'] : "FECHA COMUNICACION"; 
          if ($Cada_col == "fecha_comunicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['autor_gestion'])) ? $this->New_label['autor_gestion'] : "AUTOR GESTION"; 
          if ($Cada_col == "autor_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['departamento_paciente'])) ? $this->New_label['departamento_paciente'] : "DEPARTAMENTO PACIENTE"; 
          if ($Cada_col == "departamento_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['ciudad_paciente'])) ? $this->New_label['ciudad_paciente'] : "CIUDAD PACIENTE"; 
          if ($Cada_col == "ciudad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['status_paciente'])) ? $this->New_label['status_paciente'] : "STATUS PACIENTE"; 
          if ($Cada_col == "status_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['codigo_xofigo'])) ? $this->New_label['codigo_xofigo'] : "CODIGO XOFIGO"; 
          if ($Cada_col == "codigo_xofigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['nombre_referencia'])) ? $this->New_label['nombre_referencia'] : "NOMBRE REFERENCIA"; 
          if ($Cada_col == "nombre_referencia" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['dosis_tratamiento'])) ? $this->New_label['dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          if ($Cada_col == "dosis_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['numero_cajas'])) ? $this->New_label['numero_cajas'] : "NUMERO CAJAS"; 
          if ($Cada_col == "numero_cajas" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['asegurador_tratamiento'])) ? $this->New_label['asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['operador_logistico_tratamiento'])) ? $this->New_label['operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['punto_entrega'])) ? $this->New_label['punto_entrega'] : "PUNTO ENTREGA"; 
          if ($Cada_col == "punto_entrega" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo_gestion'])) ? $this->New_label['reclamo_gestion'] : "RECLAMO GESTION"; 
          if ($Cada_col == "reclamo_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion_gestion'])) ? $this->New_label['fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          if ($Cada_col == "fecha_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['causa_no_reclamacion_gestion'])) ? $this->New_label['causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          if ($Cada_col == "causa_no_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['id_historial_reclamacion'])) ? $this->New_label['id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION"; 
          if ($Cada_col == "id_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['anio_historial_reclamacion'])) ? $this->New_label['anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION"; 
          if ($Cada_col == "anio_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes1'])) ? $this->New_label['mes1'] : "MES1"; 
          if ($Cada_col == "mes1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo1'])) ? $this->New_label['reclamo1'] : "RECLAMO1"; 
          if ($Cada_col == "reclamo1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion1'])) ? $this->New_label['fecha_reclamacion1'] : "FECHA RECLAMACION1"; 
          if ($Cada_col == "fecha_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion1'])) ? $this->New_label['motivo_no_reclamacion1'] : "MOTIVO NO RECLAMACION1"; 
          if ($Cada_col == "motivo_no_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes2'])) ? $this->New_label['mes2'] : "MES2"; 
          if ($Cada_col == "mes2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo2'])) ? $this->New_label['reclamo2'] : "RECLAMO2"; 
          if ($Cada_col == "reclamo2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion2'])) ? $this->New_label['fecha_reclamacion2'] : "FECHA RECLAMACION2"; 
          if ($Cada_col == "fecha_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion2'])) ? $this->New_label['motivo_no_reclamacion2'] : "MOTIVO NO RECLAMACION2"; 
          if ($Cada_col == "motivo_no_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes3'])) ? $this->New_label['mes3'] : "MES3"; 
          if ($Cada_col == "mes3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo3'])) ? $this->New_label['reclamo3'] : "RECLAMO3"; 
          if ($Cada_col == "reclamo3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion3'])) ? $this->New_label['fecha_reclamacion3'] : "FECHA RECLAMACION3"; 
          if ($Cada_col == "fecha_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion3'])) ? $this->New_label['motivo_no_reclamacion3'] : "MOTIVO NO RECLAMACION3"; 
          if ($Cada_col == "motivo_no_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes4'])) ? $this->New_label['mes4'] : "MES4"; 
          if ($Cada_col == "mes4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo4'])) ? $this->New_label['reclamo4'] : "RECLAMO4"; 
          if ($Cada_col == "reclamo4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion4'])) ? $this->New_label['fecha_reclamacion4'] : "FECHA RECLAMACION4"; 
          if ($Cada_col == "fecha_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion4'])) ? $this->New_label['motivo_no_reclamacion4'] : "MOTIVO NO RECLAMACION4"; 
          if ($Cada_col == "motivo_no_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes5'])) ? $this->New_label['mes5'] : "MES5"; 
          if ($Cada_col == "mes5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo5'])) ? $this->New_label['reclamo5'] : "RECLAMO5"; 
          if ($Cada_col == "reclamo5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion5'])) ? $this->New_label['fecha_reclamacion5'] : "FECHA RECLAMACION5"; 
          if ($Cada_col == "fecha_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion5'])) ? $this->New_label['motivo_no_reclamacion5'] : "MOTIVO NO RECLAMACION5"; 
          if ($Cada_col == "motivo_no_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes6'])) ? $this->New_label['mes6'] : "MES6"; 
          if ($Cada_col == "mes6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo6'])) ? $this->New_label['reclamo6'] : "RECLAMO6"; 
          if ($Cada_col == "reclamo6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion6'])) ? $this->New_label['fecha_reclamacion6'] : "FECHA RECLAMACION6"; 
          if ($Cada_col == "fecha_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion6'])) ? $this->New_label['motivo_no_reclamacion6'] : "MOTIVO NO RECLAMACION6"; 
          if ($Cada_col == "motivo_no_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes7'])) ? $this->New_label['mes7'] : "MES7"; 
          if ($Cada_col == "mes7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo7'])) ? $this->New_label['reclamo7'] : "RECLAMO7"; 
          if ($Cada_col == "reclamo7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion7'])) ? $this->New_label['fecha_reclamacion7'] : "FECHA RECLAMACION7"; 
          if ($Cada_col == "fecha_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion7'])) ? $this->New_label['motivo_no_reclamacion7'] : "MOTIVO NO RECLAMACION7"; 
          if ($Cada_col == "motivo_no_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes8'])) ? $this->New_label['mes8'] : "MES8"; 
          if ($Cada_col == "mes8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo8'])) ? $this->New_label['reclamo8'] : "RECLAMO8"; 
          if ($Cada_col == "reclamo8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion8'])) ? $this->New_label['fecha_reclamacion8'] : "FECHA RECLAMACION8"; 
          if ($Cada_col == "fecha_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion8'])) ? $this->New_label['motivo_no_reclamacion8'] : "MOTIVO NO RECLAMACION8"; 
          if ($Cada_col == "motivo_no_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes9'])) ? $this->New_label['mes9'] : "MES9"; 
          if ($Cada_col == "mes9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo9'])) ? $this->New_label['reclamo9'] : "RECLAMO9"; 
          if ($Cada_col == "reclamo9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion9'])) ? $this->New_label['fecha_reclamacion9'] : "FECHA RECLAMACION9"; 
          if ($Cada_col == "fecha_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion9'])) ? $this->New_label['motivo_no_reclamacion9'] : "MOTIVO NO RECLAMACION9"; 
          if ($Cada_col == "motivo_no_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes10'])) ? $this->New_label['mes10'] : "MES10"; 
          if ($Cada_col == "mes10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo10'])) ? $this->New_label['reclamo10'] : "RECLAMO10"; 
          if ($Cada_col == "reclamo10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion10'])) ? $this->New_label['fecha_reclamacion10'] : "FECHA RECLAMACION10"; 
          if ($Cada_col == "fecha_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion10'])) ? $this->New_label['motivo_no_reclamacion10'] : "MOTIVO NO RECLAMACION10"; 
          if ($Cada_col == "motivo_no_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes11'])) ? $this->New_label['mes11'] : "MES11"; 
          if ($Cada_col == "mes11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo11'])) ? $this->New_label['reclamo11'] : "RECLAMO11"; 
          if ($Cada_col == "reclamo11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion11'])) ? $this->New_label['fecha_reclamacion11'] : "FECHA RECLAMACION11"; 
          if ($Cada_col == "fecha_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion11'])) ? $this->New_label['motivo_no_reclamacion11'] : "MOTIVO NO RECLAMACION11"; 
          if ($Cada_col == "motivo_no_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['mes12'])) ? $this->New_label['mes12'] : "MES12"; 
          if ($Cada_col == "mes12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo12'])) ? $this->New_label['reclamo12'] : "RECLAMO12"; 
          if ($Cada_col == "reclamo12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion12'])) ? $this->New_label['fecha_reclamacion12'] : "FECHA RECLAMACION12"; 
          if ($Cada_col == "fecha_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion12'])) ? $this->New_label['motivo_no_reclamacion12'] : "MOTIVO NO RECLAMACION12"; 
          if ($Cada_col == "motivo_no_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
      } 
      while (!$rs->EOF)
      {
         $this->Xls_col = 0;
         $this->Xls_row++;
         $this->id_paciente = $rs->fields[0] ;  
         $this->id_paciente = (string)$this->id_paciente;
         $this->logro_comunicacion_gestion = $rs->fields[1] ;  
         $this->fecha_comunicacion = $rs->fields[2] ;  
         $this->autor_gestion = $rs->fields[3] ;  
         $this->departamento_paciente = $rs->fields[4] ;  
         $this->ciudad_paciente = $rs->fields[5] ;  
         $this->estado_paciente = $rs->fields[6] ;  
         $this->status_paciente = $rs->fields[7] ;  
         $this->fecha_activacion_paciente = $rs->fields[8] ;  
         $this->codigo_xofigo = $rs->fields[9] ;  
         $this->codigo_xofigo = (string)$this->codigo_xofigo;
         $this->producto_tratamiento = $rs->fields[10] ;  
         $this->nombre_referencia = $rs->fields[11] ;  
         $this->dosis_tratamiento = $rs->fields[12] ;  
         $this->numero_cajas = $rs->fields[13] ;  
         $this->asegurador_tratamiento = $rs->fields[14] ;  
         $this->operador_logistico_tratamiento = $rs->fields[15] ;  
         $this->punto_entrega = $rs->fields[16] ;  
         $this->reclamo_gestion = $rs->fields[17] ;  
         $this->fecha_reclamacion_gestion = $rs->fields[18] ;  
         $this->causa_no_reclamacion_gestion = $rs->fields[19] ;  
         $this->id_historial_reclamacion = $rs->fields[20] ;  
         $this->id_historial_reclamacion = (string)$this->id_historial_reclamacion;
         $this->anio_historial_reclamacion = $rs->fields[21] ;  
         $this->anio_historial_reclamacion = (string)$this->anio_historial_reclamacion;
         $this->mes1 = $rs->fields[22] ;  
         $this->reclamo1 = $rs->fields[23] ;  
         $this->fecha_reclamacion1 = $rs->fields[24] ;  
         $this->motivo_no_reclamacion1 = $rs->fields[25] ;  
         $this->mes2 = $rs->fields[26] ;  
         $this->reclamo2 = $rs->fields[27] ;  
         $this->fecha_reclamacion2 = $rs->fields[28] ;  
         $this->motivo_no_reclamacion2 = $rs->fields[29] ;  
         $this->mes3 = $rs->fields[30] ;  
         $this->reclamo3 = $rs->fields[31] ;  
         $this->fecha_reclamacion3 = $rs->fields[32] ;  
         $this->motivo_no_reclamacion3 = $rs->fields[33] ;  
         $this->mes4 = $rs->fields[34] ;  
         $this->reclamo4 = $rs->fields[35] ;  
         $this->fecha_reclamacion4 = $rs->fields[36] ;  
         $this->motivo_no_reclamacion4 = $rs->fields[37] ;  
         $this->mes5 = $rs->fields[38] ;  
         $this->reclamo5 = $rs->fields[39] ;  
         $this->fecha_reclamacion5 = $rs->fields[40] ;  
         $this->motivo_no_reclamacion5 = $rs->fields[41] ;  
         $this->mes6 = $rs->fields[42] ;  
         $this->reclamo6 = $rs->fields[43] ;  
         $this->fecha_reclamacion6 = $rs->fields[44] ;  
         $this->motivo_no_reclamacion6 = $rs->fields[45] ;  
         $this->mes7 = $rs->fields[46] ;  
         $this->reclamo7 = $rs->fields[47] ;  
         $this->fecha_reclamacion7 = $rs->fields[48] ;  
         $this->motivo_no_reclamacion7 = $rs->fields[49] ;  
         $this->mes8 = $rs->fields[50] ;  
         $this->reclamo8 = $rs->fields[51] ;  
         $this->fecha_reclamacion8 = $rs->fields[52] ;  
         $this->motivo_no_reclamacion8 = $rs->fields[53] ;  
         $this->mes9 = $rs->fields[54] ;  
         $this->reclamo9 = $rs->fields[55] ;  
         $this->fecha_reclamacion9 = $rs->fields[56] ;  
         $this->motivo_no_reclamacion9 = $rs->fields[57] ;  
         $this->mes10 = $rs->fields[58] ;  
         $this->reclamo10 = $rs->fields[59] ;  
         $this->fecha_reclamacion10 = $rs->fields[60] ;  
         $this->motivo_no_reclamacion10 = $rs->fields[61] ;  
         $this->mes11 = $rs->fields[62] ;  
         $this->reclamo11 = $rs->fields[63] ;  
         $this->fecha_reclamacion11 = $rs->fields[64] ;  
         $this->motivo_no_reclamacion11 = $rs->fields[65] ;  
         $this->mes12 = $rs->fields[66] ;  
         $this->reclamo12 = $rs->fields[67] ;  
         $this->fecha_reclamacion12 = $rs->fields[68] ;  
         $this->motivo_no_reclamacion12 = $rs->fields[69] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         if (isset($this->NM_Row_din[$this->Xls_row]))
         { 
             $this->Nm_ActiveSheet->getRowDimension($this->Xls_row)->setRowHeight($this->NM_Row_din[$this->Xls_row]);
         } 
         $rs->MoveNext();
      }
      if (isset($this->NM_Col_din))
      { 
          foreach ($this->NM_Col_din as $col => $width)
          { 
              $this->Nm_ActiveSheet->getColumnDimension($col)->setWidth($width / 5);
          } 
      } 
      $rs->Close();
      $objWriter = new PHPExcel_Writer_Excel5($this->Xls_dados);
      $objWriter->save($this->Xls_f);
   }
   //----- id_paciente
   function NM_export_id_paciente()
   {
         $this->id_paciente = html_entity_decode($this->id_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->id_paciente = strip_tags($this->id_paciente);
         if (!NM_is_utf8($this->id_paciente))
         {
             $this->id_paciente = sc_convert_encoding($this->id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->id_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- logro_comunicacion_gestion
   function NM_export_logro_comunicacion_gestion()
   {
         $this->logro_comunicacion_gestion = html_entity_decode($this->logro_comunicacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->logro_comunicacion_gestion = strip_tags($this->logro_comunicacion_gestion);
         if (!NM_is_utf8($this->logro_comunicacion_gestion))
         {
             $this->logro_comunicacion_gestion = sc_convert_encoding($this->logro_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->logro_comunicacion_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_comunicacion
   function NM_export_fecha_comunicacion()
   {
         $this->fecha_comunicacion = html_entity_decode($this->fecha_comunicacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_comunicacion = strip_tags($this->fecha_comunicacion);
         if (!NM_is_utf8($this->fecha_comunicacion))
         {
             $this->fecha_comunicacion = sc_convert_encoding($this->fecha_comunicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_comunicacion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- autor_gestion
   function NM_export_autor_gestion()
   {
         $this->autor_gestion = html_entity_decode($this->autor_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->autor_gestion = strip_tags($this->autor_gestion);
         if (!NM_is_utf8($this->autor_gestion))
         {
             $this->autor_gestion = sc_convert_encoding($this->autor_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->autor_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- departamento_paciente
   function NM_export_departamento_paciente()
   {
         $this->departamento_paciente = html_entity_decode($this->departamento_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->departamento_paciente = strip_tags($this->departamento_paciente);
         if (!NM_is_utf8($this->departamento_paciente))
         {
             $this->departamento_paciente = sc_convert_encoding($this->departamento_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->departamento_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- ciudad_paciente
   function NM_export_ciudad_paciente()
   {
         $this->ciudad_paciente = html_entity_decode($this->ciudad_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ciudad_paciente = strip_tags($this->ciudad_paciente);
         if (!NM_is_utf8($this->ciudad_paciente))
         {
             $this->ciudad_paciente = sc_convert_encoding($this->ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->ciudad_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- estado_paciente
   function NM_export_estado_paciente()
   {
         $this->estado_paciente = html_entity_decode($this->estado_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->estado_paciente = strip_tags($this->estado_paciente);
         if (!NM_is_utf8($this->estado_paciente))
         {
             $this->estado_paciente = sc_convert_encoding($this->estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->estado_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- status_paciente
   function NM_export_status_paciente()
   {
         $this->status_paciente = html_entity_decode($this->status_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->status_paciente = strip_tags($this->status_paciente);
         if (!NM_is_utf8($this->status_paciente))
         {
             $this->status_paciente = sc_convert_encoding($this->status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->status_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_activacion_paciente
   function NM_export_fecha_activacion_paciente()
   {
         $this->fecha_activacion_paciente = html_entity_decode($this->fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_activacion_paciente = strip_tags($this->fecha_activacion_paciente);
         if (!NM_is_utf8($this->fecha_activacion_paciente))
         {
             $this->fecha_activacion_paciente = sc_convert_encoding($this->fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_activacion_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- codigo_xofigo
   function NM_export_codigo_xofigo()
   {
         $this->codigo_xofigo = html_entity_decode($this->codigo_xofigo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->codigo_xofigo = strip_tags($this->codigo_xofigo);
         if (!NM_is_utf8($this->codigo_xofigo))
         {
             $this->codigo_xofigo = sc_convert_encoding($this->codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->codigo_xofigo, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- producto_tratamiento
   function NM_export_producto_tratamiento()
   {
         $this->producto_tratamiento = html_entity_decode($this->producto_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->producto_tratamiento = strip_tags($this->producto_tratamiento);
         if (!NM_is_utf8($this->producto_tratamiento))
         {
             $this->producto_tratamiento = sc_convert_encoding($this->producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->producto_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- nombre_referencia
   function NM_export_nombre_referencia()
   {
         $this->nombre_referencia = html_entity_decode($this->nombre_referencia, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->nombre_referencia = strip_tags($this->nombre_referencia);
         if (!NM_is_utf8($this->nombre_referencia))
         {
             $this->nombre_referencia = sc_convert_encoding($this->nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->nombre_referencia, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- dosis_tratamiento
   function NM_export_dosis_tratamiento()
   {
         $this->dosis_tratamiento = html_entity_decode($this->dosis_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->dosis_tratamiento = strip_tags($this->dosis_tratamiento);
         if (!NM_is_utf8($this->dosis_tratamiento))
         {
             $this->dosis_tratamiento = sc_convert_encoding($this->dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->dosis_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- numero_cajas
   function NM_export_numero_cajas()
   {
         $this->numero_cajas = html_entity_decode($this->numero_cajas, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->numero_cajas = strip_tags($this->numero_cajas);
         if (!NM_is_utf8($this->numero_cajas))
         {
             $this->numero_cajas = sc_convert_encoding($this->numero_cajas, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->numero_cajas, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- asegurador_tratamiento
   function NM_export_asegurador_tratamiento()
   {
         $this->asegurador_tratamiento = html_entity_decode($this->asegurador_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->asegurador_tratamiento = strip_tags($this->asegurador_tratamiento);
         if (!NM_is_utf8($this->asegurador_tratamiento))
         {
             $this->asegurador_tratamiento = sc_convert_encoding($this->asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->asegurador_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- operador_logistico_tratamiento
   function NM_export_operador_logistico_tratamiento()
   {
         $this->operador_logistico_tratamiento = html_entity_decode($this->operador_logistico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->operador_logistico_tratamiento = strip_tags($this->operador_logistico_tratamiento);
         if (!NM_is_utf8($this->operador_logistico_tratamiento))
         {
             $this->operador_logistico_tratamiento = sc_convert_encoding($this->operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->operador_logistico_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- punto_entrega
   function NM_export_punto_entrega()
   {
         $this->punto_entrega = html_entity_decode($this->punto_entrega, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->punto_entrega = strip_tags($this->punto_entrega);
         if (!NM_is_utf8($this->punto_entrega))
         {
             $this->punto_entrega = sc_convert_encoding($this->punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->punto_entrega, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo_gestion
   function NM_export_reclamo_gestion()
   {
         $this->reclamo_gestion = html_entity_decode($this->reclamo_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo_gestion = strip_tags($this->reclamo_gestion);
         if (!NM_is_utf8($this->reclamo_gestion))
         {
             $this->reclamo_gestion = sc_convert_encoding($this->reclamo_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion_gestion
   function NM_export_fecha_reclamacion_gestion()
   {
         $this->fecha_reclamacion_gestion = html_entity_decode($this->fecha_reclamacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion_gestion = strip_tags($this->fecha_reclamacion_gestion);
         if (!NM_is_utf8($this->fecha_reclamacion_gestion))
         {
             $this->fecha_reclamacion_gestion = sc_convert_encoding($this->fecha_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- causa_no_reclamacion_gestion
   function NM_export_causa_no_reclamacion_gestion()
   {
         $this->causa_no_reclamacion_gestion = html_entity_decode($this->causa_no_reclamacion_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->causa_no_reclamacion_gestion = strip_tags($this->causa_no_reclamacion_gestion);
         if (!NM_is_utf8($this->causa_no_reclamacion_gestion))
         {
             $this->causa_no_reclamacion_gestion = sc_convert_encoding($this->causa_no_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->causa_no_reclamacion_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- id_historial_reclamacion
   function NM_export_id_historial_reclamacion()
   {
         $this->id_historial_reclamacion = html_entity_decode($this->id_historial_reclamacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->id_historial_reclamacion = strip_tags($this->id_historial_reclamacion);
         if (!NM_is_utf8($this->id_historial_reclamacion))
         {
             $this->id_historial_reclamacion = sc_convert_encoding($this->id_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->id_historial_reclamacion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- anio_historial_reclamacion
   function NM_export_anio_historial_reclamacion()
   {
         $this->anio_historial_reclamacion = html_entity_decode($this->anio_historial_reclamacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->anio_historial_reclamacion = strip_tags($this->anio_historial_reclamacion);
         if (!NM_is_utf8($this->anio_historial_reclamacion))
         {
             $this->anio_historial_reclamacion = sc_convert_encoding($this->anio_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->anio_historial_reclamacion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes1
   function NM_export_mes1()
   {
         $this->mes1 = html_entity_decode($this->mes1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes1 = strip_tags($this->mes1);
         if (!NM_is_utf8($this->mes1))
         {
             $this->mes1 = sc_convert_encoding($this->mes1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo1
   function NM_export_reclamo1()
   {
         $this->reclamo1 = html_entity_decode($this->reclamo1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo1 = strip_tags($this->reclamo1);
         if (!NM_is_utf8($this->reclamo1))
         {
             $this->reclamo1 = sc_convert_encoding($this->reclamo1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion1
   function NM_export_fecha_reclamacion1()
   {
         $this->fecha_reclamacion1 = html_entity_decode($this->fecha_reclamacion1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion1 = strip_tags($this->fecha_reclamacion1);
         if (!NM_is_utf8($this->fecha_reclamacion1))
         {
             $this->fecha_reclamacion1 = sc_convert_encoding($this->fecha_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion1
   function NM_export_motivo_no_reclamacion1()
   {
         $this->motivo_no_reclamacion1 = html_entity_decode($this->motivo_no_reclamacion1, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion1 = strip_tags($this->motivo_no_reclamacion1);
         if (!NM_is_utf8($this->motivo_no_reclamacion1))
         {
             $this->motivo_no_reclamacion1 = sc_convert_encoding($this->motivo_no_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion1, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes2
   function NM_export_mes2()
   {
         $this->mes2 = html_entity_decode($this->mes2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes2 = strip_tags($this->mes2);
         if (!NM_is_utf8($this->mes2))
         {
             $this->mes2 = sc_convert_encoding($this->mes2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo2
   function NM_export_reclamo2()
   {
         $this->reclamo2 = html_entity_decode($this->reclamo2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo2 = strip_tags($this->reclamo2);
         if (!NM_is_utf8($this->reclamo2))
         {
             $this->reclamo2 = sc_convert_encoding($this->reclamo2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion2
   function NM_export_fecha_reclamacion2()
   {
         $this->fecha_reclamacion2 = html_entity_decode($this->fecha_reclamacion2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion2 = strip_tags($this->fecha_reclamacion2);
         if (!NM_is_utf8($this->fecha_reclamacion2))
         {
             $this->fecha_reclamacion2 = sc_convert_encoding($this->fecha_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion2
   function NM_export_motivo_no_reclamacion2()
   {
         $this->motivo_no_reclamacion2 = html_entity_decode($this->motivo_no_reclamacion2, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion2 = strip_tags($this->motivo_no_reclamacion2);
         if (!NM_is_utf8($this->motivo_no_reclamacion2))
         {
             $this->motivo_no_reclamacion2 = sc_convert_encoding($this->motivo_no_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion2, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes3
   function NM_export_mes3()
   {
         $this->mes3 = html_entity_decode($this->mes3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes3 = strip_tags($this->mes3);
         if (!NM_is_utf8($this->mes3))
         {
             $this->mes3 = sc_convert_encoding($this->mes3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo3
   function NM_export_reclamo3()
   {
         $this->reclamo3 = html_entity_decode($this->reclamo3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo3 = strip_tags($this->reclamo3);
         if (!NM_is_utf8($this->reclamo3))
         {
             $this->reclamo3 = sc_convert_encoding($this->reclamo3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion3
   function NM_export_fecha_reclamacion3()
   {
         $this->fecha_reclamacion3 = html_entity_decode($this->fecha_reclamacion3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion3 = strip_tags($this->fecha_reclamacion3);
         if (!NM_is_utf8($this->fecha_reclamacion3))
         {
             $this->fecha_reclamacion3 = sc_convert_encoding($this->fecha_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion3
   function NM_export_motivo_no_reclamacion3()
   {
         $this->motivo_no_reclamacion3 = html_entity_decode($this->motivo_no_reclamacion3, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion3 = strip_tags($this->motivo_no_reclamacion3);
         if (!NM_is_utf8($this->motivo_no_reclamacion3))
         {
             $this->motivo_no_reclamacion3 = sc_convert_encoding($this->motivo_no_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion3, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes4
   function NM_export_mes4()
   {
         $this->mes4 = html_entity_decode($this->mes4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes4 = strip_tags($this->mes4);
         if (!NM_is_utf8($this->mes4))
         {
             $this->mes4 = sc_convert_encoding($this->mes4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo4
   function NM_export_reclamo4()
   {
         $this->reclamo4 = html_entity_decode($this->reclamo4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo4 = strip_tags($this->reclamo4);
         if (!NM_is_utf8($this->reclamo4))
         {
             $this->reclamo4 = sc_convert_encoding($this->reclamo4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion4
   function NM_export_fecha_reclamacion4()
   {
         $this->fecha_reclamacion4 = html_entity_decode($this->fecha_reclamacion4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion4 = strip_tags($this->fecha_reclamacion4);
         if (!NM_is_utf8($this->fecha_reclamacion4))
         {
             $this->fecha_reclamacion4 = sc_convert_encoding($this->fecha_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion4
   function NM_export_motivo_no_reclamacion4()
   {
         $this->motivo_no_reclamacion4 = html_entity_decode($this->motivo_no_reclamacion4, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion4 = strip_tags($this->motivo_no_reclamacion4);
         if (!NM_is_utf8($this->motivo_no_reclamacion4))
         {
             $this->motivo_no_reclamacion4 = sc_convert_encoding($this->motivo_no_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion4, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes5
   function NM_export_mes5()
   {
         $this->mes5 = html_entity_decode($this->mes5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes5 = strip_tags($this->mes5);
         if (!NM_is_utf8($this->mes5))
         {
             $this->mes5 = sc_convert_encoding($this->mes5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo5
   function NM_export_reclamo5()
   {
         $this->reclamo5 = html_entity_decode($this->reclamo5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo5 = strip_tags($this->reclamo5);
         if (!NM_is_utf8($this->reclamo5))
         {
             $this->reclamo5 = sc_convert_encoding($this->reclamo5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion5
   function NM_export_fecha_reclamacion5()
   {
         $this->fecha_reclamacion5 = html_entity_decode($this->fecha_reclamacion5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion5 = strip_tags($this->fecha_reclamacion5);
         if (!NM_is_utf8($this->fecha_reclamacion5))
         {
             $this->fecha_reclamacion5 = sc_convert_encoding($this->fecha_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion5
   function NM_export_motivo_no_reclamacion5()
   {
         $this->motivo_no_reclamacion5 = html_entity_decode($this->motivo_no_reclamacion5, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion5 = strip_tags($this->motivo_no_reclamacion5);
         if (!NM_is_utf8($this->motivo_no_reclamacion5))
         {
             $this->motivo_no_reclamacion5 = sc_convert_encoding($this->motivo_no_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion5, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes6
   function NM_export_mes6()
   {
         $this->mes6 = html_entity_decode($this->mes6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes6 = strip_tags($this->mes6);
         if (!NM_is_utf8($this->mes6))
         {
             $this->mes6 = sc_convert_encoding($this->mes6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo6
   function NM_export_reclamo6()
   {
         $this->reclamo6 = html_entity_decode($this->reclamo6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo6 = strip_tags($this->reclamo6);
         if (!NM_is_utf8($this->reclamo6))
         {
             $this->reclamo6 = sc_convert_encoding($this->reclamo6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion6
   function NM_export_fecha_reclamacion6()
   {
         $this->fecha_reclamacion6 = html_entity_decode($this->fecha_reclamacion6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion6 = strip_tags($this->fecha_reclamacion6);
         if (!NM_is_utf8($this->fecha_reclamacion6))
         {
             $this->fecha_reclamacion6 = sc_convert_encoding($this->fecha_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion6
   function NM_export_motivo_no_reclamacion6()
   {
         $this->motivo_no_reclamacion6 = html_entity_decode($this->motivo_no_reclamacion6, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion6 = strip_tags($this->motivo_no_reclamacion6);
         if (!NM_is_utf8($this->motivo_no_reclamacion6))
         {
             $this->motivo_no_reclamacion6 = sc_convert_encoding($this->motivo_no_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion6, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes7
   function NM_export_mes7()
   {
         $this->mes7 = html_entity_decode($this->mes7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes7 = strip_tags($this->mes7);
         if (!NM_is_utf8($this->mes7))
         {
             $this->mes7 = sc_convert_encoding($this->mes7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo7
   function NM_export_reclamo7()
   {
         $this->reclamo7 = html_entity_decode($this->reclamo7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo7 = strip_tags($this->reclamo7);
         if (!NM_is_utf8($this->reclamo7))
         {
             $this->reclamo7 = sc_convert_encoding($this->reclamo7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion7
   function NM_export_fecha_reclamacion7()
   {
         $this->fecha_reclamacion7 = html_entity_decode($this->fecha_reclamacion7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion7 = strip_tags($this->fecha_reclamacion7);
         if (!NM_is_utf8($this->fecha_reclamacion7))
         {
             $this->fecha_reclamacion7 = sc_convert_encoding($this->fecha_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion7
   function NM_export_motivo_no_reclamacion7()
   {
         $this->motivo_no_reclamacion7 = html_entity_decode($this->motivo_no_reclamacion7, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion7 = strip_tags($this->motivo_no_reclamacion7);
         if (!NM_is_utf8($this->motivo_no_reclamacion7))
         {
             $this->motivo_no_reclamacion7 = sc_convert_encoding($this->motivo_no_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion7, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes8
   function NM_export_mes8()
   {
         $this->mes8 = html_entity_decode($this->mes8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes8 = strip_tags($this->mes8);
         if (!NM_is_utf8($this->mes8))
         {
             $this->mes8 = sc_convert_encoding($this->mes8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo8
   function NM_export_reclamo8()
   {
         $this->reclamo8 = html_entity_decode($this->reclamo8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo8 = strip_tags($this->reclamo8);
         if (!NM_is_utf8($this->reclamo8))
         {
             $this->reclamo8 = sc_convert_encoding($this->reclamo8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion8
   function NM_export_fecha_reclamacion8()
   {
         $this->fecha_reclamacion8 = html_entity_decode($this->fecha_reclamacion8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion8 = strip_tags($this->fecha_reclamacion8);
         if (!NM_is_utf8($this->fecha_reclamacion8))
         {
             $this->fecha_reclamacion8 = sc_convert_encoding($this->fecha_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion8
   function NM_export_motivo_no_reclamacion8()
   {
         $this->motivo_no_reclamacion8 = html_entity_decode($this->motivo_no_reclamacion8, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion8 = strip_tags($this->motivo_no_reclamacion8);
         if (!NM_is_utf8($this->motivo_no_reclamacion8))
         {
             $this->motivo_no_reclamacion8 = sc_convert_encoding($this->motivo_no_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion8, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes9
   function NM_export_mes9()
   {
         $this->mes9 = html_entity_decode($this->mes9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes9 = strip_tags($this->mes9);
         if (!NM_is_utf8($this->mes9))
         {
             $this->mes9 = sc_convert_encoding($this->mes9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo9
   function NM_export_reclamo9()
   {
         $this->reclamo9 = html_entity_decode($this->reclamo9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo9 = strip_tags($this->reclamo9);
         if (!NM_is_utf8($this->reclamo9))
         {
             $this->reclamo9 = sc_convert_encoding($this->reclamo9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion9
   function NM_export_fecha_reclamacion9()
   {
         $this->fecha_reclamacion9 = html_entity_decode($this->fecha_reclamacion9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion9 = strip_tags($this->fecha_reclamacion9);
         if (!NM_is_utf8($this->fecha_reclamacion9))
         {
             $this->fecha_reclamacion9 = sc_convert_encoding($this->fecha_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion9
   function NM_export_motivo_no_reclamacion9()
   {
         $this->motivo_no_reclamacion9 = html_entity_decode($this->motivo_no_reclamacion9, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion9 = strip_tags($this->motivo_no_reclamacion9);
         if (!NM_is_utf8($this->motivo_no_reclamacion9))
         {
             $this->motivo_no_reclamacion9 = sc_convert_encoding($this->motivo_no_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion9, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes10
   function NM_export_mes10()
   {
         $this->mes10 = html_entity_decode($this->mes10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes10 = strip_tags($this->mes10);
         if (!NM_is_utf8($this->mes10))
         {
             $this->mes10 = sc_convert_encoding($this->mes10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo10
   function NM_export_reclamo10()
   {
         $this->reclamo10 = html_entity_decode($this->reclamo10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo10 = strip_tags($this->reclamo10);
         if (!NM_is_utf8($this->reclamo10))
         {
             $this->reclamo10 = sc_convert_encoding($this->reclamo10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion10
   function NM_export_fecha_reclamacion10()
   {
         $this->fecha_reclamacion10 = html_entity_decode($this->fecha_reclamacion10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion10 = strip_tags($this->fecha_reclamacion10);
         if (!NM_is_utf8($this->fecha_reclamacion10))
         {
             $this->fecha_reclamacion10 = sc_convert_encoding($this->fecha_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion10
   function NM_export_motivo_no_reclamacion10()
   {
         $this->motivo_no_reclamacion10 = html_entity_decode($this->motivo_no_reclamacion10, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion10 = strip_tags($this->motivo_no_reclamacion10);
         if (!NM_is_utf8($this->motivo_no_reclamacion10))
         {
             $this->motivo_no_reclamacion10 = sc_convert_encoding($this->motivo_no_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion10, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes11
   function NM_export_mes11()
   {
         $this->mes11 = html_entity_decode($this->mes11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes11 = strip_tags($this->mes11);
         if (!NM_is_utf8($this->mes11))
         {
             $this->mes11 = sc_convert_encoding($this->mes11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo11
   function NM_export_reclamo11()
   {
         $this->reclamo11 = html_entity_decode($this->reclamo11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo11 = strip_tags($this->reclamo11);
         if (!NM_is_utf8($this->reclamo11))
         {
             $this->reclamo11 = sc_convert_encoding($this->reclamo11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion11
   function NM_export_fecha_reclamacion11()
   {
         $this->fecha_reclamacion11 = html_entity_decode($this->fecha_reclamacion11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion11 = strip_tags($this->fecha_reclamacion11);
         if (!NM_is_utf8($this->fecha_reclamacion11))
         {
             $this->fecha_reclamacion11 = sc_convert_encoding($this->fecha_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion11
   function NM_export_motivo_no_reclamacion11()
   {
         $this->motivo_no_reclamacion11 = html_entity_decode($this->motivo_no_reclamacion11, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion11 = strip_tags($this->motivo_no_reclamacion11);
         if (!NM_is_utf8($this->motivo_no_reclamacion11))
         {
             $this->motivo_no_reclamacion11 = sc_convert_encoding($this->motivo_no_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion11, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- mes12
   function NM_export_mes12()
   {
         $this->mes12 = html_entity_decode($this->mes12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->mes12 = strip_tags($this->mes12);
         if (!NM_is_utf8($this->mes12))
         {
             $this->mes12 = sc_convert_encoding($this->mes12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->mes12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- reclamo12
   function NM_export_reclamo12()
   {
         $this->reclamo12 = html_entity_decode($this->reclamo12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->reclamo12 = strip_tags($this->reclamo12);
         if (!NM_is_utf8($this->reclamo12))
         {
             $this->reclamo12 = sc_convert_encoding($this->reclamo12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->reclamo12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- fecha_reclamacion12
   function NM_export_fecha_reclamacion12()
   {
         $this->fecha_reclamacion12 = html_entity_decode($this->fecha_reclamacion12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->fecha_reclamacion12 = strip_tags($this->fecha_reclamacion12);
         if (!NM_is_utf8($this->fecha_reclamacion12))
         {
             $this->fecha_reclamacion12 = sc_convert_encoding($this->fecha_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->fecha_reclamacion12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- motivo_no_reclamacion12
   function NM_export_motivo_no_reclamacion12()
   {
         $this->motivo_no_reclamacion12 = html_entity_decode($this->motivo_no_reclamacion12, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->motivo_no_reclamacion12 = strip_tags($this->motivo_no_reclamacion12);
         if (!NM_is_utf8($this->motivo_no_reclamacion12))
         {
             $this->motivo_no_reclamacion12 = sc_convert_encoding($this->motivo_no_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->motivo_no_reclamacion12, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }

   function calc_cell($col)
   {
       $arr_alfa = array("","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
       $val_ret = "";
       $result = $col + 1;
       while ($result > 26)
       {
           $cel      = $result % 26;
           $result   = $result / 26;
           if ($cel == 0)
           {
               $cel    = 26;
               $result--;
           }
           $val_ret = $arr_alfa[$cel] . $val_ret;
       }
       $val_ret = $arr_alfa[$result] . $val_ret;
       return $val_ret;
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xls_file']);
      if (is_file($this->Xls_f))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xls_file'] = $this->Xls_f;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Reclamaciones Historial :: Excel</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XLS</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="Informe_reclamacion_historial_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="Informe_reclamacion_historial"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
