<?php

class Informe_reclamacion_historial_xml
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Arquivo_view;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function Informe_reclamacion_historial_xml()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_xml()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nm_data    = new nm_data("es");
      $this->Arquivo      = "sc_xml";
      $this->Arquivo     .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo     .= "_Informe_reclamacion_historial";
      $this->Arquivo_view = $this->Arquivo . "_view.xml";
      $this->Arquivo     .= ".xml";
      $this->Tit_doc      = "Informe_reclamacion_historial.xml";
      $this->Grava_view   = false;
      if (strtolower($_SESSION['scriptcase']['charset']) != strtolower($_SESSION['scriptcase']['charset_html']))
      {
          $this->Grava_view = true;
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_paciente = $Busca_temp['id_paciente']; 
          $tmp_pos = strpos($this->id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
          }
          $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
          $this->logro_comunicacion_gestion = $Busca_temp['logro_comunicacion_gestion']; 
          $tmp_pos = strpos($this->logro_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->logro_comunicacion_gestion = substr($this->logro_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->fecha_comunicacion = $Busca_temp['fecha_comunicacion']; 
          $tmp_pos = strpos($this->fecha_comunicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_comunicacion = substr($this->fecha_comunicacion, 0, $tmp_pos);
          }
          $this->autor_gestion = $Busca_temp['autor_gestion']; 
          $tmp_pos = strpos($this->autor_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->autor_gestion = substr($this->autor_gestion, 0, $tmp_pos);
          }
          $this->departamento_paciente = $Busca_temp['departamento_paciente']; 
          $tmp_pos = strpos($this->departamento_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->departamento_paciente = substr($this->departamento_paciente, 0, $tmp_pos);
          }
          $this->ciudad_paciente = $Busca_temp['ciudad_paciente']; 
          $tmp_pos = strpos($this->ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ciudad_paciente = substr($this->ciudad_paciente, 0, $tmp_pos);
          }
          $this->estado_paciente = $Busca_temp['estado_paciente']; 
          $tmp_pos = strpos($this->estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->estado_paciente = substr($this->estado_paciente, 0, $tmp_pos);
          }
          $this->status_paciente = $Busca_temp['status_paciente']; 
          $tmp_pos = strpos($this->status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->status_paciente = substr($this->status_paciente, 0, $tmp_pos);
          }
          $this->fecha_activacion_paciente = $Busca_temp['fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_activacion_paciente = substr($this->fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->codigo_xofigo = $Busca_temp['codigo_xofigo']; 
          $tmp_pos = strpos($this->codigo_xofigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->codigo_xofigo = substr($this->codigo_xofigo, 0, $tmp_pos);
          }
          $this->codigo_xofigo_2 = $Busca_temp['codigo_xofigo_input_2']; 
          $this->producto_tratamiento = $Busca_temp['producto_tratamiento']; 
          $tmp_pos = strpos($this->producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->producto_tratamiento = substr($this->producto_tratamiento, 0, $tmp_pos);
          }
          $this->nombre_referencia = $Busca_temp['nombre_referencia']; 
          $tmp_pos = strpos($this->nombre_referencia, "##@@");
          if ($tmp_pos !== false)
          {
              $this->nombre_referencia = substr($this->nombre_referencia, 0, $tmp_pos);
          }
          $this->dosis_tratamiento = $Busca_temp['dosis_tratamiento']; 
          $tmp_pos = strpos($this->dosis_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->dosis_tratamiento = substr($this->dosis_tratamiento, 0, $tmp_pos);
          }
          $this->numero_cajas = $Busca_temp['numero_cajas']; 
          $tmp_pos = strpos($this->numero_cajas, "##@@");
          if ($tmp_pos !== false)
          {
              $this->numero_cajas = substr($this->numero_cajas, 0, $tmp_pos);
          }
          $this->asegurador_tratamiento = $Busca_temp['asegurador_tratamiento']; 
          $tmp_pos = strpos($this->asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->asegurador_tratamiento = substr($this->asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->operador_logistico_tratamiento = $Busca_temp['operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->operador_logistico_tratamiento = substr($this->operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->punto_entrega = $Busca_temp['punto_entrega']; 
          $tmp_pos = strpos($this->punto_entrega, "##@@");
          if ($tmp_pos !== false)
          {
              $this->punto_entrega = substr($this->punto_entrega, 0, $tmp_pos);
          }
          $this->reclamo_gestion = $Busca_temp['reclamo_gestion']; 
          $tmp_pos = strpos($this->reclamo_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->reclamo_gestion = substr($this->reclamo_gestion, 0, $tmp_pos);
          }
          $this->fecha_reclamacion_gestion = $Busca_temp['fecha_reclamacion_gestion']; 
          $tmp_pos = strpos($this->fecha_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_reclamacion_gestion = substr($this->fecha_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->causa_no_reclamacion_gestion = $Busca_temp['causa_no_reclamacion_gestion']; 
          $tmp_pos = strpos($this->causa_no_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->causa_no_reclamacion_gestion = substr($this->causa_no_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->id_historial_reclamacion = $Busca_temp['id_historial_reclamacion']; 
          $tmp_pos = strpos($this->id_historial_reclamacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_historial_reclamacion = substr($this->id_historial_reclamacion, 0, $tmp_pos);
          }
          $this->id_historial_reclamacion_2 = $Busca_temp['id_historial_reclamacion_input_2']; 
          $this->anio_historial_reclamacion = $Busca_temp['anio_historial_reclamacion']; 
          $tmp_pos = strpos($this->anio_historial_reclamacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->anio_historial_reclamacion = substr($this->anio_historial_reclamacion, 0, $tmp_pos);
          }
          $this->anio_historial_reclamacion_2 = $Busca_temp['anio_historial_reclamacion_input_2']; 
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xml_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xml_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xml_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xml_name']);
      }
      if (!$this->Grava_view)
      {
          $this->Arquivo_view = $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
       } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
       } 
      else 
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $xml_charset = $_SESSION['scriptcase']['charset'];
      $xml_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      fwrite($xml_f, "<?xml version=\"1.0\" encoding=\"$xml_charset\" ?>\r\n");
      fwrite($xml_f, "<root>\r\n");
      if ($this->Grava_view)
      {
          $xml_charset_v = $_SESSION['scriptcase']['charset_html'];
          $xml_v         = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo_view, "w");
          fwrite($xml_v, "<?xml version=\"1.0\" encoding=\"$xml_charset_v\" ?>\r\n");
          fwrite($xml_v, "<root>\r\n");
      }
      while (!$rs->EOF)
      {
         $this->xml_registro = "<Informe_reclamacion_historial";
         $this->id_paciente = $rs->fields[0] ;  
         $this->id_paciente = (string)$this->id_paciente;
         $this->logro_comunicacion_gestion = $rs->fields[1] ;  
         $this->fecha_comunicacion = $rs->fields[2] ;  
         $this->autor_gestion = $rs->fields[3] ;  
         $this->departamento_paciente = $rs->fields[4] ;  
         $this->ciudad_paciente = $rs->fields[5] ;  
         $this->estado_paciente = $rs->fields[6] ;  
         $this->status_paciente = $rs->fields[7] ;  
         $this->fecha_activacion_paciente = $rs->fields[8] ;  
         $this->codigo_xofigo = $rs->fields[9] ;  
         $this->codigo_xofigo = (string)$this->codigo_xofigo;
         $this->producto_tratamiento = $rs->fields[10] ;  
         $this->nombre_referencia = $rs->fields[11] ;  
         $this->dosis_tratamiento = $rs->fields[12] ;  
         $this->numero_cajas = $rs->fields[13] ;  
         $this->asegurador_tratamiento = $rs->fields[14] ;  
         $this->operador_logistico_tratamiento = $rs->fields[15] ;  
         $this->punto_entrega = $rs->fields[16] ;  
         $this->reclamo_gestion = $rs->fields[17] ;  
         $this->fecha_reclamacion_gestion = $rs->fields[18] ;  
         $this->causa_no_reclamacion_gestion = $rs->fields[19] ;  
         $this->id_historial_reclamacion = $rs->fields[20] ;  
         $this->id_historial_reclamacion = (string)$this->id_historial_reclamacion;
         $this->anio_historial_reclamacion = $rs->fields[21] ;  
         $this->anio_historial_reclamacion = (string)$this->anio_historial_reclamacion;
         $this->mes1 = $rs->fields[22] ;  
         $this->reclamo1 = $rs->fields[23] ;  
         $this->fecha_reclamacion1 = $rs->fields[24] ;  
         $this->motivo_no_reclamacion1 = $rs->fields[25] ;  
         $this->mes2 = $rs->fields[26] ;  
         $this->reclamo2 = $rs->fields[27] ;  
         $this->fecha_reclamacion2 = $rs->fields[28] ;  
         $this->motivo_no_reclamacion2 = $rs->fields[29] ;  
         $this->mes3 = $rs->fields[30] ;  
         $this->reclamo3 = $rs->fields[31] ;  
         $this->fecha_reclamacion3 = $rs->fields[32] ;  
         $this->motivo_no_reclamacion3 = $rs->fields[33] ;  
         $this->mes4 = $rs->fields[34] ;  
         $this->reclamo4 = $rs->fields[35] ;  
         $this->fecha_reclamacion4 = $rs->fields[36] ;  
         $this->motivo_no_reclamacion4 = $rs->fields[37] ;  
         $this->mes5 = $rs->fields[38] ;  
         $this->reclamo5 = $rs->fields[39] ;  
         $this->fecha_reclamacion5 = $rs->fields[40] ;  
         $this->motivo_no_reclamacion5 = $rs->fields[41] ;  
         $this->mes6 = $rs->fields[42] ;  
         $this->reclamo6 = $rs->fields[43] ;  
         $this->fecha_reclamacion6 = $rs->fields[44] ;  
         $this->motivo_no_reclamacion6 = $rs->fields[45] ;  
         $this->mes7 = $rs->fields[46] ;  
         $this->reclamo7 = $rs->fields[47] ;  
         $this->fecha_reclamacion7 = $rs->fields[48] ;  
         $this->motivo_no_reclamacion7 = $rs->fields[49] ;  
         $this->mes8 = $rs->fields[50] ;  
         $this->reclamo8 = $rs->fields[51] ;  
         $this->fecha_reclamacion8 = $rs->fields[52] ;  
         $this->motivo_no_reclamacion8 = $rs->fields[53] ;  
         $this->mes9 = $rs->fields[54] ;  
         $this->reclamo9 = $rs->fields[55] ;  
         $this->fecha_reclamacion9 = $rs->fields[56] ;  
         $this->motivo_no_reclamacion9 = $rs->fields[57] ;  
         $this->mes10 = $rs->fields[58] ;  
         $this->reclamo10 = $rs->fields[59] ;  
         $this->fecha_reclamacion10 = $rs->fields[60] ;  
         $this->motivo_no_reclamacion10 = $rs->fields[61] ;  
         $this->mes11 = $rs->fields[62] ;  
         $this->reclamo11 = $rs->fields[63] ;  
         $this->fecha_reclamacion11 = $rs->fields[64] ;  
         $this->motivo_no_reclamacion11 = $rs->fields[65] ;  
         $this->mes12 = $rs->fields[66] ;  
         $this->reclamo12 = $rs->fields[67] ;  
         $this->fecha_reclamacion12 = $rs->fields[68] ;  
         $this->motivo_no_reclamacion12 = $rs->fields[69] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->xml_registro .= " />\r\n";
         fwrite($xml_f, $this->xml_registro);
         if ($this->Grava_view)
         {
            fwrite($xml_v, $this->xml_registro);
         }
         $rs->MoveNext();
      }
      fwrite($xml_f, "</root>");
      fclose($xml_f);
      if ($this->Grava_view)
      {
         fwrite($xml_v, "</root>");
         fclose($xml_v);
      }

      $rs->Close();
   }
   //----- id_paciente
   function NM_export_id_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->id_paciente))
         {
             $this->id_paciente = sc_convert_encoding($this->id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " id_paciente =\"" . $this->trata_dados($this->id_paciente) . "\"";
   }
   //----- logro_comunicacion_gestion
   function NM_export_logro_comunicacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->logro_comunicacion_gestion))
         {
             $this->logro_comunicacion_gestion = sc_convert_encoding($this->logro_comunicacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " logro_comunicacion_gestion =\"" . $this->trata_dados($this->logro_comunicacion_gestion) . "\"";
   }
   //----- fecha_comunicacion
   function NM_export_fecha_comunicacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_comunicacion))
         {
             $this->fecha_comunicacion = sc_convert_encoding($this->fecha_comunicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_comunicacion =\"" . $this->trata_dados($this->fecha_comunicacion) . "\"";
   }
   //----- autor_gestion
   function NM_export_autor_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->autor_gestion))
         {
             $this->autor_gestion = sc_convert_encoding($this->autor_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " autor_gestion =\"" . $this->trata_dados($this->autor_gestion) . "\"";
   }
   //----- departamento_paciente
   function NM_export_departamento_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->departamento_paciente))
         {
             $this->departamento_paciente = sc_convert_encoding($this->departamento_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " departamento_paciente =\"" . $this->trata_dados($this->departamento_paciente) . "\"";
   }
   //----- ciudad_paciente
   function NM_export_ciudad_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->ciudad_paciente))
         {
             $this->ciudad_paciente = sc_convert_encoding($this->ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " ciudad_paciente =\"" . $this->trata_dados($this->ciudad_paciente) . "\"";
   }
   //----- estado_paciente
   function NM_export_estado_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->estado_paciente))
         {
             $this->estado_paciente = sc_convert_encoding($this->estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " estado_paciente =\"" . $this->trata_dados($this->estado_paciente) . "\"";
   }
   //----- status_paciente
   function NM_export_status_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->status_paciente))
         {
             $this->status_paciente = sc_convert_encoding($this->status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " status_paciente =\"" . $this->trata_dados($this->status_paciente) . "\"";
   }
   //----- fecha_activacion_paciente
   function NM_export_fecha_activacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_activacion_paciente))
         {
             $this->fecha_activacion_paciente = sc_convert_encoding($this->fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_activacion_paciente =\"" . $this->trata_dados($this->fecha_activacion_paciente) . "\"";
   }
   //----- codigo_xofigo
   function NM_export_codigo_xofigo()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->codigo_xofigo))
         {
             $this->codigo_xofigo = sc_convert_encoding($this->codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " codigo_xofigo =\"" . $this->trata_dados($this->codigo_xofigo) . "\"";
   }
   //----- producto_tratamiento
   function NM_export_producto_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->producto_tratamiento))
         {
             $this->producto_tratamiento = sc_convert_encoding($this->producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " producto_tratamiento =\"" . $this->trata_dados($this->producto_tratamiento) . "\"";
   }
   //----- nombre_referencia
   function NM_export_nombre_referencia()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->nombre_referencia))
         {
             $this->nombre_referencia = sc_convert_encoding($this->nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " nombre_referencia =\"" . $this->trata_dados($this->nombre_referencia) . "\"";
   }
   //----- dosis_tratamiento
   function NM_export_dosis_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->dosis_tratamiento))
         {
             $this->dosis_tratamiento = sc_convert_encoding($this->dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " dosis_tratamiento =\"" . $this->trata_dados($this->dosis_tratamiento) . "\"";
   }
   //----- numero_cajas
   function NM_export_numero_cajas()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->numero_cajas))
         {
             $this->numero_cajas = sc_convert_encoding($this->numero_cajas, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " numero_cajas =\"" . $this->trata_dados($this->numero_cajas) . "\"";
   }
   //----- asegurador_tratamiento
   function NM_export_asegurador_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->asegurador_tratamiento))
         {
             $this->asegurador_tratamiento = sc_convert_encoding($this->asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " asegurador_tratamiento =\"" . $this->trata_dados($this->asegurador_tratamiento) . "\"";
   }
   //----- operador_logistico_tratamiento
   function NM_export_operador_logistico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->operador_logistico_tratamiento))
         {
             $this->operador_logistico_tratamiento = sc_convert_encoding($this->operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " operador_logistico_tratamiento =\"" . $this->trata_dados($this->operador_logistico_tratamiento) . "\"";
   }
   //----- punto_entrega
   function NM_export_punto_entrega()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->punto_entrega))
         {
             $this->punto_entrega = sc_convert_encoding($this->punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " punto_entrega =\"" . $this->trata_dados($this->punto_entrega) . "\"";
   }
   //----- reclamo_gestion
   function NM_export_reclamo_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo_gestion))
         {
             $this->reclamo_gestion = sc_convert_encoding($this->reclamo_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo_gestion =\"" . $this->trata_dados($this->reclamo_gestion) . "\"";
   }
   //----- fecha_reclamacion_gestion
   function NM_export_fecha_reclamacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion_gestion))
         {
             $this->fecha_reclamacion_gestion = sc_convert_encoding($this->fecha_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion_gestion =\"" . $this->trata_dados($this->fecha_reclamacion_gestion) . "\"";
   }
   //----- causa_no_reclamacion_gestion
   function NM_export_causa_no_reclamacion_gestion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->causa_no_reclamacion_gestion))
         {
             $this->causa_no_reclamacion_gestion = sc_convert_encoding($this->causa_no_reclamacion_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " causa_no_reclamacion_gestion =\"" . $this->trata_dados($this->causa_no_reclamacion_gestion) . "\"";
   }
   //----- id_historial_reclamacion
   function NM_export_id_historial_reclamacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->id_historial_reclamacion))
         {
             $this->id_historial_reclamacion = sc_convert_encoding($this->id_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " id_historial_reclamacion =\"" . $this->trata_dados($this->id_historial_reclamacion) . "\"";
   }
   //----- anio_historial_reclamacion
   function NM_export_anio_historial_reclamacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->anio_historial_reclamacion))
         {
             $this->anio_historial_reclamacion = sc_convert_encoding($this->anio_historial_reclamacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " anio_historial_reclamacion =\"" . $this->trata_dados($this->anio_historial_reclamacion) . "\"";
   }
   //----- mes1
   function NM_export_mes1()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes1))
         {
             $this->mes1 = sc_convert_encoding($this->mes1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes1 =\"" . $this->trata_dados($this->mes1) . "\"";
   }
   //----- reclamo1
   function NM_export_reclamo1()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo1))
         {
             $this->reclamo1 = sc_convert_encoding($this->reclamo1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo1 =\"" . $this->trata_dados($this->reclamo1) . "\"";
   }
   //----- fecha_reclamacion1
   function NM_export_fecha_reclamacion1()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion1))
         {
             $this->fecha_reclamacion1 = sc_convert_encoding($this->fecha_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion1 =\"" . $this->trata_dados($this->fecha_reclamacion1) . "\"";
   }
   //----- motivo_no_reclamacion1
   function NM_export_motivo_no_reclamacion1()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion1))
         {
             $this->motivo_no_reclamacion1 = sc_convert_encoding($this->motivo_no_reclamacion1, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion1 =\"" . $this->trata_dados($this->motivo_no_reclamacion1) . "\"";
   }
   //----- mes2
   function NM_export_mes2()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes2))
         {
             $this->mes2 = sc_convert_encoding($this->mes2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes2 =\"" . $this->trata_dados($this->mes2) . "\"";
   }
   //----- reclamo2
   function NM_export_reclamo2()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo2))
         {
             $this->reclamo2 = sc_convert_encoding($this->reclamo2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo2 =\"" . $this->trata_dados($this->reclamo2) . "\"";
   }
   //----- fecha_reclamacion2
   function NM_export_fecha_reclamacion2()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion2))
         {
             $this->fecha_reclamacion2 = sc_convert_encoding($this->fecha_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion2 =\"" . $this->trata_dados($this->fecha_reclamacion2) . "\"";
   }
   //----- motivo_no_reclamacion2
   function NM_export_motivo_no_reclamacion2()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion2))
         {
             $this->motivo_no_reclamacion2 = sc_convert_encoding($this->motivo_no_reclamacion2, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion2 =\"" . $this->trata_dados($this->motivo_no_reclamacion2) . "\"";
   }
   //----- mes3
   function NM_export_mes3()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes3))
         {
             $this->mes3 = sc_convert_encoding($this->mes3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes3 =\"" . $this->trata_dados($this->mes3) . "\"";
   }
   //----- reclamo3
   function NM_export_reclamo3()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo3))
         {
             $this->reclamo3 = sc_convert_encoding($this->reclamo3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo3 =\"" . $this->trata_dados($this->reclamo3) . "\"";
   }
   //----- fecha_reclamacion3
   function NM_export_fecha_reclamacion3()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion3))
         {
             $this->fecha_reclamacion3 = sc_convert_encoding($this->fecha_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion3 =\"" . $this->trata_dados($this->fecha_reclamacion3) . "\"";
   }
   //----- motivo_no_reclamacion3
   function NM_export_motivo_no_reclamacion3()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion3))
         {
             $this->motivo_no_reclamacion3 = sc_convert_encoding($this->motivo_no_reclamacion3, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion3 =\"" . $this->trata_dados($this->motivo_no_reclamacion3) . "\"";
   }
   //----- mes4
   function NM_export_mes4()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes4))
         {
             $this->mes4 = sc_convert_encoding($this->mes4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes4 =\"" . $this->trata_dados($this->mes4) . "\"";
   }
   //----- reclamo4
   function NM_export_reclamo4()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo4))
         {
             $this->reclamo4 = sc_convert_encoding($this->reclamo4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo4 =\"" . $this->trata_dados($this->reclamo4) . "\"";
   }
   //----- fecha_reclamacion4
   function NM_export_fecha_reclamacion4()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion4))
         {
             $this->fecha_reclamacion4 = sc_convert_encoding($this->fecha_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion4 =\"" . $this->trata_dados($this->fecha_reclamacion4) . "\"";
   }
   //----- motivo_no_reclamacion4
   function NM_export_motivo_no_reclamacion4()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion4))
         {
             $this->motivo_no_reclamacion4 = sc_convert_encoding($this->motivo_no_reclamacion4, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion4 =\"" . $this->trata_dados($this->motivo_no_reclamacion4) . "\"";
   }
   //----- mes5
   function NM_export_mes5()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes5))
         {
             $this->mes5 = sc_convert_encoding($this->mes5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes5 =\"" . $this->trata_dados($this->mes5) . "\"";
   }
   //----- reclamo5
   function NM_export_reclamo5()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo5))
         {
             $this->reclamo5 = sc_convert_encoding($this->reclamo5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo5 =\"" . $this->trata_dados($this->reclamo5) . "\"";
   }
   //----- fecha_reclamacion5
   function NM_export_fecha_reclamacion5()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion5))
         {
             $this->fecha_reclamacion5 = sc_convert_encoding($this->fecha_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion5 =\"" . $this->trata_dados($this->fecha_reclamacion5) . "\"";
   }
   //----- motivo_no_reclamacion5
   function NM_export_motivo_no_reclamacion5()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion5))
         {
             $this->motivo_no_reclamacion5 = sc_convert_encoding($this->motivo_no_reclamacion5, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion5 =\"" . $this->trata_dados($this->motivo_no_reclamacion5) . "\"";
   }
   //----- mes6
   function NM_export_mes6()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes6))
         {
             $this->mes6 = sc_convert_encoding($this->mes6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes6 =\"" . $this->trata_dados($this->mes6) . "\"";
   }
   //----- reclamo6
   function NM_export_reclamo6()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo6))
         {
             $this->reclamo6 = sc_convert_encoding($this->reclamo6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo6 =\"" . $this->trata_dados($this->reclamo6) . "\"";
   }
   //----- fecha_reclamacion6
   function NM_export_fecha_reclamacion6()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion6))
         {
             $this->fecha_reclamacion6 = sc_convert_encoding($this->fecha_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion6 =\"" . $this->trata_dados($this->fecha_reclamacion6) . "\"";
   }
   //----- motivo_no_reclamacion6
   function NM_export_motivo_no_reclamacion6()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion6))
         {
             $this->motivo_no_reclamacion6 = sc_convert_encoding($this->motivo_no_reclamacion6, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion6 =\"" . $this->trata_dados($this->motivo_no_reclamacion6) . "\"";
   }
   //----- mes7
   function NM_export_mes7()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes7))
         {
             $this->mes7 = sc_convert_encoding($this->mes7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes7 =\"" . $this->trata_dados($this->mes7) . "\"";
   }
   //----- reclamo7
   function NM_export_reclamo7()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo7))
         {
             $this->reclamo7 = sc_convert_encoding($this->reclamo7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo7 =\"" . $this->trata_dados($this->reclamo7) . "\"";
   }
   //----- fecha_reclamacion7
   function NM_export_fecha_reclamacion7()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion7))
         {
             $this->fecha_reclamacion7 = sc_convert_encoding($this->fecha_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion7 =\"" . $this->trata_dados($this->fecha_reclamacion7) . "\"";
   }
   //----- motivo_no_reclamacion7
   function NM_export_motivo_no_reclamacion7()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion7))
         {
             $this->motivo_no_reclamacion7 = sc_convert_encoding($this->motivo_no_reclamacion7, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion7 =\"" . $this->trata_dados($this->motivo_no_reclamacion7) . "\"";
   }
   //----- mes8
   function NM_export_mes8()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes8))
         {
             $this->mes8 = sc_convert_encoding($this->mes8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes8 =\"" . $this->trata_dados($this->mes8) . "\"";
   }
   //----- reclamo8
   function NM_export_reclamo8()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo8))
         {
             $this->reclamo8 = sc_convert_encoding($this->reclamo8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo8 =\"" . $this->trata_dados($this->reclamo8) . "\"";
   }
   //----- fecha_reclamacion8
   function NM_export_fecha_reclamacion8()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion8))
         {
             $this->fecha_reclamacion8 = sc_convert_encoding($this->fecha_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion8 =\"" . $this->trata_dados($this->fecha_reclamacion8) . "\"";
   }
   //----- motivo_no_reclamacion8
   function NM_export_motivo_no_reclamacion8()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion8))
         {
             $this->motivo_no_reclamacion8 = sc_convert_encoding($this->motivo_no_reclamacion8, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion8 =\"" . $this->trata_dados($this->motivo_no_reclamacion8) . "\"";
   }
   //----- mes9
   function NM_export_mes9()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes9))
         {
             $this->mes9 = sc_convert_encoding($this->mes9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes9 =\"" . $this->trata_dados($this->mes9) . "\"";
   }
   //----- reclamo9
   function NM_export_reclamo9()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo9))
         {
             $this->reclamo9 = sc_convert_encoding($this->reclamo9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo9 =\"" . $this->trata_dados($this->reclamo9) . "\"";
   }
   //----- fecha_reclamacion9
   function NM_export_fecha_reclamacion9()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion9))
         {
             $this->fecha_reclamacion9 = sc_convert_encoding($this->fecha_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion9 =\"" . $this->trata_dados($this->fecha_reclamacion9) . "\"";
   }
   //----- motivo_no_reclamacion9
   function NM_export_motivo_no_reclamacion9()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion9))
         {
             $this->motivo_no_reclamacion9 = sc_convert_encoding($this->motivo_no_reclamacion9, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion9 =\"" . $this->trata_dados($this->motivo_no_reclamacion9) . "\"";
   }
   //----- mes10
   function NM_export_mes10()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes10))
         {
             $this->mes10 = sc_convert_encoding($this->mes10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes10 =\"" . $this->trata_dados($this->mes10) . "\"";
   }
   //----- reclamo10
   function NM_export_reclamo10()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo10))
         {
             $this->reclamo10 = sc_convert_encoding($this->reclamo10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo10 =\"" . $this->trata_dados($this->reclamo10) . "\"";
   }
   //----- fecha_reclamacion10
   function NM_export_fecha_reclamacion10()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion10))
         {
             $this->fecha_reclamacion10 = sc_convert_encoding($this->fecha_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion10 =\"" . $this->trata_dados($this->fecha_reclamacion10) . "\"";
   }
   //----- motivo_no_reclamacion10
   function NM_export_motivo_no_reclamacion10()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion10))
         {
             $this->motivo_no_reclamacion10 = sc_convert_encoding($this->motivo_no_reclamacion10, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion10 =\"" . $this->trata_dados($this->motivo_no_reclamacion10) . "\"";
   }
   //----- mes11
   function NM_export_mes11()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes11))
         {
             $this->mes11 = sc_convert_encoding($this->mes11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes11 =\"" . $this->trata_dados($this->mes11) . "\"";
   }
   //----- reclamo11
   function NM_export_reclamo11()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo11))
         {
             $this->reclamo11 = sc_convert_encoding($this->reclamo11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo11 =\"" . $this->trata_dados($this->reclamo11) . "\"";
   }
   //----- fecha_reclamacion11
   function NM_export_fecha_reclamacion11()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion11))
         {
             $this->fecha_reclamacion11 = sc_convert_encoding($this->fecha_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion11 =\"" . $this->trata_dados($this->fecha_reclamacion11) . "\"";
   }
   //----- motivo_no_reclamacion11
   function NM_export_motivo_no_reclamacion11()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion11))
         {
             $this->motivo_no_reclamacion11 = sc_convert_encoding($this->motivo_no_reclamacion11, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion11 =\"" . $this->trata_dados($this->motivo_no_reclamacion11) . "\"";
   }
   //----- mes12
   function NM_export_mes12()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->mes12))
         {
             $this->mes12 = sc_convert_encoding($this->mes12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " mes12 =\"" . $this->trata_dados($this->mes12) . "\"";
   }
   //----- reclamo12
   function NM_export_reclamo12()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->reclamo12))
         {
             $this->reclamo12 = sc_convert_encoding($this->reclamo12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " reclamo12 =\"" . $this->trata_dados($this->reclamo12) . "\"";
   }
   //----- fecha_reclamacion12
   function NM_export_fecha_reclamacion12()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->fecha_reclamacion12))
         {
             $this->fecha_reclamacion12 = sc_convert_encoding($this->fecha_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " fecha_reclamacion12 =\"" . $this->trata_dados($this->fecha_reclamacion12) . "\"";
   }
   //----- motivo_no_reclamacion12
   function NM_export_motivo_no_reclamacion12()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->motivo_no_reclamacion12))
         {
             $this->motivo_no_reclamacion12 = sc_convert_encoding($this->motivo_no_reclamacion12, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " motivo_no_reclamacion12 =\"" . $this->trata_dados($this->motivo_no_reclamacion12) . "\"";
   }

   //----- 
   function trata_dados($conteudo)
   {
      $str_temp =  $conteudo;
      $str_temp =  str_replace("<br />", "",  $str_temp);
      $str_temp =  str_replace("&", "&amp;",  $str_temp);
      $str_temp =  str_replace("<", "&lt;",   $str_temp);
      $str_temp =  str_replace(">", "&gt;",   $str_temp);
      $str_temp =  str_replace("'", "&apos;", $str_temp);
      $str_temp =  str_replace('"', "&quot;",  $str_temp);
      $str_temp =  str_replace('(', "_",  $str_temp);
      $str_temp =  str_replace(')', "",  $str_temp);
      return ($str_temp);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xml_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['xml_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Reclamaciones Historial :: XML</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XML</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo_view ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="Informe_reclamacion_historial_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="Informe_reclamacion_historial"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./" style="display: none"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
