<?php

class Informe_reclamacion_historial_csv
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Tit_doc;
   var $Delim_dados;
   var $Delim_line;
   var $Delim_col;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function Informe_reclamacion_historial_csv()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_csv()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
     global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo     = "sc_csv";
      $this->Arquivo    .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo    .= "_Informe_reclamacion_historial";
      $this->Arquivo    .= ".csv";
      $this->Tit_doc    = "Informe_reclamacion_historial.csv";
      $this->Delim_dados = "\"";
      $this->Delim_col   = ";";
      $this->Delim_line  = "\r\n";
   }

   //----- 
   function grava_arquivo()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_paciente = $Busca_temp['id_paciente']; 
          $tmp_pos = strpos($this->id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
          }
          $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
          $this->logro_comunicacion_gestion = $Busca_temp['logro_comunicacion_gestion']; 
          $tmp_pos = strpos($this->logro_comunicacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->logro_comunicacion_gestion = substr($this->logro_comunicacion_gestion, 0, $tmp_pos);
          }
          $this->fecha_comunicacion = $Busca_temp['fecha_comunicacion']; 
          $tmp_pos = strpos($this->fecha_comunicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_comunicacion = substr($this->fecha_comunicacion, 0, $tmp_pos);
          }
          $this->autor_gestion = $Busca_temp['autor_gestion']; 
          $tmp_pos = strpos($this->autor_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->autor_gestion = substr($this->autor_gestion, 0, $tmp_pos);
          }
          $this->departamento_paciente = $Busca_temp['departamento_paciente']; 
          $tmp_pos = strpos($this->departamento_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->departamento_paciente = substr($this->departamento_paciente, 0, $tmp_pos);
          }
          $this->ciudad_paciente = $Busca_temp['ciudad_paciente']; 
          $tmp_pos = strpos($this->ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ciudad_paciente = substr($this->ciudad_paciente, 0, $tmp_pos);
          }
          $this->estado_paciente = $Busca_temp['estado_paciente']; 
          $tmp_pos = strpos($this->estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->estado_paciente = substr($this->estado_paciente, 0, $tmp_pos);
          }
          $this->status_paciente = $Busca_temp['status_paciente']; 
          $tmp_pos = strpos($this->status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->status_paciente = substr($this->status_paciente, 0, $tmp_pos);
          }
          $this->fecha_activacion_paciente = $Busca_temp['fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_activacion_paciente = substr($this->fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->codigo_xofigo = $Busca_temp['codigo_xofigo']; 
          $tmp_pos = strpos($this->codigo_xofigo, "##@@");
          if ($tmp_pos !== false)
          {
              $this->codigo_xofigo = substr($this->codigo_xofigo, 0, $tmp_pos);
          }
          $this->codigo_xofigo_2 = $Busca_temp['codigo_xofigo_input_2']; 
          $this->producto_tratamiento = $Busca_temp['producto_tratamiento']; 
          $tmp_pos = strpos($this->producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->producto_tratamiento = substr($this->producto_tratamiento, 0, $tmp_pos);
          }
          $this->nombre_referencia = $Busca_temp['nombre_referencia']; 
          $tmp_pos = strpos($this->nombre_referencia, "##@@");
          if ($tmp_pos !== false)
          {
              $this->nombre_referencia = substr($this->nombre_referencia, 0, $tmp_pos);
          }
          $this->dosis_tratamiento = $Busca_temp['dosis_tratamiento']; 
          $tmp_pos = strpos($this->dosis_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->dosis_tratamiento = substr($this->dosis_tratamiento, 0, $tmp_pos);
          }
          $this->numero_cajas = $Busca_temp['numero_cajas']; 
          $tmp_pos = strpos($this->numero_cajas, "##@@");
          if ($tmp_pos !== false)
          {
              $this->numero_cajas = substr($this->numero_cajas, 0, $tmp_pos);
          }
          $this->asegurador_tratamiento = $Busca_temp['asegurador_tratamiento']; 
          $tmp_pos = strpos($this->asegurador_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->asegurador_tratamiento = substr($this->asegurador_tratamiento, 0, $tmp_pos);
          }
          $this->operador_logistico_tratamiento = $Busca_temp['operador_logistico_tratamiento']; 
          $tmp_pos = strpos($this->operador_logistico_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->operador_logistico_tratamiento = substr($this->operador_logistico_tratamiento, 0, $tmp_pos);
          }
          $this->punto_entrega = $Busca_temp['punto_entrega']; 
          $tmp_pos = strpos($this->punto_entrega, "##@@");
          if ($tmp_pos !== false)
          {
              $this->punto_entrega = substr($this->punto_entrega, 0, $tmp_pos);
          }
          $this->reclamo_gestion = $Busca_temp['reclamo_gestion']; 
          $tmp_pos = strpos($this->reclamo_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->reclamo_gestion = substr($this->reclamo_gestion, 0, $tmp_pos);
          }
          $this->fecha_reclamacion_gestion = $Busca_temp['fecha_reclamacion_gestion']; 
          $tmp_pos = strpos($this->fecha_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->fecha_reclamacion_gestion = substr($this->fecha_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->causa_no_reclamacion_gestion = $Busca_temp['causa_no_reclamacion_gestion']; 
          $tmp_pos = strpos($this->causa_no_reclamacion_gestion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->causa_no_reclamacion_gestion = substr($this->causa_no_reclamacion_gestion, 0, $tmp_pos);
          }
          $this->id_historial_reclamacion = $Busca_temp['id_historial_reclamacion']; 
          $tmp_pos = strpos($this->id_historial_reclamacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_historial_reclamacion = substr($this->id_historial_reclamacion, 0, $tmp_pos);
          }
          $this->id_historial_reclamacion_2 = $Busca_temp['id_historial_reclamacion_input_2']; 
          $this->anio_historial_reclamacion = $Busca_temp['anio_historial_reclamacion']; 
          $tmp_pos = strpos($this->anio_historial_reclamacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->anio_historial_reclamacion = substr($this->anio_historial_reclamacion, 0, $tmp_pos);
          }
          $this->anio_historial_reclamacion_2 = $Busca_temp['anio_historial_reclamacion_input_2']; 
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['csv_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['csv_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['csv_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['csv_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
       } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
       } 
      else 
      { 
          $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $csv_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      $this->NM_prim_col  = 0;
      $this->csv_registro = "";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['logro_comunicacion_gestion'])) ? $this->New_label['logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          if ($Cada_col == "logro_comunicacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_comunicacion'])) ? $this->New_label['fecha_comunicacion'] : "FECHA COMUNICACION"; 
          if ($Cada_col == "fecha_comunicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['autor_gestion'])) ? $this->New_label['autor_gestion'] : "AUTOR GESTION"; 
          if ($Cada_col == "autor_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['departamento_paciente'])) ? $this->New_label['departamento_paciente'] : "DEPARTAMENTO PACIENTE"; 
          if ($Cada_col == "departamento_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['ciudad_paciente'])) ? $this->New_label['ciudad_paciente'] : "CIUDAD PACIENTE"; 
          if ($Cada_col == "ciudad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['status_paciente'])) ? $this->New_label['status_paciente'] : "STATUS PACIENTE"; 
          if ($Cada_col == "status_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['codigo_xofigo'])) ? $this->New_label['codigo_xofigo'] : "CODIGO XOFIGO"; 
          if ($Cada_col == "codigo_xofigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['nombre_referencia'])) ? $this->New_label['nombre_referencia'] : "NOMBRE REFERENCIA"; 
          if ($Cada_col == "nombre_referencia" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['dosis_tratamiento'])) ? $this->New_label['dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          if ($Cada_col == "dosis_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['numero_cajas'])) ? $this->New_label['numero_cajas'] : "NUMERO CAJAS"; 
          if ($Cada_col == "numero_cajas" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['asegurador_tratamiento'])) ? $this->New_label['asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['operador_logistico_tratamiento'])) ? $this->New_label['operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['punto_entrega'])) ? $this->New_label['punto_entrega'] : "PUNTO ENTREGA"; 
          if ($Cada_col == "punto_entrega" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo_gestion'])) ? $this->New_label['reclamo_gestion'] : "RECLAMO GESTION"; 
          if ($Cada_col == "reclamo_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion_gestion'])) ? $this->New_label['fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          if ($Cada_col == "fecha_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['causa_no_reclamacion_gestion'])) ? $this->New_label['causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          if ($Cada_col == "causa_no_reclamacion_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['id_historial_reclamacion'])) ? $this->New_label['id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION"; 
          if ($Cada_col == "id_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['anio_historial_reclamacion'])) ? $this->New_label['anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION"; 
          if ($Cada_col == "anio_historial_reclamacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes1'])) ? $this->New_label['mes1'] : "MES1"; 
          if ($Cada_col == "mes1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo1'])) ? $this->New_label['reclamo1'] : "RECLAMO1"; 
          if ($Cada_col == "reclamo1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion1'])) ? $this->New_label['fecha_reclamacion1'] : "FECHA RECLAMACION1"; 
          if ($Cada_col == "fecha_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion1'])) ? $this->New_label['motivo_no_reclamacion1'] : "MOTIVO NO RECLAMACION1"; 
          if ($Cada_col == "motivo_no_reclamacion1" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes2'])) ? $this->New_label['mes2'] : "MES2"; 
          if ($Cada_col == "mes2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo2'])) ? $this->New_label['reclamo2'] : "RECLAMO2"; 
          if ($Cada_col == "reclamo2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion2'])) ? $this->New_label['fecha_reclamacion2'] : "FECHA RECLAMACION2"; 
          if ($Cada_col == "fecha_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion2'])) ? $this->New_label['motivo_no_reclamacion2'] : "MOTIVO NO RECLAMACION2"; 
          if ($Cada_col == "motivo_no_reclamacion2" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes3'])) ? $this->New_label['mes3'] : "MES3"; 
          if ($Cada_col == "mes3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo3'])) ? $this->New_label['reclamo3'] : "RECLAMO3"; 
          if ($Cada_col == "reclamo3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion3'])) ? $this->New_label['fecha_reclamacion3'] : "FECHA RECLAMACION3"; 
          if ($Cada_col == "fecha_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion3'])) ? $this->New_label['motivo_no_reclamacion3'] : "MOTIVO NO RECLAMACION3"; 
          if ($Cada_col == "motivo_no_reclamacion3" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes4'])) ? $this->New_label['mes4'] : "MES4"; 
          if ($Cada_col == "mes4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo4'])) ? $this->New_label['reclamo4'] : "RECLAMO4"; 
          if ($Cada_col == "reclamo4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion4'])) ? $this->New_label['fecha_reclamacion4'] : "FECHA RECLAMACION4"; 
          if ($Cada_col == "fecha_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion4'])) ? $this->New_label['motivo_no_reclamacion4'] : "MOTIVO NO RECLAMACION4"; 
          if ($Cada_col == "motivo_no_reclamacion4" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes5'])) ? $this->New_label['mes5'] : "MES5"; 
          if ($Cada_col == "mes5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo5'])) ? $this->New_label['reclamo5'] : "RECLAMO5"; 
          if ($Cada_col == "reclamo5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion5'])) ? $this->New_label['fecha_reclamacion5'] : "FECHA RECLAMACION5"; 
          if ($Cada_col == "fecha_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion5'])) ? $this->New_label['motivo_no_reclamacion5'] : "MOTIVO NO RECLAMACION5"; 
          if ($Cada_col == "motivo_no_reclamacion5" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes6'])) ? $this->New_label['mes6'] : "MES6"; 
          if ($Cada_col == "mes6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo6'])) ? $this->New_label['reclamo6'] : "RECLAMO6"; 
          if ($Cada_col == "reclamo6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion6'])) ? $this->New_label['fecha_reclamacion6'] : "FECHA RECLAMACION6"; 
          if ($Cada_col == "fecha_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion6'])) ? $this->New_label['motivo_no_reclamacion6'] : "MOTIVO NO RECLAMACION6"; 
          if ($Cada_col == "motivo_no_reclamacion6" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes7'])) ? $this->New_label['mes7'] : "MES7"; 
          if ($Cada_col == "mes7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo7'])) ? $this->New_label['reclamo7'] : "RECLAMO7"; 
          if ($Cada_col == "reclamo7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion7'])) ? $this->New_label['fecha_reclamacion7'] : "FECHA RECLAMACION7"; 
          if ($Cada_col == "fecha_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion7'])) ? $this->New_label['motivo_no_reclamacion7'] : "MOTIVO NO RECLAMACION7"; 
          if ($Cada_col == "motivo_no_reclamacion7" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes8'])) ? $this->New_label['mes8'] : "MES8"; 
          if ($Cada_col == "mes8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo8'])) ? $this->New_label['reclamo8'] : "RECLAMO8"; 
          if ($Cada_col == "reclamo8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion8'])) ? $this->New_label['fecha_reclamacion8'] : "FECHA RECLAMACION8"; 
          if ($Cada_col == "fecha_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion8'])) ? $this->New_label['motivo_no_reclamacion8'] : "MOTIVO NO RECLAMACION8"; 
          if ($Cada_col == "motivo_no_reclamacion8" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes9'])) ? $this->New_label['mes9'] : "MES9"; 
          if ($Cada_col == "mes9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo9'])) ? $this->New_label['reclamo9'] : "RECLAMO9"; 
          if ($Cada_col == "reclamo9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion9'])) ? $this->New_label['fecha_reclamacion9'] : "FECHA RECLAMACION9"; 
          if ($Cada_col == "fecha_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion9'])) ? $this->New_label['motivo_no_reclamacion9'] : "MOTIVO NO RECLAMACION9"; 
          if ($Cada_col == "motivo_no_reclamacion9" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes10'])) ? $this->New_label['mes10'] : "MES10"; 
          if ($Cada_col == "mes10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo10'])) ? $this->New_label['reclamo10'] : "RECLAMO10"; 
          if ($Cada_col == "reclamo10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion10'])) ? $this->New_label['fecha_reclamacion10'] : "FECHA RECLAMACION10"; 
          if ($Cada_col == "fecha_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion10'])) ? $this->New_label['motivo_no_reclamacion10'] : "MOTIVO NO RECLAMACION10"; 
          if ($Cada_col == "motivo_no_reclamacion10" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes11'])) ? $this->New_label['mes11'] : "MES11"; 
          if ($Cada_col == "mes11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo11'])) ? $this->New_label['reclamo11'] : "RECLAMO11"; 
          if ($Cada_col == "reclamo11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion11'])) ? $this->New_label['fecha_reclamacion11'] : "FECHA RECLAMACION11"; 
          if ($Cada_col == "fecha_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion11'])) ? $this->New_label['motivo_no_reclamacion11'] : "MOTIVO NO RECLAMACION11"; 
          if ($Cada_col == "motivo_no_reclamacion11" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['mes12'])) ? $this->New_label['mes12'] : "MES12"; 
          if ($Cada_col == "mes12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['reclamo12'])) ? $this->New_label['reclamo12'] : "RECLAMO12"; 
          if ($Cada_col == "reclamo12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['fecha_reclamacion12'])) ? $this->New_label['fecha_reclamacion12'] : "FECHA RECLAMACION12"; 
          if ($Cada_col == "fecha_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion12'])) ? $this->New_label['motivo_no_reclamacion12'] : "MOTIVO NO RECLAMACION12"; 
          if ($Cada_col == "motivo_no_reclamacion12" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
              $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $SC_Label);
              $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
              $this->NM_prim_col++;
          }
      } 
      $this->csv_registro .= $this->Delim_line;
      fwrite($csv_f, $this->csv_registro);
      while (!$rs->EOF)
      {
         $this->csv_registro = "";
         $this->NM_prim_col  = 0;
         $this->id_paciente = $rs->fields[0] ;  
         $this->id_paciente = (string)$this->id_paciente;
         $this->logro_comunicacion_gestion = $rs->fields[1] ;  
         $this->fecha_comunicacion = $rs->fields[2] ;  
         $this->autor_gestion = $rs->fields[3] ;  
         $this->departamento_paciente = $rs->fields[4] ;  
         $this->ciudad_paciente = $rs->fields[5] ;  
         $this->estado_paciente = $rs->fields[6] ;  
         $this->status_paciente = $rs->fields[7] ;  
         $this->fecha_activacion_paciente = $rs->fields[8] ;  
         $this->codigo_xofigo = $rs->fields[9] ;  
         $this->codigo_xofigo = (string)$this->codigo_xofigo;
         $this->producto_tratamiento = $rs->fields[10] ;  
         $this->nombre_referencia = $rs->fields[11] ;  
         $this->dosis_tratamiento = $rs->fields[12] ;  
         $this->numero_cajas = $rs->fields[13] ;  
         $this->asegurador_tratamiento = $rs->fields[14] ;  
         $this->operador_logistico_tratamiento = $rs->fields[15] ;  
         $this->punto_entrega = $rs->fields[16] ;  
         $this->reclamo_gestion = $rs->fields[17] ;  
         $this->fecha_reclamacion_gestion = $rs->fields[18] ;  
         $this->causa_no_reclamacion_gestion = $rs->fields[19] ;  
         $this->id_historial_reclamacion = $rs->fields[20] ;  
         $this->id_historial_reclamacion = (string)$this->id_historial_reclamacion;
         $this->anio_historial_reclamacion = $rs->fields[21] ;  
         $this->anio_historial_reclamacion = (string)$this->anio_historial_reclamacion;
         $this->mes1 = $rs->fields[22] ;  
         $this->reclamo1 = $rs->fields[23] ;  
         $this->fecha_reclamacion1 = $rs->fields[24] ;  
         $this->motivo_no_reclamacion1 = $rs->fields[25] ;  
         $this->mes2 = $rs->fields[26] ;  
         $this->reclamo2 = $rs->fields[27] ;  
         $this->fecha_reclamacion2 = $rs->fields[28] ;  
         $this->motivo_no_reclamacion2 = $rs->fields[29] ;  
         $this->mes3 = $rs->fields[30] ;  
         $this->reclamo3 = $rs->fields[31] ;  
         $this->fecha_reclamacion3 = $rs->fields[32] ;  
         $this->motivo_no_reclamacion3 = $rs->fields[33] ;  
         $this->mes4 = $rs->fields[34] ;  
         $this->reclamo4 = $rs->fields[35] ;  
         $this->fecha_reclamacion4 = $rs->fields[36] ;  
         $this->motivo_no_reclamacion4 = $rs->fields[37] ;  
         $this->mes5 = $rs->fields[38] ;  
         $this->reclamo5 = $rs->fields[39] ;  
         $this->fecha_reclamacion5 = $rs->fields[40] ;  
         $this->motivo_no_reclamacion5 = $rs->fields[41] ;  
         $this->mes6 = $rs->fields[42] ;  
         $this->reclamo6 = $rs->fields[43] ;  
         $this->fecha_reclamacion6 = $rs->fields[44] ;  
         $this->motivo_no_reclamacion6 = $rs->fields[45] ;  
         $this->mes7 = $rs->fields[46] ;  
         $this->reclamo7 = $rs->fields[47] ;  
         $this->fecha_reclamacion7 = $rs->fields[48] ;  
         $this->motivo_no_reclamacion7 = $rs->fields[49] ;  
         $this->mes8 = $rs->fields[50] ;  
         $this->reclamo8 = $rs->fields[51] ;  
         $this->fecha_reclamacion8 = $rs->fields[52] ;  
         $this->motivo_no_reclamacion8 = $rs->fields[53] ;  
         $this->mes9 = $rs->fields[54] ;  
         $this->reclamo9 = $rs->fields[55] ;  
         $this->fecha_reclamacion9 = $rs->fields[56] ;  
         $this->motivo_no_reclamacion9 = $rs->fields[57] ;  
         $this->mes10 = $rs->fields[58] ;  
         $this->reclamo10 = $rs->fields[59] ;  
         $this->fecha_reclamacion10 = $rs->fields[60] ;  
         $this->motivo_no_reclamacion10 = $rs->fields[61] ;  
         $this->mes11 = $rs->fields[62] ;  
         $this->reclamo11 = $rs->fields[63] ;  
         $this->fecha_reclamacion11 = $rs->fields[64] ;  
         $this->motivo_no_reclamacion11 = $rs->fields[65] ;  
         $this->mes12 = $rs->fields[66] ;  
         $this->reclamo12 = $rs->fields[67] ;  
         $this->fecha_reclamacion12 = $rs->fields[68] ;  
         $this->motivo_no_reclamacion12 = $rs->fields[69] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->csv_registro .= $this->Delim_line;
         fwrite($csv_f, $this->csv_registro);
         $rs->MoveNext();
      }
      fclose($csv_f);

      $rs->Close();
   }
   //----- id_paciente
   function NM_export_id_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->id_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- logro_comunicacion_gestion
   function NM_export_logro_comunicacion_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->logro_comunicacion_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_comunicacion
   function NM_export_fecha_comunicacion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_comunicacion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- autor_gestion
   function NM_export_autor_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->autor_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- departamento_paciente
   function NM_export_departamento_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->departamento_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- ciudad_paciente
   function NM_export_ciudad_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->ciudad_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- estado_paciente
   function NM_export_estado_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->estado_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- status_paciente
   function NM_export_status_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->status_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_activacion_paciente
   function NM_export_fecha_activacion_paciente()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_activacion_paciente);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- codigo_xofigo
   function NM_export_codigo_xofigo()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->codigo_xofigo);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- producto_tratamiento
   function NM_export_producto_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->producto_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- nombre_referencia
   function NM_export_nombre_referencia()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->nombre_referencia);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- dosis_tratamiento
   function NM_export_dosis_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->dosis_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- numero_cajas
   function NM_export_numero_cajas()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->numero_cajas);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- asegurador_tratamiento
   function NM_export_asegurador_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->asegurador_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- operador_logistico_tratamiento
   function NM_export_operador_logistico_tratamiento()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->operador_logistico_tratamiento);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- punto_entrega
   function NM_export_punto_entrega()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->punto_entrega);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo_gestion
   function NM_export_reclamo_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion_gestion
   function NM_export_fecha_reclamacion_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- causa_no_reclamacion_gestion
   function NM_export_causa_no_reclamacion_gestion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->causa_no_reclamacion_gestion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- id_historial_reclamacion
   function NM_export_id_historial_reclamacion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->id_historial_reclamacion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- anio_historial_reclamacion
   function NM_export_anio_historial_reclamacion()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->anio_historial_reclamacion);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes1
   function NM_export_mes1()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes1);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo1
   function NM_export_reclamo1()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo1);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion1
   function NM_export_fecha_reclamacion1()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion1);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion1
   function NM_export_motivo_no_reclamacion1()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion1);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes2
   function NM_export_mes2()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes2);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo2
   function NM_export_reclamo2()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo2);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion2
   function NM_export_fecha_reclamacion2()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion2);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion2
   function NM_export_motivo_no_reclamacion2()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion2);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes3
   function NM_export_mes3()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes3);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo3
   function NM_export_reclamo3()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo3);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion3
   function NM_export_fecha_reclamacion3()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion3);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion3
   function NM_export_motivo_no_reclamacion3()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion3);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes4
   function NM_export_mes4()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes4);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo4
   function NM_export_reclamo4()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo4);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion4
   function NM_export_fecha_reclamacion4()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion4);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion4
   function NM_export_motivo_no_reclamacion4()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion4);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes5
   function NM_export_mes5()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes5);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo5
   function NM_export_reclamo5()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo5);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion5
   function NM_export_fecha_reclamacion5()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion5);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion5
   function NM_export_motivo_no_reclamacion5()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion5);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes6
   function NM_export_mes6()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes6);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo6
   function NM_export_reclamo6()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo6);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion6
   function NM_export_fecha_reclamacion6()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion6);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion6
   function NM_export_motivo_no_reclamacion6()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion6);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes7
   function NM_export_mes7()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes7);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo7
   function NM_export_reclamo7()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo7);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion7
   function NM_export_fecha_reclamacion7()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion7);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion7
   function NM_export_motivo_no_reclamacion7()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion7);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes8
   function NM_export_mes8()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes8);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo8
   function NM_export_reclamo8()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo8);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion8
   function NM_export_fecha_reclamacion8()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion8);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion8
   function NM_export_motivo_no_reclamacion8()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion8);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes9
   function NM_export_mes9()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes9);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo9
   function NM_export_reclamo9()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo9);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion9
   function NM_export_fecha_reclamacion9()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion9);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion9
   function NM_export_motivo_no_reclamacion9()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion9);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes10
   function NM_export_mes10()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes10);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo10
   function NM_export_reclamo10()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo10);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion10
   function NM_export_fecha_reclamacion10()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion10);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion10
   function NM_export_motivo_no_reclamacion10()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion10);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes11
   function NM_export_mes11()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes11);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo11
   function NM_export_reclamo11()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo11);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion11
   function NM_export_fecha_reclamacion11()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion11);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion11
   function NM_export_motivo_no_reclamacion11()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion11);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- mes12
   function NM_export_mes12()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->mes12);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- reclamo12
   function NM_export_reclamo12()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->reclamo12);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- fecha_reclamacion12
   function NM_export_fecha_reclamacion12()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->fecha_reclamacion12);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }
   //----- motivo_no_reclamacion12
   function NM_export_motivo_no_reclamacion12()
   {
      $col_sep = ($this->NM_prim_col > 0) ? $this->Delim_col : "";
      $conteudo = str_replace($this->Delim_dados, $this->Delim_dados . $this->Delim_dados, $this->motivo_no_reclamacion12);
      $this->csv_registro .= $col_sep . $this->Delim_dados . $conteudo . $this->Delim_dados;
      $this->NM_prim_col++;
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['csv_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['csv_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Reclamaciones Historial :: CSV</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT">
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?>" GMT">
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate">
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0">
 <META http-equiv="Pragma" content="no-cache">
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">CSV</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="Informe_reclamacion_historial_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="Informe_reclamacion_historial"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
