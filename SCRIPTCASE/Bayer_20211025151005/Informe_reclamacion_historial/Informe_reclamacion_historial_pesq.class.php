<?php

class Informe_reclamacion_historial_pesq
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $cmp_formatado;
   var $nm_data;
   var $Campos_Mens_erro;

   var $comando;
   var $comando_sum;
   var $comando_filtro;
   var $comando_ini;
   var $comando_fim;
   var $NM_operador;
   var $NM_data_qp;
   var $NM_path_filter;
   var $NM_curr_fil;
   var $nm_location;
   var $nmgp_botoes = array();
   var $NM_fil_ant = array();

   /**
    * @access  public
    */
   function Informe_reclamacion_historial_pesq()
   {
   }

   /**
    * @access  public
    * @global  string  $bprocessa  
    */
   function monta_busca()
   {
      global $bprocessa;
      include("../_lib/css/" . $this->Ini->str_schema_filter . "_filter.php");
      $this->Ini->Str_btn_filter = trim($str_button) . "/" . trim($str_button) . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".php";
      $this->Str_btn_filter_css  = trim($str_button) . "/" . trim($str_button) . ".css";
      include($this->Ini->path_btn . $this->Ini->Str_btn_filter);
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['path_libs_php'] = $this->Ini->path_lib_php;
      $this->Img_sep_filter = "/" . trim($str_toolbar_separator);
      $this->Block_img_col  = trim($str_block_col);
      $this->Block_img_exp  = trim($str_block_exp);
      $this->Bubble_tail    = trim($str_bubble_tail);
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_gp_config_btn.php", "F", "nmButtonOutput"); 
      $this->init();
      if ($this->NM_ajax_flag)
      {
          ob_start();
          $this->Arr_result = array();
          $this->processa_ajax();
          $Temp = ob_get_clean();
          if ($Temp !== false && trim($Temp) != "")
          {
              $this->Arr_result['htmOutput'] = NM_charset_to_utf8($Temp);
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($this->Arr_result);
          if ($this->Db)
          {
              $this->Db->Close(); 
          }
          exit;
      }
      if (isset($bprocessa) && "pesq" == $bprocessa)
      {
         $this->processa_busca();
      }
      else
      {
         $this->monta_formulario();
      }
   }

   /**
    * @access  public
    */
   function monta_formulario()
   {
      $this->monta_html_ini();
      $this->monta_cabecalho();
      $this->monta_form();
      $this->monta_html_fim();
   }

   /**
    * @access  public
    */
   function init()
   {
      global $bprocessa;
      $_SESSION['scriptcase']['sc_tab_meses']['int'] = array(
                                  $this->Ini->Nm_lang['lang_mnth_janu'],
                                  $this->Ini->Nm_lang['lang_mnth_febr'],
                                  $this->Ini->Nm_lang['lang_mnth_marc'],
                                  $this->Ini->Nm_lang['lang_mnth_apri'],
                                  $this->Ini->Nm_lang['lang_mnth_mayy'],
                                  $this->Ini->Nm_lang['lang_mnth_june'],
                                  $this->Ini->Nm_lang['lang_mnth_july'],
                                  $this->Ini->Nm_lang['lang_mnth_augu'],
                                  $this->Ini->Nm_lang['lang_mnth_sept'],
                                  $this->Ini->Nm_lang['lang_mnth_octo'],
                                  $this->Ini->Nm_lang['lang_mnth_nove'],
                                  $this->Ini->Nm_lang['lang_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_meses']['abr'] = array(
                                  $this->Ini->Nm_lang['lang_shrt_mnth_janu'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_febr'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_marc'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_apri'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_mayy'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_june'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_july'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_augu'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_sept'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_octo'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_nove'],
                                  $this->Ini->Nm_lang['lang_shrt_mnth_dece']);
      $_SESSION['scriptcase']['sc_tab_dias']['int'] = array(
                                  $this->Ini->Nm_lang['lang_days_sund'],
                                  $this->Ini->Nm_lang['lang_days_mond'],
                                  $this->Ini->Nm_lang['lang_days_tued'],
                                  $this->Ini->Nm_lang['lang_days_wend'],
                                  $this->Ini->Nm_lang['lang_days_thud'],
                                  $this->Ini->Nm_lang['lang_days_frid'],
                                  $this->Ini->Nm_lang['lang_days_satd']);
      $_SESSION['scriptcase']['sc_tab_dias']['abr'] = array(
                                  $this->Ini->Nm_lang['lang_shrt_days_sund'],
                                  $this->Ini->Nm_lang['lang_shrt_days_mond'],
                                  $this->Ini->Nm_lang['lang_shrt_days_tued'],
                                  $this->Ini->Nm_lang['lang_shrt_days_wend'],
                                  $this->Ini->Nm_lang['lang_shrt_days_thud'],
                                  $this->Ini->Nm_lang['lang_shrt_days_frid'],
                                  $this->Ini->Nm_lang['lang_shrt_days_satd']);
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_functions.php", "", "") ; 
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_data.class.php", "C", "nm_data") ; 
      $this->nm_data = new nm_data("es");
      $pos_path = strrpos($this->Ini->path_prod, "/");
      $this->NM_path_filter = $this->Ini->root . substr($this->Ini->path_prod, 0, $pos_path) . "/conf/filters/";
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['opcao'] = "igual";
   }

   function processa_ajax()
   {
      global $NM_filters, $NM_filters_del, $nmgp_save_name, $nmgp_save_option, $NM_fields_refresh, $NM_parms_refresh, $Campo_bi, $Opc_bi, $NM_operador;
//-- ajax metodos ---
      if ($this->NM_ajax_opcao == "ajax_filter_save")
      {
          $this->salva_filtro();
          $this->NM_fil_ant = $this->gera_array_filtros();
          $Nome_filter = "";
          $Opt_filter  = "<option value=\"\"></option>\r\n";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              if ($_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Tipo_filter[1] = sc_convert_encoding($Tipo_filter[1], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  $Opt_filter .= "<option value=\"\">" . Informe_reclamacion_historial_pack_protect_string($Nome_filter) . "</option>\r\n";
              }
              $Opt_filter .= "<option value=\"" . Informe_reclamacion_historial_pack_protect_string($Tipo_filter[0]) . "\">.." . Informe_reclamacion_historial_pack_protect_string($Cada_filter) .  "</option>\r\n";
          }
          $Ajax_select  = "<SELECT id=\"sel_recup_filters_bot\" name=\"NM_filters_bot\" onChange=\"nm_submit_filter(this, 'bot')\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_bot", 'value' => $Ajax_select);
          $Ajax_select = "<SELECT id=\"sel_filters_del_bot\" name=\"NM_filters_del_bot\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_del_bot", 'value' => $Ajax_select);
      }

      if ($this->NM_ajax_opcao == "ajax_filter_delete")
      {
          $this->apaga_filtro();
          $this->NM_fil_ant = $this->gera_array_filtros();
          $Nome_filter = "";
          $Opt_filter  = "<option value=\"\"></option>\r\n";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              if ($_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Tipo_filter[1] = sc_convert_encoding($Tipo_filter[1], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter  = $Tipo_filter[1];
                  $Opt_filter .= "<option value=\"\">" .  Informe_reclamacion_historial_pack_protect_string($Nome_filter) . "</option>\r\n";
              }
              $Opt_filter .= "<option value=\"" . Informe_reclamacion_historial_pack_protect_string($Tipo_filter[0]) . "\">.." . Informe_reclamacion_historial_pack_protect_string($Cada_filter) .  "</option>\r\n";
          }
          $Ajax_select  = "<SELECT id=\"sel_recup_filters_bot\" name=\"NM_filters_bot\" onChange=\"nm_submit_filter(this, 'bot')\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_bot", 'value' => $Ajax_select);
          $Ajax_select = "<SELECT id=\"sel_filters_del_bot\" name=\"NM_filters_del_bot\" size=\"1\">\r\n";
          $Ajax_select .= $Opt_filter;
          $Ajax_select .= "</SELECT>\r\n";
          $this->Arr_result['setValue'][] = array('field' => "idAjaxSelect_NM_filters_del_bot", 'value' => $Ajax_select);
      }
      if ($this->NM_ajax_opcao == "ajax_filter_select")
      {
          $this->Arr_result = $this->recupera_filtro();
      }

      if ($this->NM_ajax_opcao == 'autocomp_id_paciente')
      {
          $id_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_id_paciente($id_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_logro_comunicacion_gestion')
      {
          $logro_comunicacion_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_logro_comunicacion_gestion($logro_comunicacion_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_fecha_comunicacion')
      {
          $fecha_comunicacion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_fecha_comunicacion($fecha_comunicacion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_autor_gestion')
      {
          $autor_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_autor_gestion($autor_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_departamento_paciente')
      {
          $departamento_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_departamento_paciente($departamento_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_ciudad_paciente')
      {
          $ciudad_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_ciudad_paciente($ciudad_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_estado_paciente')
      {
          $estado_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_estado_paciente($estado_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_status_paciente')
      {
          $status_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_status_paciente($status_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_fecha_activacion_paciente')
      {
          $fecha_activacion_paciente = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_fecha_activacion_paciente($fecha_activacion_paciente);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_codigo_xofigo')
      {
          $codigo_xofigo = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_codigo_xofigo($codigo_xofigo);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_producto_tratamiento')
      {
          $producto_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_producto_tratamiento($producto_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_nombre_referencia')
      {
          $nombre_referencia = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_nombre_referencia($nombre_referencia);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_dosis_tratamiento')
      {
          $dosis_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_dosis_tratamiento($dosis_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_numero_cajas')
      {
          $numero_cajas = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_numero_cajas($numero_cajas);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_asegurador_tratamiento')
      {
          $asegurador_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_asegurador_tratamiento($asegurador_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_operador_logistico_tratamiento')
      {
          $operador_logistico_tratamiento = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_operador_logistico_tratamiento($operador_logistico_tratamiento);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_punto_entrega')
      {
          $punto_entrega = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_punto_entrega($punto_entrega);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_reclamo_gestion')
      {
          $reclamo_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_reclamo_gestion($reclamo_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_fecha_reclamacion_gestion')
      {
          $fecha_reclamacion_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_fecha_reclamacion_gestion($fecha_reclamacion_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_causa_no_reclamacion_gestion')
      {
          $causa_no_reclamacion_gestion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_causa_no_reclamacion_gestion($causa_no_reclamacion_gestion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_id_historial_reclamacion')
      {
          $id_historial_reclamacion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_id_historial_reclamacion($id_historial_reclamacion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
      if ($this->NM_ajax_opcao == 'autocomp_anio_historial_reclamacion')
      {
          $anio_historial_reclamacion = ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($_GET['q'])) ? sc_convert_encoding($_GET['q'], $_SESSION['scriptcase']['charset'], "UTF-8") : $_GET['q'];
          $nmgp_def_dados = $this->lookup_ajax_anio_historial_reclamacion($anio_historial_reclamacion);
          ob_end_clean();
          $count_aut_comp = 0;
          $resp_aut_comp  = array();
          foreach ($nmgp_def_dados as $Ind => $Lista)
          {
             if (is_array($Lista))
             {
                 foreach ($Lista as $Cod => $Valor)
                 {
                     if ($_GET['cod_desc'] == "S")
                     {
                         $Valor = $Cod . " - " . $Valor;
                     }
                     $resp_aut_comp[] = array('label' => $Valor , 'value' => $Cod);
                     $count_aut_comp++;
                 }
             }
             if ($count_aut_comp == $_GET['max_itens'])
             {
                 break;
             }
          }
          $oJson = new Services_JSON();
          echo $oJson->encode($resp_aut_comp);
          $this->Db->Close(); 
          exit;
      }
   }
   function lookup_ajax_id_paciente($id_paciente)
   {
      $id_paciente = substr($this->Db->qstr($id_paciente), 1, -1);
            $id_paciente_look = substr($this->Db->qstr($id_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ID_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where   CAST (ID_PACIENTE AS TEXT)  like '%" . $id_paciente . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct ID_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  ID_PACIENTE like '%" . $id_paciente . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_logro_comunicacion_gestion($logro_comunicacion_gestion)
   {
      $logro_comunicacion_gestion = substr($this->Db->qstr($logro_comunicacion_gestion), 1, -1);
            $logro_comunicacion_gestion_look = substr($this->Db->qstr($logro_comunicacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct LOGRO_COMUNICACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  LOGRO_COMUNICACION_GESTION like '%" . $logro_comunicacion_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_fecha_comunicacion($fecha_comunicacion)
   {
      $fecha_comunicacion = substr($this->Db->qstr($fecha_comunicacion), 1, -1);
            $fecha_comunicacion_look = substr($this->Db->qstr($fecha_comunicacion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_COMUNICACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  FECHA_COMUNICACION like '%" . $fecha_comunicacion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_autor_gestion($autor_gestion)
   {
      $autor_gestion = substr($this->Db->qstr($autor_gestion), 1, -1);
            $autor_gestion_look = substr($this->Db->qstr($autor_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct AUTOR_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  AUTOR_GESTION like '%" . $autor_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_departamento_paciente($departamento_paciente)
   {
      $departamento_paciente = substr($this->Db->qstr($departamento_paciente), 1, -1);
            $departamento_paciente_look = substr($this->Db->qstr($departamento_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct DEPARTAMENTO_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  DEPARTAMENTO_PACIENTE like '%" . $departamento_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_ciudad_paciente($ciudad_paciente)
   {
      $ciudad_paciente = substr($this->Db->qstr($ciudad_paciente), 1, -1);
            $ciudad_paciente_look = substr($this->Db->qstr($ciudad_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct CIUDAD_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  CIUDAD_PACIENTE like '%" . $ciudad_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_estado_paciente($estado_paciente)
   {
      $estado_paciente = substr($this->Db->qstr($estado_paciente), 1, -1);
            $estado_paciente_look = substr($this->Db->qstr($estado_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct ESTADO_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  ESTADO_PACIENTE like '%" . $estado_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_status_paciente($status_paciente)
   {
      $status_paciente = substr($this->Db->qstr($status_paciente), 1, -1);
            $status_paciente_look = substr($this->Db->qstr($status_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct STATUS_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  STATUS_PACIENTE like '%" . $status_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_fecha_activacion_paciente($fecha_activacion_paciente)
   {
      $fecha_activacion_paciente = substr($this->Db->qstr($fecha_activacion_paciente), 1, -1);
            $fecha_activacion_paciente_look = substr($this->Db->qstr($fecha_activacion_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_ACTIVACION_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  FECHA_ACTIVACION_PACIENTE like '%" . $fecha_activacion_paciente . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_codigo_xofigo($codigo_xofigo)
   {
      $codigo_xofigo = substr($this->Db->qstr($codigo_xofigo), 1, -1);
            $codigo_xofigo_look = substr($this->Db->qstr($codigo_xofigo), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct CODIGO_XOFIGO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where   CAST (CODIGO_XOFIGO AS TEXT)  like '%" . $codigo_xofigo . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct CODIGO_XOFIGO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  CODIGO_XOFIGO like '%" . $codigo_xofigo . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_producto_tratamiento($producto_tratamiento)
   {
      $producto_tratamiento = substr($this->Db->qstr($producto_tratamiento), 1, -1);
            $producto_tratamiento_look = substr($this->Db->qstr($producto_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct PRODUCTO_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  PRODUCTO_TRATAMIENTO like '%" . $producto_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_nombre_referencia($nombre_referencia)
   {
      $nombre_referencia = substr($this->Db->qstr($nombre_referencia), 1, -1);
            $nombre_referencia_look = substr($this->Db->qstr($nombre_referencia), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct NOMBRE_REFERENCIA from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  NOMBRE_REFERENCIA like '%" . $nombre_referencia . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_dosis_tratamiento($dosis_tratamiento)
   {
      $dosis_tratamiento = substr($this->Db->qstr($dosis_tratamiento), 1, -1);
            $dosis_tratamiento_look = substr($this->Db->qstr($dosis_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct DOSIS_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  DOSIS_TRATAMIENTO like '%" . $dosis_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_numero_cajas($numero_cajas)
   {
      $numero_cajas = substr($this->Db->qstr($numero_cajas), 1, -1);
            $numero_cajas_look = substr($this->Db->qstr($numero_cajas), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct NUMERO_CAJAS from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  NUMERO_CAJAS like '%" . $numero_cajas . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_asegurador_tratamiento($asegurador_tratamiento)
   {
      $asegurador_tratamiento = substr($this->Db->qstr($asegurador_tratamiento), 1, -1);
            $asegurador_tratamiento_look = substr($this->Db->qstr($asegurador_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct ASEGURADOR_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  ASEGURADOR_TRATAMIENTO like '%" . $asegurador_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_operador_logistico_tratamiento($operador_logistico_tratamiento)
   {
      $operador_logistico_tratamiento = substr($this->Db->qstr($operador_logistico_tratamiento), 1, -1);
            $operador_logistico_tratamiento_look = substr($this->Db->qstr($operador_logistico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct OPERADOR_LOGISTICO_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  OPERADOR_LOGISTICO_TRATAMIENTO like '%" . $operador_logistico_tratamiento . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_punto_entrega($punto_entrega)
   {
      $punto_entrega = substr($this->Db->qstr($punto_entrega), 1, -1);
            $punto_entrega_look = substr($this->Db->qstr($punto_entrega), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct PUNTO_ENTREGA from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  PUNTO_ENTREGA like '%" . $punto_entrega . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_reclamo_gestion($reclamo_gestion)
   {
      $reclamo_gestion = substr($this->Db->qstr($reclamo_gestion), 1, -1);
            $reclamo_gestion_look = substr($this->Db->qstr($reclamo_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct RECLAMO_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  RECLAMO_GESTION like '%" . $reclamo_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_fecha_reclamacion_gestion($fecha_reclamacion_gestion)
   {
      $fecha_reclamacion_gestion = substr($this->Db->qstr($fecha_reclamacion_gestion), 1, -1);
            $fecha_reclamacion_gestion_look = substr($this->Db->qstr($fecha_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_RECLAMACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  FECHA_RECLAMACION_GESTION like '%" . $fecha_reclamacion_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_causa_no_reclamacion_gestion($causa_no_reclamacion_gestion)
   {
      $causa_no_reclamacion_gestion = substr($this->Db->qstr($causa_no_reclamacion_gestion), 1, -1);
            $causa_no_reclamacion_gestion_look = substr($this->Db->qstr($causa_no_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct CAUSA_NO_RECLAMACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  CAUSA_NO_RECLAMACION_GESTION like '%" . $causa_no_reclamacion_gestion . "%'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_id_historial_reclamacion($id_historial_reclamacion)
   {
      $id_historial_reclamacion = substr($this->Db->qstr($id_historial_reclamacion), 1, -1);
            $id_historial_reclamacion_look = substr($this->Db->qstr($id_historial_reclamacion), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ID_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where   CAST (ID_HISTORIAL_RECLAMACION AS TEXT)  like '%" . $id_historial_reclamacion . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct ID_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  ID_HISTORIAL_RECLAMACION like '%" . $id_historial_reclamacion . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   
   function lookup_ajax_anio_historial_reclamacion($anio_historial_reclamacion)
   {
      $anio_historial_reclamacion = substr($this->Db->qstr($anio_historial_reclamacion), 1, -1);
            $anio_historial_reclamacion_look = substr($this->Db->qstr($anio_historial_reclamacion), 1, -1); 
      $nmgp_def_dados = array(); 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ANIO_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where   CAST (ANIO_HISTORIAL_RECLAMACION AS TEXT)  like '%" . $anio_historial_reclamacion . "%'"; 
      }
      else
      {
          $nm_comando = "select distinct ANIO_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where  ANIO_HISTORIAL_RECLAMACION like '%" . $anio_historial_reclamacion . "%'"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      return $nmgp_def_dados;
   }
   

   /**
    * @access  public
    */
   function processa_busca()
   {
      $this->inicializa_vars();
      $this->trata_campos();
      if (!empty($this->Campos_Mens_erro)) 
      {
          scriptcase_error_display($this->Campos_Mens_erro, ""); 
          $this->monta_formulario();
      }
      else
      {
          $this->finaliza_resultado();
      }
   }

   /**
    * @access  public
    */
   function and_or()
   {
      $posWhere = strpos(strtolower($this->comando), "where");
      if (FALSE === $posWhere)
      {
         $this->comando     .= " where ";
         $this->comando_sum .= " and ";
      }
      if ($this->comando_ini == "ini")
      {
          if (FALSE !== $posWhere)
          {
              $this->comando     .= " and ( ";
              $this->comando_sum .= " and ( ";
              $this->comando_fim  = " ) ";
          }
         $this->comando_ini  = "";
      }
      elseif ("or" == $this->NM_operador)
      {
         $this->comando        .= " or ";
         $this->comando_sum    .= " or ";
         $this->comando_filtro .= " or ";
      }
      else
      {
         $this->comando        .= " and ";
         $this->comando_sum    .= " and ";
         $this->comando_filtro .= " and ";
      }
   }

   /**
    * @access  public
    * @param  string  $nome  
    * @param  string  $condicao  
    * @param  mixed  $campo  
    * @param  mixed  $campo2  
    * @param  string  $nome_campo  
    * @param  string  $tp_campo  
    * @global  array  $nmgp_tab_label  
    */
   function monta_condicao($nome, $condicao, $campo, $campo2 = "", $nome_campo="", $tp_campo="")
   {
      global $nmgp_tab_label;
      $condicao   = strtoupper($condicao);
      $nm_aspas   = "'";
      $nm_aspas1  = "'";
      $Nm_numeric = array();
      $nm_esp_postgres = array();
      $nm_ini_lower = "";
      $nm_fim_lower = "";
      $Nm_numeric[] = "id_paciente";$Nm_numeric[] = "codigo_xofigo";$Nm_numeric[] = "id_historial_reclamacion";$Nm_numeric[] = "anio_historial_reclamacion";
      $campo_join = strtolower(str_replace(".", "_", $nome));
      if (in_array($campo_join, $Nm_numeric))
      {
          if ($condicao == "EP" || $condicao == "NE")
          {
              return;
          }
         if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['decimal_db'] == ".")
         {
            $nm_aspas  = "";
            $nm_aspas1 = "";
         }
         if ($condicao != "IN")
         {
            if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['decimal_db'] == ".")
            {
               $campo  = str_replace(",", ".", $campo);
               $campo2 = str_replace(",", ".", $campo2);
            }
            if ($campo == "")
            {
               $campo = 0;
            }
            if ($campo2 == "")
            {
               $campo2 = 0;
            }
         }
      }
      if ($campo == "" && $condicao != "NU" && $condicao != "NN" && $condicao != "EP" && $condicao != "NE")
      {
         return;
      }
      else
      {
         $tmp_pos = strpos($campo, "##@@");
         if ($tmp_pos === false)
         {
             $res_lookup = $campo;
         }
         else
         {
             $res_lookup = substr($campo, $tmp_pos + 4);
             $campo = substr($campo, 0, $tmp_pos);
             if ($campo == "" && $condicao != "NU" && $condicao != "NN" && $condicao != "EP" && $condicao != "NE")
             {
                 return;
             }
         }
         $tmp_pos = strpos($this->cmp_formatado[$nome_campo], "##@@");
         if ($tmp_pos !== false)
         {
             $this->cmp_formatado[$nome_campo] = substr($this->cmp_formatado[$nome_campo], $tmp_pos + 4);
         }
         $this->and_or();
         $campo  = substr($this->Db->qstr($campo), 1, -1);
         $campo2 = substr($this->Db->qstr($campo2), 1, -1);
         $nome_sum = ".$nome";
         if ($tp_campo == "TIMESTAMP")
         {
             $tp_campo = "DATETIME";
         }
         if (in_array($campo_join, $Nm_numeric) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && ($condicao == "II" || $condicao == "QP" || $condicao == "NP"))
         {
             $nome     = "CAST ($nome AS TEXT)";
             $nome_sum = "CAST ($nome_sum AS TEXT)";
         }
         if (in_array($campo_join, $nm_esp_postgres) && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
         {
             $nome     = "CAST ($nome AS TEXT)";
             $nome_sum = "CAST ($nome_sum AS TEXT)";
         }
         if (substr($tp_campo, 0, 8) == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && !$this->Date_part)
         {
             $nome     = "to_char ($nome, 'YYYY-MM-DD hh24:mi:ss')";
             $nome_sum = "to_char ($nome_sum, 'YYYY-MM-DD hh24:mi:ss')";
         }
         elseif (substr($tp_campo, 0, 4) == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && !$this->Date_part)
         {
             $nome     = "to_char ($nome, 'YYYY-MM-DD')";
             $nome_sum = "to_char ($nome_sum, 'YYYY-MM-DD')";
         }
         elseif (substr($tp_campo, 0, 4) == "TIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres) && !$this->Date_part)
         {
             $nome     = "to_char ($nome, 'hh24:mi:ss')";
             $nome_sum = "to_char ($nome_sum, 'hh24:mi:ss')";
         }
         if (substr($tp_campo, 0, 4) == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase) && !$this->Date_part)
         {
             $nome     = "str_replace (convert(char(10),$nome,102), '.', '-') + ' ' + convert(char(8),$nome,20)";
             $nome_sum = "str_replace (convert(char(10),$nome_sum,102), '.', '-') + ' ' + convert(char(8),$nome_sum,20)";
         }
         if ($tp_campo == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql) && !$this->Date_part)
         {
             $nome     = "convert(char(10),$nome,121)";
             $nome_sum = "convert(char(10),$nome_sum,121)";
         }
         if ($tp_campo == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql) && !$this->Date_part)
         {
             $nome     = "convert(char(19),$nome,121)";
             $nome_sum = "convert(char(19),$nome_sum,121)";
         }
         if (substr($tp_campo, 0, 8) == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle) && !$this->Date_part)
         {
             $nome     = "TO_DATE(TO_CHAR($nome, 'yyyy-mm-dd hh24:mi:ss'), 'yyyy-mm-dd hh24:mi:ss')";
             $nome_sum = "TO_DATE(TO_CHAR($nome_sum, 'yyyy-mm-dd hh24:mi:ss'), 'yyyy-mm-dd hh24:mi:ss')";
             $tp_campo = "DATETIME";
         }
         if (substr($tp_campo, 0, 8) == "DATETIME" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix) && !$this->Date_part)
         {
             $nome     = "EXTEND($nome, YEAR TO FRACTION)";
             $nome_sum = "EXTEND($nome_sum, YEAR TO FRACTION)";
         }
         elseif (substr($tp_campo, 0, 4) == "DATE" && in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix) && !$this->Date_part)
         {
             $nome     = "EXTEND($nome, YEAR TO DAY)";
             $nome_sum = "EXTEND($nome_sum, YEAR TO DAY)";
         }
         switch ($condicao)
         {
            case "EQ":     // 
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " = " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " = " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower. " = " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_equl'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "II":     // 
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " like '" . $campo . "%'";
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " like '" . $campo . "%'";
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . " like '" . $campo . "%'";
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_strt'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
             case "QP";     // 
             case "NP";     // 
                $concat = " " . $this->NM_operador . " ";
                if ($condicao == "QP")
                {
                    $op_all    = " like ";
                    $lang_like = $this->Ini->Nm_lang['lang_srch_like'];
                }
                else
                {
                    $op_all    = " not like ";
                    $lang_like = $this->Ini->Nm_lang['lang_srch_not_like'];
                }
               $NM_cond    = "";
               $NM_cmd     = "";
               $NM_cmd_sum = "";
               if (substr($tp_campo, 0, 4) == "DATE" && $this->Date_part)
               {
                   if ($this->NM_data_qp['ano'] != "____")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_year'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['ano'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%Y', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%Y', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(year from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(year from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('year' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('year' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(year from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(year from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'YYYY')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'YYYY')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(year, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(year, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "year(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                           $NM_cmd_sum .= "year(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['ano'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['mes'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_mnth'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['mes'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%m', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%m', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(month from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(month from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('month' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('month' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(month from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(month from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'MM')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'MM')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(month, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(month, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "month(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                           $NM_cmd_sum .= "month(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['mes'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['dia'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_days'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['dia'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%d', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%d', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(day from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(day from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('day' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('day' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(day from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(day from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'DD')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'DD')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(day, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(day, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "day(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                           $NM_cmd_sum .= "day(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['dia'] . $this->End_date_part;
                       }
                   }
               }
               if (strpos($tp_campo, "TIME") !== false && $this->Date_part)
               {
                   if ($this->NM_data_qp['hor'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_time'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['hor'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%H', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%H', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(hour from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(hour from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('hour' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('hour' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(hour from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(hour from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'HH24')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'HH24')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(hour, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(hour, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "hour(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                           $NM_cmd_sum .= "hour(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['hor'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['min'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_mint'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['min'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%M', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%M', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(minute from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(minute from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('minute' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('minute' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(minute from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(minute from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'MI')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'MI')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(minute, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(minute, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "minute(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                           $NM_cmd_sum .= "minute(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['min'] . $this->End_date_part;
                       }
                   }
                   if ($this->NM_data_qp['seg'] != "__")
                   {
                       $NM_cond    .= (empty($NM_cmd)) ? "" : " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " ";
                       $NM_cond    .= $this->Ini->Nm_lang['lang_srch_scnd'] . " " . $this->Lang_date_part . " " . $this->NM_data_qp['seg'];
                       $NM_cmd     .= (empty($NM_cmd)) ? "" : $concat;
                       $NM_cmd_sum .= (empty($NM_cmd_sum)) ? "" : $concat;
                       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
                       {
                           $NM_cmd     .= "strftime('%S', " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "strftime('%S', " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
                       {
                           $NM_cmd     .= "extract(second from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(second from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                       {
                           $NM_cmd     .= $this->Ini_date_char . "extract('second' from " . $nome . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= $this->Ini_date_char . "extract('second' from " . $nome_sum . ")" . $this->End_date_char . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
                       {
                           $NM_cmd     .= "extract(second from " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "extract(second from " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
                       {
                           $NM_cmd     .= "TO_CHAR(" . $nome . ", 'SS')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "TO_CHAR(" . $nome_sum . ", 'SS')" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
                       {
                           $NM_cmd     .= "DATEPART(second, " . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "DATEPART(second, " . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                       else
                       {
                           $NM_cmd     .= "second(" . $nome . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                           $NM_cmd_sum .= "second(" . $nome_sum . ")" . $this->Operador_date_part . $this->Ini_date_part . $this->NM_data_qp['seg'] . $this->End_date_part;
                       }
                   }
               }
               if ($this->Date_part)
               {
                   if (!empty($NM_cmd))
                   {
                       $NM_cmd     = " (" . $NM_cmd . ")";
                       $NM_cmd_sum = " (" . $NM_cmd_sum . ")";
                       $this->comando        .= $NM_cmd;
                       $this->comando_sum    .= $NM_cmd_sum;
                       $this->comando_filtro .= $NM_cmd;
                       $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . ": " . $NM_cond . "##*@@";
                   }
               }
               else
               {
                   $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . $op_all . "'%" . $campo . "%'";
                   $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . $op_all . "'%" . $campo . "%'";
                   $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . $op_all . "'%" . $campo . "%'";
                   $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $lang_like . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
               }
            break;
            case "DF":     // 
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " <> " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " <> " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . " <> " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_diff'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "GT":     // 
               $this->comando        .= " $nome > " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum > " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome > " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_grtr'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "GE":     // 
               $this->comando        .= " $nome >= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum >= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome >= " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_grtr_equl'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "LT":     // 
               $this->comando        .= " $nome < " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum < " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome < " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_less'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "LE":     // 
               $this->comando        .= " $nome <= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum <= " . $nm_aspas . $campo . $nm_aspas1;
               $this->comando_filtro .= " $nome <= " . $nm_aspas . $campo . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_less_equl'] . " " . $this->cmp_formatado[$nome_campo] . "##*@@";
            break;
            case "BW":     // 
               $this->comando        .= " $nome between " . $nm_aspas . $campo . $nm_aspas1 . " and " . $nm_aspas . $campo2 . $nm_aspas1;
               $this->comando_sum    .= " $nome_sum between " . $nm_aspas . $campo . $nm_aspas1 . " and " . $nm_aspas . $campo2 . $nm_aspas1;
               $this->comando_filtro .= " $nome between " . $nm_aspas . $campo . $nm_aspas1 . " and " . $nm_aspas . $campo2 . $nm_aspas1;
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_betw'] . " " . $this->cmp_formatado[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_and_cond'] . " " . $this->cmp_formatado[$nome_campo . "_input_2"] . "##*@@";
            break;
            case "IN":     // 
               $nm_sc_valores = explode(",", $campo);
               $cond_str = "";
               $nm_cond  = "";
               if (!empty($nm_sc_valores))
               {
                   foreach ($nm_sc_valores as $nm_sc_valor)
                   {
                      if (in_array($campo_join, $Nm_numeric) && substr_count($nm_sc_valor, ".") > 1)
                      {
                         $nm_sc_valor = str_replace(".", "", $nm_sc_valor);
                      }
                      if ("" != $cond_str)
                      {
                         $cond_str .= ",";
                         $nm_cond  .= " " . $this->Ini->Nm_lang['lang_srch_orr_cond'] . " ";
                      }
                      $cond_str .= $nm_aspas . $nm_sc_valor . $nm_aspas1;
                      $nm_cond  .= $nm_aspas . $nm_sc_valor . $nm_aspas1;
                   }
               }
               $this->comando        .= $nm_ini_lower . $nome . $nm_fim_lower . " in (" . $cond_str . ")";
               $this->comando_sum    .= $nm_ini_lower . $nome_sum . $nm_fim_lower . " in (" . $cond_str . ")";
               $this->comando_filtro .= $nm_ini_lower . $nome . $nm_fim_lower . " in (" . $cond_str . ")";
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_like'] . " " . $nm_cond . "##*@@";
            break;
            case "NU":     // 
               $this->comando        .= " $nome IS NULL ";
               $this->comando_sum    .= " $nome_sum IS NULL ";
               $this->comando_filtro .= " $nome IS NULL ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_null'] . "##*@@";
            break;
            case "NN":     // 
               $this->comando        .= " $nome IS NOT NULL ";
               $this->comando_sum    .= " $nome_sum IS NOT NULL ";
               $this->comando_filtro .= " $nome IS NOT NULL ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_nnul'] . "##*@@";
            break;
            case "EP":     // 
               $this->comando        .= " $nome = '' ";
               $this->comando_sum    .= " $nome_sum = '' ";
               $this->comando_filtro .= " $nome = '' ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_empty'] . "##*@@";
            break;
            case "NE":     // 
               $this->comando        .= " $nome <> '' ";
               $this->comando_sum    .= " $nome_sum <> '' ";
               $this->comando_filtro .= " $nome <> '' ";
               $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $nmgp_tab_label[$nome_campo] . " " . $this->Ini->Nm_lang['lang_srch_nempty'] . "##*@@";
            break;
         }
      }
   }

   function nm_prep_date(&$val, $tp, $tsql, &$cond, $format_nd, $tp_nd)
   {
       $fill_dt = false;
       if ($tsql == "TIMESTAMP")
       {
           $tsql = "DATETIME";
       }
       $cond = strtoupper($cond);
       if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access) && $tp != "ND")
       {
           if ($cond == "EP")
           {
               $cond = "NU";
           }
           if ($cond == "NE")
           {
               $cond = "NN";
           }
       }
       if ($cond == "NU" || $cond == "NN" || $cond == "EP" || $cond == "NE")
       {
           $val    = array();
           $val[0] = "";
           return;
       }
       if ($cond != "II" && $cond != "QP" && $cond != "NP")
       {
           $fill_dt = true;
       }
       if ($fill_dt)
       {
           $val[0]['dia'] = (!empty($val[0]['dia']) && strlen($val[0]['dia']) == 1) ? "0" . $val[0]['dia'] : $val[0]['dia'];
           $val[0]['mes'] = (!empty($val[0]['mes']) && strlen($val[0]['mes']) == 1) ? "0" . $val[0]['mes'] : $val[0]['mes'];
           if ($tp == "DH")
           {
               $val[0]['hor'] = (!empty($val[0]['hor']) && strlen($val[0]['hor']) == 1) ? "0" . $val[0]['hor'] : $val[0]['hor'];
               $val[0]['min'] = (!empty($val[0]['min']) && strlen($val[0]['min']) == 1) ? "0" . $val[0]['min'] : $val[0]['min'];
               $val[0]['seg'] = (!empty($val[0]['seg']) && strlen($val[0]['seg']) == 1) ? "0" . $val[0]['seg'] : $val[0]['seg'];
           }
           if ($cond == "BW")
           {
               $val[1]['dia'] = (!empty($val[1]['dia']) && strlen($val[1]['dia']) == 1) ? "0" . $val[1]['dia'] : $val[1]['dia'];
               $val[1]['mes'] = (!empty($val[1]['mes']) && strlen($val[1]['mes']) == 1) ? "0" . $val[1]['mes'] : $val[1]['mes'];
               if ($tp == "DH")
               {
                   $val[1]['hor'] = (!empty($val[1]['hor']) && strlen($val[1]['hor']) == 1) ? "0" . $val[1]['hor'] : $val[1]['hor'];
                   $val[1]['min'] = (!empty($val[1]['min']) && strlen($val[1]['min']) == 1) ? "0" . $val[1]['min'] : $val[1]['min'];
                   $val[1]['seg'] = (!empty($val[1]['seg']) && strlen($val[1]['seg']) == 1) ? "0" . $val[1]['seg'] : $val[1]['seg'];
               }
           }
       }
       if ($cond == "BW")
       {
           $this->NM_data_1 = array();
           $this->NM_data_1['ano'] = (isset($val[0]['ano']) && !empty($val[0]['ano'])) ? $val[0]['ano'] : "____";
           $this->NM_data_1['mes'] = (isset($val[0]['mes']) && !empty($val[0]['mes'])) ? $val[0]['mes'] : "__";
           $this->NM_data_1['dia'] = (isset($val[0]['dia']) && !empty($val[0]['dia'])) ? $val[0]['dia'] : "__";
           $this->NM_data_1['hor'] = (isset($val[0]['hor']) && !empty($val[0]['hor'])) ? $val[0]['hor'] : "__";
           $this->NM_data_1['min'] = (isset($val[0]['min']) && !empty($val[0]['min'])) ? $val[0]['min'] : "__";
           $this->NM_data_1['seg'] = (isset($val[0]['seg']) && !empty($val[0]['seg'])) ? $val[0]['seg'] : "__";
           $this->data_menor($this->NM_data_1);
           $this->NM_data_2 = array();
           $this->NM_data_2['ano'] = (isset($val[1]['ano']) && !empty($val[1]['ano'])) ? $val[1]['ano'] : "____";
           $this->NM_data_2['mes'] = (isset($val[1]['mes']) && !empty($val[1]['mes'])) ? $val[1]['mes'] : "__";
           $this->NM_data_2['dia'] = (isset($val[1]['dia']) && !empty($val[1]['dia'])) ? $val[1]['dia'] : "__";
           $this->NM_data_2['hor'] = (isset($val[1]['hor']) && !empty($val[1]['hor'])) ? $val[1]['hor'] : "__";
           $this->NM_data_2['min'] = (isset($val[1]['min']) && !empty($val[1]['min'])) ? $val[1]['min'] : "__";
           $this->NM_data_2['seg'] = (isset($val[1]['seg']) && !empty($val[1]['seg'])) ? $val[1]['seg'] : "__";
           $this->data_maior($this->NM_data_2);
           $val = array();
           if ($tp == "ND")
           {
               $out_dt1 = $format_nd;
               $out_dt1 = str_replace("yyyy", $this->NM_data_1['ano'], $out_dt1);
               $out_dt1 = str_replace("mm",   $this->NM_data_1['mes'], $out_dt1);
               $out_dt1 = str_replace("dd",   $this->NM_data_1['dia'], $out_dt1);
               $out_dt1 = str_replace("hh",   "", $out_dt1);
               $out_dt1 = str_replace("ii",   "", $out_dt1);
               $out_dt1 = str_replace("ss",   "", $out_dt1);
               $out_dt2 = $format_nd;
               $out_dt2 = str_replace("yyyy", $this->NM_data_2['ano'], $out_dt2);
               $out_dt2 = str_replace("mm",   $this->NM_data_2['mes'], $out_dt2);
               $out_dt2 = str_replace("dd",   $this->NM_data_2['dia'], $out_dt2);
               $out_dt2 = str_replace("hh",   "", $out_dt2);
               $out_dt2 = str_replace("ii",   "", $out_dt2);
               $out_dt2 = str_replace("ss",   "", $out_dt2);
               $val[0] = $out_dt1;
               $val[1] = $out_dt2;
               return;
           }
           if ($tsql == "TIME")
           {
               $val[0] = $this->NM_data_1['hor'] . ":" . $this->NM_data_1['min'] . ":" . $this->NM_data_1['seg'];
               $val[1] = $this->NM_data_2['hor'] . ":" . $this->NM_data_2['min'] . ":" . $this->NM_data_2['seg'];
           }
           elseif (substr($tsql, 0, 4) == "DATE")
           {
               $val[0] = $this->NM_data_1['ano'] . "-" . $this->NM_data_1['mes'] . "-" . $this->NM_data_1['dia'];
               $val[1] = $this->NM_data_2['ano'] . "-" . $this->NM_data_2['mes'] . "-" . $this->NM_data_2['dia'];
               if (strpos($tsql, "TIME") !== false)
               {
                   $val[0] .= " " . $this->NM_data_1['hor'] . ":" . $this->NM_data_1['min'] . ":" . $this->NM_data_1['seg'];
                   $val[1] .= " " . $this->NM_data_2['hor'] . ":" . $this->NM_data_2['min'] . ":" . $this->NM_data_2['seg'];
               }
           }
           return;
       }
       $this->NM_data_qp = array();
       $this->NM_data_qp['ano'] = (isset($val[0]['ano']) && $val[0]['ano'] != "") ? $val[0]['ano'] : "____";
       $this->NM_data_qp['mes'] = (isset($val[0]['mes']) && $val[0]['mes'] != "") ? $val[0]['mes'] : "__";
       $this->NM_data_qp['dia'] = (isset($val[0]['dia']) && $val[0]['dia'] != "") ? $val[0]['dia'] : "__";
       $this->NM_data_qp['hor'] = (isset($val[0]['hor']) && $val[0]['hor'] != "") ? $val[0]['hor'] : "__";
       $this->NM_data_qp['min'] = (isset($val[0]['min']) && $val[0]['min'] != "") ? $val[0]['min'] : "__";
       $this->NM_data_qp['seg'] = (isset($val[0]['seg']) && $val[0]['seg'] != "") ? $val[0]['seg'] : "__";
       if ($tp != "ND" && ($cond == "LE" || $cond == "LT" || $cond == "GE" || $cond == "GT"))
       {
           $count_fill = 0;
           foreach ($this->NM_data_qp as $x => $tx)
           {
               if (substr($tx, 0, 2) != "__")
               {
                   $count_fill++;
               }
           }
           if ($count_fill > 1)
           {
               if ($cond == "LE" || $cond == "GT")
               {
                   $this->data_maior($this->NM_data_qp);
               }
               else
               {
                   $this->data_menor($this->NM_data_qp);
               }
               if ($tsql == "TIME")
               {
                   $val[0] = $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
               }
               elseif (substr($tsql, 0, 4) == "DATE")
               {
                   $val[0] = $this->NM_data_qp['ano'] . "-" . $this->NM_data_qp['mes'] . "-" . $this->NM_data_qp['dia'];
                   if (strpos($tsql, "TIME") !== false)
                   {
                       $val[0] .= " " . $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
                   }
               }
               return;
           }
       }
       foreach ($this->NM_data_qp as $x => $tx)
       {
           if (substr($tx, 0, 2) == "__" && ($x == "dia" || $x == "mes" || $x == "ano"))
           {
               if (substr($tsql, 0, 4) == "DATE")
               {
                   $this->Date_part = true;
                   break;
               }
           }
           if (substr($tx, 0, 2) == "__" && ($x == "hor" || $x == "min" || $x == "seg"))
           {
               if (strpos($tsql, "TIME") !== false && ($tp == "DH" || ($tp == "DT" && $cond != "LE" && $cond != "LT" && $cond != "GE" && $cond != "GT")))
               {
                   $this->Date_part = true;
                   break;
               }
           }
       }
       if ($this->Date_part)
       {
           $this->Ini_date_part = "";
           $this->End_date_part = "";
           $this->Ini_date_char = "";
           $this->End_date_char = "";
           if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sqlite))
           {
               $this->Ini_date_part = "'";
               $this->End_date_part = "'";
           }
           if ($tp != "ND")
           {
               if ($cond == "EQ")
               {
                   $this->Operador_date_part = " = ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_equl'];
               }
               elseif ($cond == "II")
               {
                   $this->Operador_date_part = " like ";
                   $this->Ini_date_part = "'";
                   $this->End_date_part = "%'";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_strt'];
                   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                   {
                       $this->Ini_date_char = "CAST (";
                       $this->End_date_char = " AS TEXT)";
                   }
               }
               elseif ($cond == "DF")
               {
                   $this->Operador_date_part = " <> ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_diff'];
               }
               elseif ($cond == "GT")
               {
                   $this->Operador_date_part = " > ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['pesq_cond_maior'];
               }
               elseif ($cond == "GE")
               {
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_grtr_equl'];
                   $this->Operador_date_part = " >= ";
               }
               elseif ($cond == "LT")
               {
                   $this->Operador_date_part = " < ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_less'];
               }
               elseif ($cond == "LE")
               {
                   $this->Operador_date_part = " <= ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_less_equl'];
               }
               elseif ($cond == "NP")
               {
                   $this->Operador_date_part = " not like ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_diff'];
                   $this->Ini_date_part = "'%";
                   $this->End_date_part = "%'";
                   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                   {
                       $this->Ini_date_char = "CAST (";
                       $this->End_date_char = " AS TEXT)";
                   }
               }
               else
               {
                   $this->Operador_date_part = " like ";
                   $this->Lang_date_part = $this->Ini->Nm_lang['lang_srch_equl'];
                   $this->Ini_date_part = "'%";
                   $this->End_date_part = "%'";
                   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
                   {
                       $this->Ini_date_char = "CAST (";
                       $this->End_date_char = " AS TEXT)";
                   }
               }
           }
           if ($cond == "DF")
           {
               $cond = "NP";
           }
           if ($cond != "NP")
           {
               $cond = "QP";
           }
       }
       $val = array();
       if ($tp != "ND" && ($cond == "QP" || $cond == "NP"))
       {
           $val[0] = "";
           if (substr($tsql, 0, 4) == "DATE")
           {
               $val[0] .= $this->NM_data_qp['ano'] . "-" . $this->NM_data_qp['mes'] . "-" . $this->NM_data_qp['dia'];
               if (strpos($tsql, "TIME") !== false)
               {
                   $val[0] .= " ";
               }
           }
           if (strpos($tsql, "TIME") !== false)
           {
               $val[0] .= $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
           }
           return;
       }
       if ($cond == "II" || $cond == "DF" || $cond == "EQ" || $cond == "LT" || $cond == "GE")
       {
           $this->data_menor($this->NM_data_qp);
       }
       else
       {
           $this->data_maior($this->NM_data_qp);
       }
       if ($tsql == "TIME")
       {
           $val[0] = $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
           return;
       }
       $format_sql = "";
       if (substr($tsql, 0, 4) == "DATE")
       {
           $format_sql .= $this->NM_data_qp['ano'] . "-" . $this->NM_data_qp['mes'] . "-" . $this->NM_data_qp['dia'];
           if (strpos($tsql, "TIME") !== false)
           {
               $format_sql .= " ";
           }
       }
       if (strpos($tsql, "TIME") !== false)
       {
           $format_sql .=  $this->NM_data_qp['hor'] . ":" . $this->NM_data_qp['min'] . ":" . $this->NM_data_qp['seg'];
       }
       if ($tp != "ND")
       {
           $val[0] = $format_sql;
           return;
       }
       if ($tp == "ND")
       {
           $format_nd = str_replace("yyyy", $this->NM_data_qp['ano'], $format_nd);
           $format_nd = str_replace("mm",   $this->NM_data_qp['mes'], $format_nd);
           $format_nd = str_replace("dd",   $this->NM_data_qp['dia'], $format_nd);
           $format_nd = str_replace("hh",   $this->NM_data_qp['hor'], $format_nd);
           $format_nd = str_replace("ii",   $this->NM_data_qp['min'], $format_nd);
           $format_nd = str_replace("ss",   $this->NM_data_qp['seg'], $format_nd);
           $val[0] = $format_nd;
           return;
       }
   }
   function data_menor(&$data_arr)
   {
       $data_arr["ano"] = ("____" == $data_arr["ano"]) ? "0001" : $data_arr["ano"];
       $data_arr["mes"] = ("__" == $data_arr["mes"])   ? "01" : $data_arr["mes"];
       $data_arr["dia"] = ("__" == $data_arr["dia"])   ? "01" : $data_arr["dia"];
       $data_arr["hor"] = ("__" == $data_arr["hor"])   ? "00" : $data_arr["hor"];
       $data_arr["min"] = ("__" == $data_arr["min"])   ? "00" : $data_arr["min"];
       $data_arr["seg"] = ("__" == $data_arr["seg"])   ? "00" : $data_arr["seg"];
   }

   function data_maior(&$data_arr)
   {
       $data_arr["ano"] = ("____" == $data_arr["ano"]) ? "9999" : $data_arr["ano"];
       $data_arr["mes"] = ("__" == $data_arr["mes"])   ? "12" : $data_arr["mes"];
       $data_arr["hor"] = ("__" == $data_arr["hor"])   ? "23" : $data_arr["hor"];
       $data_arr["min"] = ("__" == $data_arr["min"])   ? "59" : $data_arr["min"];
       $data_arr["seg"] = ("__" == $data_arr["seg"])   ? "59" : $data_arr["seg"];
       if ("__" == $data_arr["dia"])
       {
           $data_arr["dia"] = "31";
           if ($data_arr["mes"] == "04" || $data_arr["mes"] == "06" || $data_arr["mes"] == "09" || $data_arr["mes"] == "11")
           {
               $data_arr["dia"] = 30;
           }
           elseif ($data_arr["mes"] == "02")
           { 
                if  ($data_arr["ano"] % 4 == 0)
                {
                     $data_arr["dia"] = 29;
                }
                else 
                {
                     $data_arr["dia"] = 28;
                }
           }
       }
   }

   /**
    * @access  public
    * @param  string  $nm_data_hora  
    */
   function limpa_dt_hor_pesq(&$nm_data_hora)
   {
      $nm_data_hora = str_replace("Y", "", $nm_data_hora); 
      $nm_data_hora = str_replace("M", "", $nm_data_hora); 
      $nm_data_hora = str_replace("D", "", $nm_data_hora); 
      $nm_data_hora = str_replace("H", "", $nm_data_hora); 
      $nm_data_hora = str_replace("I", "", $nm_data_hora); 
      $nm_data_hora = str_replace("S", "", $nm_data_hora); 
      $tmp_pos = strpos($nm_data_hora, "--");
      if ($tmp_pos !== FALSE)
      {
          $nm_data_hora = str_replace("--", "-", $nm_data_hora); 
      }
      $tmp_pos = strpos($nm_data_hora, "::");
      if ($tmp_pos !== FALSE)
      {
          $nm_data_hora = str_replace("::", ":", $nm_data_hora); 
      }
   }

   /**
    * @access  public
    */
   function retorna_pesq()
   {
      global $nm_apl_dependente;
   $NM_retorno = "./";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML>
<HEAD>
 <TITLE>Informe Reclamaciones Historial</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
</HEAD>
<BODY class="scGridPage">
<FORM style="display:none;" name="form_ok" method="POST" action="<?php echo $NM_retorno; ?>" target="_self">
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="pesq"> 
</FORM>
<SCRIPT type="text/javascript">
 document.form_ok.submit();
</SCRIPT>
</BODY>
</HTML>
<?php
}

   /**
    * @access  public
    */
   function monta_html_ini()
   {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Informe Reclamaciones Historial</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
 <link rel="stylesheet" href="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/thickbox.css" type="text/css" media="screen" />
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_filter.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_filter<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_error.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_error<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Str_btn_filter_css ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_form.css" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_filter ?>_form<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
 <link rel="stylesheet" type="text/css" href="<?php echo $this->Ini->path_link ?>Informe_reclamacion_historial/Informe_reclamacion_historial_fil_<?php echo strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) ?>.css" />
</HEAD>
<BODY class="scFilterPage">
<?php echo $this->Ini->Ajax_result_set ?>
<SCRIPT type="text/javascript" src="<?php echo $this->Ini->path_js . "/browserSniffer.js" ?>"></SCRIPT>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery/js/jquery.js"></script>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod; ?>/third/jquery/js/jquery-ui.js"></script>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/malsup-blockui/jquery.blockUI.js"></script>
 <script type="text/javascript" src="../_lib/lib/js/jquery.scInput.js"></script>
 <link rel="stylesheet" href="<?php echo $this->Ini->path_prod ?>/third/jquery/css/smoothness/jquery-ui.css" type="text/css" media="screen" />
 <script type="text/javascript">var sc_pathToTB = '<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/';</script>
 <script type="text/javascript" src="<?php echo $this->Ini->path_prod ?>/third/jquery_plugin/thickbox/thickbox-compressed.js"></script>
 <script type="text/javascript" src="Informe_reclamacion_historial_ajax_search.js"></script>
 <script type="text/javascript" src="Informe_reclamacion_historial_ajax.js"></script>
 <script type="text/javascript">
   var sc_ajaxBg = '<?php echo $this->Ini->Color_bg_ajax ?>';
   var sc_ajaxBordC = '<?php echo $this->Ini->Border_c_ajax ?>';
   var sc_ajaxBordS = '<?php echo $this->Ini->Border_s_ajax ?>';
   var sc_ajaxBordW = '<?php echo $this->Ini->Border_w_ajax ?>';
 </script>
<?php
$Cod_Btn = nmButtonOutput($this->arr_buttons, "berrm_clse", "nmAjaxHideDebug()", "nmAjaxHideDebug()", "", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<div id="id_debug_window" style="display: none; position: absolute; left: 50px; top: 50px"><table class="scFormMessageTable">
<tr><td class="scFormMessageTitle"><?php echo $Cod_Btn ?>&nbsp;&nbsp;Output</td></tr>
<tr><td class="scFormMessageMessage" style="padding: 0px; vertical-align: top"><div style="padding: 2px; height: 200px; width: 350px; overflow: auto" id="id_debug_text"></div></td></tr>
</table></div>
 <SCRIPT type="text/javascript">

<?php
if (is_file($this->Ini->root . $this->Ini->path_link . "_lib/js/tab_erro_" . $this->Ini->str_lang . ".js"))
{
    $Tb_err_js = file($this->Ini->root . $this->Ini->path_link . "_lib/js/tab_erro_" . $this->Ini->str_lang . ".js");
    foreach ($Tb_err_js as $Lines)
    {
        if (NM_is_utf8($Lines) && $_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Lines = sc_convert_encoding($Lines, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        echo $Lines;
    }
}
 if (NM_is_utf8($Lines) && $_SESSION['scriptcase']['charset'] != "UTF-8")
 {
    $Msg_Inval = sc_convert_encoding("Inv�lido", $_SESSION['scriptcase']['charset'], "UTF-8");
 }
?>
var SC_crit_inv = "<?php echo $Msg_Inval ?>";
var nmdg_Form = "F1";

 $(function() {

   SC_carga_evt_jquery();
   $('input:text.sc-js-input').listen();
 });
var NM_index = 0;
var NM_hidden = new Array();
var NM_IE = (navigator.userAgent.indexOf('MSIE') > -1) ? 1 : 0;
function NM_hitTest(o, l)
{
    function getOffset(o){
        for(var r = {l: o.offsetLeft, t: o.offsetTop, r: o.offsetWidth, b: o.offsetHeight};
            o = o.offsetParent; r.l += o.offsetLeft, r.t += o.offsetTop);
        return r.r += r.l, r.b += r.t, r;
    }
    for(var b, s, r = [], a = getOffset(o), j = isNaN(l.length), i = (j ? l = [l] : l).length; i;
        b = getOffset(l[--i]), (a.l == b.l || (a.l > b.l ? a.l <= b.r : b.l <= a.r))
        && (a.t == b.t || (a.t > b.t ? a.t <= b.b : b.t <= a.b)) && (r[r.length] = l[i]));
    return j ? !!r.length : r;
}
var tem_obj = false;
function NM_show_menu(nn)
{
    if (!NM_IE)
    {
         return;
    }
    x = document.getElementById(nn);
    x.style.display = "block";
    obj_sel = document.body;
    tem_obj = true;
    x.ieFix = NM_hitTest(x, obj_sel.getElementsByTagName("select"));
    for (i = 0; i <  x.ieFix.length; i++)
    {
      if (x.ieFix[i].style.visibility != "hidden")
      {
          x.ieFix[i].style.visibility = "hidden";
          NM_hidden[NM_index] = x.ieFix[i];
          NM_index++;
      }
    }
}
function NM_hide_menu()
{
    if (!NM_IE)
    {
         return;
    }
    obj_del = document.body;
    if (tem_obj && obj_del == obj_sel)
    {
        for(var i = NM_hidden.length; i; NM_hidden[--i].style.visibility = "visible");
    }
    NM_index = 0;
    NM_hidden = new Array();
}
 function nm_campos_between(nm_campo, nm_cond, nm_nome_obj)
 {
  if (nm_cond.value == "bw")
  {
   nm_campo.style.display = "";
  }
  else
  {
    if (nm_campo)
    {
      nm_campo.style.display = "none";
    }
  }
  if (document.getElementById('id_hide_' + nm_nome_obj))
  {
      if (nm_cond.value == "nu" || nm_cond.value == "nn" || nm_cond.value == "ep" || nm_cond.value == "ne")
      {
          document.getElementById('id_hide_' + nm_nome_obj).style.display = 'none';
      }
      else
      {
          document.getElementById('id_hide_' + nm_nome_obj).style.display = '';
      }
  }
 }
 function nm_save_form(pos)
 {
  if (pos == 'top' && document.F1.nmgp_save_name_top.value == '')
  {
      return;
  }
  if (pos == 'bot' && document.F1.nmgp_save_name_bot.value == '')
  {
      return;
  }
  var str_out = "";
  str_out += 'SC_id_paciente_cond#NMF#' + search_get_sel_txt('SC_id_paciente_cond') + '@NMF@';
  str_out += 'SC_id_paciente#NMF#' + search_get_text('SC_id_paciente') + '@NMF@';
  str_out += 'id_ac_id_paciente#NMF#' + search_get_text('id_ac_id_paciente') + '@NMF@';
  str_out += 'SC_id_paciente_input_2#NMF#' + search_get_text('SC_id_paciente_input_2') + '@NMF@';
  str_out += 'SC_logro_comunicacion_gestion_cond#NMF#' + search_get_sel_txt('SC_logro_comunicacion_gestion_cond') + '@NMF@';
  str_out += 'SC_logro_comunicacion_gestion#NMF#' + search_get_text('SC_logro_comunicacion_gestion') + '@NMF@';
  str_out += 'id_ac_logro_comunicacion_gestion#NMF#' + search_get_text('id_ac_logro_comunicacion_gestion') + '@NMF@';
  str_out += 'SC_fecha_comunicacion_cond#NMF#' + search_get_sel_txt('SC_fecha_comunicacion_cond') + '@NMF@';
  str_out += 'SC_fecha_comunicacion#NMF#' + search_get_text('SC_fecha_comunicacion') + '@NMF@';
  str_out += 'id_ac_fecha_comunicacion#NMF#' + search_get_text('id_ac_fecha_comunicacion') + '@NMF@';
  str_out += 'SC_autor_gestion_cond#NMF#' + search_get_sel_txt('SC_autor_gestion_cond') + '@NMF@';
  str_out += 'SC_autor_gestion#NMF#' + search_get_text('SC_autor_gestion') + '@NMF@';
  str_out += 'id_ac_autor_gestion#NMF#' + search_get_text('id_ac_autor_gestion') + '@NMF@';
  str_out += 'SC_departamento_paciente_cond#NMF#' + search_get_sel_txt('SC_departamento_paciente_cond') + '@NMF@';
  str_out += 'SC_departamento_paciente#NMF#' + search_get_text('SC_departamento_paciente') + '@NMF@';
  str_out += 'id_ac_departamento_paciente#NMF#' + search_get_text('id_ac_departamento_paciente') + '@NMF@';
  str_out += 'SC_ciudad_paciente_cond#NMF#' + search_get_sel_txt('SC_ciudad_paciente_cond') + '@NMF@';
  str_out += 'SC_ciudad_paciente#NMF#' + search_get_text('SC_ciudad_paciente') + '@NMF@';
  str_out += 'id_ac_ciudad_paciente#NMF#' + search_get_text('id_ac_ciudad_paciente') + '@NMF@';
  str_out += 'SC_estado_paciente_cond#NMF#' + search_get_sel_txt('SC_estado_paciente_cond') + '@NMF@';
  str_out += 'SC_estado_paciente#NMF#' + search_get_text('SC_estado_paciente') + '@NMF@';
  str_out += 'id_ac_estado_paciente#NMF#' + search_get_text('id_ac_estado_paciente') + '@NMF@';
  str_out += 'SC_status_paciente_cond#NMF#' + search_get_sel_txt('SC_status_paciente_cond') + '@NMF@';
  str_out += 'SC_status_paciente#NMF#' + search_get_text('SC_status_paciente') + '@NMF@';
  str_out += 'id_ac_status_paciente#NMF#' + search_get_text('id_ac_status_paciente') + '@NMF@';
  str_out += 'SC_fecha_activacion_paciente_cond#NMF#' + search_get_sel_txt('SC_fecha_activacion_paciente_cond') + '@NMF@';
  str_out += 'SC_fecha_activacion_paciente#NMF#' + search_get_text('SC_fecha_activacion_paciente') + '@NMF@';
  str_out += 'id_ac_fecha_activacion_paciente#NMF#' + search_get_text('id_ac_fecha_activacion_paciente') + '@NMF@';
  str_out += 'SC_codigo_xofigo_cond#NMF#' + search_get_sel_txt('SC_codigo_xofigo_cond') + '@NMF@';
  str_out += 'SC_codigo_xofigo#NMF#' + search_get_text('SC_codigo_xofigo') + '@NMF@';
  str_out += 'id_ac_codigo_xofigo#NMF#' + search_get_text('id_ac_codigo_xofigo') + '@NMF@';
  str_out += 'SC_codigo_xofigo_input_2#NMF#' + search_get_text('SC_codigo_xofigo_input_2') + '@NMF@';
  str_out += 'SC_producto_tratamiento_cond#NMF#' + search_get_sel_txt('SC_producto_tratamiento_cond') + '@NMF@';
  str_out += 'SC_producto_tratamiento#NMF#' + search_get_text('SC_producto_tratamiento') + '@NMF@';
  str_out += 'id_ac_producto_tratamiento#NMF#' + search_get_text('id_ac_producto_tratamiento') + '@NMF@';
  str_out += 'SC_nombre_referencia_cond#NMF#' + search_get_sel_txt('SC_nombre_referencia_cond') + '@NMF@';
  str_out += 'SC_nombre_referencia#NMF#' + search_get_text('SC_nombre_referencia') + '@NMF@';
  str_out += 'id_ac_nombre_referencia#NMF#' + search_get_text('id_ac_nombre_referencia') + '@NMF@';
  str_out += 'SC_dosis_tratamiento_cond#NMF#' + search_get_sel_txt('SC_dosis_tratamiento_cond') + '@NMF@';
  str_out += 'SC_dosis_tratamiento#NMF#' + search_get_text('SC_dosis_tratamiento') + '@NMF@';
  str_out += 'id_ac_dosis_tratamiento#NMF#' + search_get_text('id_ac_dosis_tratamiento') + '@NMF@';
  str_out += 'SC_numero_cajas_cond#NMF#' + search_get_sel_txt('SC_numero_cajas_cond') + '@NMF@';
  str_out += 'SC_numero_cajas#NMF#' + search_get_text('SC_numero_cajas') + '@NMF@';
  str_out += 'id_ac_numero_cajas#NMF#' + search_get_text('id_ac_numero_cajas') + '@NMF@';
  str_out += 'SC_asegurador_tratamiento_cond#NMF#' + search_get_sel_txt('SC_asegurador_tratamiento_cond') + '@NMF@';
  str_out += 'SC_asegurador_tratamiento#NMF#' + search_get_text('SC_asegurador_tratamiento') + '@NMF@';
  str_out += 'id_ac_asegurador_tratamiento#NMF#' + search_get_text('id_ac_asegurador_tratamiento') + '@NMF@';
  str_out += 'SC_operador_logistico_tratamiento_cond#NMF#' + search_get_sel_txt('SC_operador_logistico_tratamiento_cond') + '@NMF@';
  str_out += 'SC_operador_logistico_tratamiento#NMF#' + search_get_text('SC_operador_logistico_tratamiento') + '@NMF@';
  str_out += 'id_ac_operador_logistico_tratamiento#NMF#' + search_get_text('id_ac_operador_logistico_tratamiento') + '@NMF@';
  str_out += 'SC_punto_entrega_cond#NMF#' + search_get_sel_txt('SC_punto_entrega_cond') + '@NMF@';
  str_out += 'SC_punto_entrega#NMF#' + search_get_text('SC_punto_entrega') + '@NMF@';
  str_out += 'id_ac_punto_entrega#NMF#' + search_get_text('id_ac_punto_entrega') + '@NMF@';
  str_out += 'SC_reclamo_gestion_cond#NMF#' + search_get_sel_txt('SC_reclamo_gestion_cond') + '@NMF@';
  str_out += 'SC_reclamo_gestion#NMF#' + search_get_text('SC_reclamo_gestion') + '@NMF@';
  str_out += 'id_ac_reclamo_gestion#NMF#' + search_get_text('id_ac_reclamo_gestion') + '@NMF@';
  str_out += 'SC_fecha_reclamacion_gestion_cond#NMF#' + search_get_sel_txt('SC_fecha_reclamacion_gestion_cond') + '@NMF@';
  str_out += 'SC_fecha_reclamacion_gestion#NMF#' + search_get_text('SC_fecha_reclamacion_gestion') + '@NMF@';
  str_out += 'id_ac_fecha_reclamacion_gestion#NMF#' + search_get_text('id_ac_fecha_reclamacion_gestion') + '@NMF@';
  str_out += 'SC_causa_no_reclamacion_gestion_cond#NMF#' + search_get_sel_txt('SC_causa_no_reclamacion_gestion_cond') + '@NMF@';
  str_out += 'SC_causa_no_reclamacion_gestion#NMF#' + search_get_text('SC_causa_no_reclamacion_gestion') + '@NMF@';
  str_out += 'id_ac_causa_no_reclamacion_gestion#NMF#' + search_get_text('id_ac_causa_no_reclamacion_gestion') + '@NMF@';
  str_out += 'SC_id_historial_reclamacion_cond#NMF#' + search_get_sel_txt('SC_id_historial_reclamacion_cond') + '@NMF@';
  str_out += 'SC_id_historial_reclamacion#NMF#' + search_get_text('SC_id_historial_reclamacion') + '@NMF@';
  str_out += 'id_ac_id_historial_reclamacion#NMF#' + search_get_text('id_ac_id_historial_reclamacion') + '@NMF@';
  str_out += 'SC_id_historial_reclamacion_input_2#NMF#' + search_get_text('SC_id_historial_reclamacion_input_2') + '@NMF@';
  str_out += 'SC_anio_historial_reclamacion_cond#NMF#' + search_get_sel_txt('SC_anio_historial_reclamacion_cond') + '@NMF@';
  str_out += 'SC_anio_historial_reclamacion#NMF#' + search_get_text('SC_anio_historial_reclamacion') + '@NMF@';
  str_out += 'id_ac_anio_historial_reclamacion#NMF#' + search_get_text('id_ac_anio_historial_reclamacion') + '@NMF@';
  str_out += 'SC_anio_historial_reclamacion_input_2#NMF#' + search_get_text('SC_anio_historial_reclamacion_input_2') + '@NMF@';
  str_out += 'NM_operador#NMF#' + search_get_text('SC_NM_operador');
  str_out  = str_out.replace(/[+]/g, "__NM_PLUS__");
  var save_name = search_get_text('SC_nmgp_save_name_' + pos);
  var save_opt  = search_get_sel_txt('SC_nmgp_save_option_' + pos);
  ajax_save_filter(save_name, save_opt, str_out, pos);
 }
 function nm_submit_filter(obj_sel, pos)
 {
  index   = obj_sel.selectedIndex;
  if (obj_sel.options[index].value == "") 
  {
      return false;
  }
  ajax_select_filter(obj_sel.options[index].value);
 }
 function nm_submit_filter_del(pos)
 {
  if (pos == 'top')
  {
      obj_sel = document.F1.elements['NM_filters_del_top'];
  }
  if (pos == 'bot')
  {
      obj_sel = document.F1.elements['NM_filters_del_bot'];
  }
  index   = obj_sel.selectedIndex;
  if (index == -1 || obj_sel.options[index].value == "") 
  {
      return false;
  }
  parm = obj_sel.options[index].value;
  ajax_delete_filter(parm);
 }
 function search_get_select(obj_id)
 {
    var index = document.getElementById(obj_id).selectedIndex;
    if (index != -1) {
        return document.getElementById(obj_id).options[index].value;
    }
    else {
        return '';
    }
 }
 function search_get_selmult(obj_id)
 {
    var obj = document.getElementById(obj_id);
    var val = "_NM_array_";
    for (iSelect = 0; iSelect < obj.length; iSelect++)
    {
        if (obj[iSelect].selected)
        {
            val += "#NMARR#" + obj[iSelect].value;
        }
    }
    return val;
 }
 function search_get_Dselelect(obj_id)
 {
    var obj = document.getElementById(obj_id);
    var val = "_NM_array_";
    for (iSelect = 0; iSelect < obj.length; iSelect++)
    {
         val += "#NMARR#" + obj[iSelect].value;
    }
    return val;
 }
 function search_get_radio(obj_id)
 {
    var val  = "";
    if (document.getElementById(obj_id)) {
       var Nobj = document.getElementById(obj_id).name;
       var obj  = document.getElementsByName(Nobj);
       for (iRadio = 0; iRadio < obj.length; iRadio++) {
           if (obj[iRadio].checked) {
               val = obj[iRadio].value;
           }
       }
    }
    return val;
 }
 function search_get_checkbox(obj_id)
 {
    var val  = "_NM_array_";
    if (document.getElementById(obj_id)) {
       var Nobj = document.getElementById(obj_id).name;
       var obj  = document.getElementsByName(Nobj);
       if (!obj.length) {
           if (obj.checked) {
               val += "#NMARR#" + obj.value;
           }
       }
       else {
           for (iCheck = 0; iCheck < obj.length; iCheck++) {
               if (obj[iCheck].checked) {
                   val += "#NMARR#" + obj[iCheck].value;
               }
           }
       }
    }
    return val;
 }
 function search_get_text(obj_id)
 {
    var obj = document.getElementById(obj_id);
    return (obj) ? obj.value : '';
 }
 function search_get_sel_txt(obj_id)
 {
    var val = "";
    obj_part  = document.getElementById(obj_id);
    if (obj_part && obj_part.type.substr(0, 6) == 'select')
    {
        val = search_get_select(obj_id);
    }
    else
    {
        val = (obj_part) ? obj_part.value : '';
    }
    return val;
 }
 function search_get_html(obj_id)
 {
    var obj = document.getElementById(obj_id);
    return obj.innerHTML;
 }
function nm_open_popup(parms)
{
    NovaJanela = window.open (parms, '', 'resizable, scrollbars');
}
 </SCRIPT>
<script type="text/javascript">
 $(function() {
   $("#id_ac_id_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_id_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_id_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_id_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_logro_comunicacion_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_logro_comunicacion_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_logro_comunicacion_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_logro_comunicacion_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_fecha_comunicacion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_fecha_comunicacion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_fecha_comunicacion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_fecha_comunicacion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_autor_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_autor_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_autor_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_autor_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_departamento_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_departamento_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_departamento_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_departamento_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_ciudad_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_ciudad_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_ciudad_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_ciudad_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_estado_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_estado_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_estado_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_estado_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_status_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_status_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_status_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_status_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_fecha_activacion_paciente").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_fecha_activacion_paciente",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_fecha_activacion_paciente").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_fecha_activacion_paciente").val( $(this).val() );
       }
     }
   });
   $("#id_ac_codigo_xofigo").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_codigo_xofigo",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_codigo_xofigo").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_codigo_xofigo").val( $(this).val() );
       }
     }
   });
   $("#id_ac_producto_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_producto_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_producto_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_producto_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_nombre_referencia").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_nombre_referencia",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_nombre_referencia").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_nombre_referencia").val( $(this).val() );
       }
     }
   });
   $("#id_ac_dosis_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_dosis_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_dosis_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_dosis_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_numero_cajas").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_numero_cajas",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_numero_cajas").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_numero_cajas").val( $(this).val() );
       }
     }
   });
   $("#id_ac_asegurador_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_asegurador_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_asegurador_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_asegurador_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_operador_logistico_tratamiento").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_operador_logistico_tratamiento",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_operador_logistico_tratamiento").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_operador_logistico_tratamiento").val( $(this).val() );
       }
     }
   });
   $("#id_ac_punto_entrega").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_punto_entrega",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_punto_entrega").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_punto_entrega").val( $(this).val() );
       }
     }
   });
   $("#id_ac_reclamo_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_reclamo_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_reclamo_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_reclamo_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_fecha_reclamacion_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_fecha_reclamacion_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_fecha_reclamacion_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_fecha_reclamacion_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_causa_no_reclamacion_gestion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_causa_no_reclamacion_gestion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_causa_no_reclamacion_gestion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_causa_no_reclamacion_gestion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_id_historial_reclamacion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_id_historial_reclamacion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_id_historial_reclamacion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_id_historial_reclamacion").val( $(this).val() );
       }
     }
   });
   $("#id_ac_anio_historial_reclamacion").autocomplete({
     source: function (request, response) {
     $.ajax({
       url: "index.php",
       dataType: "json",
       data: {
          q: request.term,
          nmgp_opcao: "ajax_autocomp",
          nmgp_parms: "NM_ajax_opcao?#?autocomp_anio_historial_reclamacion",
          max_itens: "10",
          cod_desc: "N",
          script_case_init: <?php echo $this->Ini->sc_page ?>
        },
       success: function (data) {
         response(data);
       }
      });
    },
     select: function (event, ui) {
       $("#SC_anio_historial_reclamacion").val(ui.item.value);
       $(this).val(ui.item.label);
       event.preventDefault();
     },
     change: function (event, ui) {
       if (null == ui.item) {
          $("#SC_anio_historial_reclamacion").val( $(this).val() );
       }
     }
   });
 });
</script>
 <FORM name="F1" action="./" method="post" target="_self"> 
 <INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
 <INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
 <INPUT type="hidden" name="nmgp_opcao" value="busca"> 
 <div id="idJSSpecChar" style="display:none;"></div>
 <div id="id_div_process" style="display: none; position: absolute"><table class="scFilterTable"><tr><td class="scFilterLabelOdd"><?php echo $this->Ini->Nm_lang['lang_othr_prcs']; ?>...</td></tr></table></div>
 <div id="id_fatal_error" class="scFilterFieldOdd" style="display:none; position: absolute"></div>
<TABLE id="main_table" align="center" valign="top" >
<tr>
<td>
<div class="scFilterBorder">
  <div id="id_div_process_block" style="display: none; margin: 10px; whitespace: nowrap"><span class="scFormProcess"><img border="0" src="<?php echo $this->Ini->path_icones ?>/scriptcase__NM__ajax_load.gif" align="absmiddle" />&nbsp;<?php echo $this->Ini->Nm_lang['lang_othr_prcs'] ?>...</span></div>
<table cellspacing=0 cellpadding=0 width='100%'>
<?php
   }

   /**
    * @access  public
    * @global  string  $bprocessa  
    */
   /**
    * @access  public
    */
   function monta_cabecalho()
   {
      $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
      $Lim   = strlen($Str_date);
      $Ult   = "";
      $Arr_D = array();
      for ($I = 0; $I < $Lim; $I++)
      {
          $Char = substr($Str_date, $I, 1);
          if ($Char != $Ult)
          {
              $Arr_D[] = $Char;
          }
          $Ult = $Char;
      }
      $Prim = true;
      $Str  = "";
      foreach ($Arr_D as $Cada_d)
      {
          $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
          $Str .= $Cada_d;
          $Prim = false;
      }
      $Str = str_replace("a", "Y", $Str);
      $Str = str_replace("y", "Y", $Str);
      $nm_data_fixa = date($Str); 
?>
 <TR align="center">
  <TD class="scFilterTableTd">
<style>
#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 
#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}
</style>

<div style="width: 100%">
 <div class="scFilterHeader" style="height:11px; display: block; border-width:0px; "></div>
 <div style="height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block">
 	<table style="width:100%; border-collapse:collapse; padding:0;">
    	<tr>
        	<td id="lin1_col1" class="scFilterHeaderFont"><span>Informe Reclamaciones Historial</span></td>
            <td id="lin1_col2" class="scFilterHeaderFont"><span><?php echo $nm_data_fixa; ?></span></td>
        </tr>
    </table>		 
 </div>
</div>
  </TD>
 </TR>
<?php
   }

   /**
    * @access  public
    * @global  string  $nm_url_saida  $this->Ini->Nm_lang['pesq_global_nm_url_saida']
    * @global  integer  $nm_apl_dependente  $this->Ini->Nm_lang['pesq_global_nm_apl_dependente']
    * @global  string  $nmgp_parms  
    * @global  string  $bprocessa  $this->Ini->Nm_lang['pesq_global_bprocessa']
    */
   function monta_form()
   {
      global 
             $id_paciente_cond, $id_paciente, $id_paciente_input_2, $id_paciente_autocomp,
             $logro_comunicacion_gestion_cond, $logro_comunicacion_gestion, $logro_comunicacion_gestion_autocomp,
             $fecha_comunicacion_cond, $fecha_comunicacion, $fecha_comunicacion_autocomp,
             $autor_gestion_cond, $autor_gestion, $autor_gestion_autocomp,
             $departamento_paciente_cond, $departamento_paciente, $departamento_paciente_autocomp,
             $ciudad_paciente_cond, $ciudad_paciente, $ciudad_paciente_autocomp,
             $estado_paciente_cond, $estado_paciente, $estado_paciente_autocomp,
             $status_paciente_cond, $status_paciente, $status_paciente_autocomp,
             $fecha_activacion_paciente_cond, $fecha_activacion_paciente, $fecha_activacion_paciente_autocomp,
             $codigo_xofigo_cond, $codigo_xofigo, $codigo_xofigo_input_2, $codigo_xofigo_autocomp,
             $producto_tratamiento_cond, $producto_tratamiento, $producto_tratamiento_autocomp,
             $nombre_referencia_cond, $nombre_referencia, $nombre_referencia_autocomp,
             $dosis_tratamiento_cond, $dosis_tratamiento, $dosis_tratamiento_autocomp,
             $numero_cajas_cond, $numero_cajas, $numero_cajas_autocomp,
             $asegurador_tratamiento_cond, $asegurador_tratamiento, $asegurador_tratamiento_autocomp,
             $operador_logistico_tratamiento_cond, $operador_logistico_tratamiento, $operador_logistico_tratamiento_autocomp,
             $punto_entrega_cond, $punto_entrega, $punto_entrega_autocomp,
             $reclamo_gestion_cond, $reclamo_gestion, $reclamo_gestion_autocomp,
             $fecha_reclamacion_gestion_cond, $fecha_reclamacion_gestion, $fecha_reclamacion_gestion_autocomp,
             $causa_no_reclamacion_gestion_cond, $causa_no_reclamacion_gestion, $causa_no_reclamacion_gestion_autocomp,
             $id_historial_reclamacion_cond, $id_historial_reclamacion, $id_historial_reclamacion_input_2, $id_historial_reclamacion_autocomp,
             $anio_historial_reclamacion_cond, $anio_historial_reclamacion, $anio_historial_reclamacion_input_2, $anio_historial_reclamacion_autocomp,
             $nm_url_saida, $nm_apl_dependente, $nmgp_parms, $bprocessa, $nmgp_save_name, $NM_operador, $NM_filters, $nmgp_save_option, $NM_filters_del, $Script_BI;
      $Script_BI = "";
      $this->nmgp_botoes['clear'] = "on";
      $this->nmgp_botoes['save'] = "on";
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['btn_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
          {
              $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['scriptcase']['sc_aba_iframe']))
      {
          foreach ($_SESSION['scriptcase']['sc_aba_iframe'] as $aba => $apls_aba)
          {
              if (in_array("Informe_reclamacion_historial", $apls_aba))
              {
                  $this->aba_iframe = true;
                  break;
              }
          }
      }
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['iframe_menu'] && (!isset($_SESSION['scriptcase']['menu_mobile']) || empty($_SESSION['scriptcase']['menu_mobile'])))
      {
          $this->aba_iframe = true;
      }
      $nmgp_tab_label = "";
      $delimitador = "##@@";
      if (!empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']) && $bprocessa != "recarga" && $bprocessa != "save_form" && $bprocessa != "filter_save" && $bprocessa != "filter_delete")
      { 
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'] = NM_conv_charset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'], $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $id_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_paciente']; 
          $id_paciente_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_paciente_input_2']; 
          $id_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_paciente_cond']; 
          $logro_comunicacion_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['logro_comunicacion_gestion']; 
          $logro_comunicacion_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['logro_comunicacion_gestion_cond']; 
          $fecha_comunicacion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_comunicacion']; 
          $fecha_comunicacion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_comunicacion_cond']; 
          $autor_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['autor_gestion']; 
          $autor_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['autor_gestion_cond']; 
          $departamento_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['departamento_paciente']; 
          $departamento_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['departamento_paciente_cond']; 
          $ciudad_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['ciudad_paciente']; 
          $ciudad_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['ciudad_paciente_cond']; 
          $estado_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['estado_paciente']; 
          $estado_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['estado_paciente_cond']; 
          $status_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['status_paciente']; 
          $status_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['status_paciente_cond']; 
          $fecha_activacion_paciente = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_activacion_paciente']; 
          $fecha_activacion_paciente_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_activacion_paciente_cond']; 
          $codigo_xofigo = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['codigo_xofigo']; 
          $codigo_xofigo_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['codigo_xofigo_input_2']; 
          $codigo_xofigo_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['codigo_xofigo_cond']; 
          $producto_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['producto_tratamiento']; 
          $producto_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['producto_tratamiento_cond']; 
          $nombre_referencia = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['nombre_referencia']; 
          $nombre_referencia_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['nombre_referencia_cond']; 
          $dosis_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['dosis_tratamiento']; 
          $dosis_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['dosis_tratamiento_cond']; 
          $numero_cajas = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['numero_cajas']; 
          $numero_cajas_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['numero_cajas_cond']; 
          $asegurador_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['asegurador_tratamiento']; 
          $asegurador_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['asegurador_tratamiento_cond']; 
          $operador_logistico_tratamiento = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['operador_logistico_tratamiento']; 
          $operador_logistico_tratamiento_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['operador_logistico_tratamiento_cond']; 
          $punto_entrega = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['punto_entrega']; 
          $punto_entrega_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['punto_entrega_cond']; 
          $reclamo_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['reclamo_gestion']; 
          $reclamo_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['reclamo_gestion_cond']; 
          $fecha_reclamacion_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_reclamacion_gestion']; 
          $fecha_reclamacion_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_reclamacion_gestion_cond']; 
          $causa_no_reclamacion_gestion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['causa_no_reclamacion_gestion']; 
          $causa_no_reclamacion_gestion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['causa_no_reclamacion_gestion_cond']; 
          $id_historial_reclamacion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_historial_reclamacion']; 
          $id_historial_reclamacion_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_historial_reclamacion_input_2']; 
          $id_historial_reclamacion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_historial_reclamacion_cond']; 
          $anio_historial_reclamacion = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['anio_historial_reclamacion']; 
          $anio_historial_reclamacion_input_2 = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['anio_historial_reclamacion_input_2']; 
          $anio_historial_reclamacion_cond = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['anio_historial_reclamacion_cond']; 
          $this->NM_operador = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['NM_operador']; 
      } 
      if (!isset($id_paciente_cond) || empty($id_paciente_cond))
      {
         $id_paciente_cond = "gt";
      }
      if (!isset($logro_comunicacion_gestion_cond) || empty($logro_comunicacion_gestion_cond))
      {
         $logro_comunicacion_gestion_cond = "qp";
      }
      if (!isset($fecha_comunicacion_cond) || empty($fecha_comunicacion_cond))
      {
         $fecha_comunicacion_cond = "qp";
      }
      if (!isset($autor_gestion_cond) || empty($autor_gestion_cond))
      {
         $autor_gestion_cond = "qp";
      }
      if (!isset($departamento_paciente_cond) || empty($departamento_paciente_cond))
      {
         $departamento_paciente_cond = "qp";
      }
      if (!isset($ciudad_paciente_cond) || empty($ciudad_paciente_cond))
      {
         $ciudad_paciente_cond = "qp";
      }
      if (!isset($estado_paciente_cond) || empty($estado_paciente_cond))
      {
         $estado_paciente_cond = "qp";
      }
      if (!isset($status_paciente_cond) || empty($status_paciente_cond))
      {
         $status_paciente_cond = "qp";
      }
      if (!isset($fecha_activacion_paciente_cond) || empty($fecha_activacion_paciente_cond))
      {
         $fecha_activacion_paciente_cond = "qp";
      }
      if (!isset($codigo_xofigo_cond) || empty($codigo_xofigo_cond))
      {
         $codigo_xofigo_cond = "gt";
      }
      if (!isset($producto_tratamiento_cond) || empty($producto_tratamiento_cond))
      {
         $producto_tratamiento_cond = "qp";
      }
      if (!isset($nombre_referencia_cond) || empty($nombre_referencia_cond))
      {
         $nombre_referencia_cond = "qp";
      }
      if (!isset($dosis_tratamiento_cond) || empty($dosis_tratamiento_cond))
      {
         $dosis_tratamiento_cond = "qp";
      }
      if (!isset($numero_cajas_cond) || empty($numero_cajas_cond))
      {
         $numero_cajas_cond = "qp";
      }
      if (!isset($asegurador_tratamiento_cond) || empty($asegurador_tratamiento_cond))
      {
         $asegurador_tratamiento_cond = "qp";
      }
      if (!isset($operador_logistico_tratamiento_cond) || empty($operador_logistico_tratamiento_cond))
      {
         $operador_logistico_tratamiento_cond = "qp";
      }
      if (!isset($punto_entrega_cond) || empty($punto_entrega_cond))
      {
         $punto_entrega_cond = "qp";
      }
      if (!isset($reclamo_gestion_cond) || empty($reclamo_gestion_cond))
      {
         $reclamo_gestion_cond = "qp";
      }
      if (!isset($fecha_reclamacion_gestion_cond) || empty($fecha_reclamacion_gestion_cond))
      {
         $fecha_reclamacion_gestion_cond = "qp";
      }
      if (!isset($causa_no_reclamacion_gestion_cond) || empty($causa_no_reclamacion_gestion_cond))
      {
         $causa_no_reclamacion_gestion_cond = "qp";
      }
      if (!isset($id_historial_reclamacion_cond) || empty($id_historial_reclamacion_cond))
      {
         $id_historial_reclamacion_cond = "gt";
      }
      if (!isset($anio_historial_reclamacion_cond) || empty($anio_historial_reclamacion_cond))
      {
         $anio_historial_reclamacion_cond = "gt";
      }
      $display_aberto  = "style=display:";
      $display_fechado = "style=display:none";
      $opc_hide_input = array("nu","nn","ep","ne");
      $str_hide_id_paciente = (in_array($id_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_logro_comunicacion_gestion = (in_array($logro_comunicacion_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_fecha_comunicacion = (in_array($fecha_comunicacion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_autor_gestion = (in_array($autor_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_departamento_paciente = (in_array($departamento_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_ciudad_paciente = (in_array($ciudad_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_estado_paciente = (in_array($estado_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_status_paciente = (in_array($status_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_fecha_activacion_paciente = (in_array($fecha_activacion_paciente_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_codigo_xofigo = (in_array($codigo_xofigo_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_producto_tratamiento = (in_array($producto_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_nombre_referencia = (in_array($nombre_referencia_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_dosis_tratamiento = (in_array($dosis_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_numero_cajas = (in_array($numero_cajas_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_asegurador_tratamiento = (in_array($asegurador_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_operador_logistico_tratamiento = (in_array($operador_logistico_tratamiento_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_punto_entrega = (in_array($punto_entrega_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_reclamo_gestion = (in_array($reclamo_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_fecha_reclamacion_gestion = (in_array($fecha_reclamacion_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_causa_no_reclamacion_gestion = (in_array($causa_no_reclamacion_gestion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_id_historial_reclamacion = (in_array($id_historial_reclamacion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;
      $str_hide_anio_historial_reclamacion = (in_array($anio_historial_reclamacion_cond, $opc_hide_input)) ? $display_fechado : $display_aberto;

      $str_display_id_paciente = ('bw' == $id_paciente_cond) ? $display_aberto : $display_fechado;
      $str_display_codigo_xofigo = ('bw' == $codigo_xofigo_cond) ? $display_aberto : $display_fechado;
      $str_display_id_historial_reclamacion = ('bw' == $id_historial_reclamacion_cond) ? $display_aberto : $display_fechado;
      $str_display_anio_historial_reclamacion = ('bw' == $anio_historial_reclamacion_cond) ? $display_aberto : $display_fechado;

      if (!isset($id_paciente) || $id_paciente == "")
      {
          $id_paciente = "";
      }
      if (isset($id_paciente) && !empty($id_paciente))
      {
         $tmp_pos = strpos($id_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $id_paciente = substr($id_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($logro_comunicacion_gestion) || $logro_comunicacion_gestion == "")
      {
          $logro_comunicacion_gestion = "";
      }
      if (isset($logro_comunicacion_gestion) && !empty($logro_comunicacion_gestion))
      {
         $tmp_pos = strpos($logro_comunicacion_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $logro_comunicacion_gestion = substr($logro_comunicacion_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($fecha_comunicacion) || $fecha_comunicacion == "")
      {
          $fecha_comunicacion = "";
      }
      if (isset($fecha_comunicacion) && !empty($fecha_comunicacion))
      {
         $tmp_pos = strpos($fecha_comunicacion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $fecha_comunicacion = substr($fecha_comunicacion, 0, $tmp_pos);
         }
      }
      if (!isset($autor_gestion) || $autor_gestion == "")
      {
          $autor_gestion = "";
      }
      if (isset($autor_gestion) && !empty($autor_gestion))
      {
         $tmp_pos = strpos($autor_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $autor_gestion = substr($autor_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($departamento_paciente) || $departamento_paciente == "")
      {
          $departamento_paciente = "";
      }
      if (isset($departamento_paciente) && !empty($departamento_paciente))
      {
         $tmp_pos = strpos($departamento_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $departamento_paciente = substr($departamento_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($ciudad_paciente) || $ciudad_paciente == "")
      {
          $ciudad_paciente = "";
      }
      if (isset($ciudad_paciente) && !empty($ciudad_paciente))
      {
         $tmp_pos = strpos($ciudad_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $ciudad_paciente = substr($ciudad_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($estado_paciente) || $estado_paciente == "")
      {
          $estado_paciente = "";
      }
      if (isset($estado_paciente) && !empty($estado_paciente))
      {
         $tmp_pos = strpos($estado_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $estado_paciente = substr($estado_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($status_paciente) || $status_paciente == "")
      {
          $status_paciente = "";
      }
      if (isset($status_paciente) && !empty($status_paciente))
      {
         $tmp_pos = strpos($status_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $status_paciente = substr($status_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($fecha_activacion_paciente) || $fecha_activacion_paciente == "")
      {
          $fecha_activacion_paciente = "";
      }
      if (isset($fecha_activacion_paciente) && !empty($fecha_activacion_paciente))
      {
         $tmp_pos = strpos($fecha_activacion_paciente, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $fecha_activacion_paciente = substr($fecha_activacion_paciente, 0, $tmp_pos);
         }
      }
      if (!isset($codigo_xofigo) || $codigo_xofigo == "")
      {
          $codigo_xofigo = "";
      }
      if (isset($codigo_xofigo) && !empty($codigo_xofigo))
      {
         $tmp_pos = strpos($codigo_xofigo, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $codigo_xofigo = substr($codigo_xofigo, 0, $tmp_pos);
         }
      }
      if (!isset($producto_tratamiento) || $producto_tratamiento == "")
      {
          $producto_tratamiento = "";
      }
      if (isset($producto_tratamiento) && !empty($producto_tratamiento))
      {
         $tmp_pos = strpos($producto_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $producto_tratamiento = substr($producto_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($nombre_referencia) || $nombre_referencia == "")
      {
          $nombre_referencia = "";
      }
      if (isset($nombre_referencia) && !empty($nombre_referencia))
      {
         $tmp_pos = strpos($nombre_referencia, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $nombre_referencia = substr($nombre_referencia, 0, $tmp_pos);
         }
      }
      if (!isset($dosis_tratamiento) || $dosis_tratamiento == "")
      {
          $dosis_tratamiento = "";
      }
      if (isset($dosis_tratamiento) && !empty($dosis_tratamiento))
      {
         $tmp_pos = strpos($dosis_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $dosis_tratamiento = substr($dosis_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($numero_cajas) || $numero_cajas == "")
      {
          $numero_cajas = "";
      }
      if (isset($numero_cajas) && !empty($numero_cajas))
      {
         $tmp_pos = strpos($numero_cajas, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $numero_cajas = substr($numero_cajas, 0, $tmp_pos);
         }
      }
      if (!isset($asegurador_tratamiento) || $asegurador_tratamiento == "")
      {
          $asegurador_tratamiento = "";
      }
      if (isset($asegurador_tratamiento) && !empty($asegurador_tratamiento))
      {
         $tmp_pos = strpos($asegurador_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $asegurador_tratamiento = substr($asegurador_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($operador_logistico_tratamiento) || $operador_logistico_tratamiento == "")
      {
          $operador_logistico_tratamiento = "";
      }
      if (isset($operador_logistico_tratamiento) && !empty($operador_logistico_tratamiento))
      {
         $tmp_pos = strpos($operador_logistico_tratamiento, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $operador_logistico_tratamiento = substr($operador_logistico_tratamiento, 0, $tmp_pos);
         }
      }
      if (!isset($punto_entrega) || $punto_entrega == "")
      {
          $punto_entrega = "";
      }
      if (isset($punto_entrega) && !empty($punto_entrega))
      {
         $tmp_pos = strpos($punto_entrega, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $punto_entrega = substr($punto_entrega, 0, $tmp_pos);
         }
      }
      if (!isset($reclamo_gestion) || $reclamo_gestion == "")
      {
          $reclamo_gestion = "";
      }
      if (isset($reclamo_gestion) && !empty($reclamo_gestion))
      {
         $tmp_pos = strpos($reclamo_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $reclamo_gestion = substr($reclamo_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($fecha_reclamacion_gestion) || $fecha_reclamacion_gestion == "")
      {
          $fecha_reclamacion_gestion = "";
      }
      if (isset($fecha_reclamacion_gestion) && !empty($fecha_reclamacion_gestion))
      {
         $tmp_pos = strpos($fecha_reclamacion_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $fecha_reclamacion_gestion = substr($fecha_reclamacion_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($causa_no_reclamacion_gestion) || $causa_no_reclamacion_gestion == "")
      {
          $causa_no_reclamacion_gestion = "";
      }
      if (isset($causa_no_reclamacion_gestion) && !empty($causa_no_reclamacion_gestion))
      {
         $tmp_pos = strpos($causa_no_reclamacion_gestion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $causa_no_reclamacion_gestion = substr($causa_no_reclamacion_gestion, 0, $tmp_pos);
         }
      }
      if (!isset($id_historial_reclamacion) || $id_historial_reclamacion == "")
      {
          $id_historial_reclamacion = "";
      }
      if (isset($id_historial_reclamacion) && !empty($id_historial_reclamacion))
      {
         $tmp_pos = strpos($id_historial_reclamacion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $id_historial_reclamacion = substr($id_historial_reclamacion, 0, $tmp_pos);
         }
      }
      if (!isset($anio_historial_reclamacion) || $anio_historial_reclamacion == "")
      {
          $anio_historial_reclamacion = "";
      }
      if (isset($anio_historial_reclamacion) && !empty($anio_historial_reclamacion))
      {
         $tmp_pos = strpos($anio_historial_reclamacion, "##@@");
         if ($tmp_pos === false)
         { }
         else
         {
         $anio_historial_reclamacion = substr($anio_historial_reclamacion, 0, $tmp_pos);
         }
      }
?>
 <?php
     if ($_SESSION['scriptcase']['proc_mobile'])
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
<?php
   if (is_file("Informe_reclamacion_historial_help.txt"))
   {
      $Arq_WebHelp = file("Informe_reclamacion_historial_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_top" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_top').style.display = 'none'", "document.getElementById('Salvar_filters_top').style.display = 'none'", "Cancel_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_top" name="nmgp_save_name_top" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('top')", "nm_save_form('top')", "Save_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_top" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_top">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_top" name="NM_filters_del_top" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('top')", "nm_submit_filter_del('top')", "Exc_filtro_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
     else
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
<?php
   if (is_file("Informe_reclamacion_historial_help.txt"))
   {
      $Arq_WebHelp = file("Informe_reclamacion_historial_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_top" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_top').style.display = 'none'", "document.getElementById('Salvar_filters_top').style.display = 'none'", "Cancel_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_top" name="nmgp_save_name_top" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('top')", "nm_save_form('top')", "Save_frm_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_top" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_top">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_top" name="NM_filters_del_top" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('top')", "nm_submit_filter_del('top')", "Exc_filtro_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
 ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <TABLE style="padding: 0px; spacing: 0px; border-width: 0px;" width="100%" height="100%">
   <TR valign="top" >
  <TD width="100%" height="">
   <TABLE class="scFilterTable" id="hidden_bloco_0" valign="top" width="100%" style="height: 100%;">
   <tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_id_paciente_cond" name="id_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_id_paciente'), this, 'id_paciente')">
       <OPTION value="gt" <?php if ("gt" == $id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $id_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_id_paciente" <?php echo $str_hide_id_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE";
 $nmgp_tab_label .= "id_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $id_paciente_look = substr($this->Db->qstr($id_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($id_paciente))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ID_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_PACIENTE = $id_paciente_look"; 
      }
      else
      {
          $nm_comando = "select distinct ID_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_PACIENTE = $id_paciente_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$id_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$id_paciente];
      }
      else
      {
          $sAutocompValue = $id_paciente;
      }
?>
<INPUT  type="text" id="SC_id_paciente" name="id_paciente" value="<?php echo NM_encode_input($id_paciente) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_id_paciente" name="id_paciente_autocomp" size="19" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_id_paciente"  <?php echo $str_display_id_paciente; ?> class="scFilterFieldFontOdd">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_id_paciente_input_2" name="id_paciente_input_2" value="<?php echo NM_encode_input($id_paciente_input_2) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectOdd">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['logro_comunicacion_gestion'])) ? $this->New_label['logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_logro_comunicacion_gestion_cond" name="logro_comunicacion_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_logro_comunicacion_gestion'), this, 'logro_comunicacion_gestion')">
       <OPTION value="qp" <?php if ("qp" == $logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $logro_comunicacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_logro_comunicacion_gestion" <?php echo $str_hide_logro_comunicacion_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['logro_comunicacion_gestion'])) ? $this->New_label['logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION";
 $nmgp_tab_label .= "logro_comunicacion_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $logro_comunicacion_gestion_look = substr($this->Db->qstr($logro_comunicacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct LOGRO_COMUNICACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where LOGRO_COMUNICACION_GESTION = '$logro_comunicacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$logro_comunicacion_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$logro_comunicacion_gestion];
      }
      else
      {
          $sAutocompValue = $logro_comunicacion_gestion;
      }
?>
<INPUT  type="text" id="SC_logro_comunicacion_gestion" name="logro_comunicacion_gestion" value="<?php echo NM_encode_input($logro_comunicacion_gestion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_logro_comunicacion_gestion" name="logro_comunicacion_gestion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['fecha_comunicacion'])) ? $this->New_label['fecha_comunicacion'] : "FECHA COMUNICACION"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_fecha_comunicacion_cond" name="fecha_comunicacion_cond" onChange="nm_campos_between(document.getElementById('id_vis_fecha_comunicacion'), this, 'fecha_comunicacion')">
       <OPTION value="qp" <?php if ("qp" == $fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $fecha_comunicacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_fecha_comunicacion" <?php echo $str_hide_fecha_comunicacion?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['fecha_comunicacion'])) ? $this->New_label['fecha_comunicacion'] : "FECHA COMUNICACION";
 $nmgp_tab_label .= "fecha_comunicacion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $fecha_comunicacion_look = substr($this->Db->qstr($fecha_comunicacion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_COMUNICACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where FECHA_COMUNICACION = '$fecha_comunicacion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$fecha_comunicacion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$fecha_comunicacion];
      }
      else
      {
          $sAutocompValue = $fecha_comunicacion;
      }
?>
<INPUT  type="text" id="SC_fecha_comunicacion" name="fecha_comunicacion" value="<?php echo NM_encode_input($fecha_comunicacion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_fecha_comunicacion" name="fecha_comunicacion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['autor_gestion'])) ? $this->New_label['autor_gestion'] : "AUTOR GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_autor_gestion_cond" name="autor_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_autor_gestion'), this, 'autor_gestion')">
       <OPTION value="qp" <?php if ("qp" == $autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $autor_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_autor_gestion" <?php echo $str_hide_autor_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['autor_gestion'])) ? $this->New_label['autor_gestion'] : "AUTOR GESTION";
 $nmgp_tab_label .= "autor_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $autor_gestion_look = substr($this->Db->qstr($autor_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct AUTOR_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where AUTOR_GESTION = '$autor_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$autor_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$autor_gestion];
      }
      else
      {
          $sAutocompValue = $autor_gestion;
      }
?>
<INPUT  type="text" id="SC_autor_gestion" name="autor_gestion" value="<?php echo NM_encode_input($autor_gestion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_autor_gestion" name="autor_gestion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['departamento_paciente'])) ? $this->New_label['departamento_paciente'] : "DEPARTAMENTO PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_departamento_paciente_cond" name="departamento_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_departamento_paciente'), this, 'departamento_paciente')">
       <OPTION value="qp" <?php if ("qp" == $departamento_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $departamento_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $departamento_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $departamento_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_departamento_paciente" <?php echo $str_hide_departamento_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['departamento_paciente'])) ? $this->New_label['departamento_paciente'] : "DEPARTAMENTO PACIENTE";
 $nmgp_tab_label .= "departamento_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $departamento_paciente_look = substr($this->Db->qstr($departamento_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct DEPARTAMENTO_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where DEPARTAMENTO_PACIENTE = '$departamento_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$departamento_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$departamento_paciente];
      }
      else
      {
          $sAutocompValue = $departamento_paciente;
      }
?>
<INPUT  type="text" id="SC_departamento_paciente" name="departamento_paciente" value="<?php echo NM_encode_input($departamento_paciente) ?>" size=50 alt="{datatype: 'text', maxLength: 100, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_departamento_paciente" name="departamento_paciente_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['ciudad_paciente'])) ? $this->New_label['ciudad_paciente'] : "CIUDAD PACIENTE"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_ciudad_paciente_cond" name="ciudad_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_ciudad_paciente'), this, 'ciudad_paciente')">
       <OPTION value="qp" <?php if ("qp" == $ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $ciudad_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_ciudad_paciente" <?php echo $str_hide_ciudad_paciente?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['ciudad_paciente'])) ? $this->New_label['ciudad_paciente'] : "CIUDAD PACIENTE";
 $nmgp_tab_label .= "ciudad_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $ciudad_paciente_look = substr($this->Db->qstr($ciudad_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct CIUDAD_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CIUDAD_PACIENTE = '$ciudad_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$ciudad_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$ciudad_paciente];
      }
      else
      {
          $sAutocompValue = $ciudad_paciente;
      }
?>
<INPUT  type="text" id="SC_ciudad_paciente" name="ciudad_paciente" value="<?php echo NM_encode_input($ciudad_paciente) ?>" size=50 alt="{datatype: 'text', maxLength: 100, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_ciudad_paciente" name="ciudad_paciente_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_estado_paciente_cond" name="estado_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_estado_paciente'), this, 'estado_paciente')">
       <OPTION value="qp" <?php if ("qp" == $estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $estado_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_estado_paciente" <?php echo $str_hide_estado_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE";
 $nmgp_tab_label .= "estado_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $estado_paciente_look = substr($this->Db->qstr($estado_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct ESTADO_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ESTADO_PACIENTE = '$estado_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$estado_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$estado_paciente];
      }
      else
      {
          $sAutocompValue = $estado_paciente;
      }
?>
<INPUT  type="text" id="SC_estado_paciente" name="estado_paciente" value="<?php echo NM_encode_input($estado_paciente) ?>" size=20 alt="{datatype: 'text', maxLength: 20, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_estado_paciente" name="estado_paciente_autocomp" size="20" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 20, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['status_paciente'])) ? $this->New_label['status_paciente'] : "STATUS PACIENTE"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_status_paciente_cond" name="status_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_status_paciente'), this, 'status_paciente')">
       <OPTION value="qp" <?php if ("qp" == $status_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $status_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $status_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $status_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_status_paciente" <?php echo $str_hide_status_paciente?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['status_paciente'])) ? $this->New_label['status_paciente'] : "STATUS PACIENTE";
 $nmgp_tab_label .= "status_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $status_paciente_look = substr($this->Db->qstr($status_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct STATUS_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where STATUS_PACIENTE = '$status_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$status_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$status_paciente];
      }
      else
      {
          $sAutocompValue = $status_paciente;
      }
?>
<INPUT  type="text" id="SC_status_paciente" name="status_paciente" value="<?php echo NM_encode_input($status_paciente) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_status_paciente" name="status_paciente_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_fecha_activacion_paciente_cond" name="fecha_activacion_paciente_cond" onChange="nm_campos_between(document.getElementById('id_vis_fecha_activacion_paciente'), this, 'fecha_activacion_paciente')">
       <OPTION value="qp" <?php if ("qp" == $fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $fecha_activacion_paciente_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_fecha_activacion_paciente" <?php echo $str_hide_fecha_activacion_paciente?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE";
 $nmgp_tab_label .= "fecha_activacion_paciente?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $fecha_activacion_paciente_look = substr($this->Db->qstr($fecha_activacion_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_ACTIVACION_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where FECHA_ACTIVACION_PACIENTE = '$fecha_activacion_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$fecha_activacion_paciente]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$fecha_activacion_paciente];
      }
      else
      {
          $sAutocompValue = $fecha_activacion_paciente;
      }
?>
<INPUT  type="text" id="SC_fecha_activacion_paciente" name="fecha_activacion_paciente" value="<?php echo NM_encode_input($fecha_activacion_paciente) ?>" size=10 alt="{datatype: 'text', maxLength: 10, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_fecha_activacion_paciente" name="fecha_activacion_paciente_autocomp" size="10" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 10, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['codigo_xofigo'])) ? $this->New_label['codigo_xofigo'] : "CODIGO XOFIGO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_codigo_xofigo_cond" name="codigo_xofigo_cond" onChange="nm_campos_between(document.getElementById('id_vis_codigo_xofigo'), this, 'codigo_xofigo')">
       <OPTION value="gt" <?php if ("gt" == $codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $codigo_xofigo_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_codigo_xofigo" <?php echo $str_hide_codigo_xofigo?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['codigo_xofigo'])) ? $this->New_label['codigo_xofigo'] : "CODIGO XOFIGO";
 $nmgp_tab_label .= "codigo_xofigo?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $codigo_xofigo_look = substr($this->Db->qstr($codigo_xofigo), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($codigo_xofigo))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct CODIGO_XOFIGO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CODIGO_XOFIGO = $codigo_xofigo_look"; 
      }
      else
      {
          $nm_comando = "select distinct CODIGO_XOFIGO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CODIGO_XOFIGO = $codigo_xofigo_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$codigo_xofigo]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$codigo_xofigo];
      }
      else
      {
          $sAutocompValue = $codigo_xofigo;
      }
?>
<INPUT  type="text" id="SC_codigo_xofigo" name="codigo_xofigo" value="<?php echo NM_encode_input($codigo_xofigo) ?>" size=4 alt="{datatype: 'integer', maxLength: 4, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_codigo_xofigo" name="codigo_xofigo_autocomp" size="4" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 4, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_codigo_xofigo"  <?php echo $str_display_codigo_xofigo; ?> class="scFilterFieldFontEven">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_codigo_xofigo_input_2" name="codigo_xofigo_input_2" value="<?php echo NM_encode_input($codigo_xofigo_input_2) ?>" size=4 alt="{datatype: 'integer', maxLength: 4, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectEven">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_producto_tratamiento_cond" name="producto_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_producto_tratamiento'), this, 'producto_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $producto_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_producto_tratamiento" <?php echo $str_hide_producto_tratamiento?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO";
 $nmgp_tab_label .= "producto_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $producto_tratamiento_look = substr($this->Db->qstr($producto_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct PRODUCTO_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where PRODUCTO_TRATAMIENTO = '$producto_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$producto_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$producto_tratamiento];
      }
      else
      {
          $sAutocompValue = $producto_tratamiento;
      }
?>
<INPUT  type="text" id="SC_producto_tratamiento" name="producto_tratamiento" value="<?php echo NM_encode_input($producto_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 200, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_producto_tratamiento" name="producto_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['nombre_referencia'])) ? $this->New_label['nombre_referencia'] : "NOMBRE REFERENCIA"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_nombre_referencia_cond" name="nombre_referencia_cond" onChange="nm_campos_between(document.getElementById('id_vis_nombre_referencia'), this, 'nombre_referencia')">
       <OPTION value="qp" <?php if ("qp" == $nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $nombre_referencia_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_nombre_referencia" <?php echo $str_hide_nombre_referencia?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['nombre_referencia'])) ? $this->New_label['nombre_referencia'] : "NOMBRE REFERENCIA";
 $nmgp_tab_label .= "nombre_referencia?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $nombre_referencia_look = substr($this->Db->qstr($nombre_referencia), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct NOMBRE_REFERENCIA from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where NOMBRE_REFERENCIA = '$nombre_referencia_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$nombre_referencia]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$nombre_referencia];
      }
      else
      {
          $sAutocompValue = $nombre_referencia;
      }
?>
<INPUT  type="text" id="SC_nombre_referencia" name="nombre_referencia" value="<?php echo NM_encode_input($nombre_referencia) ?>" size=50 alt="{datatype: 'text', maxLength: 200, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_nombre_referencia" name="nombre_referencia_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['dosis_tratamiento'])) ? $this->New_label['dosis_tratamiento'] : "DOSIS TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_dosis_tratamiento_cond" name="dosis_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_dosis_tratamiento'), this, 'dosis_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $dosis_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_dosis_tratamiento" <?php echo $str_hide_dosis_tratamiento?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['dosis_tratamiento'])) ? $this->New_label['dosis_tratamiento'] : "DOSIS TRATAMIENTO";
 $nmgp_tab_label .= "dosis_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $dosis_tratamiento_look = substr($this->Db->qstr($dosis_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct DOSIS_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where DOSIS_TRATAMIENTO = '$dosis_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$dosis_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$dosis_tratamiento];
      }
      else
      {
          $sAutocompValue = $dosis_tratamiento;
      }
?>
<INPUT  type="text" id="SC_dosis_tratamiento" name="dosis_tratamiento" value="<?php echo NM_encode_input($dosis_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 70, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_dosis_tratamiento" name="dosis_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['numero_cajas'])) ? $this->New_label['numero_cajas'] : "NUMERO CAJAS"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_numero_cajas_cond" name="numero_cajas_cond" onChange="nm_campos_between(document.getElementById('id_vis_numero_cajas'), this, 'numero_cajas')">
       <OPTION value="qp" <?php if ("qp" == $numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $numero_cajas_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_numero_cajas" <?php echo $str_hide_numero_cajas?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['numero_cajas'])) ? $this->New_label['numero_cajas'] : "NUMERO CAJAS";
 $nmgp_tab_label .= "numero_cajas?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $numero_cajas_look = substr($this->Db->qstr($numero_cajas), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct NUMERO_CAJAS from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where NUMERO_CAJAS = '$numero_cajas_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$numero_cajas]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$numero_cajas];
      }
      else
      {
          $sAutocompValue = $numero_cajas;
      }
?>
<INPUT  type="text" id="SC_numero_cajas" name="numero_cajas" value="<?php echo NM_encode_input($numero_cajas) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_numero_cajas" name="numero_cajas_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['asegurador_tratamiento'])) ? $this->New_label['asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_asegurador_tratamiento_cond" name="asegurador_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_asegurador_tratamiento'), this, 'asegurador_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $asegurador_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_asegurador_tratamiento" <?php echo $str_hide_asegurador_tratamiento?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['asegurador_tratamiento'])) ? $this->New_label['asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO";
 $nmgp_tab_label .= "asegurador_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $asegurador_tratamiento_look = substr($this->Db->qstr($asegurador_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct ASEGURADOR_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ASEGURADOR_TRATAMIENTO = '$asegurador_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$asegurador_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$asegurador_tratamiento];
      }
      else
      {
          $sAutocompValue = $asegurador_tratamiento;
      }
?>
<INPUT  type="text" id="SC_asegurador_tratamiento" name="asegurador_tratamiento" value="<?php echo NM_encode_input($asegurador_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 60, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_asegurador_tratamiento" name="asegurador_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['operador_logistico_tratamiento'])) ? $this->New_label['operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_operador_logistico_tratamiento_cond" name="operador_logistico_tratamiento_cond" onChange="nm_campos_between(document.getElementById('id_vis_operador_logistico_tratamiento'), this, 'operador_logistico_tratamiento')">
       <OPTION value="qp" <?php if ("qp" == $operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $operador_logistico_tratamiento_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_operador_logistico_tratamiento" <?php echo $str_hide_operador_logistico_tratamiento?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['operador_logistico_tratamiento'])) ? $this->New_label['operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO";
 $nmgp_tab_label .= "operador_logistico_tratamiento?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $operador_logistico_tratamiento_look = substr($this->Db->qstr($operador_logistico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct OPERADOR_LOGISTICO_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where OPERADOR_LOGISTICO_TRATAMIENTO = '$operador_logistico_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$operador_logistico_tratamiento]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$operador_logistico_tratamiento];
      }
      else
      {
          $sAutocompValue = $operador_logistico_tratamiento;
      }
?>
<INPUT  type="text" id="SC_operador_logistico_tratamiento" name="operador_logistico_tratamiento" value="<?php echo NM_encode_input($operador_logistico_tratamiento) ?>" size=50 alt="{datatype: 'text', maxLength: 100, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_operador_logistico_tratamiento" name="operador_logistico_tratamiento_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['punto_entrega'])) ? $this->New_label['punto_entrega'] : "PUNTO ENTREGA"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_punto_entrega_cond" name="punto_entrega_cond" onChange="nm_campos_between(document.getElementById('id_vis_punto_entrega'), this, 'punto_entrega')">
       <OPTION value="qp" <?php if ("qp" == $punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $punto_entrega_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_punto_entrega" <?php echo $str_hide_punto_entrega?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['punto_entrega'])) ? $this->New_label['punto_entrega'] : "PUNTO ENTREGA";
 $nmgp_tab_label .= "punto_entrega?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $punto_entrega_look = substr($this->Db->qstr($punto_entrega), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct PUNTO_ENTREGA from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where PUNTO_ENTREGA = '$punto_entrega_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$punto_entrega]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$punto_entrega];
      }
      else
      {
          $sAutocompValue = $punto_entrega;
      }
?>
<INPUT  type="text" id="SC_punto_entrega" name="punto_entrega" value="<?php echo NM_encode_input($punto_entrega) ?>" size=50 alt="{datatype: 'text', maxLength: 500, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_punto_entrega" name="punto_entrega_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['reclamo_gestion'])) ? $this->New_label['reclamo_gestion'] : "RECLAMO GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_reclamo_gestion_cond" name="reclamo_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_reclamo_gestion'), this, 'reclamo_gestion')">
       <OPTION value="qp" <?php if ("qp" == $reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $reclamo_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_reclamo_gestion" <?php echo $str_hide_reclamo_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['reclamo_gestion'])) ? $this->New_label['reclamo_gestion'] : "RECLAMO GESTION";
 $nmgp_tab_label .= "reclamo_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $reclamo_gestion_look = substr($this->Db->qstr($reclamo_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct RECLAMO_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where RECLAMO_GESTION = '$reclamo_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$reclamo_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$reclamo_gestion];
      }
      else
      {
          $sAutocompValue = $reclamo_gestion;
      }
?>
<INPUT  type="text" id="SC_reclamo_gestion" name="reclamo_gestion" value="<?php echo NM_encode_input($reclamo_gestion) ?>" size=50 alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_reclamo_gestion" name="reclamo_gestion_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['fecha_reclamacion_gestion'])) ? $this->New_label['fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_fecha_reclamacion_gestion_cond" name="fecha_reclamacion_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_fecha_reclamacion_gestion'), this, 'fecha_reclamacion_gestion')">
       <OPTION value="qp" <?php if ("qp" == $fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $fecha_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_fecha_reclamacion_gestion" <?php echo $str_hide_fecha_reclamacion_gestion?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['fecha_reclamacion_gestion'])) ? $this->New_label['fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION";
 $nmgp_tab_label .= "fecha_reclamacion_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $fecha_reclamacion_gestion_look = substr($this->Db->qstr($fecha_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_RECLAMACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where FECHA_RECLAMACION_GESTION = '$fecha_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$fecha_reclamacion_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$fecha_reclamacion_gestion];
      }
      else
      {
          $sAutocompValue = $fecha_reclamacion_gestion;
      }
?>
<INPUT  type="text" id="SC_fecha_reclamacion_gestion" name="fecha_reclamacion_gestion" value="<?php echo NM_encode_input($fecha_reclamacion_gestion) ?>" size=30 alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_fecha_reclamacion_gestion" name="fecha_reclamacion_gestion_autocomp" size="30" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 30, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['causa_no_reclamacion_gestion'])) ? $this->New_label['causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_causa_no_reclamacion_gestion_cond" name="causa_no_reclamacion_gestion_cond" onChange="nm_campos_between(document.getElementById('id_vis_causa_no_reclamacion_gestion'), this, 'causa_no_reclamacion_gestion')">
       <OPTION value="qp" <?php if ("qp" == $causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_like'] ?></OPTION>
       <OPTION value="np" <?php if ("np" == $causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_not_like'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="ep" <?php if ("ep" == $causa_no_reclamacion_gestion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_empty'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_causa_no_reclamacion_gestion" <?php echo $str_hide_causa_no_reclamacion_gestion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['causa_no_reclamacion_gestion'])) ? $this->New_label['causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION";
 $nmgp_tab_label .= "causa_no_reclamacion_gestion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $causa_no_reclamacion_gestion_look = substr($this->Db->qstr($causa_no_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct CAUSA_NO_RECLAMACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CAUSA_NO_RECLAMACION_GESTION = '$causa_no_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
      if (isset($nmgp_def_dados[0][$causa_no_reclamacion_gestion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$causa_no_reclamacion_gestion];
      }
      else
      {
          $sAutocompValue = $causa_no_reclamacion_gestion;
      }
?>
<INPUT  type="text" id="SC_causa_no_reclamacion_gestion" name="causa_no_reclamacion_gestion" value="<?php echo NM_encode_input($causa_no_reclamacion_gestion) ?>" size=50 alt="{datatype: 'text', maxLength: 150, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_causa_no_reclamacion_gestion" name="causa_no_reclamacion_gestion_autocomp" size="50" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'text', maxLength: 50, allowedChars: '', lettersCase: '', autoTab: false, enterTab: false}">


        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelOdd"><?php echo (isset($this->New_label['id_historial_reclamacion'])) ? $this->New_label['id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION"; ?></TD>
     <TD class="scFilterFieldOdd"> 
      <SELECT class="scFilterObjectOdd" id="SC_id_historial_reclamacion_cond" name="id_historial_reclamacion_cond" onChange="nm_campos_between(document.getElementById('id_vis_id_historial_reclamacion'), this, 'id_historial_reclamacion')">
       <OPTION value="gt" <?php if ("gt" == $id_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $id_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $id_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $id_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldOdd">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_id_historial_reclamacion" <?php echo $str_hide_id_historial_reclamacion?> valign="top">
        <TD class="scFilterFieldFontOdd">
           <?php
 $SC_Label = (isset($this->New_label['id_historial_reclamacion'])) ? $this->New_label['id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION";
 $nmgp_tab_label .= "id_historial_reclamacion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $id_historial_reclamacion_look = substr($this->Db->qstr($id_historial_reclamacion), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($id_historial_reclamacion))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ID_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_HISTORIAL_RECLAMACION = $id_historial_reclamacion_look"; 
      }
      else
      {
          $nm_comando = "select distinct ID_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_HISTORIAL_RECLAMACION = $id_historial_reclamacion_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$id_historial_reclamacion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$id_historial_reclamacion];
      }
      else
      {
          $sAutocompValue = $id_historial_reclamacion;
      }
?>
<INPUT  type="text" id="SC_id_historial_reclamacion" name="id_historial_reclamacion" value="<?php echo NM_encode_input($id_historial_reclamacion) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectOdd" type="text" id="id_ac_id_historial_reclamacion" name="id_historial_reclamacion_autocomp" size="19" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_id_historial_reclamacion"  <?php echo $str_display_id_historial_reclamacion; ?> class="scFilterFieldFontOdd">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_id_historial_reclamacion_input_2" name="id_historial_reclamacion_input_2" value="<?php echo NM_encode_input($id_historial_reclamacion_input_2) ?>" size=19 alt="{datatype: 'integer', maxLength: 19, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectOdd">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr><tr>





      <TD class="scFilterLabelEven"><?php echo (isset($this->New_label['anio_historial_reclamacion'])) ? $this->New_label['anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION"; ?></TD>
     <TD class="scFilterFieldEven"> 
      <SELECT class="scFilterObjectEven" id="SC_anio_historial_reclamacion_cond" name="anio_historial_reclamacion_cond" onChange="nm_campos_between(document.getElementById('id_vis_anio_historial_reclamacion'), this, 'anio_historial_reclamacion')">
       <OPTION value="gt" <?php if ("gt" == $anio_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_grtr'] ?></OPTION>
       <OPTION value="lt" <?php if ("lt" == $anio_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_less'] ?></OPTION>
       <OPTION value="eq" <?php if ("eq" == $anio_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_exac'] ?></OPTION>
       <OPTION value="bw" <?php if ("bw" == $anio_historial_reclamacion_cond) { echo "selected"; } ?>><?php echo $this->Ini->Nm_lang['lang_srch_betw'] ?></OPTION>
      </SELECT>
       </TD>
     <TD  class="scFilterFieldEven">
      <TABLE  border="0" cellpadding="0" cellspacing="0">
       <TR id="id_hide_anio_historial_reclamacion" <?php echo $str_hide_anio_historial_reclamacion?> valign="top">
        <TD class="scFilterFieldFontEven">
           <?php
 $SC_Label = (isset($this->New_label['anio_historial_reclamacion'])) ? $this->New_label['anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION";
 $nmgp_tab_label .= "anio_historial_reclamacion?#?" . $SC_Label . "?@?";
 $date_sep_bw = "";
?>
<?php
      $anio_historial_reclamacion_look = substr($this->Db->qstr($anio_historial_reclamacion), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($anio_historial_reclamacion))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ANIO_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ANIO_HISTORIAL_RECLAMACION = $anio_historial_reclamacion_look"; 
      }
      else
      {
          $nm_comando = "select distinct ANIO_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ANIO_HISTORIAL_RECLAMACION = $anio_historial_reclamacion_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = trim($rs->fields[0]);
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 
      if (isset($nmgp_def_dados[0][$anio_historial_reclamacion]))
      {
          $sAutocompValue = $nmgp_def_dados[0][$anio_historial_reclamacion];
      }
      else
      {
          $sAutocompValue = $anio_historial_reclamacion;
      }
?>
<INPUT  type="text" id="SC_anio_historial_reclamacion" name="anio_historial_reclamacion" value="<?php echo NM_encode_input($anio_historial_reclamacion) ?>" size=11 alt="{datatype: 'integer', maxLength: 11, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" style="display: none">
<input class="sc-js-input scFilterObjectEven" type="text" id="id_ac_anio_historial_reclamacion" name="anio_historial_reclamacion_autocomp" size="11" value="<?php echo NM_encode_input($sAutocompValue); ?>" alt="{datatype: 'integer', maxLength: 11, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}">
        </TD>
       </TR>
       <TR valign="top">
        <TD id="id_vis_anio_historial_reclamacion"  <?php echo $str_display_anio_historial_reclamacion; ?> class="scFilterFieldFontEven">
         <?php echo $date_sep_bw ?>&nbsp;
         <BR>
         <INPUT type="text" id="SC_anio_historial_reclamacion_input_2" name="anio_historial_reclamacion_input_2" value="<?php echo NM_encode_input($anio_historial_reclamacion_input_2) ?>" size=11 alt="{datatype: 'integer', maxLength: 11, thousandsSep: '<?php echo $_SESSION['scriptcase']['reg_conf']['grup_num'] ?>', allowNegative: false, onlyNegative: false, enterTab: false}" class="sc-js-input sc-js-input sc-js-input scFilterObjectEven">

        </TD>
       </TR>
      </TABLE>
     </TD>

   </tr>
   </TABLE>
  </TD>
 </TR>
 </TABLE>
 </TD>
 </TR>
 <TR>
  <TD class="scFilterTableTd" align="center">
<INPUT type="hidden" id="SC_NM_operador" name="NM_operador" value="and">  </TD>
 </TR>
   <INPUT type="hidden" name="nmgp_tab_label" value="<?php echo NM_encode_input($nmgp_tab_label); ?>"> 
   <INPUT type="hidden" name="bprocessa" value="pesq"> 
 <?php
     if ($_SESSION['scriptcase']['proc_mobile'])
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
   <?php echo nmButtonOutput($this->arr_buttons, "bpesquisa", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "sc_b_pesq_bot", "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone'] . "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone_hint'] . "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   if ($this->nmgp_botoes['clear'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "blimpar", "limpa_form()", "limpa_form()", "limpa_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (!isset($this->nmgp_botoes['save']) || $this->nmgp_botoes['save'] == "on")
   {
       $this->NM_fil_ant = $this->gera_array_filtros();
?>
     <span id="idAjaxSelect_NM_filters_bot">
       <SELECT class="scFilterToolbar_obj" id="sel_recup_filters_bot" name="NM_filters_bot" onChange="nm_submit_filter(this, 'bot')" size="1">
           <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "           <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
           <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
       </SELECT>
     </span>
<?php
   }
?>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bedit_filter", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "Ativa_save_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (is_file("Informe_reclamacion_historial_help.txt"))
   {
      $Arq_WebHelp = file("Informe_reclamacion_historial_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
<?php
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['start'] == 'filter' && $nm_apl_dependente != 1)
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bsair", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_bot" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "Cancel_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_bot" name="nmgp_save_name_bot" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('bot')", "nm_save_form('bot')", "Save_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_bot" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_bot">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_bot" name="NM_filters_del_bot" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('bot')", "nm_submit_filter_del('bot')", "Exc_filtro_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
     else
     {
     ?>
 <TR align="center">
  <TD class="scFilterTableTd">
   <table width="100%" class="scFilterToolbar"><tr>
    <td class="scFilterToolbarPadding" align="left" width="33%" nowrap>
    </td>
    <td class="scFilterToolbarPadding" align="center" width="33%" nowrap>
   <?php echo nmButtonOutput($this->arr_buttons, "bpesquisa", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "document.F1.bprocessa.value='pesq'; setTimeout(function() {nm_submit_form()}, 200)", "sc_b_pesq_bot", "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone'] . "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "" . $this->Ini->Nm_lang['nmgp_lang_btns_srch_lone_hint'] . "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   if ($this->nmgp_botoes['clear'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "blimpar", "limpa_form()", "limpa_form()", "limpa_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (!isset($this->nmgp_botoes['save']) || $this->nmgp_botoes['save'] == "on")
   {
       $this->NM_fil_ant = $this->gera_array_filtros();
?>
     <span id="idAjaxSelect_NM_filters_bot">
       <SELECT class="scFilterToolbar_obj" id="sel_recup_filters_bot" name="NM_filters_bot" onChange="nm_submit_filter(this, 'bot')" size="1">
           <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "           <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
           <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
       </SELECT>
     </span>
<?php
   }
?>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bedit_filter", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "document.getElementById('Salvar_filters_bot').style.display = ''; document.F1.nmgp_save_name_bot.focus()", "Ativa_save_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
<?php
   if (is_file("Informe_reclamacion_historial_help.txt"))
   {
      $Arq_WebHelp = file("Informe_reclamacion_historial_help.txt"); 
      if (isset($Arq_WebHelp[0]) && !empty($Arq_WebHelp[0]))
      {
          $Arq_WebHelp[0] = str_replace("\r\n" , "", trim($Arq_WebHelp[0]));
          $Tmp = explode(";", $Arq_WebHelp[0]); 
          foreach ($Tmp as $Cada_help)
          {
              $Tmp1 = explode(":", $Cada_help); 
              if (!empty($Tmp1[0]) && isset($Tmp1[1]) && !empty($Tmp1[1]) && $Tmp1[0] == "fil" && is_file($this->Ini->root . $this->Ini->path_help . $Tmp1[1]))
              {
?>
          <?php echo nmButtonOutput($this->arr_buttons, "bhelp", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "nm_open_popup('" . $this->Ini->path_help . $Tmp1[1] . "')", "sc_b_help_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
              }
          }
      }
   }
?>
<?php
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['start'] == 'filter' && $nm_apl_dependente != 1)
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bsair", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
       <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.form_cancel.submit()", "document.form_cancel.submit()", "sc_b_cancel_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
    </td>
    <td class="scFilterToolbarPadding" align="right" width="33%" nowrap>
    </td>
   </tr></table>
<?php
   if ($this->nmgp_botoes['save'] == "on")
   {
?>
    </TD></TR><TR><TD>
    <DIV id="Salvar_filters_bot" style="display:none">
     <TABLE align="center" class="scFilterTable">
      <TR>
       <TD class="scFilterBlock">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top" class="scFilterBlockFont"><?php echo $this->Ini->Nm_lang['lang_othr_srch_head'] ?></td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "document.getElementById('Salvar_filters_bot').style.display = 'none'", "Cancel_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldOdd">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
           <input class="scFilterObjectOdd" type="text" id="SC_nmgp_save_name_bot" name="nmgp_save_name_bot" value="">
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bsalvar", "nm_save_form('bot')", "nm_save_form('bot')", "Save_frm_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </TD>
      </TR>
      <TR>
       <TD class="scFilterFieldEven">
       <DIV id="Apaga_filters_bot" style="display:''">
        <table style="border-width: 0px; border-collapse: collapse" width="100%">
         <tr>
          <td style="padding: 0px" valign="top">
          <div id="idAjaxSelect_NM_filters_del_bot">
           <SELECT class="scFilterObjectOdd" id="sel_filters_del_bot" name="NM_filters_del_bot" size="1">
            <option value=""></option>
<?php
          $Nome_filter = "";
          foreach ($this->NM_fil_ant as $Cada_filter => $Tipo_filter)
          {
              $Select = "";
              if ($Cada_filter == $this->NM_curr_fil)
              {
                  $Select = "selected";
              }
              if (NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] != "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, $_SESSION['scriptcase']['charset'], "UTF-8");
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], $_SESSION['scriptcase']['charset'], "UTF-8");
              }
              elseif (!NM_is_utf8($Cada_filter) && $_SESSION['scriptcase']['charset'] == "UTF-8")
              {
                  $Cada_filter    = sc_convert_encoding($Cada_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
                  $Tipo_filter[0] = sc_convert_encoding($Tipo_filter[0], "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              if ($Tipo_filter[1] != $Nome_filter)
              {
                  $Nome_filter = $Tipo_filter[1];
                  echo "            <option value=\"\">" . NM_encode_input($Nome_filter) . "</option>\r\n";
              }
?>
            <option value="<?php echo NM_encode_input($Tipo_filter[0]) . "\" " . $Select . ">.." . $Cada_filter ?></option>
<?php
          }
?>
           </SELECT>
          </div>
          </td>
          <td style="padding: 0px" align="right" valign="top">
           <?php echo nmButtonOutput($this->arr_buttons, "bexcluir", "nm_submit_filter_del('bot')", "nm_submit_filter_del('bot')", "Exc_filtro_bot", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
          </td>
         </tr>
        </table>
       </DIV>
       </TD>
      </TR>
     </TABLE>
    </DIV> 
<?php
   }
?>
  </TD>
 </TR>
     <?php
     }
 ?>
<?php
   }

   function monta_html_fim()
   {
       global $bprocessa, $nm_url_saida, $Script_BI;
?>

</TABLE>
   <INPUT type="hidden" name="form_condicao" value="3">
</FORM> 
<?php
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['start']) && $_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['start'] == 'filter')
   {
?>
   <FORM style="display:none;" name="form_cancel"  method="POST" action="<?php echo $nm_url_saida; ?>" target="_self"> 
<?php
   }
   else
   {
?>
   <FORM style="display:none;" name="form_cancel"  method="POST" action="./" target="_self"> 
<?php
   }
?>
   <INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
   <INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<?php
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['orig_pesq'] == "grid")
   {
       $Ret_cancel_pesq = "volta_grid";
   }
   else
   {
       $Ret_cancel_pesq = "resumo";
   }
?>
   <INPUT type="hidden" name="nmgp_opcao" value="<?php echo $Ret_cancel_pesq; ?>"> 
   </FORM> 
<SCRIPT type="text/javascript">
<?php
   if (empty($this->NM_fil_ant))
   {
?>
      document.getElementById('Apaga_filters_bot').style.display = 'none';
      document.getElementById('sel_recup_filters_bot').style.display = 'none';
<?php
   }
?>
 function nm_submit_form()
 {
    document.F1.submit();
 }
 function limpa_form()
 {
   document.F1.reset();
   if (document.F1.NM_filters)
   {
       document.F1.NM_filters.selectedIndex = -1;
   }
   document.getElementById('Salvar_filters_bot').style.display = 'none';
   nm_campos_between(document.getElementById('id_vis_id_paciente'), document.F1.id_paciente_cond, 'id_paciente');
   document.F1.id_paciente.value = "";
   document.F1.id_paciente_autocomp.value = "";
   document.F1.id_paciente_input_2.value = "";
   nm_campos_between(document.getElementById('id_vis_logro_comunicacion_gestion'), document.F1.logro_comunicacion_gestion_cond, 'logro_comunicacion_gestion');
   document.F1.logro_comunicacion_gestion.value = "";
   document.F1.logro_comunicacion_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_fecha_comunicacion'), document.F1.fecha_comunicacion_cond, 'fecha_comunicacion');
   document.F1.fecha_comunicacion.value = "";
   document.F1.fecha_comunicacion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_autor_gestion'), document.F1.autor_gestion_cond, 'autor_gestion');
   document.F1.autor_gestion.value = "";
   document.F1.autor_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_departamento_paciente'), document.F1.departamento_paciente_cond, 'departamento_paciente');
   document.F1.departamento_paciente.value = "";
   document.F1.departamento_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_ciudad_paciente'), document.F1.ciudad_paciente_cond, 'ciudad_paciente');
   document.F1.ciudad_paciente.value = "";
   document.F1.ciudad_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_estado_paciente'), document.F1.estado_paciente_cond, 'estado_paciente');
   document.F1.estado_paciente.value = "";
   document.F1.estado_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_status_paciente'), document.F1.status_paciente_cond, 'status_paciente');
   document.F1.status_paciente.value = "";
   document.F1.status_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_fecha_activacion_paciente'), document.F1.fecha_activacion_paciente_cond, 'fecha_activacion_paciente');
   document.F1.fecha_activacion_paciente.value = "";
   document.F1.fecha_activacion_paciente_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_codigo_xofigo'), document.F1.codigo_xofigo_cond, 'codigo_xofigo');
   document.F1.codigo_xofigo.value = "";
   document.F1.codigo_xofigo_autocomp.value = "";
   document.F1.codigo_xofigo_input_2.value = "";
   nm_campos_between(document.getElementById('id_vis_producto_tratamiento'), document.F1.producto_tratamiento_cond, 'producto_tratamiento');
   document.F1.producto_tratamiento.value = "";
   document.F1.producto_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_nombre_referencia'), document.F1.nombre_referencia_cond, 'nombre_referencia');
   document.F1.nombre_referencia.value = "";
   document.F1.nombre_referencia_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_dosis_tratamiento'), document.F1.dosis_tratamiento_cond, 'dosis_tratamiento');
   document.F1.dosis_tratamiento.value = "";
   document.F1.dosis_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_numero_cajas'), document.F1.numero_cajas_cond, 'numero_cajas');
   document.F1.numero_cajas.value = "";
   document.F1.numero_cajas_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_asegurador_tratamiento'), document.F1.asegurador_tratamiento_cond, 'asegurador_tratamiento');
   document.F1.asegurador_tratamiento.value = "";
   document.F1.asegurador_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_operador_logistico_tratamiento'), document.F1.operador_logistico_tratamiento_cond, 'operador_logistico_tratamiento');
   document.F1.operador_logistico_tratamiento.value = "";
   document.F1.operador_logistico_tratamiento_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_punto_entrega'), document.F1.punto_entrega_cond, 'punto_entrega');
   document.F1.punto_entrega.value = "";
   document.F1.punto_entrega_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_reclamo_gestion'), document.F1.reclamo_gestion_cond, 'reclamo_gestion');
   document.F1.reclamo_gestion.value = "";
   document.F1.reclamo_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_fecha_reclamacion_gestion'), document.F1.fecha_reclamacion_gestion_cond, 'fecha_reclamacion_gestion');
   document.F1.fecha_reclamacion_gestion.value = "";
   document.F1.fecha_reclamacion_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_causa_no_reclamacion_gestion'), document.F1.causa_no_reclamacion_gestion_cond, 'causa_no_reclamacion_gestion');
   document.F1.causa_no_reclamacion_gestion.value = "";
   document.F1.causa_no_reclamacion_gestion_autocomp.value = "";
   nm_campos_between(document.getElementById('id_vis_id_historial_reclamacion'), document.F1.id_historial_reclamacion_cond, 'id_historial_reclamacion');
   document.F1.id_historial_reclamacion.value = "";
   document.F1.id_historial_reclamacion_autocomp.value = "";
   document.F1.id_historial_reclamacion_input_2.value = "";
   nm_campos_between(document.getElementById('id_vis_anio_historial_reclamacion'), document.F1.anio_historial_reclamacion_cond, 'anio_historial_reclamacion');
   document.F1.anio_historial_reclamacion.value = "";
   document.F1.anio_historial_reclamacion_autocomp.value = "";
   document.F1.anio_historial_reclamacion_input_2.value = "";
 }
function nm_tabula(obj, tam, cond)
{
   if (obj.value.length == tam)
   {
       for (i=0; i < document.F1.elements.length;i++)
       {
            if (document.F1.elements[i].name == obj.name)
            {
                i++;
                campo = document.F1.elements[i].name;
                campo2 = campo.lastIndexOf('_input_2');
                if (document.F1.elements[i].type == 'text' && (campo2 == -1 || cond == 'bw'))
                {
                    eval('document.F1.' + campo + '.focus()');
                }
                break;
            }
       }
   }
}
 function SC_carga_evt_jquery()
 {
 }
</SCRIPT>
</BODY>
</HTML>
<?php
   }

   function gera_array_filtros()
   {
       $this->NM_fil_ant = array();
       $NM_patch   = "Bayer/Informe_reclamacion_historial";
       if (is_dir($this->NM_path_filter . $NM_patch))
       {
           $NM_dir = @opendir($this->NM_path_filter . $NM_patch);
           while (FALSE !== ($NM_arq = @readdir($NM_dir)))
           {
             if (@is_file($this->NM_path_filter . $NM_patch . "/" . $NM_arq))
             {
                 $Sc_v6 = false;
                 $NMcmp_filter = file($this->NM_path_filter . $NM_patch . "/" . $NM_arq);
                 $NMcmp_filter = explode("@NMF@", $NMcmp_filter[0]);
                 if (substr($NMcmp_filter[0], 0, 6) == "SC_V6_" || substr($NMcmp_filter[0], 0, 6) == "SC_V8_")
                 {
                     $Name_filter = substr($NMcmp_filter[0], 6);
                     if (!empty($Name_filter))
                     {
                         $nmgp_save_name = str_replace('/', ' ', $Name_filter);
                         $nmgp_save_name = str_replace('\\', ' ', $nmgp_save_name);
                         $nmgp_save_name = str_replace('.', ' ', $nmgp_save_name);
                         $this->NM_fil_ant[$Name_filter][0] = $NM_patch . "/" . $nmgp_save_name;
                         $this->NM_fil_ant[$Name_filter][1] = "" . $this->Ini->Nm_lang['lang_srch_public'] . "";
                         $Sc_v6 = true;
                     }
                 }
                 if (!$Sc_v6)
                 {
                     $this->NM_fil_ant[$NM_arq][0] = $NM_patch . "/" . $NM_arq;
                     $this->NM_fil_ant[$NM_arq][1] = "" . $this->Ini->Nm_lang['lang_srch_public'] . "";
                 }
             }
           }
       }
       return $this->NM_fil_ant;
   }
   /**
    * @access  public
    * @param  string  $NM_operador  $this->Ini->Nm_lang['pesq_global_NM_operador']
    * @param  array  $nmgp_tab_label  
    */
   function inicializa_vars()
   {
      global $NM_operador, $nmgp_tab_label;

      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/");  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1);  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz;
      $this->Campos_Mens_erro = ""; 
      $this->nm_data = new nm_data("es");
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] = "";
      if (!empty($nmgp_tab_label))
      {
         $nm_tab_campos = explode("?@?", $nmgp_tab_label);
         $nmgp_tab_label = array();
         foreach ($nm_tab_campos as $cada_campo)
         {
             $parte_campo = explode("?#?", $cada_campo);
             $nmgp_tab_label[$parte_campo[0]] = $parte_campo[1];
         }
      }
      if (!isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig']))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig'] = "";
      }
      $this->comando        = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig'];
      $this->comando_sum    = "";
      $this->comando_filtro = "";
      $this->comando_ini    = "ini";
      $this->comando_fim    = "";
      $this->NM_operador    = (isset($NM_operador) && ("and" == strtolower($NM_operador) || "or" == strtolower($NM_operador))) ? $NM_operador : "and";
   }

   function salva_filtro()
   {
      global $NM_filters_save, $nmgp_save_name, $nmgp_save_option, $script_case_init;
          $NM_filters_save = str_replace("__NM_PLUS__", "+", $NM_filters_save);
          $NM_str_filter  = "SC_V8_" . $nmgp_save_name . "@NMF@";
          $nmgp_save_name = str_replace('/', ' ', $nmgp_save_name);
          $nmgp_save_name = str_replace('\\', ' ', $nmgp_save_name);
          $nmgp_save_name = str_replace('.', ' ', $nmgp_save_name);
          if (!NM_is_utf8($nmgp_save_name))
          {
              $nmgp_save_name = sc_convert_encoding($nmgp_save_name, "UTF-8", $_SESSION['scriptcase']['charset']);
          }
          $NM_str_filter  .= $NM_filters_save;
          $NM_patch = $this->NM_path_filter;
          if (!is_dir($NM_patch))
          {
              $NMdir = mkdir($NM_patch, 0755);
          }
          $NM_patch .= "Bayer/";
          if (!is_dir($NM_patch))
          {
              $NMdir = mkdir($NM_patch, 0755);
          }
          $NM_patch .= "Informe_reclamacion_historial/";
          if (!is_dir($NM_patch))
          {
              $NMdir = mkdir($NM_patch, 0755);
          }
          $Parms_usr  = "";
          $NM_filter = fopen ($NM_patch . $nmgp_save_name, 'w');
          if (!NM_is_utf8($NM_str_filter))
          {
              $NM_str_filter = sc_convert_encoding($NM_str_filter, "UTF-8", $_SESSION['scriptcase']['charset']);
          }
          fwrite($NM_filter, $NM_str_filter);
          fclose($NM_filter);
   }
   function recupera_filtro()
   {
      global $NM_filters, $NM_operador, $script_case_init;
      $NM_patch = $this->NM_path_filter . "/" . $NM_filters;
      if (!is_file($NM_patch))
      {
          $NM_filters = sc_convert_encoding($NM_filters, "UTF-8", $_SESSION['scriptcase']['charset']);
          $NM_patch = $this->NM_path_filter . "/" . $NM_filters;
      }
      $return_fields = array();
      $tp_fields     = array();
      $tb_fields_esp = array();
      $tp_fields['SC_id_paciente_cond'] = 'cond';
      $tp_fields['SC_id_paciente'] = 'text_aut';
      $tp_fields['id_ac_id_paciente'] = 'text_aut';
      $tp_fields['SC_id_paciente_input_2'] = 'text';
      $tp_fields['SC_logro_comunicacion_gestion_cond'] = 'cond';
      $tp_fields['SC_logro_comunicacion_gestion'] = 'text_aut';
      $tp_fields['id_ac_logro_comunicacion_gestion'] = 'text_aut';
      $tp_fields['SC_fecha_comunicacion_cond'] = 'cond';
      $tp_fields['SC_fecha_comunicacion'] = 'text_aut';
      $tp_fields['id_ac_fecha_comunicacion'] = 'text_aut';
      $tp_fields['SC_autor_gestion_cond'] = 'cond';
      $tp_fields['SC_autor_gestion'] = 'text_aut';
      $tp_fields['id_ac_autor_gestion'] = 'text_aut';
      $tp_fields['SC_departamento_paciente_cond'] = 'cond';
      $tp_fields['SC_departamento_paciente'] = 'text_aut';
      $tp_fields['id_ac_departamento_paciente'] = 'text_aut';
      $tp_fields['SC_ciudad_paciente_cond'] = 'cond';
      $tp_fields['SC_ciudad_paciente'] = 'text_aut';
      $tp_fields['id_ac_ciudad_paciente'] = 'text_aut';
      $tp_fields['SC_estado_paciente_cond'] = 'cond';
      $tp_fields['SC_estado_paciente'] = 'text_aut';
      $tp_fields['id_ac_estado_paciente'] = 'text_aut';
      $tp_fields['SC_status_paciente_cond'] = 'cond';
      $tp_fields['SC_status_paciente'] = 'text_aut';
      $tp_fields['id_ac_status_paciente'] = 'text_aut';
      $tp_fields['SC_fecha_activacion_paciente_cond'] = 'cond';
      $tp_fields['SC_fecha_activacion_paciente'] = 'text_aut';
      $tp_fields['id_ac_fecha_activacion_paciente'] = 'text_aut';
      $tp_fields['SC_codigo_xofigo_cond'] = 'cond';
      $tp_fields['SC_codigo_xofigo'] = 'text_aut';
      $tp_fields['id_ac_codigo_xofigo'] = 'text_aut';
      $tp_fields['SC_codigo_xofigo_input_2'] = 'text';
      $tp_fields['SC_producto_tratamiento_cond'] = 'cond';
      $tp_fields['SC_producto_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_producto_tratamiento'] = 'text_aut';
      $tp_fields['SC_nombre_referencia_cond'] = 'cond';
      $tp_fields['SC_nombre_referencia'] = 'text_aut';
      $tp_fields['id_ac_nombre_referencia'] = 'text_aut';
      $tp_fields['SC_dosis_tratamiento_cond'] = 'cond';
      $tp_fields['SC_dosis_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_dosis_tratamiento'] = 'text_aut';
      $tp_fields['SC_numero_cajas_cond'] = 'cond';
      $tp_fields['SC_numero_cajas'] = 'text_aut';
      $tp_fields['id_ac_numero_cajas'] = 'text_aut';
      $tp_fields['SC_asegurador_tratamiento_cond'] = 'cond';
      $tp_fields['SC_asegurador_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_asegurador_tratamiento'] = 'text_aut';
      $tp_fields['SC_operador_logistico_tratamiento_cond'] = 'cond';
      $tp_fields['SC_operador_logistico_tratamiento'] = 'text_aut';
      $tp_fields['id_ac_operador_logistico_tratamiento'] = 'text_aut';
      $tp_fields['SC_punto_entrega_cond'] = 'cond';
      $tp_fields['SC_punto_entrega'] = 'text_aut';
      $tp_fields['id_ac_punto_entrega'] = 'text_aut';
      $tp_fields['SC_reclamo_gestion_cond'] = 'cond';
      $tp_fields['SC_reclamo_gestion'] = 'text_aut';
      $tp_fields['id_ac_reclamo_gestion'] = 'text_aut';
      $tp_fields['SC_fecha_reclamacion_gestion_cond'] = 'cond';
      $tp_fields['SC_fecha_reclamacion_gestion'] = 'text_aut';
      $tp_fields['id_ac_fecha_reclamacion_gestion'] = 'text_aut';
      $tp_fields['SC_causa_no_reclamacion_gestion_cond'] = 'cond';
      $tp_fields['SC_causa_no_reclamacion_gestion'] = 'text_aut';
      $tp_fields['id_ac_causa_no_reclamacion_gestion'] = 'text_aut';
      $tp_fields['SC_id_historial_reclamacion_cond'] = 'cond';
      $tp_fields['SC_id_historial_reclamacion'] = 'text_aut';
      $tp_fields['id_ac_id_historial_reclamacion'] = 'text_aut';
      $tp_fields['SC_id_historial_reclamacion_input_2'] = 'text';
      $tp_fields['SC_anio_historial_reclamacion_cond'] = 'cond';
      $tp_fields['SC_anio_historial_reclamacion'] = 'text_aut';
      $tp_fields['id_ac_anio_historial_reclamacion'] = 'text_aut';
      $tp_fields['SC_anio_historial_reclamacion_input_2'] = 'text';
      if (is_file($NM_patch))
      {
          $SC_V8    = false;
          $NMfilter = file($NM_patch);
          $NMcmp_filter = explode("@NMF@", $NMfilter[0]);
          if (substr($NMcmp_filter[0], 0, 5) == "SC_V8")
          {
              $SC_V8 = true;
          }
          if (substr($NMcmp_filter[0], 0, 5) == "SC_V6" || substr($NMcmp_filter[0], 0, 5) == "SC_V8")
          {
              unset($NMcmp_filter[0]);
          }
          foreach ($NMcmp_filter as $Cada_cmp)
          {
              $Cada_cmp = explode("#NMF#", $Cada_cmp);
              if (isset($tb_fields_esp[$Cada_cmp[0]]))
              {
                  $Cada_cmp[0] = $tb_fields_esp[$Cada_cmp[0]];
              }
              if (!$SC_V8 && substr($Cada_cmp[0], 0, 11) != "div_ac_lab_" && substr($Cada_cmp[0], 0, 6) != "id_ac_" && substr($Cada_cmp[0], 0, 11) != "NM_operador")
              {
                  $Cada_cmp[0] = "SC_" . $Cada_cmp[0];
              }
              if (!isset($tp_fields[$Cada_cmp[0]]))
              {
                  continue;
              }
              $list   = array();
              $list_a = array();
              if (substr($Cada_cmp[1], 0, 10) == "_NM_array_")
              {
                  if (substr($Cada_cmp[1], 0, 17) == "_NM_array_#NMARR#")
                  {
                      $Sc_temp = explode("#NMARR#", substr($Cada_cmp[1], 17));
                      foreach ($Sc_temp as $Cada_val)
                      {
                          $list[]   = $Cada_val;
                          $tmp_pos  = strpos($Cada_val, "##@@");
                          $val_a    = ($tmp_pos !== false) ?  substr($Cada_val, $tmp_pos + 4) : $Cada_val;
                          $list_a[] = array('opt' => $Cada_val, 'value' => $val_a);
                      }
                  }
              }
              else
              {
                  $list[0] = $Cada_cmp[1];
              }
              if ($tp_fields[$Cada_cmp[0]] == 'dselect')
              {
                  $return_fields['set_dselect'][] = array('field' => $Cada_cmp[0], 'value' => $list_a);
              }
              elseif ($tp_fields[$Cada_cmp[0]] == 'fil_order')
              {
                  $return_fields['set_fil_order'][] = array('field' => $Cada_cmp[0], 'value' => $list);
              }
              elseif ($tp_fields[$Cada_cmp[0]] == 'selmult')
              {
                  $return_fields['set_selmult'][] = array('field' => $Cada_cmp[0], 'value' => $list);
              }
              elseif ($tp_fields[$Cada_cmp[0]] == 'checkbox')
              {
                  $return_fields['set_checkbox'][] = array('field' => $Cada_cmp[0], 'value' => $list);
              }
              else
              {
                  if (!isset($list[0]))
                  {
                      $list[0] = "";
                  }
                  if ($tp_fields[$Cada_cmp[0]] == 'html')
                  {
                      $return_fields['set_html'][] = array('field' => $Cada_cmp[0], 'value' => $list[0]);
                  }
                  elseif ($tp_fields[$Cada_cmp[0]] == 'radio')
                  {
                      $return_fields['set_radio'][] = array('field' => $Cada_cmp[0], 'value' => $list[0]);
                  }
                  else
                  {
                      $return_fields['set_val'][] = array('field' => $Cada_cmp[0], 'value' => $list[0]);
                  }
              }
          }
          $this->NM_curr_fil = $NM_filters;
      }
      return $return_fields;
   }
   function apaga_filtro()
   {
      global $NM_filters_del;
      if (isset($NM_filters_del) && !empty($NM_filters_del))
      { 
          $NM_patch = $this->NM_path_filter . "/" . $NM_filters_del;
          if (!is_file($NM_patch))
          {
              $NM_filters_del = sc_convert_encoding($NM_filters_del, "UTF-8", $_SESSION['scriptcase']['charset']);
              $NM_patch = $this->NM_path_filter . "/" . $NM_filters_del;
          }
          if (is_file($NM_patch))
          {
              @unlink($NM_patch);
          }
          if ($NM_filters_del == $this->NM_curr_fil)
          {
              $this->NM_curr_fil = "";
          }
      }
   }
   /**
    * @access  public
    */
   function trata_campos()
   {
      global $id_paciente_cond, $id_paciente, $id_paciente_input_2, $id_paciente_autocomp,
             $logro_comunicacion_gestion_cond, $logro_comunicacion_gestion, $logro_comunicacion_gestion_autocomp,
             $fecha_comunicacion_cond, $fecha_comunicacion, $fecha_comunicacion_autocomp,
             $autor_gestion_cond, $autor_gestion, $autor_gestion_autocomp,
             $departamento_paciente_cond, $departamento_paciente, $departamento_paciente_autocomp,
             $ciudad_paciente_cond, $ciudad_paciente, $ciudad_paciente_autocomp,
             $estado_paciente_cond, $estado_paciente, $estado_paciente_autocomp,
             $status_paciente_cond, $status_paciente, $status_paciente_autocomp,
             $fecha_activacion_paciente_cond, $fecha_activacion_paciente, $fecha_activacion_paciente_autocomp,
             $codigo_xofigo_cond, $codigo_xofigo, $codigo_xofigo_input_2, $codigo_xofigo_autocomp,
             $producto_tratamiento_cond, $producto_tratamiento, $producto_tratamiento_autocomp,
             $nombre_referencia_cond, $nombre_referencia, $nombre_referencia_autocomp,
             $dosis_tratamiento_cond, $dosis_tratamiento, $dosis_tratamiento_autocomp,
             $numero_cajas_cond, $numero_cajas, $numero_cajas_autocomp,
             $asegurador_tratamiento_cond, $asegurador_tratamiento, $asegurador_tratamiento_autocomp,
             $operador_logistico_tratamiento_cond, $operador_logistico_tratamiento, $operador_logistico_tratamiento_autocomp,
             $punto_entrega_cond, $punto_entrega, $punto_entrega_autocomp,
             $reclamo_gestion_cond, $reclamo_gestion, $reclamo_gestion_autocomp,
             $fecha_reclamacion_gestion_cond, $fecha_reclamacion_gestion, $fecha_reclamacion_gestion_autocomp,
             $causa_no_reclamacion_gestion_cond, $causa_no_reclamacion_gestion, $causa_no_reclamacion_gestion_autocomp,
             $id_historial_reclamacion_cond, $id_historial_reclamacion, $id_historial_reclamacion_input_2, $id_historial_reclamacion_autocomp,
             $anio_historial_reclamacion_cond, $anio_historial_reclamacion, $anio_historial_reclamacion_input_2, $anio_historial_reclamacion_autocomp, $nmgp_tab_label;

      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_gp_limpa.php", "F", "nm_limpa_valor") ; 
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_conv_dados.php", "F", "nm_conv_limpa_dado") ; 
      $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
      if (!empty($id_paciente_autocomp) && empty($id_paciente))
      {
          $id_paciente = $id_paciente_autocomp;
      }
      if (!empty($logro_comunicacion_gestion_autocomp) && empty($logro_comunicacion_gestion))
      {
          $logro_comunicacion_gestion = $logro_comunicacion_gestion_autocomp;
      }
      if (!empty($fecha_comunicacion_autocomp) && empty($fecha_comunicacion))
      {
          $fecha_comunicacion = $fecha_comunicacion_autocomp;
      }
      if (!empty($autor_gestion_autocomp) && empty($autor_gestion))
      {
          $autor_gestion = $autor_gestion_autocomp;
      }
      if (!empty($departamento_paciente_autocomp) && empty($departamento_paciente))
      {
          $departamento_paciente = $departamento_paciente_autocomp;
      }
      if (!empty($ciudad_paciente_autocomp) && empty($ciudad_paciente))
      {
          $ciudad_paciente = $ciudad_paciente_autocomp;
      }
      if (!empty($estado_paciente_autocomp) && empty($estado_paciente))
      {
          $estado_paciente = $estado_paciente_autocomp;
      }
      if (!empty($status_paciente_autocomp) && empty($status_paciente))
      {
          $status_paciente = $status_paciente_autocomp;
      }
      if (!empty($fecha_activacion_paciente_autocomp) && empty($fecha_activacion_paciente))
      {
          $fecha_activacion_paciente = $fecha_activacion_paciente_autocomp;
      }
      if (!empty($codigo_xofigo_autocomp) && empty($codigo_xofigo))
      {
          $codigo_xofigo = $codigo_xofigo_autocomp;
      }
      if (!empty($producto_tratamiento_autocomp) && empty($producto_tratamiento))
      {
          $producto_tratamiento = $producto_tratamiento_autocomp;
      }
      if (!empty($nombre_referencia_autocomp) && empty($nombre_referencia))
      {
          $nombre_referencia = $nombre_referencia_autocomp;
      }
      if (!empty($dosis_tratamiento_autocomp) && empty($dosis_tratamiento))
      {
          $dosis_tratamiento = $dosis_tratamiento_autocomp;
      }
      if (!empty($numero_cajas_autocomp) && empty($numero_cajas))
      {
          $numero_cajas = $numero_cajas_autocomp;
      }
      if (!empty($asegurador_tratamiento_autocomp) && empty($asegurador_tratamiento))
      {
          $asegurador_tratamiento = $asegurador_tratamiento_autocomp;
      }
      if (!empty($operador_logistico_tratamiento_autocomp) && empty($operador_logistico_tratamiento))
      {
          $operador_logistico_tratamiento = $operador_logistico_tratamiento_autocomp;
      }
      if (!empty($punto_entrega_autocomp) && empty($punto_entrega))
      {
          $punto_entrega = $punto_entrega_autocomp;
      }
      if (!empty($reclamo_gestion_autocomp) && empty($reclamo_gestion))
      {
          $reclamo_gestion = $reclamo_gestion_autocomp;
      }
      if (!empty($fecha_reclamacion_gestion_autocomp) && empty($fecha_reclamacion_gestion))
      {
          $fecha_reclamacion_gestion = $fecha_reclamacion_gestion_autocomp;
      }
      if (!empty($causa_no_reclamacion_gestion_autocomp) && empty($causa_no_reclamacion_gestion))
      {
          $causa_no_reclamacion_gestion = $causa_no_reclamacion_gestion_autocomp;
      }
      if (!empty($id_historial_reclamacion_autocomp) && empty($id_historial_reclamacion))
      {
          $id_historial_reclamacion = $id_historial_reclamacion_autocomp;
      }
      if (!empty($anio_historial_reclamacion_autocomp) && empty($anio_historial_reclamacion))
      {
          $anio_historial_reclamacion = $anio_historial_reclamacion_autocomp;
      }
      $id_paciente_cond_salva = $id_paciente_cond; 
      if (!isset($id_paciente_input_2) || $id_paciente_input_2 == "")
      {
          $id_paciente_input_2 = $id_paciente;
      }
      $logro_comunicacion_gestion_cond_salva = $logro_comunicacion_gestion_cond; 
      if (!isset($logro_comunicacion_gestion_input_2) || $logro_comunicacion_gestion_input_2 == "")
      {
          $logro_comunicacion_gestion_input_2 = $logro_comunicacion_gestion;
      }
      $fecha_comunicacion_cond_salva = $fecha_comunicacion_cond; 
      if (!isset($fecha_comunicacion_input_2) || $fecha_comunicacion_input_2 == "")
      {
          $fecha_comunicacion_input_2 = $fecha_comunicacion;
      }
      $autor_gestion_cond_salva = $autor_gestion_cond; 
      if (!isset($autor_gestion_input_2) || $autor_gestion_input_2 == "")
      {
          $autor_gestion_input_2 = $autor_gestion;
      }
      $departamento_paciente_cond_salva = $departamento_paciente_cond; 
      if (!isset($departamento_paciente_input_2) || $departamento_paciente_input_2 == "")
      {
          $departamento_paciente_input_2 = $departamento_paciente;
      }
      $ciudad_paciente_cond_salva = $ciudad_paciente_cond; 
      if (!isset($ciudad_paciente_input_2) || $ciudad_paciente_input_2 == "")
      {
          $ciudad_paciente_input_2 = $ciudad_paciente;
      }
      $estado_paciente_cond_salva = $estado_paciente_cond; 
      if (!isset($estado_paciente_input_2) || $estado_paciente_input_2 == "")
      {
          $estado_paciente_input_2 = $estado_paciente;
      }
      $status_paciente_cond_salva = $status_paciente_cond; 
      if (!isset($status_paciente_input_2) || $status_paciente_input_2 == "")
      {
          $status_paciente_input_2 = $status_paciente;
      }
      $fecha_activacion_paciente_cond_salva = $fecha_activacion_paciente_cond; 
      if (!isset($fecha_activacion_paciente_input_2) || $fecha_activacion_paciente_input_2 == "")
      {
          $fecha_activacion_paciente_input_2 = $fecha_activacion_paciente;
      }
      $codigo_xofigo_cond_salva = $codigo_xofigo_cond; 
      if (!isset($codigo_xofigo_input_2) || $codigo_xofigo_input_2 == "")
      {
          $codigo_xofigo_input_2 = $codigo_xofigo;
      }
      $producto_tratamiento_cond_salva = $producto_tratamiento_cond; 
      if (!isset($producto_tratamiento_input_2) || $producto_tratamiento_input_2 == "")
      {
          $producto_tratamiento_input_2 = $producto_tratamiento;
      }
      $nombre_referencia_cond_salva = $nombre_referencia_cond; 
      if (!isset($nombre_referencia_input_2) || $nombre_referencia_input_2 == "")
      {
          $nombre_referencia_input_2 = $nombre_referencia;
      }
      $dosis_tratamiento_cond_salva = $dosis_tratamiento_cond; 
      if (!isset($dosis_tratamiento_input_2) || $dosis_tratamiento_input_2 == "")
      {
          $dosis_tratamiento_input_2 = $dosis_tratamiento;
      }
      $numero_cajas_cond_salva = $numero_cajas_cond; 
      if (!isset($numero_cajas_input_2) || $numero_cajas_input_2 == "")
      {
          $numero_cajas_input_2 = $numero_cajas;
      }
      $asegurador_tratamiento_cond_salva = $asegurador_tratamiento_cond; 
      if (!isset($asegurador_tratamiento_input_2) || $asegurador_tratamiento_input_2 == "")
      {
          $asegurador_tratamiento_input_2 = $asegurador_tratamiento;
      }
      $operador_logistico_tratamiento_cond_salva = $operador_logistico_tratamiento_cond; 
      if (!isset($operador_logistico_tratamiento_input_2) || $operador_logistico_tratamiento_input_2 == "")
      {
          $operador_logistico_tratamiento_input_2 = $operador_logistico_tratamiento;
      }
      $punto_entrega_cond_salva = $punto_entrega_cond; 
      if (!isset($punto_entrega_input_2) || $punto_entrega_input_2 == "")
      {
          $punto_entrega_input_2 = $punto_entrega;
      }
      $reclamo_gestion_cond_salva = $reclamo_gestion_cond; 
      if (!isset($reclamo_gestion_input_2) || $reclamo_gestion_input_2 == "")
      {
          $reclamo_gestion_input_2 = $reclamo_gestion;
      }
      $fecha_reclamacion_gestion_cond_salva = $fecha_reclamacion_gestion_cond; 
      if (!isset($fecha_reclamacion_gestion_input_2) || $fecha_reclamacion_gestion_input_2 == "")
      {
          $fecha_reclamacion_gestion_input_2 = $fecha_reclamacion_gestion;
      }
      $causa_no_reclamacion_gestion_cond_salva = $causa_no_reclamacion_gestion_cond; 
      if (!isset($causa_no_reclamacion_gestion_input_2) || $causa_no_reclamacion_gestion_input_2 == "")
      {
          $causa_no_reclamacion_gestion_input_2 = $causa_no_reclamacion_gestion;
      }
      $id_historial_reclamacion_cond_salva = $id_historial_reclamacion_cond; 
      if (!isset($id_historial_reclamacion_input_2) || $id_historial_reclamacion_input_2 == "")
      {
          $id_historial_reclamacion_input_2 = $id_historial_reclamacion;
      }
      $anio_historial_reclamacion_cond_salva = $anio_historial_reclamacion_cond; 
      if (!isset($anio_historial_reclamacion_input_2) || $anio_historial_reclamacion_input_2 == "")
      {
          $anio_historial_reclamacion_input_2 = $anio_historial_reclamacion;
      }
      if ($id_paciente_cond != "in")
      {
          nm_limpa_numero($id_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $id_paciente);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $id_paciente = implode(",", $Nm_sc_valores);
      }
      if ($id_paciente_cond != "in")
      {
          nm_limpa_numero($id_paciente_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      if ($codigo_xofigo_cond != "in")
      {
          nm_limpa_numero($codigo_xofigo, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $codigo_xofigo);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $codigo_xofigo = implode(",", $Nm_sc_valores);
      }
      if ($codigo_xofigo_cond != "in")
      {
          nm_limpa_numero($codigo_xofigo_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      if ($id_historial_reclamacion_cond != "in")
      {
          nm_limpa_numero($id_historial_reclamacion, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $id_historial_reclamacion);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $id_historial_reclamacion = implode(",", $Nm_sc_valores);
      }
      if ($id_historial_reclamacion_cond != "in")
      {
          nm_limpa_numero($id_historial_reclamacion_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      if ($anio_historial_reclamacion_cond != "in")
      {
          nm_limpa_numero($anio_historial_reclamacion, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      else
      {
          $Nm_sc_valores = explode(",", $anio_historial_reclamacion);
          foreach ($Nm_sc_valores as $II => $Nm_sc_valor)
          {
              $Nm_sc_valor = trim($Nm_sc_valor);
              nm_limpa_numero($Nm_sc_valor, $_SESSION['scriptcase']['reg_conf']['grup_num']); 
              $Nm_sc_valores[$II] = $Nm_sc_valor;
          }
          $anio_historial_reclamacion = implode(",", $Nm_sc_valores);
      }
      if ($anio_historial_reclamacion_cond != "in")
      {
          nm_limpa_numero($anio_historial_reclamacion_input_2, $_SESSION['scriptcase']['reg_conf']['grup_num']) ; 
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']  = array(); 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_paciente'] = $id_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_paciente_input_2'] = $id_paciente_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_paciente_cond'] = $id_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['logro_comunicacion_gestion'] = $logro_comunicacion_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['logro_comunicacion_gestion_cond'] = $logro_comunicacion_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_comunicacion'] = $fecha_comunicacion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_comunicacion_cond'] = $fecha_comunicacion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['autor_gestion'] = $autor_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['autor_gestion_cond'] = $autor_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['departamento_paciente'] = $departamento_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['departamento_paciente_cond'] = $departamento_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['ciudad_paciente'] = $ciudad_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['ciudad_paciente_cond'] = $ciudad_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['estado_paciente'] = $estado_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['estado_paciente_cond'] = $estado_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['status_paciente'] = $status_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['status_paciente_cond'] = $status_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_activacion_paciente'] = $fecha_activacion_paciente; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_activacion_paciente_cond'] = $fecha_activacion_paciente_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['codigo_xofigo'] = $codigo_xofigo; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['codigo_xofigo_input_2'] = $codigo_xofigo_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['codigo_xofigo_cond'] = $codigo_xofigo_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['producto_tratamiento'] = $producto_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['producto_tratamiento_cond'] = $producto_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['nombre_referencia'] = $nombre_referencia; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['nombre_referencia_cond'] = $nombre_referencia_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['dosis_tratamiento'] = $dosis_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['dosis_tratamiento_cond'] = $dosis_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['numero_cajas'] = $numero_cajas; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['numero_cajas_cond'] = $numero_cajas_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['asegurador_tratamiento'] = $asegurador_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['asegurador_tratamiento_cond'] = $asegurador_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['operador_logistico_tratamiento'] = $operador_logistico_tratamiento; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['operador_logistico_tratamiento_cond'] = $operador_logistico_tratamiento_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['punto_entrega'] = $punto_entrega; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['punto_entrega_cond'] = $punto_entrega_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['reclamo_gestion'] = $reclamo_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['reclamo_gestion_cond'] = $reclamo_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_reclamacion_gestion'] = $fecha_reclamacion_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['fecha_reclamacion_gestion_cond'] = $fecha_reclamacion_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['causa_no_reclamacion_gestion'] = $causa_no_reclamacion_gestion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['causa_no_reclamacion_gestion_cond'] = $causa_no_reclamacion_gestion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_historial_reclamacion'] = $id_historial_reclamacion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_historial_reclamacion_input_2'] = $id_historial_reclamacion_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['id_historial_reclamacion_cond'] = $id_historial_reclamacion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['anio_historial_reclamacion'] = $anio_historial_reclamacion; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['anio_historial_reclamacion_input_2'] = $anio_historial_reclamacion_input_2; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['anio_historial_reclamacion_cond'] = $anio_historial_reclamacion_cond_salva; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']['NM_operador'] = $this->NM_operador; 
      if ($id_paciente_cond != "in" && $id_paciente_cond != "bw" && !empty($id_paciente) && !is_numeric($id_paciente))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID PACIENTE";
      }
      if ($id_paciente_cond == "bw" && ((!empty($id_paciente) && !is_numeric($id_paciente)) || (!empty($id_paciente_input_2) && !is_numeric($id_paciente_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID PACIENTE";
      }
      if ($codigo_xofigo_cond != "in" && $codigo_xofigo_cond != "bw" && !empty($codigo_xofigo) && !is_numeric($codigo_xofigo))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : CODIGO XOFIGO";
      }
      if ($codigo_xofigo_cond == "bw" && ((!empty($codigo_xofigo) && !is_numeric($codigo_xofigo)) || (!empty($codigo_xofigo_input_2) && !is_numeric($codigo_xofigo_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : CODIGO XOFIGO";
      }
      if ($id_historial_reclamacion_cond != "in" && $id_historial_reclamacion_cond != "bw" && !empty($id_historial_reclamacion) && !is_numeric($id_historial_reclamacion))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID HISTORIAL RECLAMACION";
      }
      if ($id_historial_reclamacion_cond == "bw" && ((!empty($id_historial_reclamacion) && !is_numeric($id_historial_reclamacion)) || (!empty($id_historial_reclamacion_input_2) && !is_numeric($id_historial_reclamacion_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ID HISTORIAL RECLAMACION";
      }
      if ($anio_historial_reclamacion_cond != "in" && $anio_historial_reclamacion_cond != "bw" && !empty($anio_historial_reclamacion) && !is_numeric($anio_historial_reclamacion))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ANIO HISTORIAL RECLAMACION";
      }
      if ($anio_historial_reclamacion_cond == "bw" && ((!empty($anio_historial_reclamacion) && !is_numeric($anio_historial_reclamacion)) || (!empty($anio_historial_reclamacion_input_2) && !is_numeric($anio_historial_reclamacion_input_2)) ))
      {
          if (!empty($this->Campos_Mens_erro)){$this->Campos_Mens_erro .= "<br>";}$this->Campos_Mens_erro .= $this->Ini->Nm_lang['lang_errm_ajax_data'] . " : ANIO HISTORIAL RECLAMACION";
      }
      if (!empty($this->Campos_Mens_erro)) 
      {
          return;
      }
      $id_paciente_look = substr($this->Db->qstr($id_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($id_paciente))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ID_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_PACIENTE = $id_paciente_look"; 
      }
      else
      {
          $nm_comando = "select distinct ID_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_PACIENTE = $id_paciente_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['id_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['id_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['id_paciente'] = $id_paciente;
      }
      $Conteudo = $id_paciente_input_2;
      if (strtoupper($id_paciente_cond) != "II" && strtoupper($id_paciente_cond) != "QP" && strtoupper($id_paciente_cond) != "NP" && strtoupper($id_paciente_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['id_paciente_input_2'] = $Conteudo;
      $logro_comunicacion_gestion_look = substr($this->Db->qstr($logro_comunicacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct LOGRO_COMUNICACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where LOGRO_COMUNICACION_GESTION = '$logro_comunicacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['logro_comunicacion_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['logro_comunicacion_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['logro_comunicacion_gestion'] = $logro_comunicacion_gestion;
      }
      $fecha_comunicacion_look = substr($this->Db->qstr($fecha_comunicacion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_COMUNICACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where FECHA_COMUNICACION = '$fecha_comunicacion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['fecha_comunicacion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['fecha_comunicacion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['fecha_comunicacion'] = $fecha_comunicacion;
      }
      $autor_gestion_look = substr($this->Db->qstr($autor_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct AUTOR_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where AUTOR_GESTION = '$autor_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['autor_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['autor_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['autor_gestion'] = $autor_gestion;
      }
      $departamento_paciente_look = substr($this->Db->qstr($departamento_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct DEPARTAMENTO_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where DEPARTAMENTO_PACIENTE = '$departamento_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['departamento_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['departamento_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['departamento_paciente'] = $departamento_paciente;
      }
      $ciudad_paciente_look = substr($this->Db->qstr($ciudad_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct CIUDAD_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CIUDAD_PACIENTE = '$ciudad_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['ciudad_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['ciudad_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['ciudad_paciente'] = $ciudad_paciente;
      }
      $estado_paciente_look = substr($this->Db->qstr($estado_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct ESTADO_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ESTADO_PACIENTE = '$estado_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['estado_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['estado_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['estado_paciente'] = $estado_paciente;
      }
      $status_paciente_look = substr($this->Db->qstr($status_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct STATUS_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where STATUS_PACIENTE = '$status_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['status_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['status_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['status_paciente'] = $status_paciente;
      }
      $fecha_activacion_paciente_look = substr($this->Db->qstr($fecha_activacion_paciente), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_ACTIVACION_PACIENTE from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where FECHA_ACTIVACION_PACIENTE = '$fecha_activacion_paciente_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['fecha_activacion_paciente'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['fecha_activacion_paciente'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['fecha_activacion_paciente'] = $fecha_activacion_paciente;
      }
      $codigo_xofigo_look = substr($this->Db->qstr($codigo_xofigo), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($codigo_xofigo))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct CODIGO_XOFIGO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CODIGO_XOFIGO = $codigo_xofigo_look"; 
      }
      else
      {
          $nm_comando = "select distinct CODIGO_XOFIGO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CODIGO_XOFIGO = $codigo_xofigo_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['codigo_xofigo'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['codigo_xofigo'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['codigo_xofigo'] = $codigo_xofigo;
      }
      $Conteudo = $codigo_xofigo_input_2;
      if (strtoupper($codigo_xofigo_cond) != "II" && strtoupper($codigo_xofigo_cond) != "QP" && strtoupper($codigo_xofigo_cond) != "NP" && strtoupper($codigo_xofigo_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['codigo_xofigo_input_2'] = $Conteudo;
      $producto_tratamiento_look = substr($this->Db->qstr($producto_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct PRODUCTO_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where PRODUCTO_TRATAMIENTO = '$producto_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['producto_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['producto_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['producto_tratamiento'] = $producto_tratamiento;
      }
      $nombre_referencia_look = substr($this->Db->qstr($nombre_referencia), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct NOMBRE_REFERENCIA from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where NOMBRE_REFERENCIA = '$nombre_referencia_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['nombre_referencia'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['nombre_referencia'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['nombre_referencia'] = $nombre_referencia;
      }
      $dosis_tratamiento_look = substr($this->Db->qstr($dosis_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct DOSIS_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where DOSIS_TRATAMIENTO = '$dosis_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['dosis_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['dosis_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['dosis_tratamiento'] = $dosis_tratamiento;
      }
      $numero_cajas_look = substr($this->Db->qstr($numero_cajas), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct NUMERO_CAJAS from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where NUMERO_CAJAS = '$numero_cajas_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['numero_cajas'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['numero_cajas'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['numero_cajas'] = $numero_cajas;
      }
      $asegurador_tratamiento_look = substr($this->Db->qstr($asegurador_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct ASEGURADOR_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ASEGURADOR_TRATAMIENTO = '$asegurador_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['asegurador_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['asegurador_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['asegurador_tratamiento'] = $asegurador_tratamiento;
      }
      $operador_logistico_tratamiento_look = substr($this->Db->qstr($operador_logistico_tratamiento), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct OPERADOR_LOGISTICO_TRATAMIENTO from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where OPERADOR_LOGISTICO_TRATAMIENTO = '$operador_logistico_tratamiento_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['operador_logistico_tratamiento'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['operador_logistico_tratamiento'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['operador_logistico_tratamiento'] = $operador_logistico_tratamiento;
      }
      $punto_entrega_look = substr($this->Db->qstr($punto_entrega), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct PUNTO_ENTREGA from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where PUNTO_ENTREGA = '$punto_entrega_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['punto_entrega'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['punto_entrega'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['punto_entrega'] = $punto_entrega;
      }
      $reclamo_gestion_look = substr($this->Db->qstr($reclamo_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct RECLAMO_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where RECLAMO_GESTION = '$reclamo_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['reclamo_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['reclamo_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['reclamo_gestion'] = $reclamo_gestion;
      }
      $fecha_reclamacion_gestion_look = substr($this->Db->qstr($fecha_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct FECHA_RECLAMACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where FECHA_RECLAMACION_GESTION = '$fecha_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['fecha_reclamacion_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['fecha_reclamacion_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['fecha_reclamacion_gestion'] = $fecha_reclamacion_gestion;
      }
      $causa_no_reclamacion_gestion_look = substr($this->Db->qstr($causa_no_reclamacion_gestion), 1, -1); 
      $nmgp_def_dados = array(); 
      $nm_comando = "select distinct CAUSA_NO_RECLAMACION_GESTION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where CAUSA_NO_RECLAMACION_GESTION = '$causa_no_reclamacion_gestion_look'"; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['causa_no_reclamacion_gestion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['causa_no_reclamacion_gestion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['causa_no_reclamacion_gestion'] = $causa_no_reclamacion_gestion;
      }
      $id_historial_reclamacion_look = substr($this->Db->qstr($id_historial_reclamacion), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($id_historial_reclamacion))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ID_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_HISTORIAL_RECLAMACION = $id_historial_reclamacion_look"; 
      }
      else
      {
          $nm_comando = "select distinct ID_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ID_HISTORIAL_RECLAMACION = $id_historial_reclamacion_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['id_historial_reclamacion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['id_historial_reclamacion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['id_historial_reclamacion'] = $id_historial_reclamacion;
      }
      $Conteudo = $id_historial_reclamacion_input_2;
      if (strtoupper($id_historial_reclamacion_cond) != "II" && strtoupper($id_historial_reclamacion_cond) != "QP" && strtoupper($id_historial_reclamacion_cond) != "NP" && strtoupper($id_historial_reclamacion_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['id_historial_reclamacion_input_2'] = $Conteudo;
      $anio_historial_reclamacion_look = substr($this->Db->qstr($anio_historial_reclamacion), 1, -1); 
      $nmgp_def_dados = array(); 
   if (is_numeric($anio_historial_reclamacion))
   { 
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
      {
          $nm_comando = "select distinct ANIO_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ANIO_HISTORIAL_RECLAMACION = $anio_historial_reclamacion_look"; 
      }
      else
      {
          $nm_comando = "select distinct ANIO_HISTORIAL_RECLAMACION from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp where ANIO_HISTORIAL_RECLAMACION = $anio_historial_reclamacion_look"; 
      }
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando; 
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      if ($rs = $this->Db->Execute($nm_comando)) 
      { 
         while (!$rs->EOF) 
         { 
            nmgp_Form_Num_Val($rs->fields[0], $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
            $cmp1 = NM_charset_to_utf8(trim($rs->fields[0]));
            $nmgp_def_dados[] = array($cmp1 => $cmp1); 
            $rs->MoveNext() ; 
         } 
         $rs->Close() ; 
      } 
      else  
      {  
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit; 
      } 
   } 

      if (!empty($nmgp_def_dados) && isset($cmp2) && !empty($cmp2))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp2 = NM_conv_charset($cmp2, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['anio_historial_reclamacion'] = $cmp2;
      }
      elseif (!empty($nmgp_def_dados) && isset($cmp1) && !empty($cmp1))
      {
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
             $cmp1 = NM_conv_charset($cmp1, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->cmp_formatado['anio_historial_reclamacion'] = $cmp1;
      }
      else
      {
          $this->cmp_formatado['anio_historial_reclamacion'] = $anio_historial_reclamacion;
      }
      $Conteudo = $anio_historial_reclamacion_input_2;
      if (strtoupper($anio_historial_reclamacion_cond) != "II" && strtoupper($anio_historial_reclamacion_cond) != "QP" && strtoupper($anio_historial_reclamacion_cond) != "NP" && strtoupper($anio_historial_reclamacion_cond) != "IN") 
      { 
          nmgp_Form_Num_Val($Conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "1", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
      } 
      $this->cmp_formatado['anio_historial_reclamacion_input_2'] = $Conteudo;

      //----- $id_paciente
      $this->Date_part = false;
      if (isset($id_paciente) || $id_paciente_cond == "nu" || $id_paciente_cond == "nn" || $id_paciente_cond == "ep" || $id_paciente_cond == "ne")
      {
         $this->monta_condicao("ID_PACIENTE", $id_paciente_cond, $id_paciente, $id_paciente_input_2, "id_paciente");
      }

      //----- $logro_comunicacion_gestion
      $this->Date_part = false;
      if (isset($logro_comunicacion_gestion) || $logro_comunicacion_gestion_cond == "nu" || $logro_comunicacion_gestion_cond == "nn" || $logro_comunicacion_gestion_cond == "ep" || $logro_comunicacion_gestion_cond == "ne")
      {
         $this->monta_condicao("LOGRO_COMUNICACION_GESTION", $logro_comunicacion_gestion_cond, $logro_comunicacion_gestion, "", "logro_comunicacion_gestion");
      }

      //----- $fecha_comunicacion
      $this->Date_part = false;
      if (isset($fecha_comunicacion) || $fecha_comunicacion_cond == "nu" || $fecha_comunicacion_cond == "nn" || $fecha_comunicacion_cond == "ep" || $fecha_comunicacion_cond == "ne")
      {
         $this->monta_condicao("FECHA_COMUNICACION", $fecha_comunicacion_cond, $fecha_comunicacion, "", "fecha_comunicacion");
      }

      //----- $autor_gestion
      $this->Date_part = false;
      if (isset($autor_gestion) || $autor_gestion_cond == "nu" || $autor_gestion_cond == "nn" || $autor_gestion_cond == "ep" || $autor_gestion_cond == "ne")
      {
         $this->monta_condicao("AUTOR_GESTION", $autor_gestion_cond, $autor_gestion, "", "autor_gestion");
      }

      //----- $departamento_paciente
      $this->Date_part = false;
      if (isset($departamento_paciente) || $departamento_paciente_cond == "nu" || $departamento_paciente_cond == "nn" || $departamento_paciente_cond == "ep" || $departamento_paciente_cond == "ne")
      {
         $this->monta_condicao("DEPARTAMENTO_PACIENTE", $departamento_paciente_cond, $departamento_paciente, "", "departamento_paciente");
      }

      //----- $ciudad_paciente
      $this->Date_part = false;
      if (isset($ciudad_paciente) || $ciudad_paciente_cond == "nu" || $ciudad_paciente_cond == "nn" || $ciudad_paciente_cond == "ep" || $ciudad_paciente_cond == "ne")
      {
         $this->monta_condicao("CIUDAD_PACIENTE", $ciudad_paciente_cond, $ciudad_paciente, "", "ciudad_paciente");
      }

      //----- $estado_paciente
      $this->Date_part = false;
      if (isset($estado_paciente) || $estado_paciente_cond == "nu" || $estado_paciente_cond == "nn" || $estado_paciente_cond == "ep" || $estado_paciente_cond == "ne")
      {
         $this->monta_condicao("ESTADO_PACIENTE", $estado_paciente_cond, $estado_paciente, "", "estado_paciente");
      }

      //----- $status_paciente
      $this->Date_part = false;
      if (isset($status_paciente) || $status_paciente_cond == "nu" || $status_paciente_cond == "nn" || $status_paciente_cond == "ep" || $status_paciente_cond == "ne")
      {
         $this->monta_condicao("STATUS_PACIENTE", $status_paciente_cond, $status_paciente, "", "status_paciente");
      }

      //----- $fecha_activacion_paciente
      $this->Date_part = false;
      if (isset($fecha_activacion_paciente) || $fecha_activacion_paciente_cond == "nu" || $fecha_activacion_paciente_cond == "nn" || $fecha_activacion_paciente_cond == "ep" || $fecha_activacion_paciente_cond == "ne")
      {
         $this->monta_condicao("FECHA_ACTIVACION_PACIENTE", $fecha_activacion_paciente_cond, $fecha_activacion_paciente, "", "fecha_activacion_paciente");
      }

      //----- $codigo_xofigo
      $this->Date_part = false;
      if (isset($codigo_xofigo) || $codigo_xofigo_cond == "nu" || $codigo_xofigo_cond == "nn" || $codigo_xofigo_cond == "ep" || $codigo_xofigo_cond == "ne")
      {
         $this->monta_condicao("CODIGO_XOFIGO", $codigo_xofigo_cond, $codigo_xofigo, $codigo_xofigo_input_2, "codigo_xofigo");
      }

      //----- $producto_tratamiento
      $this->Date_part = false;
      if (isset($producto_tratamiento) || $producto_tratamiento_cond == "nu" || $producto_tratamiento_cond == "nn" || $producto_tratamiento_cond == "ep" || $producto_tratamiento_cond == "ne")
      {
         $this->monta_condicao("PRODUCTO_TRATAMIENTO", $producto_tratamiento_cond, $producto_tratamiento, "", "producto_tratamiento");
      }

      //----- $nombre_referencia
      $this->Date_part = false;
      if (isset($nombre_referencia) || $nombre_referencia_cond == "nu" || $nombre_referencia_cond == "nn" || $nombre_referencia_cond == "ep" || $nombre_referencia_cond == "ne")
      {
         $this->monta_condicao("NOMBRE_REFERENCIA", $nombre_referencia_cond, $nombre_referencia, "", "nombre_referencia");
      }

      //----- $dosis_tratamiento
      $this->Date_part = false;
      if (isset($dosis_tratamiento) || $dosis_tratamiento_cond == "nu" || $dosis_tratamiento_cond == "nn" || $dosis_tratamiento_cond == "ep" || $dosis_tratamiento_cond == "ne")
      {
         $this->monta_condicao("DOSIS_TRATAMIENTO", $dosis_tratamiento_cond, $dosis_tratamiento, "", "dosis_tratamiento");
      }

      //----- $numero_cajas
      $this->Date_part = false;
      if (isset($numero_cajas) || $numero_cajas_cond == "nu" || $numero_cajas_cond == "nn" || $numero_cajas_cond == "ep" || $numero_cajas_cond == "ne")
      {
         $this->monta_condicao("NUMERO_CAJAS", $numero_cajas_cond, $numero_cajas, "", "numero_cajas");
      }

      //----- $asegurador_tratamiento
      $this->Date_part = false;
      if (isset($asegurador_tratamiento) || $asegurador_tratamiento_cond == "nu" || $asegurador_tratamiento_cond == "nn" || $asegurador_tratamiento_cond == "ep" || $asegurador_tratamiento_cond == "ne")
      {
         $this->monta_condicao("ASEGURADOR_TRATAMIENTO", $asegurador_tratamiento_cond, $asegurador_tratamiento, "", "asegurador_tratamiento");
      }

      //----- $operador_logistico_tratamiento
      $this->Date_part = false;
      if (isset($operador_logistico_tratamiento) || $operador_logistico_tratamiento_cond == "nu" || $operador_logistico_tratamiento_cond == "nn" || $operador_logistico_tratamiento_cond == "ep" || $operador_logistico_tratamiento_cond == "ne")
      {
         $this->monta_condicao("OPERADOR_LOGISTICO_TRATAMIENTO", $operador_logistico_tratamiento_cond, $operador_logistico_tratamiento, "", "operador_logistico_tratamiento");
      }

      //----- $punto_entrega
      $this->Date_part = false;
      if (isset($punto_entrega) || $punto_entrega_cond == "nu" || $punto_entrega_cond == "nn" || $punto_entrega_cond == "ep" || $punto_entrega_cond == "ne")
      {
         $this->monta_condicao("PUNTO_ENTREGA", $punto_entrega_cond, $punto_entrega, "", "punto_entrega");
      }

      //----- $reclamo_gestion
      $this->Date_part = false;
      if (isset($reclamo_gestion) || $reclamo_gestion_cond == "nu" || $reclamo_gestion_cond == "nn" || $reclamo_gestion_cond == "ep" || $reclamo_gestion_cond == "ne")
      {
         $this->monta_condicao("RECLAMO_GESTION", $reclamo_gestion_cond, $reclamo_gestion, "", "reclamo_gestion");
      }

      //----- $fecha_reclamacion_gestion
      $this->Date_part = false;
      if (isset($fecha_reclamacion_gestion) || $fecha_reclamacion_gestion_cond == "nu" || $fecha_reclamacion_gestion_cond == "nn" || $fecha_reclamacion_gestion_cond == "ep" || $fecha_reclamacion_gestion_cond == "ne")
      {
         $this->monta_condicao("FECHA_RECLAMACION_GESTION", $fecha_reclamacion_gestion_cond, $fecha_reclamacion_gestion, "", "fecha_reclamacion_gestion");
      }

      //----- $causa_no_reclamacion_gestion
      $this->Date_part = false;
      if (isset($causa_no_reclamacion_gestion) || $causa_no_reclamacion_gestion_cond == "nu" || $causa_no_reclamacion_gestion_cond == "nn" || $causa_no_reclamacion_gestion_cond == "ep" || $causa_no_reclamacion_gestion_cond == "ne")
      {
         $this->monta_condicao("CAUSA_NO_RECLAMACION_GESTION", $causa_no_reclamacion_gestion_cond, $causa_no_reclamacion_gestion, "", "causa_no_reclamacion_gestion");
      }

      //----- $id_historial_reclamacion
      $this->Date_part = false;
      if (isset($id_historial_reclamacion) || $id_historial_reclamacion_cond == "nu" || $id_historial_reclamacion_cond == "nn" || $id_historial_reclamacion_cond == "ep" || $id_historial_reclamacion_cond == "ne")
      {
         $this->monta_condicao("ID_HISTORIAL_RECLAMACION", $id_historial_reclamacion_cond, $id_historial_reclamacion, $id_historial_reclamacion_input_2, "id_historial_reclamacion");
      }

      //----- $anio_historial_reclamacion
      $this->Date_part = false;
      if (isset($anio_historial_reclamacion) || $anio_historial_reclamacion_cond == "nu" || $anio_historial_reclamacion_cond == "nn" || $anio_historial_reclamacion_cond == "ep" || $anio_historial_reclamacion_cond == "ne")
      {
         $this->monta_condicao("ANIO_HISTORIAL_RECLAMACION", $anio_historial_reclamacion_cond, $anio_historial_reclamacion, $anio_historial_reclamacion_input_2, "anio_historial_reclamacion");
      }
   }

   /**
    * @access  public
    */
   function finaliza_resultado()
   {
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_fast'] = "";
      if ("" == $this->comando_filtro)
      {
          $this->comando = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig'];
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']) && $_SESSION['scriptcase']['charset'] != "UTF-8")
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'] = NM_conv_charset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'], "UTF-8", $_SESSION['scriptcase']['charset']);
      }

      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_lookup']  = $this->comando_sum . $this->comando_fim;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq']         = $this->comando . $this->comando_fim;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['opcao']              = "pesq";
      if ("" == $this->comando_filtro)
      {
         $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_filtro'] = "";
      }
      else
      {
         $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_filtro'] = " (" . $this->comando_filtro . ")";
      }
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'] != $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_ant'])
      {
         $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cond_pesq'] .= $this->NM_operador;
         $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['contr_array_resumo'] = "NAO";
         $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['contr_total_geral']  = "NAO";
         unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['tot_geral']);
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_ant'] = $this->comando . $this->comando_fim;
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['fast_search']);

      $this->retorna_pesq();
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}

?>
