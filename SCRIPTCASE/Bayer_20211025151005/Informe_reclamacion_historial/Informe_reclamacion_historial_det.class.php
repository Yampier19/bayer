<?php
//--- 
class Informe_reclamacion_historial_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $nmgp_botoes; 
   var $nm_location;
   var $id_paciente;
   var $logro_comunicacion_gestion;
   var $fecha_comunicacion;
   var $autor_gestion;
   var $departamento_paciente;
   var $ciudad_paciente;
   var $estado_paciente;
   var $status_paciente;
   var $fecha_activacion_paciente;
   var $codigo_xofigo;
   var $producto_tratamiento;
   var $nombre_referencia;
   var $dosis_tratamiento;
   var $numero_cajas;
   var $asegurador_tratamiento;
   var $operador_logistico_tratamiento;
   var $punto_entrega;
   var $reclamo_gestion;
   var $fecha_reclamacion_gestion;
   var $causa_no_reclamacion_gestion;
   var $id_historial_reclamacion;
   var $anio_historial_reclamacion;
   var $mes1;
   var $reclamo1;
   var $fecha_reclamacion1;
   var $motivo_no_reclamacion1;
   var $mes2;
   var $reclamo2;
   var $fecha_reclamacion2;
   var $motivo_no_reclamacion2;
   var $mes3;
   var $reclamo3;
   var $fecha_reclamacion3;
   var $motivo_no_reclamacion3;
   var $mes4;
   var $reclamo4;
   var $fecha_reclamacion4;
   var $motivo_no_reclamacion4;
   var $mes5;
   var $reclamo5;
   var $fecha_reclamacion5;
   var $motivo_no_reclamacion5;
   var $mes6;
   var $reclamo6;
   var $fecha_reclamacion6;
   var $motivo_no_reclamacion6;
   var $mes7;
   var $reclamo7;
   var $fecha_reclamacion7;
   var $motivo_no_reclamacion7;
   var $mes8;
   var $reclamo8;
   var $fecha_reclamacion8;
   var $motivo_no_reclamacion8;
   var $mes9;
   var $reclamo9;
   var $fecha_reclamacion9;
   var $motivo_no_reclamacion9;
   var $mes10;
   var $reclamo10;
   var $fecha_reclamacion10;
   var $motivo_no_reclamacion10;
   var $mes11;
   var $reclamo11;
   var $fecha_reclamacion11;
   var $motivo_no_reclamacion11;
   var $mes12;
   var $reclamo12;
   var $fecha_reclamacion12;
   var $motivo_no_reclamacion12;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    $this->nmgp_botoes['det_pdf'] = "on";
    $this->nmgp_botoes['det_print'] = "on";
    $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
    if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['btn_display']))
    {
        foreach ($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion_historial']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
        {
            $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
        }
    }
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca']))
    { 
        $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['campos_busca'];
        if ($_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        $this->id_paciente = $Busca_temp['id_paciente']; 
        $tmp_pos = strpos($this->id_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
        }
        $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
        $this->logro_comunicacion_gestion = $Busca_temp['logro_comunicacion_gestion']; 
        $tmp_pos = strpos($this->logro_comunicacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->logro_comunicacion_gestion = substr($this->logro_comunicacion_gestion, 0, $tmp_pos);
        }
        $this->fecha_comunicacion = $Busca_temp['fecha_comunicacion']; 
        $tmp_pos = strpos($this->fecha_comunicacion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->fecha_comunicacion = substr($this->fecha_comunicacion, 0, $tmp_pos);
        }
        $this->autor_gestion = $Busca_temp['autor_gestion']; 
        $tmp_pos = strpos($this->autor_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->autor_gestion = substr($this->autor_gestion, 0, $tmp_pos);
        }
        $this->departamento_paciente = $Busca_temp['departamento_paciente']; 
        $tmp_pos = strpos($this->departamento_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->departamento_paciente = substr($this->departamento_paciente, 0, $tmp_pos);
        }
        $this->ciudad_paciente = $Busca_temp['ciudad_paciente']; 
        $tmp_pos = strpos($this->ciudad_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->ciudad_paciente = substr($this->ciudad_paciente, 0, $tmp_pos);
        }
        $this->estado_paciente = $Busca_temp['estado_paciente']; 
        $tmp_pos = strpos($this->estado_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->estado_paciente = substr($this->estado_paciente, 0, $tmp_pos);
        }
        $this->status_paciente = $Busca_temp['status_paciente']; 
        $tmp_pos = strpos($this->status_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->status_paciente = substr($this->status_paciente, 0, $tmp_pos);
        }
        $this->fecha_activacion_paciente = $Busca_temp['fecha_activacion_paciente']; 
        $tmp_pos = strpos($this->fecha_activacion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->fecha_activacion_paciente = substr($this->fecha_activacion_paciente, 0, $tmp_pos);
        }
        $this->codigo_xofigo = $Busca_temp['codigo_xofigo']; 
        $tmp_pos = strpos($this->codigo_xofigo, "##@@");
        if ($tmp_pos !== false)
        {
            $this->codigo_xofigo = substr($this->codigo_xofigo, 0, $tmp_pos);
        }
        $this->codigo_xofigo_2 = $Busca_temp['codigo_xofigo_input_2']; 
        $this->producto_tratamiento = $Busca_temp['producto_tratamiento']; 
        $tmp_pos = strpos($this->producto_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->producto_tratamiento = substr($this->producto_tratamiento, 0, $tmp_pos);
        }
        $this->nombre_referencia = $Busca_temp['nombre_referencia']; 
        $tmp_pos = strpos($this->nombre_referencia, "##@@");
        if ($tmp_pos !== false)
        {
            $this->nombre_referencia = substr($this->nombre_referencia, 0, $tmp_pos);
        }
        $this->dosis_tratamiento = $Busca_temp['dosis_tratamiento']; 
        $tmp_pos = strpos($this->dosis_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->dosis_tratamiento = substr($this->dosis_tratamiento, 0, $tmp_pos);
        }
        $this->numero_cajas = $Busca_temp['numero_cajas']; 
        $tmp_pos = strpos($this->numero_cajas, "##@@");
        if ($tmp_pos !== false)
        {
            $this->numero_cajas = substr($this->numero_cajas, 0, $tmp_pos);
        }
        $this->asegurador_tratamiento = $Busca_temp['asegurador_tratamiento']; 
        $tmp_pos = strpos($this->asegurador_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->asegurador_tratamiento = substr($this->asegurador_tratamiento, 0, $tmp_pos);
        }
        $this->operador_logistico_tratamiento = $Busca_temp['operador_logistico_tratamiento']; 
        $tmp_pos = strpos($this->operador_logistico_tratamiento, "##@@");
        if ($tmp_pos !== false)
        {
            $this->operador_logistico_tratamiento = substr($this->operador_logistico_tratamiento, 0, $tmp_pos);
        }
        $this->punto_entrega = $Busca_temp['punto_entrega']; 
        $tmp_pos = strpos($this->punto_entrega, "##@@");
        if ($tmp_pos !== false)
        {
            $this->punto_entrega = substr($this->punto_entrega, 0, $tmp_pos);
        }
        $this->reclamo_gestion = $Busca_temp['reclamo_gestion']; 
        $tmp_pos = strpos($this->reclamo_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->reclamo_gestion = substr($this->reclamo_gestion, 0, $tmp_pos);
        }
        $this->fecha_reclamacion_gestion = $Busca_temp['fecha_reclamacion_gestion']; 
        $tmp_pos = strpos($this->fecha_reclamacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->fecha_reclamacion_gestion = substr($this->fecha_reclamacion_gestion, 0, $tmp_pos);
        }
        $this->causa_no_reclamacion_gestion = $Busca_temp['causa_no_reclamacion_gestion']; 
        $tmp_pos = strpos($this->causa_no_reclamacion_gestion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->causa_no_reclamacion_gestion = substr($this->causa_no_reclamacion_gestion, 0, $tmp_pos);
        }
        $this->id_historial_reclamacion = $Busca_temp['id_historial_reclamacion']; 
        $tmp_pos = strpos($this->id_historial_reclamacion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->id_historial_reclamacion = substr($this->id_historial_reclamacion, 0, $tmp_pos);
        }
        $this->id_historial_reclamacion_2 = $Busca_temp['id_historial_reclamacion_input_2']; 
        $this->anio_historial_reclamacion = $Busca_temp['anio_historial_reclamacion']; 
        $tmp_pos = strpos($this->anio_historial_reclamacion, "##@@");
        if ($tmp_pos !== false)
        {
            $this->anio_historial_reclamacion = substr($this->anio_historial_reclamacion, 0, $tmp_pos);
        }
        $this->anio_historial_reclamacion_2 = $Busca_temp['anio_historial_reclamacion_input_2']; 
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("en_us");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
    $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['seq_dir'] = 0; 
    $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['sub_dir'] = array(); 
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   else 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, LOGRO_COMUNICACION_GESTION, FECHA_COMUNICACION, AUTOR_GESTION, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, CODIGO_XOFIGO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, NUMERO_CAJAS, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, ID_HISTORIAL_RECLAMACION, ANIO_HISTORIAL_RECLAMACION, MES1, RECLAMO1, FECHA_RECLAMACION1, MOTIVO_NO_RECLAMACION1, MES2, RECLAMO2, FECHA_RECLAMACION2, MOTIVO_NO_RECLAMACION2, MES3, RECLAMO3, FECHA_RECLAMACION3, MOTIVO_NO_RECLAMACION3, MES4, RECLAMO4, FECHA_RECLAMACION4, MOTIVO_NO_RECLAMACION4, MES5, RECLAMO5, FECHA_RECLAMACION5, MOTIVO_NO_RECLAMACION5, MES6, RECLAMO6, FECHA_RECLAMACION6, MOTIVO_NO_RECLAMACION6, MES7, RECLAMO7, FECHA_RECLAMACION7, MOTIVO_NO_RECLAMACION7, MES8, RECLAMO8, FECHA_RECLAMACION8, MOTIVO_NO_RECLAMACION8, MES9, RECLAMO9, FECHA_RECLAMACION9, MOTIVO_NO_RECLAMACION9, MES10, RECLAMO10, FECHA_RECLAMACION10, MOTIVO_NO_RECLAMACION10, MES11, RECLAMO11, FECHA_RECLAMACION11, MOTIVO_NO_RECLAMACION11, MES12, RECLAMO12, FECHA_RECLAMACION12, MOTIVO_NO_RECLAMACION12 from (SELECT    bp.ID_PACIENTE,    bg.LOGRO_COMUNICACION_GESTION,    bg.FECHA_COMUNICACION,    bg.AUTOR_GESTION,    bp.DEPARTAMENTO_PACIENTE,    bp.CIUDAD_PACIENTE,    bp.ESTADO_PACIENTE,    bp.STATUS_PACIENTE,    bp.FECHA_ACTIVACION_PACIENTE,    bp.CODIGO_XOFIGO,    bt.PRODUCTO_TRATAMIENTO,    bt.NOMBRE_REFERENCIA,    bt.DOSIS_TRATAMIENTO,    bg.NUMERO_CAJAS,    bt.ASEGURADOR_TRATAMIENTO,    bt.OPERADOR_LOGISTICO_TRATAMIENTO,    bt.PUNTO_ENTREGA,    bg.RECLAMO_GESTION,    bg.FECHA_RECLAMACION_GESTION,    bg.CAUSA_NO_RECLAMACION_GESTION,    bhc.ID_HISTORIAL_RECLAMACION,    bhc.ANIO_HISTORIAL_RECLAMACION,    bhc.MES1,    bhc.RECLAMO1,    bhc.FECHA_RECLAMACION1,    bhc.MOTIVO_NO_RECLAMACION1,    bhc.MES2,    bhc.RECLAMO2,    bhc.FECHA_RECLAMACION2,    bhc.MOTIVO_NO_RECLAMACION2,    bhc.MES3,    bhc.RECLAMO3,    bhc.FECHA_RECLAMACION3,    bhc.MOTIVO_NO_RECLAMACION3,    bhc.MES4,    bhc.RECLAMO4,    bhc.FECHA_RECLAMACION4,    bhc.MOTIVO_NO_RECLAMACION4,    bhc.MES5,    bhc.RECLAMO5,    bhc.FECHA_RECLAMACION5,    bhc.MOTIVO_NO_RECLAMACION5,    bhc.MES6,    bhc.RECLAMO6,    bhc.FECHA_RECLAMACION6,    bhc.MOTIVO_NO_RECLAMACION6,    bhc.MES7,    bhc.RECLAMO7,    bhc.FECHA_RECLAMACION7,    bhc.MOTIVO_NO_RECLAMACION7,    bhc.MES8,    bhc.RECLAMO8,    bhc.FECHA_RECLAMACION8,    bhc.MOTIVO_NO_RECLAMACION8,    bhc.MES9,    bhc.RECLAMO9,    bhc.FECHA_RECLAMACION9,    bhc.MOTIVO_NO_RECLAMACION9,    bhc.MES10,    bhc.RECLAMO10,    bhc.FECHA_RECLAMACION10,    bhc.MOTIVO_NO_RECLAMACION10,    bhc.MES11,    bhc.RECLAMO11,    bhc.FECHA_RECLAMACION11,    bhc.MOTIVO_NO_RECLAMACION11,    bhc.MES12,    bhc.RECLAMO12,    bhc.FECHA_RECLAMACION12,    bhc.MOTIVO_NO_RECLAMACION12 FROM    bayer_pacientes bp INNER JOIN bayer_gestiones bg ON bp.ID_PACIENTE = bg.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento bt ON bp.ID_PACIENTE = bt.ID_PACIENTE_FK    INNER JOIN bayer_historial_reclamacion bhc ON bp.ID_PACIENTE = bhc.ID_PACIENTE_FK WHERE bg.FECHA_COMUNICACION = (SELECT FECHA_COMUNICACION FROM bayer_gestiones bg WHERE bg.ID_PACIENTE_FK2=bp.ID_PACIENTE ORDER BY bg.FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   $parms_det = explode("*PDet*", $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  ID_PACIENTE = $parms_det[0] and LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and FECHA_COMUNICACION = '$parms_det[2]' and AUTOR_GESTION = '$parms_det[3]' and DEPARTAMENTO_PACIENTE = '$parms_det[4]' and CIUDAD_PACIENTE = '$parms_det[5]' and ESTADO_PACIENTE = '$parms_det[6]' and STATUS_PACIENTE = '$parms_det[7]' and FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and CODIGO_XOFIGO = $parms_det[9] and PRODUCTO_TRATAMIENTO = '$parms_det[10]' and NOMBRE_REFERENCIA = '$parms_det[11]' and DOSIS_TRATAMIENTO = '$parms_det[12]' and NUMERO_CAJAS = '$parms_det[13]' and ASEGURADOR_TRATAMIENTO = '$parms_det[14]' and OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[15]' and PUNTO_ENTREGA = '$parms_det[16]' and RECLAMO_GESTION = '$parms_det[17]' and FECHA_RECLAMACION_GESTION = '$parms_det[18]' and CAUSA_NO_RECLAMACION_GESTION = '$parms_det[19]' and ID_HISTORIAL_RECLAMACION = $parms_det[20] and ANIO_HISTORIAL_RECLAMACION = $parms_det[21] and MES1 = '$parms_det[22]' and RECLAMO1 = '$parms_det[23]' and FECHA_RECLAMACION1 = '$parms_det[24]' and MOTIVO_NO_RECLAMACION1 = '$parms_det[25]' and MES2 = '$parms_det[26]' and RECLAMO2 = '$parms_det[27]' and FECHA_RECLAMACION2 = '$parms_det[28]' and MOTIVO_NO_RECLAMACION2 = '$parms_det[29]' and MES3 = '$parms_det[30]' and RECLAMO3 = '$parms_det[31]' and FECHA_RECLAMACION3 = '$parms_det[32]' and MOTIVO_NO_RECLAMACION3 = '$parms_det[33]' and MES4 = '$parms_det[34]' and RECLAMO4 = '$parms_det[35]' and FECHA_RECLAMACION4 = '$parms_det[36]' and MOTIVO_NO_RECLAMACION4 = '$parms_det[37]' and MES5 = '$parms_det[38]' and RECLAMO5 = '$parms_det[39]' and FECHA_RECLAMACION5 = '$parms_det[40]' and MOTIVO_NO_RECLAMACION5 = '$parms_det[41]' and MES6 = '$parms_det[42]' and RECLAMO6 = '$parms_det[43]' and FECHA_RECLAMACION6 = '$parms_det[44]' and MOTIVO_NO_RECLAMACION6 = '$parms_det[45]' and MES7 = '$parms_det[46]' and RECLAMO7 = '$parms_det[47]' and FECHA_RECLAMACION7 = '$parms_det[48]' and MOTIVO_NO_RECLAMACION7 = '$parms_det[49]' and MES8 = '$parms_det[50]' and RECLAMO8 = '$parms_det[51]' and FECHA_RECLAMACION8 = '$parms_det[52]' and MOTIVO_NO_RECLAMACION8 = '$parms_det[53]' and MES9 = '$parms_det[54]' and RECLAMO9 = '$parms_det[55]' and FECHA_RECLAMACION9 = '$parms_det[56]' and MOTIVO_NO_RECLAMACION9 = '$parms_det[57]' and MES10 = '$parms_det[58]' and RECLAMO10 = '$parms_det[59]' and FECHA_RECLAMACION10 = '$parms_det[60]' and MOTIVO_NO_RECLAMACION10 = '$parms_det[61]' and MES11 = '$parms_det[62]' and RECLAMO11 = '$parms_det[63]' and FECHA_RECLAMACION11 = '$parms_det[64]' and MOTIVO_NO_RECLAMACION11 = '$parms_det[65]' and MES12 = '$parms_det[66]' and RECLAMO12 = '$parms_det[67]' and FECHA_RECLAMACION12 = '$parms_det[68]' and MOTIVO_NO_RECLAMACION12 = '$parms_det[69]'" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  ID_PACIENTE = $parms_det[0] and LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and FECHA_COMUNICACION = '$parms_det[2]' and AUTOR_GESTION = '$parms_det[3]' and DEPARTAMENTO_PACIENTE = '$parms_det[4]' and CIUDAD_PACIENTE = '$parms_det[5]' and ESTADO_PACIENTE = '$parms_det[6]' and STATUS_PACIENTE = '$parms_det[7]' and FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and CODIGO_XOFIGO = $parms_det[9] and PRODUCTO_TRATAMIENTO = '$parms_det[10]' and NOMBRE_REFERENCIA = '$parms_det[11]' and DOSIS_TRATAMIENTO = '$parms_det[12]' and NUMERO_CAJAS = '$parms_det[13]' and ASEGURADOR_TRATAMIENTO = '$parms_det[14]' and OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[15]' and PUNTO_ENTREGA = '$parms_det[16]' and RECLAMO_GESTION = '$parms_det[17]' and FECHA_RECLAMACION_GESTION = '$parms_det[18]' and CAUSA_NO_RECLAMACION_GESTION = '$parms_det[19]' and ID_HISTORIAL_RECLAMACION = $parms_det[20] and ANIO_HISTORIAL_RECLAMACION = $parms_det[21] and MES1 = '$parms_det[22]' and RECLAMO1 = '$parms_det[23]' and FECHA_RECLAMACION1 = '$parms_det[24]' and MOTIVO_NO_RECLAMACION1 = '$parms_det[25]' and MES2 = '$parms_det[26]' and RECLAMO2 = '$parms_det[27]' and FECHA_RECLAMACION2 = '$parms_det[28]' and MOTIVO_NO_RECLAMACION2 = '$parms_det[29]' and MES3 = '$parms_det[30]' and RECLAMO3 = '$parms_det[31]' and FECHA_RECLAMACION3 = '$parms_det[32]' and MOTIVO_NO_RECLAMACION3 = '$parms_det[33]' and MES4 = '$parms_det[34]' and RECLAMO4 = '$parms_det[35]' and FECHA_RECLAMACION4 = '$parms_det[36]' and MOTIVO_NO_RECLAMACION4 = '$parms_det[37]' and MES5 = '$parms_det[38]' and RECLAMO5 = '$parms_det[39]' and FECHA_RECLAMACION5 = '$parms_det[40]' and MOTIVO_NO_RECLAMACION5 = '$parms_det[41]' and MES6 = '$parms_det[42]' and RECLAMO6 = '$parms_det[43]' and FECHA_RECLAMACION6 = '$parms_det[44]' and MOTIVO_NO_RECLAMACION6 = '$parms_det[45]' and MES7 = '$parms_det[46]' and RECLAMO7 = '$parms_det[47]' and FECHA_RECLAMACION7 = '$parms_det[48]' and MOTIVO_NO_RECLAMACION7 = '$parms_det[49]' and MES8 = '$parms_det[50]' and RECLAMO8 = '$parms_det[51]' and FECHA_RECLAMACION8 = '$parms_det[52]' and MOTIVO_NO_RECLAMACION8 = '$parms_det[53]' and MES9 = '$parms_det[54]' and RECLAMO9 = '$parms_det[55]' and FECHA_RECLAMACION9 = '$parms_det[56]' and MOTIVO_NO_RECLAMACION9 = '$parms_det[57]' and MES10 = '$parms_det[58]' and RECLAMO10 = '$parms_det[59]' and FECHA_RECLAMACION10 = '$parms_det[60]' and MOTIVO_NO_RECLAMACION10 = '$parms_det[61]' and MES11 = '$parms_det[62]' and RECLAMO11 = '$parms_det[63]' and FECHA_RECLAMACION11 = '$parms_det[64]' and MOTIVO_NO_RECLAMACION11 = '$parms_det[65]' and MES12 = '$parms_det[66]' and RECLAMO12 = '$parms_det[67]' and FECHA_RECLAMACION12 = '$parms_det[68]' and MOTIVO_NO_RECLAMACION12 = '$parms_det[69]'" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  ID_PACIENTE = $parms_det[0] and LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and FECHA_COMUNICACION = '$parms_det[2]' and AUTOR_GESTION = '$parms_det[3]' and DEPARTAMENTO_PACIENTE = '$parms_det[4]' and CIUDAD_PACIENTE = '$parms_det[5]' and ESTADO_PACIENTE = '$parms_det[6]' and STATUS_PACIENTE = '$parms_det[7]' and FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and CODIGO_XOFIGO = $parms_det[9] and PRODUCTO_TRATAMIENTO = '$parms_det[10]' and NOMBRE_REFERENCIA = '$parms_det[11]' and DOSIS_TRATAMIENTO = '$parms_det[12]' and NUMERO_CAJAS = '$parms_det[13]' and ASEGURADOR_TRATAMIENTO = '$parms_det[14]' and OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[15]' and PUNTO_ENTREGA = '$parms_det[16]' and RECLAMO_GESTION = '$parms_det[17]' and FECHA_RECLAMACION_GESTION = '$parms_det[18]' and CAUSA_NO_RECLAMACION_GESTION = '$parms_det[19]' and ID_HISTORIAL_RECLAMACION = $parms_det[20] and ANIO_HISTORIAL_RECLAMACION = $parms_det[21] and MES1 = '$parms_det[22]' and RECLAMO1 = '$parms_det[23]' and FECHA_RECLAMACION1 = '$parms_det[24]' and MOTIVO_NO_RECLAMACION1 = '$parms_det[25]' and MES2 = '$parms_det[26]' and RECLAMO2 = '$parms_det[27]' and FECHA_RECLAMACION2 = '$parms_det[28]' and MOTIVO_NO_RECLAMACION2 = '$parms_det[29]' and MES3 = '$parms_det[30]' and RECLAMO3 = '$parms_det[31]' and FECHA_RECLAMACION3 = '$parms_det[32]' and MOTIVO_NO_RECLAMACION3 = '$parms_det[33]' and MES4 = '$parms_det[34]' and RECLAMO4 = '$parms_det[35]' and FECHA_RECLAMACION4 = '$parms_det[36]' and MOTIVO_NO_RECLAMACION4 = '$parms_det[37]' and MES5 = '$parms_det[38]' and RECLAMO5 = '$parms_det[39]' and FECHA_RECLAMACION5 = '$parms_det[40]' and MOTIVO_NO_RECLAMACION5 = '$parms_det[41]' and MES6 = '$parms_det[42]' and RECLAMO6 = '$parms_det[43]' and FECHA_RECLAMACION6 = '$parms_det[44]' and MOTIVO_NO_RECLAMACION6 = '$parms_det[45]' and MES7 = '$parms_det[46]' and RECLAMO7 = '$parms_det[47]' and FECHA_RECLAMACION7 = '$parms_det[48]' and MOTIVO_NO_RECLAMACION7 = '$parms_det[49]' and MES8 = '$parms_det[50]' and RECLAMO8 = '$parms_det[51]' and FECHA_RECLAMACION8 = '$parms_det[52]' and MOTIVO_NO_RECLAMACION8 = '$parms_det[53]' and MES9 = '$parms_det[54]' and RECLAMO9 = '$parms_det[55]' and FECHA_RECLAMACION9 = '$parms_det[56]' and MOTIVO_NO_RECLAMACION9 = '$parms_det[57]' and MES10 = '$parms_det[58]' and RECLAMO10 = '$parms_det[59]' and FECHA_RECLAMACION10 = '$parms_det[60]' and MOTIVO_NO_RECLAMACION10 = '$parms_det[61]' and MES11 = '$parms_det[62]' and RECLAMO11 = '$parms_det[63]' and FECHA_RECLAMACION11 = '$parms_det[64]' and MOTIVO_NO_RECLAMACION11 = '$parms_det[65]' and MES12 = '$parms_det[66]' and RECLAMO12 = '$parms_det[67]' and FECHA_RECLAMACION12 = '$parms_det[68]' and MOTIVO_NO_RECLAMACION12 = '$parms_det[69]'" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->id_paciente = $rs->fields[0] ;  
   $this->id_paciente = (string)$this->id_paciente;
   $this->logro_comunicacion_gestion = $rs->fields[1] ;  
   $this->fecha_comunicacion = $rs->fields[2] ;  
   $this->autor_gestion = $rs->fields[3] ;  
   $this->departamento_paciente = $rs->fields[4] ;  
   $this->ciudad_paciente = $rs->fields[5] ;  
   $this->estado_paciente = $rs->fields[6] ;  
   $this->status_paciente = $rs->fields[7] ;  
   $this->fecha_activacion_paciente = $rs->fields[8] ;  
   $this->codigo_xofigo = $rs->fields[9] ;  
   $this->codigo_xofigo = (string)$this->codigo_xofigo;
   $this->producto_tratamiento = $rs->fields[10] ;  
   $this->nombre_referencia = $rs->fields[11] ;  
   $this->dosis_tratamiento = $rs->fields[12] ;  
   $this->numero_cajas = $rs->fields[13] ;  
   $this->asegurador_tratamiento = $rs->fields[14] ;  
   $this->operador_logistico_tratamiento = $rs->fields[15] ;  
   $this->punto_entrega = $rs->fields[16] ;  
   $this->reclamo_gestion = $rs->fields[17] ;  
   $this->fecha_reclamacion_gestion = $rs->fields[18] ;  
   $this->causa_no_reclamacion_gestion = $rs->fields[19] ;  
   $this->id_historial_reclamacion = $rs->fields[20] ;  
   $this->id_historial_reclamacion = (string)$this->id_historial_reclamacion;
   $this->anio_historial_reclamacion = $rs->fields[21] ;  
   $this->anio_historial_reclamacion = (string)$this->anio_historial_reclamacion;
   $this->mes1 = $rs->fields[22] ;  
   $this->reclamo1 = $rs->fields[23] ;  
   $this->fecha_reclamacion1 = $rs->fields[24] ;  
   $this->motivo_no_reclamacion1 = $rs->fields[25] ;  
   $this->mes2 = $rs->fields[26] ;  
   $this->reclamo2 = $rs->fields[27] ;  
   $this->fecha_reclamacion2 = $rs->fields[28] ;  
   $this->motivo_no_reclamacion2 = $rs->fields[29] ;  
   $this->mes3 = $rs->fields[30] ;  
   $this->reclamo3 = $rs->fields[31] ;  
   $this->fecha_reclamacion3 = $rs->fields[32] ;  
   $this->motivo_no_reclamacion3 = $rs->fields[33] ;  
   $this->mes4 = $rs->fields[34] ;  
   $this->reclamo4 = $rs->fields[35] ;  
   $this->fecha_reclamacion4 = $rs->fields[36] ;  
   $this->motivo_no_reclamacion4 = $rs->fields[37] ;  
   $this->mes5 = $rs->fields[38] ;  
   $this->reclamo5 = $rs->fields[39] ;  
   $this->fecha_reclamacion5 = $rs->fields[40] ;  
   $this->motivo_no_reclamacion5 = $rs->fields[41] ;  
   $this->mes6 = $rs->fields[42] ;  
   $this->reclamo6 = $rs->fields[43] ;  
   $this->fecha_reclamacion6 = $rs->fields[44] ;  
   $this->motivo_no_reclamacion6 = $rs->fields[45] ;  
   $this->mes7 = $rs->fields[46] ;  
   $this->reclamo7 = $rs->fields[47] ;  
   $this->fecha_reclamacion7 = $rs->fields[48] ;  
   $this->motivo_no_reclamacion7 = $rs->fields[49] ;  
   $this->mes8 = $rs->fields[50] ;  
   $this->reclamo8 = $rs->fields[51] ;  
   $this->fecha_reclamacion8 = $rs->fields[52] ;  
   $this->motivo_no_reclamacion8 = $rs->fields[53] ;  
   $this->mes9 = $rs->fields[54] ;  
   $this->reclamo9 = $rs->fields[55] ;  
   $this->fecha_reclamacion9 = $rs->fields[56] ;  
   $this->motivo_no_reclamacion9 = $rs->fields[57] ;  
   $this->mes10 = $rs->fields[58] ;  
   $this->reclamo10 = $rs->fields[59] ;  
   $this->fecha_reclamacion10 = $rs->fields[60] ;  
   $this->motivo_no_reclamacion10 = $rs->fields[61] ;  
   $this->mes11 = $rs->fields[62] ;  
   $this->reclamo11 = $rs->fields[63] ;  
   $this->fecha_reclamacion11 = $rs->fields[64] ;  
   $this->motivo_no_reclamacion11 = $rs->fields[65] ;  
   $this->mes12 = $rs->fields[66] ;  
   $this->reclamo12 = $rs->fields[67] ;  
   $this->fecha_reclamacion12 = $rs->fields[68] ;  
   $this->motivo_no_reclamacion12 = $rs->fields[69] ;  
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cmp_acum']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cmp_acum']))
   {
       $parms_acum = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['cmp_acum']);
       foreach ($parms_acum as $cada_par)
       {
          $cada_val = explode("=", $cada_par);
          $this->$cada_val[0] = $cada_val[1];
       }
   }
//--- 
   $nm_saida->saida("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
   $nm_saida->saida("            \"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">\r\n");
   $nm_saida->saida("<html" . $_SESSION['scriptcase']['reg_conf']['html_dir'] . ">\r\n");
   $nm_saida->saida("<HEAD>\r\n");
   $nm_saida->saida("   <TITLE>Informe Reclamaciones Historial</TITLE>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "\" />\r\n");
   $nm_saida->saida(" <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Last-Modified\" content=\"" . gmdate("D, d M Y H:i:s") . " GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"no-store, no-cache, must-revalidate\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"post-check=0, pre-check=0\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n");
   if ($_SESSION['scriptcase']['proc_mobile'])
   {
       $nm_saida->saida(" <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" />\r\n");
   }

   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery/js/jquery.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/malsup-blockui/jquery.blockUI.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\">var sc_pathToTB = '" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/';</script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox-compressed.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"../_lib/lib/js/jquery.scInput.js\"></script>\r\n");
   $nm_saida->saida(" <link rel=\"stylesheet\" href=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />\r\n");
   if (($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['det_print'] == "print" && strtoupper($nmgp_cor_print) == "PB") || $nmgp_tipo_pdf == "pb")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
   else
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "Informe_reclamacion_historial/Informe_reclamacion_historial_det_" . strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) . ".css\" />\r\n");
   if (!$_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['pdf_det'] && $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['det_print'] != "print")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/buttons/" . $this->Ini->Str_btn_css . "\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema_dir'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
   }
   $nm_saida->saida("</HEAD>\r\n");
   $nm_saida->saida("  <body class=\"scGridPage\">\r\n");
   $nm_saida->saida("  " . $this->Ini->Ajax_result_set . "\r\n");
   $nm_saida->saida("<table border=0 align=\"center\" valign=\"top\" ><tr><td style=\"padding: 0px\"><div class=\"scGridBorder\"><table width='100%' cellspacing=0 cellpadding=0><tr><td>\r\n");
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<style>\r\n");
   $nm_saida->saida("#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 \r\n");
   $nm_saida->saida("#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}\r\n");
   $nm_saida->saida("</style>\r\n");
   $nm_saida->saida("<div style=\"width: 100%\">\r\n");
   $nm_saida->saida(" <div class=\"scGridHeader\" style=\"height:11px; display: block; border-width:0px; \"></div>\r\n");
   $nm_saida->saida(" <div style=\"height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block\">\r\n");
   $nm_saida->saida(" 	<table style=\"width:100%; border-collapse:collapse; padding:0;\">\r\n");
   $nm_saida->saida("    	<tr>\r\n");
   $nm_saida->saida("        	<td id=\"lin1_col1\" class=\"scGridHeaderFont\"><span>Informe Reclamaciones Historial</span></td>\r\n");
   $nm_saida->saida("            <td id=\"lin1_col2\" class=\"scGridHeaderFont\"><span></span></td>\r\n");
   $nm_saida->saida("        </tr>\r\n");
   $nm_saida->saida("    </table>		 \r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("</div>\r\n");
   $nm_saida->saida("  </TD>\r\n");
   $nm_saida->saida(" </TR>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       if ($this->nmgp_botoes['det_pdf'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bpdf", "", "", "Dpdf_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "Informe_reclamacion_historial/Informe_reclamacion_historial_config_pdf.php?nm_opc=pdf_det&nm_target=0&nm_cor=cor&papel=1&orientacao=1&largura=1200&conf_larg=S&conf_fonte=10&language=es&conf_socor=S&KeepThis=false&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       if ($this->nmgp_botoes['det_print'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bprint", "", "", "Dprint_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "Informe_reclamacion_historial/Informe_reclamacion_historial_config_print.php?nm_opc=detalhe&nm_cor=AM&language=es&KeepThis=true&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       $Cod_Btn = nmButtonOutput($this->arr_buttons, "bvoltar", "document.F3.submit()", "document.F3.submit()", "sc_b_sai_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
       $nm_saida->saida("           $Cod_Btn \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\"  align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->id_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_id_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_id_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['logro_comunicacion_gestion'])) ? $this->New_label['logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->logro_comunicacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_logro_comunicacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_logro_comunicacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_comunicacion'])) ? $this->New_label['fecha_comunicacion'] : "FECHA COMUNICACION"; 
          $conteudo = trim(sc_strip_script($this->fecha_comunicacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_comunicacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_comunicacion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['autor_gestion'])) ? $this->New_label['autor_gestion'] : "AUTOR GESTION"; 
          $conteudo = trim(sc_strip_script($this->autor_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_autor_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_autor_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['departamento_paciente'])) ? $this->New_label['departamento_paciente'] : "DEPARTAMENTO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->departamento_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_departamento_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_departamento_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['ciudad_paciente'])) ? $this->New_label['ciudad_paciente'] : "CIUDAD PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->ciudad_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_ciudad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_ciudad_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->estado_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_estado_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_estado_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['status_paciente'])) ? $this->New_label['status_paciente'] : "STATUS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->status_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_status_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_status_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->fecha_activacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_activacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_activacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['codigo_xofigo'])) ? $this->New_label['codigo_xofigo'] : "CODIGO XOFIGO"; 
          $conteudo = trim(sc_strip_script($this->codigo_xofigo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_codigo_xofigo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_codigo_xofigo_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->producto_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_producto_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_producto_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['nombre_referencia'])) ? $this->New_label['nombre_referencia'] : "NOMBRE REFERENCIA"; 
          $conteudo = trim(sc_strip_script($this->nombre_referencia)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_nombre_referencia_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_nombre_referencia_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['dosis_tratamiento'])) ? $this->New_label['dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->dosis_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_dosis_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_dosis_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['numero_cajas'])) ? $this->New_label['numero_cajas'] : "NUMERO CAJAS"; 
          $conteudo = trim(sc_strip_script($this->numero_cajas)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_numero_cajas_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_numero_cajas_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['asegurador_tratamiento'])) ? $this->New_label['asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->asegurador_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_asegurador_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_asegurador_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['operador_logistico_tratamiento'])) ? $this->New_label['operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->operador_logistico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_operador_logistico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_operador_logistico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['punto_entrega'])) ? $this->New_label['punto_entrega'] : "PUNTO ENTREGA"; 
          $conteudo = trim(sc_strip_script($this->punto_entrega)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_punto_entrega_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_punto_entrega_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo_gestion'])) ? $this->New_label['reclamo_gestion'] : "RECLAMO GESTION"; 
          $conteudo = trim(sc_strip_script($this->reclamo_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion_gestion'])) ? $this->New_label['fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['causa_no_reclamacion_gestion'])) ? $this->New_label['causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->causa_no_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_causa_no_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_causa_no_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id_historial_reclamacion'])) ? $this->New_label['id_historial_reclamacion'] : "ID HISTORIAL RECLAMACION"; 
          $conteudo = trim(sc_strip_script($this->id_historial_reclamacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_id_historial_reclamacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_id_historial_reclamacion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['anio_historial_reclamacion'])) ? $this->New_label['anio_historial_reclamacion'] : "ANIO HISTORIAL RECLAMACION"; 
          $conteudo = trim(sc_strip_script($this->anio_historial_reclamacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_anio_historial_reclamacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_anio_historial_reclamacion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes1'])) ? $this->New_label['mes1'] : "MES1"; 
          $conteudo = trim(sc_strip_script($this->mes1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo1'])) ? $this->New_label['reclamo1'] : "RECLAMO1"; 
          $conteudo = trim(sc_strip_script($this->reclamo1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion1'])) ? $this->New_label['fecha_reclamacion1'] : "FECHA RECLAMACION1"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion1'])) ? $this->New_label['motivo_no_reclamacion1'] : "MOTIVO NO RECLAMACION1"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion1)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion1_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion1_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes2'])) ? $this->New_label['mes2'] : "MES2"; 
          $conteudo = trim(sc_strip_script($this->mes2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo2'])) ? $this->New_label['reclamo2'] : "RECLAMO2"; 
          $conteudo = trim(sc_strip_script($this->reclamo2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion2'])) ? $this->New_label['fecha_reclamacion2'] : "FECHA RECLAMACION2"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion2'])) ? $this->New_label['motivo_no_reclamacion2'] : "MOTIVO NO RECLAMACION2"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion2)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion2_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion2_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes3'])) ? $this->New_label['mes3'] : "MES3"; 
          $conteudo = trim(sc_strip_script($this->mes3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo3'])) ? $this->New_label['reclamo3'] : "RECLAMO3"; 
          $conteudo = trim(sc_strip_script($this->reclamo3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion3'])) ? $this->New_label['fecha_reclamacion3'] : "FECHA RECLAMACION3"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion3'])) ? $this->New_label['motivo_no_reclamacion3'] : "MOTIVO NO RECLAMACION3"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion3)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion3_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion3_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes4'])) ? $this->New_label['mes4'] : "MES4"; 
          $conteudo = trim(sc_strip_script($this->mes4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo4'])) ? $this->New_label['reclamo4'] : "RECLAMO4"; 
          $conteudo = trim(sc_strip_script($this->reclamo4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion4'])) ? $this->New_label['fecha_reclamacion4'] : "FECHA RECLAMACION4"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion4'])) ? $this->New_label['motivo_no_reclamacion4'] : "MOTIVO NO RECLAMACION4"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion4)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion4_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion4_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes5'])) ? $this->New_label['mes5'] : "MES5"; 
          $conteudo = trim(sc_strip_script($this->mes5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo5'])) ? $this->New_label['reclamo5'] : "RECLAMO5"; 
          $conteudo = trim(sc_strip_script($this->reclamo5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion5'])) ? $this->New_label['fecha_reclamacion5'] : "FECHA RECLAMACION5"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion5'])) ? $this->New_label['motivo_no_reclamacion5'] : "MOTIVO NO RECLAMACION5"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion5)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion5_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion5_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes6'])) ? $this->New_label['mes6'] : "MES6"; 
          $conteudo = trim(sc_strip_script($this->mes6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo6'])) ? $this->New_label['reclamo6'] : "RECLAMO6"; 
          $conteudo = trim(sc_strip_script($this->reclamo6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion6'])) ? $this->New_label['fecha_reclamacion6'] : "FECHA RECLAMACION6"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion6'])) ? $this->New_label['motivo_no_reclamacion6'] : "MOTIVO NO RECLAMACION6"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion6)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion6_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion6_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes7'])) ? $this->New_label['mes7'] : "MES7"; 
          $conteudo = trim(sc_strip_script($this->mes7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo7'])) ? $this->New_label['reclamo7'] : "RECLAMO7"; 
          $conteudo = trim(sc_strip_script($this->reclamo7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion7'])) ? $this->New_label['fecha_reclamacion7'] : "FECHA RECLAMACION7"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion7'])) ? $this->New_label['motivo_no_reclamacion7'] : "MOTIVO NO RECLAMACION7"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion7)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion7_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion7_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes8'])) ? $this->New_label['mes8'] : "MES8"; 
          $conteudo = trim(sc_strip_script($this->mes8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo8'])) ? $this->New_label['reclamo8'] : "RECLAMO8"; 
          $conteudo = trim(sc_strip_script($this->reclamo8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion8'])) ? $this->New_label['fecha_reclamacion8'] : "FECHA RECLAMACION8"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion8'])) ? $this->New_label['motivo_no_reclamacion8'] : "MOTIVO NO RECLAMACION8"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion8)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion8_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion8_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes9'])) ? $this->New_label['mes9'] : "MES9"; 
          $conteudo = trim(sc_strip_script($this->mes9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo9'])) ? $this->New_label['reclamo9'] : "RECLAMO9"; 
          $conteudo = trim(sc_strip_script($this->reclamo9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion9'])) ? $this->New_label['fecha_reclamacion9'] : "FECHA RECLAMACION9"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion9'])) ? $this->New_label['motivo_no_reclamacion9'] : "MOTIVO NO RECLAMACION9"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion9)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion9_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion9_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes10'])) ? $this->New_label['mes10'] : "MES10"; 
          $conteudo = trim(sc_strip_script($this->mes10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo10'])) ? $this->New_label['reclamo10'] : "RECLAMO10"; 
          $conteudo = trim(sc_strip_script($this->reclamo10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion10'])) ? $this->New_label['fecha_reclamacion10'] : "FECHA RECLAMACION10"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion10'])) ? $this->New_label['motivo_no_reclamacion10'] : "MOTIVO NO RECLAMACION10"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion10)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion10_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion10_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes11'])) ? $this->New_label['mes11'] : "MES11"; 
          $conteudo = trim(sc_strip_script($this->mes11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo11'])) ? $this->New_label['reclamo11'] : "RECLAMO11"; 
          $conteudo = trim(sc_strip_script($this->reclamo11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion11'])) ? $this->New_label['fecha_reclamacion11'] : "FECHA RECLAMACION11"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion11'])) ? $this->New_label['motivo_no_reclamacion11'] : "MOTIVO NO RECLAMACION11"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion11)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion11_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion11_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['mes12'])) ? $this->New_label['mes12'] : "MES12"; 
          $conteudo = trim(sc_strip_script($this->mes12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_mes12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_mes12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo12'])) ? $this->New_label['reclamo12'] : "RECLAMO12"; 
          $conteudo = trim(sc_strip_script($this->reclamo12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_reclamo12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion12'])) ? $this->New_label['fecha_reclamacion12'] : "FECHA RECLAMACION12"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_reclamacion12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_no_reclamacion12'])) ? $this->New_label['motivo_no_reclamacion12'] : "MOTIVO NO RECLAMACION12"; 
          $conteudo = trim(sc_strip_script($this->motivo_no_reclamacion12)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_no_reclamacion12_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_no_reclamacion12_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion_historial']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
//--- 
//--- 
   $nm_saida->saida("<form name=\"F3\" method=post\r\n");
   $nm_saida->saida("                  target=\"_self\"\r\n");
   $nm_saida->saida("                  action=\"./\">\r\n");
   $nm_saida->saida("<input type=hidden name=\"nmgp_opcao\" value=\"igual\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_init\" value=\"" . NM_encode_input($this->Ini->sc_page) . "\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_session\" value=\"" . NM_encode_input(session_id()) . "\"/>\r\n");
   $nm_saida->saida("</form>\r\n");
   $nm_saida->saida("<script language=JavaScript>\r\n");
   $nm_saida->saida("   function nm_mostra_doc(campo1, campo2, campo3)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       NovaJanela = window.open (\"Informe_reclamacion_historial_doc.php?script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&nm_cod_doc=\" + campo1 + \"&nm_nome_doc=\" + campo2 + \"&nm_cod_apl=\" + campo3, \"ScriptCase\", \"resizable\");\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_move(x, y, z, p, g) \r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("      window.location = \"" . $this->Ini->path_link . "Informe_reclamacion_historial/index.php?nmgp_opcao=pdf_det&nmgp_tipo_pdf=\" + z + \"&nmgp_parms_pdf=\" + p +  \"&nmgp_graf_pdf=\" + g + \"&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "\";\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_print_conf(tp, cor)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       window.open('" . $this->Ini->path_link . "Informe_reclamacion_historial/Informe_reclamacion_historial_iframe_prt.php?path_botoes=" . $this->Ini->path_botoes . "&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&opcao=det_print&cor_print=' + cor,'','location=no,menubar,resizable,scrollbars,status=no,toolbar');\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("</script>\r\n");
   $nm_saida->saida("</body>\r\n");
   $nm_saida->saida("</html>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
