<?php

class Paciente_terapia_xml
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;

   var $Arquivo;
   var $Arquivo_view;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function Paciente_terapia_xml()
   {
      $this->nm_data   = new nm_data("es");
   }

   //---- 
   function monta_xml()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->nm_data    = new nm_data("es");
      $this->Arquivo      = "sc_xml";
      $this->Arquivo     .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo     .= "_Paciente_terapia";
      $this->Arquivo_view = $this->Arquivo . "_view.xml";
      $this->Arquivo     .= ".xml";
      $this->Tit_doc      = "Paciente_terapia.xml";
      $this->Grava_view   = false;
      if (strtolower($_SESSION['scriptcase']['charset']) != strtolower($_SESSION['scriptcase']['charset_html']))
      {
          $this->Grava_view = true;
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['Paciente_terapia']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Paciente_terapia']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['Paciente_terapia']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->bp_id_paciente = $Busca_temp['bp_id_paciente']; 
          $tmp_pos = strpos($this->bp_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_id_paciente = substr($this->bp_id_paciente, 0, $tmp_pos);
          }
          $this->bp_id_paciente_2 = $Busca_temp['bp_id_paciente_input_2']; 
          $this->bt_producto_tratamiento = $Busca_temp['bt_producto_tratamiento']; 
          $tmp_pos = strpos($this->bt_producto_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bt_producto_tratamiento = substr($this->bt_producto_tratamiento, 0, $tmp_pos);
          }
          $this->bp_estado_paciente = $Busca_temp['bp_estado_paciente']; 
          $tmp_pos = strpos($this->bp_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_estado_paciente = substr($this->bp_estado_paciente, 0, $tmp_pos);
          }
          $this->bp_status_paciente = $Busca_temp['bp_status_paciente']; 
          $tmp_pos = strpos($this->bp_status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_status_paciente = substr($this->bp_status_paciente, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['xml_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['xml_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['xml_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['xml_name']);
      }
      if (!$this->Grava_view)
      {
          $this->Arquivo_view = $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_2, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_3, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5 from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $xml_charset = $_SESSION['scriptcase']['charset'];
      $xml_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      fwrite($xml_f, "<?xml version=\"1.0\" encoding=\"$xml_charset\" ?>\r\n");
      fwrite($xml_f, "<root>\r\n");
      if ($this->Grava_view)
      {
          $xml_charset_v = $_SESSION['scriptcase']['charset_html'];
          $xml_v         = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo_view, "w");
          fwrite($xml_v, "<?xml version=\"1.0\" encoding=\"$xml_charset_v\" ?>\r\n");
          fwrite($xml_v, "<root>\r\n");
      }
      while (!$rs->EOF)
      {
         $this->xml_registro = "<Paciente_terapia";
         $this->bp_id_paciente = $rs->fields[0] ;  
         $this->bp_id_paciente = (string)$this->bp_id_paciente;
         $this->bt_producto_tratamiento = $rs->fields[1] ;  
         $this->bp_estado_paciente = $rs->fields[2] ;  
         $this->bp_status_paciente = $rs->fields[3] ;  
         $this->bp_fecha_activacion_paciente = $rs->fields[4] ;  
         $this->bt_clasificacion_patologica_tratamiento = $rs->fields[5] ;  
         $this->bt_dosis_tratamiento = $rs->fields[6] ;  
         $this->bt_medico_tratamiento = $rs->fields[7] ;  
         $this->bp_motivo_retiro_paciente = $rs->fields[8] ;  
         $this->bp_observacion_motivo_retiro_paciente = $rs->fields[9] ;  
         $this->bp_identificacion_paciente = $rs->fields[10] ;  
         $this->bp_nombre_paciente = $rs->fields[11] ;  
         $this->bp_apellido_paciente = $rs->fields[12] ;  
         $this->bp_telefono_paciente = $rs->fields[13] ;  
         $this->bp_telefono2_paciente = $rs->fields[14] ;  
         $this->bp_telefono3_paciente = $rs->fields[15] ;  
         $this->bp_correo_paciente = $rs->fields[16] ;  
         $this->bp_direccion_paciente = $rs->fields[17] ;  
         $this->bp_barrio_paciente = $rs->fields[18] ;  
         $this->bp_departamento_paciente = $rs->fields[19] ;  
         $this->bp_ciudad_paciente = $rs->fields[20] ;  
         $this->bt_zona_atencion_paramedico_tratamiento = $rs->fields[21] ;  
         $this->bp_genero_paciente = $rs->fields[22] ;  
         $this->bp_fecha_nacimineto_paciente = $rs->fields[23] ;  
         $this->bp_edad_paciente = $rs->fields[24] ;  
         $this->bp_edad_paciente = (string)$this->bp_edad_paciente;
         $this->bp_codigo_xofigo = $rs->fields[25] ;  
         $this->bp_codigo_xofigo = (string)$this->bp_codigo_xofigo;
         $this->bp_usuario_creacion = $rs->fields[26] ;  
         $this->bt_asegurador_tratamiento = $rs->fields[27] ;  
         $this->bt_operador_logistico_tratamiento = $rs->fields[28] ;  
         $this->bt_punto_entrega = $rs->fields[29] ;  
         $this->bt_fecha_ultima_reclamacion_tratamiento = $rs->fields[30] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->xml_registro .= " />\r\n";
         fwrite($xml_f, $this->xml_registro);
         if ($this->Grava_view)
         {
            fwrite($xml_v, $this->xml_registro);
         }
         $rs->MoveNext();
      }
      fwrite($xml_f, "</root>");
      fclose($xml_f);
      if ($this->Grava_view)
      {
         fwrite($xml_v, "</root>");
         fclose($xml_v);
      }

      $rs->Close();
   }
   //----- bp_id_paciente
   function NM_export_bp_id_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_id_paciente))
         {
             $this->bp_id_paciente = sc_convert_encoding($this->bp_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_id_paciente =\"" . $this->trata_dados($this->bp_id_paciente) . "\"";
   }
   //----- bt_producto_tratamiento
   function NM_export_bt_producto_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_producto_tratamiento))
         {
             $this->bt_producto_tratamiento = sc_convert_encoding($this->bt_producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_producto_tratamiento =\"" . $this->trata_dados($this->bt_producto_tratamiento) . "\"";
   }
   //----- bp_estado_paciente
   function NM_export_bp_estado_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_estado_paciente))
         {
             $this->bp_estado_paciente = sc_convert_encoding($this->bp_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_estado_paciente =\"" . $this->trata_dados($this->bp_estado_paciente) . "\"";
   }
   //----- bp_status_paciente
   function NM_export_bp_status_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_status_paciente))
         {
             $this->bp_status_paciente = sc_convert_encoding($this->bp_status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_status_paciente =\"" . $this->trata_dados($this->bp_status_paciente) . "\"";
   }
   //----- bp_fecha_activacion_paciente
   function NM_export_bp_fecha_activacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_fecha_activacion_paciente))
         {
             $this->bp_fecha_activacion_paciente = sc_convert_encoding($this->bp_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_fecha_activacion_paciente =\"" . $this->trata_dados($this->bp_fecha_activacion_paciente) . "\"";
   }
   //----- bt_clasificacion_patologica_tratamiento
   function NM_export_bt_clasificacion_patologica_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_clasificacion_patologica_tratamiento))
         {
             $this->bt_clasificacion_patologica_tratamiento = sc_convert_encoding($this->bt_clasificacion_patologica_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_clasificacion_patologica_tratamiento =\"" . $this->trata_dados($this->bt_clasificacion_patologica_tratamiento) . "\"";
   }
   //----- bt_dosis_tratamiento
   function NM_export_bt_dosis_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_dosis_tratamiento))
         {
             $this->bt_dosis_tratamiento = sc_convert_encoding($this->bt_dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_dosis_tratamiento =\"" . $this->trata_dados($this->bt_dosis_tratamiento) . "\"";
   }
   //----- bt_medico_tratamiento
   function NM_export_bt_medico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_medico_tratamiento))
         {
             $this->bt_medico_tratamiento = sc_convert_encoding($this->bt_medico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_medico_tratamiento =\"" . $this->trata_dados($this->bt_medico_tratamiento) . "\"";
   }
   //----- bp_motivo_retiro_paciente
   function NM_export_bp_motivo_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_motivo_retiro_paciente))
         {
             $this->bp_motivo_retiro_paciente = sc_convert_encoding($this->bp_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_motivo_retiro_paciente =\"" . $this->trata_dados($this->bp_motivo_retiro_paciente) . "\"";
   }
   //----- bp_observacion_motivo_retiro_paciente
   function NM_export_bp_observacion_motivo_retiro_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_observacion_motivo_retiro_paciente))
         {
             $this->bp_observacion_motivo_retiro_paciente = sc_convert_encoding($this->bp_observacion_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_observacion_motivo_retiro_paciente =\"" . $this->trata_dados($this->bp_observacion_motivo_retiro_paciente) . "\"";
   }
   //----- bp_identificacion_paciente
   function NM_export_bp_identificacion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_identificacion_paciente))
         {
             $this->bp_identificacion_paciente = sc_convert_encoding($this->bp_identificacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_identificacion_paciente =\"" . $this->trata_dados($this->bp_identificacion_paciente) . "\"";
   }
   //----- bp_nombre_paciente
   function NM_export_bp_nombre_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_nombre_paciente))
         {
             $this->bp_nombre_paciente = sc_convert_encoding($this->bp_nombre_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_nombre_paciente =\"" . $this->trata_dados($this->bp_nombre_paciente) . "\"";
   }
   //----- bp_apellido_paciente
   function NM_export_bp_apellido_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_apellido_paciente))
         {
             $this->bp_apellido_paciente = sc_convert_encoding($this->bp_apellido_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_apellido_paciente =\"" . $this->trata_dados($this->bp_apellido_paciente) . "\"";
   }
   //----- bp_telefono_paciente
   function NM_export_bp_telefono_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_telefono_paciente))
         {
             $this->bp_telefono_paciente = sc_convert_encoding($this->bp_telefono_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_telefono_paciente =\"" . $this->trata_dados($this->bp_telefono_paciente) . "\"";
   }
   //----- bp_telefono2_paciente
   function NM_export_bp_telefono2_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_telefono2_paciente))
         {
             $this->bp_telefono2_paciente = sc_convert_encoding($this->bp_telefono2_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_telefono2_paciente =\"" . $this->trata_dados($this->bp_telefono2_paciente) . "\"";
   }
   //----- bp_telefono3_paciente
   function NM_export_bp_telefono3_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_telefono3_paciente))
         {
             $this->bp_telefono3_paciente = sc_convert_encoding($this->bp_telefono3_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_telefono3_paciente =\"" . $this->trata_dados($this->bp_telefono3_paciente) . "\"";
   }
   //----- bp_correo_paciente
   function NM_export_bp_correo_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_correo_paciente))
         {
             $this->bp_correo_paciente = sc_convert_encoding($this->bp_correo_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_correo_paciente =\"" . $this->trata_dados($this->bp_correo_paciente) . "\"";
   }
   //----- bp_direccion_paciente
   function NM_export_bp_direccion_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_direccion_paciente))
         {
             $this->bp_direccion_paciente = sc_convert_encoding($this->bp_direccion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_direccion_paciente =\"" . $this->trata_dados($this->bp_direccion_paciente) . "\"";
   }
   //----- bp_barrio_paciente
   function NM_export_bp_barrio_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_barrio_paciente))
         {
             $this->bp_barrio_paciente = sc_convert_encoding($this->bp_barrio_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_barrio_paciente =\"" . $this->trata_dados($this->bp_barrio_paciente) . "\"";
   }
   //----- bp_departamento_paciente
   function NM_export_bp_departamento_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_departamento_paciente))
         {
             $this->bp_departamento_paciente = sc_convert_encoding($this->bp_departamento_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_departamento_paciente =\"" . $this->trata_dados($this->bp_departamento_paciente) . "\"";
   }
   //----- bp_ciudad_paciente
   function NM_export_bp_ciudad_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_ciudad_paciente))
         {
             $this->bp_ciudad_paciente = sc_convert_encoding($this->bp_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_ciudad_paciente =\"" . $this->trata_dados($this->bp_ciudad_paciente) . "\"";
   }
   //----- bt_zona_atencion_paramedico_tratamiento
   function NM_export_bt_zona_atencion_paramedico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_zona_atencion_paramedico_tratamiento))
         {
             $this->bt_zona_atencion_paramedico_tratamiento = sc_convert_encoding($this->bt_zona_atencion_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_zona_atencion_paramedico_tratamiento =\"" . $this->trata_dados($this->bt_zona_atencion_paramedico_tratamiento) . "\"";
   }
   //----- bp_genero_paciente
   function NM_export_bp_genero_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_genero_paciente))
         {
             $this->bp_genero_paciente = sc_convert_encoding($this->bp_genero_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_genero_paciente =\"" . $this->trata_dados($this->bp_genero_paciente) . "\"";
   }
   //----- bp_fecha_nacimineto_paciente
   function NM_export_bp_fecha_nacimineto_paciente()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_fecha_nacimineto_paciente))
         {
             $this->bp_fecha_nacimineto_paciente = sc_convert_encoding($this->bp_fecha_nacimineto_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_fecha_nacimineto_paciente =\"" . $this->trata_dados($this->bp_fecha_nacimineto_paciente) . "\"";
   }
   //----- bp_edad_paciente
   function NM_export_bp_edad_paciente()
   {
         nmgp_Form_Num_Val($this->bp_edad_paciente, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_edad_paciente))
         {
             $this->bp_edad_paciente = sc_convert_encoding($this->bp_edad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_edad_paciente =\"" . $this->trata_dados($this->bp_edad_paciente) . "\"";
   }
   //----- bp_codigo_xofigo
   function NM_export_bp_codigo_xofigo()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_codigo_xofigo))
         {
             $this->bp_codigo_xofigo = sc_convert_encoding($this->bp_codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_codigo_xofigo =\"" . $this->trata_dados($this->bp_codigo_xofigo) . "\"";
   }
   //----- bp_usuario_creacion
   function NM_export_bp_usuario_creacion()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bp_usuario_creacion))
         {
             $this->bp_usuario_creacion = sc_convert_encoding($this->bp_usuario_creacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bp_usuario_creacion =\"" . $this->trata_dados($this->bp_usuario_creacion) . "\"";
   }
   //----- bt_asegurador_tratamiento
   function NM_export_bt_asegurador_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_asegurador_tratamiento))
         {
             $this->bt_asegurador_tratamiento = sc_convert_encoding($this->bt_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_asegurador_tratamiento =\"" . $this->trata_dados($this->bt_asegurador_tratamiento) . "\"";
   }
   //----- bt_operador_logistico_tratamiento
   function NM_export_bt_operador_logistico_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_operador_logistico_tratamiento))
         {
             $this->bt_operador_logistico_tratamiento = sc_convert_encoding($this->bt_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_operador_logistico_tratamiento =\"" . $this->trata_dados($this->bt_operador_logistico_tratamiento) . "\"";
   }
   //----- bt_punto_entrega
   function NM_export_bt_punto_entrega()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_punto_entrega))
         {
             $this->bt_punto_entrega = sc_convert_encoding($this->bt_punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_punto_entrega =\"" . $this->trata_dados($this->bt_punto_entrega) . "\"";
   }
   //----- bt_fecha_ultima_reclamacion_tratamiento
   function NM_export_bt_fecha_ultima_reclamacion_tratamiento()
   {
         if ($_SESSION['scriptcase']['charset'] == "UTF-8" && !NM_is_utf8($this->bt_fecha_ultima_reclamacion_tratamiento))
         {
             $this->bt_fecha_ultima_reclamacion_tratamiento = sc_convert_encoding($this->bt_fecha_ultima_reclamacion_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->xml_registro .= " bt_fecha_ultima_reclamacion_tratamiento =\"" . $this->trata_dados($this->bt_fecha_ultima_reclamacion_tratamiento) . "\"";
   }

   //----- 
   function trata_dados($conteudo)
   {
      $str_temp =  $conteudo;
      $str_temp =  str_replace("<br />", "",  $str_temp);
      $str_temp =  str_replace("&", "&amp;",  $str_temp);
      $str_temp =  str_replace("<", "&lt;",   $str_temp);
      $str_temp =  str_replace(">", "&gt;",   $str_temp);
      $str_temp =  str_replace("'", "&apos;", $str_temp);
      $str_temp =  str_replace('"', "&quot;",  $str_temp);
      $str_temp =  str_replace('(', "_",  $str_temp);
      $str_temp =  str_replace(')', "",  $str_temp);
      return ($str_temp);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['xml_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia']['xml_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Paciente_terapia'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Paciente Terapia :: XML</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XML</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo_view ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="Paciente_terapia_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="Paciente_terapia"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./" style="display: none"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
