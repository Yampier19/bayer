<?php
   include_once('Listado_pacientes_session.php');
   session_start();
   if (!function_exists("NM_is_utf8"))
   {
       include_once("../_lib/lib/php/nm_utf8.php");
   }
    $Ord_Cmp = new Listado_pacientes_Ord_cmp(); 
    $Ord_Cmp->Ord_cmp_init();
   
class Listado_pacientes_Ord_cmp
{
function Ord_cmp_init()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos, $_POST, $_GET;
   if (isset($_POST['script_case_init']))
   {
       $sc_init    = $_POST['script_case_init'];
       $path_img   = $_POST['path_img'];
       $path_btn   = $_POST['path_btn'];
       $use_alias  = (isset($_POST['use_alias']))  ? $_POST['use_alias']  : "S";
       $fsel_ok    = (isset($_POST['fsel_ok']))    ? $_POST['fsel_ok']    : "";
       $campos_sel = (isset($_POST['campos_sel'])) ? $_POST['campos_sel'] : "";
       $sel_regra  = (isset($_POST['sel_regra']))  ? $_POST['sel_regra']  : "";
       $embbed     = isset($_POST['embbed_groupby']) && 'Y' == $_POST['embbed_groupby'];
       $tbar_pos   = isset($_POST['toolbar_pos']) ? $_POST['toolbar_pos'] : '';
   }
   elseif (isset($_GET['script_case_init']))
   {
       $sc_init    = $_GET['script_case_init'];
       $path_img   = $_GET['path_img'];
       $path_btn   = $_GET['path_btn'];
       $use_alias  = (isset($_GET['use_alias']))  ? $_GET['use_alias']  : "S";
       $fsel_ok    = (isset($_GET['fsel_ok']))    ? $_GET['fsel_ok']    : "";
       $campos_sel = (isset($_GET['campos_sel'])) ? $_GET['campos_sel'] : "";
       $sel_regra  = (isset($_GET['sel_regra']))  ? $_GET['sel_regra']  : "";
       $embbed     = isset($_GET['embbed_groupby']) && 'Y' == $_GET['embbed_groupby'];
       $tbar_pos   = isset($_GET['toolbar_pos']) ? $_GET['toolbar_pos'] : '';
   }
   $STR_lang    = (isset($_SESSION['scriptcase']['str_lang']) && !empty($_SESSION['scriptcase']['str_lang'])) ? $_SESSION['scriptcase']['str_lang'] : "es";
   $NM_arq_lang = "../_lib/lang/" . $STR_lang . ".lang.php";
   $this->Nm_lang = array();
   if (is_file($NM_arq_lang))
   {
       include_once($NM_arq_lang);
   }
   
   $tab_ger_campos = array();
   $tab_def_campos = array();
   $tab_labels     = array();
   $tab_ger_campos['bp_id_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_id_paciente'] = "bp_id_paciente";
       $tab_converte["bp_id_paciente"]   = "bp_id_paciente";
   }
   else
   {
       $tab_def_campos['bp_id_paciente'] = "bp.ID_PACIENTE";
       $tab_converte["bp.ID_PACIENTE"]   = "bp_id_paciente";
   }
   $tab_labels["bp_id_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_id_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_id_paciente"] : "ID PACIENTE";
   $tab_ger_campos['bp_estado_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_estado_paciente'] = "bp_estado_paciente";
       $tab_converte["bp_estado_paciente"]   = "bp_estado_paciente";
   }
   else
   {
       $tab_def_campos['bp_estado_paciente'] = "bp.ESTADO_PACIENTE";
       $tab_converte["bp.ESTADO_PACIENTE"]   = "bp_estado_paciente";
   }
   $tab_labels["bp_estado_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_estado_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_estado_paciente"] : "ESTADO PACIENTE";
   $tab_ger_campos['bp_fecha_activacion_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_fecha_activacion_paciente'] = "bp_fecha_activacion_paciente";
       $tab_converte["bp_fecha_activacion_paciente"]   = "bp_fecha_activacion_paciente";
   }
   else
   {
       $tab_def_campos['bp_fecha_activacion_paciente'] = "bp.FECHA_ACTIVACION_PACIENTE";
       $tab_converte["bp.FECHA_ACTIVACION_PACIENTE"]   = "bp_fecha_activacion_paciente";
   }
   $tab_labels["bp_fecha_activacion_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_fecha_activacion_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_fecha_activacion_paciente"] : "FECHA ACTIVACION PACIENTE";
   $tab_ger_campos['bp_fecha_retiro_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_fecha_retiro_paciente'] = "bp_fecha_retiro_paciente";
       $tab_converte["bp_fecha_retiro_paciente"]   = "bp_fecha_retiro_paciente";
   }
   else
   {
       $tab_def_campos['bp_fecha_retiro_paciente'] = "bp.FECHA_RETIRO_PACIENTE";
       $tab_converte["bp.FECHA_RETIRO_PACIENTE"]   = "bp_fecha_retiro_paciente";
   }
   $tab_labels["bp_fecha_retiro_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_fecha_retiro_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_fecha_retiro_paciente"] : "FECHA RETIRO PACIENTE";
   $tab_ger_campos['bp_motivo_retiro_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_motivo_retiro_paciente'] = "bp_motivo_retiro_paciente";
       $tab_converte["bp_motivo_retiro_paciente"]   = "bp_motivo_retiro_paciente";
   }
   else
   {
       $tab_def_campos['bp_motivo_retiro_paciente'] = "bp.MOTIVO_RETIRO_PACIENTE";
       $tab_converte["bp.MOTIVO_RETIRO_PACIENTE"]   = "bp_motivo_retiro_paciente";
   }
   $tab_labels["bp_motivo_retiro_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_motivo_retiro_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_motivo_retiro_paciente"] : "MOTIVO RETIRO PACIENTE";
   $tab_ger_campos['bp_observacion_motivo_retiro_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_observacion_motivo_retiro_paciente'] = "cmp_maior_30_1";
       $tab_converte["cmp_maior_30_1"]   = "bp_observacion_motivo_retiro_paciente";
   }
   else
   {
       $tab_def_campos['bp_observacion_motivo_retiro_paciente'] = "bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE";
       $tab_converte["bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE"]   = "bp_observacion_motivo_retiro_paciente";
   }
   $tab_labels["bp_observacion_motivo_retiro_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_observacion_motivo_retiro_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_observacion_motivo_retiro_paciente"] : "OBSERVACION MOTIVO RETIRO PACIENTE";
   $tab_ger_campos['bp_identificacion_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_identificacion_paciente'] = "bp_identificacion_paciente";
       $tab_converte["bp_identificacion_paciente"]   = "bp_identificacion_paciente";
   }
   else
   {
       $tab_def_campos['bp_identificacion_paciente'] = "bp.IDENTIFICACION_PACIENTE";
       $tab_converte["bp.IDENTIFICACION_PACIENTE"]   = "bp_identificacion_paciente";
   }
   $tab_labels["bp_identificacion_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_identificacion_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_identificacion_paciente"] : "IDENTIFICACION PACIENTE";
   $tab_ger_campos['bp_nombre_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_nombre_paciente'] = "bp_nombre_paciente";
       $tab_converte["bp_nombre_paciente"]   = "bp_nombre_paciente";
   }
   else
   {
       $tab_def_campos['bp_nombre_paciente'] = "bp.NOMBRE_PACIENTE";
       $tab_converte["bp.NOMBRE_PACIENTE"]   = "bp_nombre_paciente";
   }
   $tab_labels["bp_nombre_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_nombre_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_nombre_paciente"] : "NOMBRE PACIENTE";
   $tab_ger_campos['bp_apellido_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_apellido_paciente'] = "bp_apellido_paciente";
       $tab_converte["bp_apellido_paciente"]   = "bp_apellido_paciente";
   }
   else
   {
       $tab_def_campos['bp_apellido_paciente'] = "bp.APELLIDO_PACIENTE";
       $tab_converte["bp.APELLIDO_PACIENTE"]   = "bp_apellido_paciente";
   }
   $tab_labels["bp_apellido_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_apellido_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_apellido_paciente"] : "APELLIDO PACIENTE";
   $tab_ger_campos['bp_telefono_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_telefono_paciente'] = "bp_telefono_paciente";
       $tab_converte["bp_telefono_paciente"]   = "bp_telefono_paciente";
   }
   else
   {
       $tab_def_campos['bp_telefono_paciente'] = "bp.TELEFONO_PACIENTE";
       $tab_converte["bp.TELEFONO_PACIENTE"]   = "bp_telefono_paciente";
   }
   $tab_labels["bp_telefono_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono_paciente"] : "TELEFONO PACIENTE";
   $tab_ger_campos['bp_telefono2_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_telefono2_paciente'] = "bp_telefono2_paciente";
       $tab_converte["bp_telefono2_paciente"]   = "bp_telefono2_paciente";
   }
   else
   {
       $tab_def_campos['bp_telefono2_paciente'] = "bp.TELEFONO2_PACIENTE";
       $tab_converte["bp.TELEFONO2_PACIENTE"]   = "bp_telefono2_paciente";
   }
   $tab_labels["bp_telefono2_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono2_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono2_paciente"] : "TELEFONO2 PACIENTE";
   $tab_ger_campos['bp_telefono3_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_telefono3_paciente'] = "bp_telefono3_paciente";
       $tab_converte["bp_telefono3_paciente"]   = "bp_telefono3_paciente";
   }
   else
   {
       $tab_def_campos['bp_telefono3_paciente'] = "bp.TELEFONO3_PACIENTE";
       $tab_converte["bp.TELEFONO3_PACIENTE"]   = "bp_telefono3_paciente";
   }
   $tab_labels["bp_telefono3_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono3_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono3_paciente"] : "TELEFONO3 PACIENTE";
   $tab_ger_campos['bp_correo_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_correo_paciente'] = "bp_correo_paciente";
       $tab_converte["bp_correo_paciente"]   = "bp_correo_paciente";
   }
   else
   {
       $tab_def_campos['bp_correo_paciente'] = "bp.CORREO_PACIENTE";
       $tab_converte["bp.CORREO_PACIENTE"]   = "bp_correo_paciente";
   }
   $tab_labels["bp_correo_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_correo_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_correo_paciente"] : "CORREO PACIENTE";
   $tab_ger_campos['bp_direccion_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_direccion_paciente'] = "bp_direccion_paciente";
       $tab_converte["bp_direccion_paciente"]   = "bp_direccion_paciente";
   }
   else
   {
       $tab_def_campos['bp_direccion_paciente'] = "bp.DIRECCION_PACIENTE";
       $tab_converte["bp.DIRECCION_PACIENTE"]   = "bp_direccion_paciente";
   }
   $tab_labels["bp_direccion_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_direccion_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_direccion_paciente"] : "DIRECCION PACIENTE";
   $tab_ger_campos['bp_barrio_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_barrio_paciente'] = "bp_barrio_paciente";
       $tab_converte["bp_barrio_paciente"]   = "bp_barrio_paciente";
   }
   else
   {
       $tab_def_campos['bp_barrio_paciente'] = "bp.BARRIO_PACIENTE";
       $tab_converte["bp.BARRIO_PACIENTE"]   = "bp_barrio_paciente";
   }
   $tab_labels["bp_barrio_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_barrio_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_barrio_paciente"] : "BARRIO PACIENTE";
   $tab_ger_campos['bp_departamento_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_departamento_paciente'] = "bp_departamento_paciente";
       $tab_converte["bp_departamento_paciente"]   = "bp_departamento_paciente";
   }
   else
   {
       $tab_def_campos['bp_departamento_paciente'] = "bp.DEPARTAMENTO_PACIENTE";
       $tab_converte["bp.DEPARTAMENTO_PACIENTE"]   = "bp_departamento_paciente";
   }
   $tab_labels["bp_departamento_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_departamento_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_departamento_paciente"] : "DEPARTAMENTO PACIENTE";
   $tab_ger_campos['bp_ciudad_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_ciudad_paciente'] = "bp_ciudad_paciente";
       $tab_converte["bp_ciudad_paciente"]   = "bp_ciudad_paciente";
   }
   else
   {
       $tab_def_campos['bp_ciudad_paciente'] = "bp.CIUDAD_PACIENTE";
       $tab_converte["bp.CIUDAD_PACIENTE"]   = "bp_ciudad_paciente";
   }
   $tab_labels["bp_ciudad_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_ciudad_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_ciudad_paciente"] : "CIUDAD PACIENTE";
   $tab_ger_campos['bp_genero_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_genero_paciente'] = "bp_genero_paciente";
       $tab_converte["bp_genero_paciente"]   = "bp_genero_paciente";
   }
   else
   {
       $tab_def_campos['bp_genero_paciente'] = "bp.GENERO_PACIENTE";
       $tab_converte["bp.GENERO_PACIENTE"]   = "bp_genero_paciente";
   }
   $tab_labels["bp_genero_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_genero_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_genero_paciente"] : "GENERO PACIENTE";
   $tab_ger_campos['bp_fecha_nacimineto_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_fecha_nacimineto_paciente'] = "bp_fecha_nacimineto_paciente";
       $tab_converte["bp_fecha_nacimineto_paciente"]   = "bp_fecha_nacimineto_paciente";
   }
   else
   {
       $tab_def_campos['bp_fecha_nacimineto_paciente'] = "bp.FECHA_NACIMINETO_PACIENTE";
       $tab_converte["bp.FECHA_NACIMINETO_PACIENTE"]   = "bp_fecha_nacimineto_paciente";
   }
   $tab_labels["bp_fecha_nacimineto_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_fecha_nacimineto_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_fecha_nacimineto_paciente"] : "FECHA NACIMINETO PACIENTE";
   $tab_ger_campos['bp_edad_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_edad_paciente'] = "bp_edad_paciente";
       $tab_converte["bp_edad_paciente"]   = "bp_edad_paciente";
   }
   else
   {
       $tab_def_campos['bp_edad_paciente'] = "bp.EDAD_PACIENTE";
       $tab_converte["bp.EDAD_PACIENTE"]   = "bp_edad_paciente";
   }
   $tab_labels["bp_edad_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_edad_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_edad_paciente"] : "EDAD PACIENTE";
   $tab_ger_campos['bp_acudiente_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_acudiente_paciente'] = "bp_acudiente_paciente";
       $tab_converte["bp_acudiente_paciente"]   = "bp_acudiente_paciente";
   }
   else
   {
       $tab_def_campos['bp_acudiente_paciente'] = "bp.ACUDIENTE_PACIENTE";
       $tab_converte["bp.ACUDIENTE_PACIENTE"]   = "bp_acudiente_paciente";
   }
   $tab_labels["bp_acudiente_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_acudiente_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_acudiente_paciente"] : "ACUDIENTE PACIENTE";
   $tab_ger_campos['bp_telefono_acudiente_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_telefono_acudiente_paciente'] = "bp_telefono_acudiente_paciente";
       $tab_converte["bp_telefono_acudiente_paciente"]   = "bp_telefono_acudiente_paciente";
   }
   else
   {
       $tab_def_campos['bp_telefono_acudiente_paciente'] = "bp.TELEFONO_ACUDIENTE_PACIENTE";
       $tab_converte["bp.TELEFONO_ACUDIENTE_PACIENTE"]   = "bp_telefono_acudiente_paciente";
   }
   $tab_labels["bp_telefono_acudiente_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono_acudiente_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_telefono_acudiente_paciente"] : "TELEFONO ACUDIENTE PACIENTE";
   $tab_ger_campos['bp_codigo_xofigo'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_codigo_xofigo'] = "bp_codigo_xofigo";
       $tab_converte["bp_codigo_xofigo"]   = "bp_codigo_xofigo";
   }
   else
   {
       $tab_def_campos['bp_codigo_xofigo'] = "bp.CODIGO_XOFIGO";
       $tab_converte["bp.CODIGO_XOFIGO"]   = "bp_codigo_xofigo";
   }
   $tab_labels["bp_codigo_xofigo"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_codigo_xofigo"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_codigo_xofigo"] : "CODIGO XOFIGO";
   $tab_ger_campos['bp_status_paciente'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_status_paciente'] = "bp_status_paciente";
       $tab_converte["bp_status_paciente"]   = "bp_status_paciente";
   }
   else
   {
       $tab_def_campos['bp_status_paciente'] = "bp.STATUS_PACIENTE";
       $tab_converte["bp.STATUS_PACIENTE"]   = "bp_status_paciente";
   }
   $tab_labels["bp_status_paciente"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_status_paciente"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_status_paciente"] : "STATUS PACIENTE";
   $tab_ger_campos['bp_id_ultima_gestion'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_id_ultima_gestion'] = "bp_id_ultima_gestion";
       $tab_converte["bp_id_ultima_gestion"]   = "bp_id_ultima_gestion";
   }
   else
   {
       $tab_def_campos['bp_id_ultima_gestion'] = "bp.ID_ULTIMA_GESTION";
       $tab_converte["bp.ID_ULTIMA_GESTION"]   = "bp_id_ultima_gestion";
   }
   $tab_labels["bp_id_ultima_gestion"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_id_ultima_gestion"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_id_ultima_gestion"] : "ID ULTIMA GESTION";
   $tab_ger_campos['bp_usuario_creacion'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bp_usuario_creacion'] = "bp_usuario_creacion";
       $tab_converte["bp_usuario_creacion"]   = "bp_usuario_creacion";
   }
   else
   {
       $tab_def_campos['bp_usuario_creacion'] = "bp.USUARIO_CREACION";
       $tab_converte["bp.USUARIO_CREACION"]   = "bp_usuario_creacion";
   }
   $tab_labels["bp_usuario_creacion"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_usuario_creacion"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bp_usuario_creacion"] : "USUARIO CREACION";
   $tab_ger_campos['bt_id_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_id_tratamiento'] = "bt_id_tratamiento";
       $tab_converte["bt_id_tratamiento"]   = "bt_id_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_id_tratamiento'] = "bt.ID_TRATAMIENTO";
       $tab_converte["bt.ID_TRATAMIENTO"]   = "bt_id_tratamiento";
   }
   $tab_labels["bt_id_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_id_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_id_tratamiento"] : "ID TRATAMIENTO";
   $tab_ger_campos['bt_producto_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_producto_tratamiento'] = "bt_producto_tratamiento";
       $tab_converte["bt_producto_tratamiento"]   = "bt_producto_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_producto_tratamiento'] = "bt.PRODUCTO_TRATAMIENTO";
       $tab_converte["bt.PRODUCTO_TRATAMIENTO"]   = "bt_producto_tratamiento";
   }
   $tab_labels["bt_producto_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_producto_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_producto_tratamiento"] : "PRODUCTO TRATAMIENTO";
   $tab_ger_campos['bt_nombre_referencia'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_nombre_referencia'] = "bt_nombre_referencia";
       $tab_converte["bt_nombre_referencia"]   = "bt_nombre_referencia";
   }
   else
   {
       $tab_def_campos['bt_nombre_referencia'] = "bt.NOMBRE_REFERENCIA";
       $tab_converte["bt.NOMBRE_REFERENCIA"]   = "bt_nombre_referencia";
   }
   $tab_labels["bt_nombre_referencia"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_nombre_referencia"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_nombre_referencia"] : "NOMBRE REFERENCIA";
   $tab_ger_campos['bt_clasificacion_patologica_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_clasificacion_patologica_tratamiento'] = "cmp_maior_30_2";
       $tab_converte["cmp_maior_30_2"]   = "bt_clasificacion_patologica_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_clasificacion_patologica_tratamiento'] = "bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO";
       $tab_converte["bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO"]   = "bt_clasificacion_patologica_tratamiento";
   }
   $tab_labels["bt_clasificacion_patologica_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_clasificacion_patologica_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_clasificacion_patologica_tratamiento"] : "CLASIFICACION PATOLOGICA TRATAMIENTO";
   $tab_ger_campos['bt_tratamiento_previo'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_tratamiento_previo'] = "bt_tratamiento_previo";
       $tab_converte["bt_tratamiento_previo"]   = "bt_tratamiento_previo";
   }
   else
   {
       $tab_def_campos['bt_tratamiento_previo'] = "bt.TRATAMIENTO_PREVIO";
       $tab_converte["bt.TRATAMIENTO_PREVIO"]   = "bt_tratamiento_previo";
   }
   $tab_labels["bt_tratamiento_previo"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_tratamiento_previo"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_tratamiento_previo"] : "TRATAMIENTO PREVIO";
   $tab_ger_campos['bt_consentimiento_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_consentimiento_tratamiento'] = "bt_consentimiento_tratamiento";
       $tab_converte["bt_consentimiento_tratamiento"]   = "bt_consentimiento_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_consentimiento_tratamiento'] = "bt.CONSENTIMIENTO_TRATAMIENTO";
       $tab_converte["bt.CONSENTIMIENTO_TRATAMIENTO"]   = "bt_consentimiento_tratamiento";
   }
   $tab_labels["bt_consentimiento_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_consentimiento_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_consentimiento_tratamiento"] : "CONSENTIMIENTO TRATAMIENTO";
   $tab_ger_campos['bt_fecha_inicio_terapia_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_fecha_inicio_terapia_tratamiento'] = "cmp_maior_30_3";
       $tab_converte["cmp_maior_30_3"]   = "bt_fecha_inicio_terapia_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_fecha_inicio_terapia_tratamiento'] = "bt.FECHA_INICIO_TERAPIA_TRATAMIENTO";
       $tab_converte["bt.FECHA_INICIO_TERAPIA_TRATAMIENTO"]   = "bt_fecha_inicio_terapia_tratamiento";
   }
   $tab_labels["bt_fecha_inicio_terapia_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_fecha_inicio_terapia_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_fecha_inicio_terapia_tratamiento"] : "FECHA INICIO TERAPIA TRATAMIENTO";
   $tab_ger_campos['bt_regimen_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_regimen_tratamiento'] = "bt_regimen_tratamiento";
       $tab_converte["bt_regimen_tratamiento"]   = "bt_regimen_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_regimen_tratamiento'] = "bt.REGIMEN_TRATAMIENTO";
       $tab_converte["bt.REGIMEN_TRATAMIENTO"]   = "bt_regimen_tratamiento";
   }
   $tab_labels["bt_regimen_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_regimen_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_regimen_tratamiento"] : "REGIMEN TRATAMIENTO";
   $tab_ger_campos['bt_asegurador_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_asegurador_tratamiento'] = "bt_asegurador_tratamiento";
       $tab_converte["bt_asegurador_tratamiento"]   = "bt_asegurador_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_asegurador_tratamiento'] = "bt.ASEGURADOR_TRATAMIENTO";
       $tab_converte["bt.ASEGURADOR_TRATAMIENTO"]   = "bt_asegurador_tratamiento";
   }
   $tab_labels["bt_asegurador_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_asegurador_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_asegurador_tratamiento"] : "ASEGURADOR TRATAMIENTO";
   $tab_ger_campos['bt_operador_logistico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_operador_logistico_tratamiento'] = "cmp_maior_30_4";
       $tab_converte["cmp_maior_30_4"]   = "bt_operador_logistico_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_operador_logistico_tratamiento'] = "bt.OPERADOR_LOGISTICO_TRATAMIENTO";
       $tab_converte["bt.OPERADOR_LOGISTICO_TRATAMIENTO"]   = "bt_operador_logistico_tratamiento";
   }
   $tab_labels["bt_operador_logistico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_operador_logistico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_operador_logistico_tratamiento"] : "OPERADOR LOGISTICO TRATAMIENTO";
   $tab_ger_campos['bt_punto_entrega'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_punto_entrega'] = "bt_punto_entrega";
       $tab_converte["bt_punto_entrega"]   = "bt_punto_entrega";
   }
   else
   {
       $tab_def_campos['bt_punto_entrega'] = "bt.PUNTO_ENTREGA";
       $tab_converte["bt.PUNTO_ENTREGA"]   = "bt_punto_entrega";
   }
   $tab_labels["bt_punto_entrega"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_punto_entrega"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_punto_entrega"] : "PUNTO ENTREGA";
   $tab_ger_campos['bt_fecha_ultima_reclamacion_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_fecha_ultima_reclamacion_tratamiento'] = "cmp_maior_30_5";
       $tab_converte["cmp_maior_30_5"]   = "bt_fecha_ultima_reclamacion_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_fecha_ultima_reclamacion_tratamiento'] = "bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO";
       $tab_converte["bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO"]   = "bt_fecha_ultima_reclamacion_tratamiento";
   }
   $tab_labels["bt_fecha_ultima_reclamacion_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_fecha_ultima_reclamacion_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_fecha_ultima_reclamacion_tratamiento"] : "FECHA ULTIMA RECLAMACION TRATAMIENTO";
   $tab_ger_campos['bt_otros_operadores_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_otros_operadores_tratamiento'] = "cmp_maior_30_6";
       $tab_converte["cmp_maior_30_6"]   = "bt_otros_operadores_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_otros_operadores_tratamiento'] = "bt.OTROS_OPERADORES_TRATAMIENTO";
       $tab_converte["bt.OTROS_OPERADORES_TRATAMIENTO"]   = "bt_otros_operadores_tratamiento";
   }
   $tab_labels["bt_otros_operadores_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_otros_operadores_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_otros_operadores_tratamiento"] : "OTROS OPERADORES TRATAMIENTO";
   $tab_ger_campos['bt_medios_adquisicion_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_medios_adquisicion_tratamiento'] = "cmp_maior_30_7";
       $tab_converte["cmp_maior_30_7"]   = "bt_medios_adquisicion_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_medios_adquisicion_tratamiento'] = "bt.MEDIOS_ADQUISICION_TRATAMIENTO";
       $tab_converte["bt.MEDIOS_ADQUISICION_TRATAMIENTO"]   = "bt_medios_adquisicion_tratamiento";
   }
   $tab_labels["bt_medios_adquisicion_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_medios_adquisicion_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_medios_adquisicion_tratamiento"] : "MEDIOS ADQUISICION TRATAMIENTO";
   $tab_ger_campos['bt_ips_atiende_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_ips_atiende_tratamiento'] = "bt_ips_atiende_tratamiento";
       $tab_converte["bt_ips_atiende_tratamiento"]   = "bt_ips_atiende_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_ips_atiende_tratamiento'] = "bt.IPS_ATIENDE_TRATAMIENTO";
       $tab_converte["bt.IPS_ATIENDE_TRATAMIENTO"]   = "bt_ips_atiende_tratamiento";
   }
   $tab_labels["bt_ips_atiende_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_ips_atiende_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_ips_atiende_tratamiento"] : "IPS ATIENDE TRATAMIENTO";
   $tab_ger_campos['bt_medico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_medico_tratamiento'] = "bt_medico_tratamiento";
       $tab_converte["bt_medico_tratamiento"]   = "bt_medico_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_medico_tratamiento'] = "bt.MEDICO_TRATAMIENTO";
       $tab_converte["bt.MEDICO_TRATAMIENTO"]   = "bt_medico_tratamiento";
   }
   $tab_labels["bt_medico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_medico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_medico_tratamiento"] : "MEDICO TRATAMIENTO";
   $tab_ger_campos['bt_especialidad_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_especialidad_tratamiento'] = "bt_especialidad_tratamiento";
       $tab_converte["bt_especialidad_tratamiento"]   = "bt_especialidad_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_especialidad_tratamiento'] = "bt.ESPECIALIDAD_TRATAMIENTO";
       $tab_converte["bt.ESPECIALIDAD_TRATAMIENTO"]   = "bt_especialidad_tratamiento";
   }
   $tab_labels["bt_especialidad_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_especialidad_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_especialidad_tratamiento"] : "ESPECIALIDAD TRATAMIENTO";
   $tab_ger_campos['bt_paramedico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_paramedico_tratamiento'] = "bt_paramedico_tratamiento";
       $tab_converte["bt_paramedico_tratamiento"]   = "bt_paramedico_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_paramedico_tratamiento'] = "bt.PARAMEDICO_TRATAMIENTO";
       $tab_converte["bt.PARAMEDICO_TRATAMIENTO"]   = "bt_paramedico_tratamiento";
   }
   $tab_labels["bt_paramedico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_paramedico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_paramedico_tratamiento"] : "PARAMEDICO TRATAMIENTO";
   $tab_ger_campos['bt_zona_atencion_paramedico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_zona_atencion_paramedico_tratamiento'] = "cmp_maior_30_8";
       $tab_converte["cmp_maior_30_8"]   = "bt_zona_atencion_paramedico_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_zona_atencion_paramedico_tratamiento'] = "bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO";
       $tab_converte["bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO"]   = "bt_zona_atencion_paramedico_tratamiento";
   }
   $tab_labels["bt_zona_atencion_paramedico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_zona_atencion_paramedico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_zona_atencion_paramedico_tratamiento"] : "ZONA ATENCION PARAMEDICO TRATAMIENTO";
   $tab_ger_campos['bt_ciudad_base_paramedico_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_ciudad_base_paramedico_tratamiento'] = "cmp_maior_30_9";
       $tab_converte["cmp_maior_30_9"]   = "bt_ciudad_base_paramedico_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_ciudad_base_paramedico_tratamiento'] = "bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO";
       $tab_converte["bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO"]   = "bt_ciudad_base_paramedico_tratamiento";
   }
   $tab_labels["bt_ciudad_base_paramedico_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_ciudad_base_paramedico_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_ciudad_base_paramedico_tratamiento"] : "CIUDAD BASE PARAMEDICO TRATAMIENTO";
   $tab_ger_campos['bt_notas_adjuntos_tratamiento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['bt_notas_adjuntos_tratamiento'] = "bt_notas_adjuntos_tratamiento";
       $tab_converte["bt_notas_adjuntos_tratamiento"]   = "bt_notas_adjuntos_tratamiento";
   }
   else
   {
       $tab_def_campos['bt_notas_adjuntos_tratamiento'] = "bt.NOTAS_ADJUNTOS_TRATAMIENTO";
       $tab_converte["bt.NOTAS_ADJUNTOS_TRATAMIENTO"]   = "bt_notas_adjuntos_tratamiento";
   }
   $tab_labels["bt_notas_adjuntos_tratamiento"]   = (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_notas_adjuntos_tratamiento"])) ? $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['labels']["bt_notas_adjuntos_tratamiento"] : "NOTAS ADJUNTOS TRATAMIENTO";
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['field_display']))
   {
       foreach ($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['field_display'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['php_cmp_sel']))
   {
       foreach ($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (!isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_select']))
   {
       $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_select'] = array();
   }
   
   if ($fsel_ok == "cmp")
   {
       $this->Sel_processa_out_sel($campos_sel);
   }
   else
   {
       if ($embbed)
       {
           ob_start();
           $this->Sel_processa_form();
           $Temp = ob_get_clean();
           echo NM_charset_to_utf8($Temp);
       }
       else
       {
           $this->Sel_processa_form();
       }
   }
   exit;
   
}
function Sel_processa_out_sel($campos_sel)
{
   global $tab_ger_campos, $sc_init, $tab_def_campos, $tab_converte, $embbed;
   $arr_temp = array();
   $campos_sel = explode("@?@", $campos_sel);
   $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_select'] = array();
   $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_grid']   = "";
   $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_cmp']    = "";
   foreach ($campos_sel as $campo_sort)
   {
       $ordem = (substr($campo_sort, 0, 1) == "+") ? "asc" : "desc";
       $campo = substr($campo_sort, 1);
       if (isset($tab_converte[$campo]))
       {
           $_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_select'][$campo] = $ordem;
       }
   }
?>
    <script language="javascript"> 
<?php
   if (!$embbed)
   {
?>
      self.parent.tb_remove(); 
      parent.nm_gp_submit_ajax('inicio', ''); 
<?php
   }
   else
   {
?>
      nm_gp_submit_ajax('inicio', ''); 
<?php
   }
?>
   </script>
<?php
}
   
function Sel_processa_form()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos;
   $size = 10;
   $_SESSION['scriptcase']['charset']  = (isset($this->Nm_lang['Nm_charset']) && !empty($this->Nm_lang['Nm_charset'])) ? $this->Nm_lang['Nm_charset'] : "UTF-8";
   foreach ($this->Nm_lang as $ind => $dados)
   {
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($ind))
      {
          $ind = sc_convert_encoding($ind, $_SESSION['scriptcase']['charset'], "UTF-8");
          $this->Nm_lang[$ind] = $dados;
      }
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($dados))
      {
          $this->Nm_lang[$ind] = sc_convert_encoding($dados, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
   }
   $str_schema_all = (isset($_SESSION['scriptcase']['str_schema_all']) && !empty($_SESSION['scriptcase']['str_schema_all'])) ? $_SESSION['scriptcase']['str_schema_all'] : "Sc8_Turquoise/Sc8_Turquoise";
   include("../_lib/css/" . $str_schema_all . "_grid.php");
   $Str_btn_grid = trim($str_button) . "/" . trim($str_button) . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".php";
   include("../_lib/buttons/" . $Str_btn_grid);
   if (!function_exists("nmButtonOutput"))
   {
       include_once("../_lib/lib/php/nm_gp_config_btn.php");
   }
   if (!$embbed)
   {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Listado Pacientes</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $_SESSION['scriptcase']['css_btn_popup'] ?>" /> 
</HEAD>
<BODY class="scGridPage" style="margin: 0px; overflow-x: hidden">
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery_plugin/touch_punch/jquery.ui.touch-punch.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/tigra_color_picker/picker.js"></script>
<?php
   }
?>
<script language="javascript"> 
<?php
if ($embbed)
{
?>
  function scSubmitOrderCampos(sPos, sType) {
    $("#id_fsel_ok_sel_ord").val(sType);
    if(sType == 'cmp')
    {
       scPackSelectedOrd();
    }
   $.ajax({
    type: "POST",
    url: "Listado_pacientes_order_campos.php",
    data: {
     script_case_init: $("#id_script_case_init_sel_ord").val(),
     script_case_session: $("#id_script_case_session_sel_ord").val(),
     path_img: $("#id_path_img_sel_ord").val(),
     path_btn: $("#id_path_btn_sel_ord").val(),
     campos_sel: $("#id_campos_sel_sel_ord").val(),
     sel_regra: $("#id_sel_regra_sel_ord").val(),
     fsel_ok: $("#id_fsel_ok_sel_ord").val(),
     embbed_groupby: 'Y'
    }
   }).success(function(data) {
    $("#sc_id_order_campos_placeholder_" + sPos).find("td").html(data);
    scBtnOrderCamposHide(sPos);
   });
  }
<?php
}
?>
 // Submeter o formularior
 //-------------------------------------
 function submit_form_Fsel_ord()
 {
     scPackSelectedOrd();
      document.Fsel_ord.submit();
 }
 function scPackSelectedOrd() {
  var fieldList, fieldName, i, selectedFields = new Array;
 fieldList = $("#sc_id_fldord_selected").sortable("toArray");
 for (i = 0; i < fieldList.length; i++) {
  fieldName  = fieldList[i].substr(14);
  selectedFields.push($("#sc_id_class_" + fieldName).val() + fieldName);
 }
 $("#id_campos_sel_sel_ord").val( selectedFields.join("@?@") );
 }
 </script>
<FORM name="Fsel_ord" method="POST">
  <INPUT type="hidden" name="script_case_init"    id="id_script_case_init_sel_ord"    value="<?php echo NM_encode_input($sc_init); ?>"> 
  <INPUT type="hidden" name="script_case_session" id="id_script_case_session_sel_ord" value="<?php echo NM_encode_input(session_id()); ?>"> 
  <INPUT type="hidden" name="path_img"            id="id_path_img_sel_ord"            value="<?php echo NM_encode_input($path_img); ?>"> 
  <INPUT type="hidden" name="path_btn"            id="id_path_btn_sel_ord"            value="<?php echo NM_encode_input($path_btn); ?>"> 
  <INPUT type="hidden" name="fsel_ok"             id="id_fsel_ok_sel_ord"             value=""> 
<?php
if ($embbed)
{
    echo "<div class='scAppDivMoldura'>";
    echo "<table id=\"main_table\" style=\"width: 100%\" cellspacing=0 cellpadding=0>";
}
elseif ($_SESSION['scriptcase']['reg_conf']['html_dir'] == " DIR='RTL'")
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; right: 20px\">";
}
else
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; left: 20px\">";
}
?>
<?php
if (!$embbed)
{
?>
<tr>
<td>
<div class="scGridBorder">
<table width='100%' cellspacing=0 cellpadding=0>
<?php
}
?>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivHeader scAppDivHeaderText':'scGridLabelVert'; ?>">
   <?php echo $this->Nm_lang['lang_btns_sort_hint']; ?>
  </td>
 </tr>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivContent css_scAppDivContentText':'scGridTabelaTd'; ?>">
   <table class="<?php echo ($embbed)? '':'scGridTabela'; ?>" style="border-width: 0; border-collapse: collapse; width:100%;" cellspacing=0 cellpadding=0>
    <tr class="<?php echo ($embbed)? '':'scGridFieldOddVert'; ?>">
     <td style="vertical-align: top">
     <table>
   <tr><td style="vertical-align: top">
 <script language="javascript" type="text/javascript">
  $(function() {
   $(".sc_ui_litem").mouseover(function() {
    $(this).css("cursor", "all-scroll");
   });
   $("#sc_id_fldord_available").sortable({
    connectWith: ".sc_ui_fldord_selected",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).show();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   }).disableSelection();
   $("#sc_id_fldord_selected").sortable({
    connectWith: ".sc_ui_fldord_available",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).hide();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   });
   scUpdateListHeight();
  });
  function scUpdateListHeight() {
   $("#sc_id_fldord_available").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
   $("#sc_id_fldord_selected").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
  }
 </script>
 <style type="text/css">
  .sc_ui_sortable_ord {
   list-style-type: none;
   margin: 0;
   min-width: 225px;
  }
  .sc_ui_sortable_ord li {
   margin: 0 3px 3px 3px;
   padding: 1px 3px 1px 15px;
   min-height: 28px;
  }
  .sc_ui_sortable_ord li span {
   position: absolute;
   margin-left: -1.3em;
  }
 </style>
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_available scAppDivSelectFields" id="sc_id_fldord_available">
<?php
   foreach ($tab_ger_campos as $NM_cada_field => $NM_cada_opc)
   {
       if ($NM_cada_opc != "none")
       {
           if (!isset($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_select'][$tab_def_campos[$NM_cada_field]]))
           {
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>">
      <?php echo $tab_labels[$NM_cada_field]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>" class="scAppDivToolbarInput" style="display: none">
       <option value="+">Asc</option>
       <option value="-">Desc</option>
      </select><br/>
     </li>
<?php
           }
       }
   }
?>
    </ul>
   </td>
   <td style="vertical-align: top">
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_selected scAppDivSelectFields" id="sc_id_fldord_selected">
<?php
   foreach ($_SESSION['sc_session'][$sc_init]['Listado_pacientes']['ordem_select'] as $NM_cada_field => $NM_cada_opc)
   {
       if (isset($tab_converte[$NM_cada_field]))
       {
           $sAscSelected  = " selected";
           $sDescSelected = "";
           if ($NM_cada_opc == "desc")
           {
               $sAscSelected  = "";
               $sDescSelected = " selected";
           }
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo $NM_cada_field; ?>">
      <?php echo $tab_labels[$tab_converte[$NM_cada_field]]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[ $tab_converte[$NM_cada_field] ]); ?>" class="scAppDivToolbarInput" onchange="$('#f_sel_sub').css('display', 'inline-block');">
       <option value="+"<?php echo $sAscSelected; ?>>Asc</option>
       <option value="-"<?php echo $sDescSelected; ?>>Desc</option>
      </select>
     </li>
<?php
       }
   }
?>
    </ul>
    <input type="hidden" name="campos_sel" id="id_campos_sel_sel_ord" value="">
   </td>
   </tr>
   </table>
   </td>
   </tr>
   </table>
  </td>
 </tr>
   <tr><td class="<?php echo ($embbed)? 'scAppDivToolbar':'scGridToolbar'; ?>">
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bok", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "f_sel_sub", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bapply", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "f_sel_sub", "", "", "display: none;", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
  &nbsp;&nbsp;&nbsp;
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bsair", "self.parent.tb_remove()", "self.parent.tb_remove()", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
   </td>
   </tr>
<?php
if (!$embbed)
{
?>
</table>
</div>
</td>
</tr>
<?php
}
?>
</table>
<?php
if ($embbed)
{
?>
    </div>
<?php
}
?>
</FORM>
<script language="javascript"> 
var bFixed = false;
function ajusta_window_Fsel_ord()
{
<?php
   if ($embbed)
   {
?>
  return false;
<?php
   }
?>
  var mt = $(document.getElementById("main_table"));
  if (0 == mt.width() || 0 == mt.height())
  {
    setTimeout("ajusta_window_Fsel_ord()", 50);
    return;
  }
  else if(!bFixed)
  {
    var oOrig = $(document.Fsel_ord.sel_orig),
        oDest = $(document.Fsel_ord.sel_dest),
        mHeight = Math.max(oOrig.height(), oDest.height()),
        mWidth = Math.max(oOrig.width() + 5, oDest.width() + 5);
    oOrig.height(mHeight);
    oOrig.width(mWidth);
    oDest.height(mHeight);
    oDest.width(mWidth + 15);
    bFixed = true;
    if (navigator.userAgent.indexOf("Chrome/") > 0)
    {
      strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
      self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
      setTimeout("ajusta_window_Fsel_ord()", 50);
      return;
    }
  }
  strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
  self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
}
$( document ).ready(function() {
  ajusta_window_Fsel_ord();
});
</script>
<script>
    ajusta_window_Fsel_ord();
</script>
</BODY>
</HTML>
<?php
}
}
