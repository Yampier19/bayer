<?php

class Listado_pacientes_xls
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Xls_dados;
   var $Xls_workbook;
   var $Xls_col;
   var $Xls_row;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();
   var $Arquivo;
   var $Tit_doc;
   //---- 
   function Listado_pacientes_xls()
   {
   }

   //---- 
   function monta_xls()
   {
      $this->inicializa_vars();
      $this->grava_arquivo();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $this->Xls_row = 1;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      set_include_path(get_include_path() . PATH_SEPARATOR . $this->Ini->path_third . '/phpexcel/');
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel.php';
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel/IOFactory.php';
      require_once $this->Ini->path_third . '/phpexcel/PHPExcel/Cell/AdvancedValueBinder.php';
      $orig_form_dt = strtoupper($_SESSION['scriptcase']['reg_conf']['date_format']);
      $this->SC_date_conf_region = "";
      for ($i = 0; $i < 8; $i++)
      {
          if ($i > 0 && substr($orig_form_dt, $i, 1) != substr($this->SC_date_conf_region, -1, 1)) {
              $this->SC_date_conf_region .= $_SESSION['scriptcase']['reg_conf']['date_sep'];
          }
          $this->SC_date_conf_region .= substr($orig_form_dt, $i, 1);
      }
      $this->Xls_col    = 0;
      $this->nm_data    = new nm_data("es");
      $this->Arquivo    = "sc_xls";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_Listado_pacientes";
      $this->Arquivo   .= ".xls";
      $this->Tit_doc    = "Listado_pacientes.xls";
      $this->Xls_f = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );;
      $this->Xls_dados = new PHPExcel();
      $this->Xls_dados->setActiveSheetIndex(0);
      $this->Nm_ActiveSheet = $this->Xls_dados->getActiveSheet();
      if ($_SESSION['scriptcase']['reg_conf']['css_dir'] == "RTL")
      {
          $this->Nm_ActiveSheet->setRightToLeft(true);
      }
   }

   //----- 
   function grava_arquivo()
   {
      global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->bp_id_paciente = $Busca_temp['bp_id_paciente']; 
          $tmp_pos = strpos($this->bp_id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_id_paciente = substr($this->bp_id_paciente, 0, $tmp_pos);
          }
          $this->bp_id_paciente_2 = $Busca_temp['bp_id_paciente_input_2']; 
          $this->bp_estado_paciente = $Busca_temp['bp_estado_paciente']; 
          $tmp_pos = strpos($this->bp_estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_estado_paciente = substr($this->bp_estado_paciente, 0, $tmp_pos);
          }
          $this->bp_status_paciente = $Busca_temp['bp_status_paciente']; 
          $tmp_pos = strpos($this->bp_status_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_status_paciente = substr($this->bp_status_paciente, 0, $tmp_pos);
          }
          $this->bp_fecha_activacion_paciente = $Busca_temp['bp_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->bp_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->bp_fecha_activacion_paciente = substr($this->bp_fecha_activacion_paciente, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['xls_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['xls_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['xls_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['xls_name']);
          $this->Xls_f = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.STATUS_PACIENTE as bp_status_paciente, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.PARAMEDICO_TRATAMIENTO as bt_paramedico_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.STATUS_PACIENTE as bp_status_paciente, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.PARAMEDICO_TRATAMIENTO as bt_paramedico_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.STATUS_PACIENTE as bp_status_paciente, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.PARAMEDICO_TRATAMIENTO as bt_paramedico_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.STATUS_PACIENTE as bp_status_paciente, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.PARAMEDICO_TRATAMIENTO as bt_paramedico_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.STATUS_PACIENTE as bp_status_paciente, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.PARAMEDICO_TRATAMIENTO as bt_paramedico_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.STATUS_PACIENTE as bp_status_paciente, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.PARAMEDICO_TRATAMIENTO as bt_paramedico_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['bp_id_paciente'])) ? $this->New_label['bp_id_paciente'] : "ID PACIENTE"; 
          if ($Cada_col == "bp_id_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_estado_paciente'])) ? $this->New_label['bp_estado_paciente'] : "ESTADO PACIENTE"; 
          if ($Cada_col == "bp_estado_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_fecha_activacion_paciente'])) ? $this->New_label['bp_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "bp_fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_fecha_retiro_paciente'])) ? $this->New_label['bp_fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; 
          if ($Cada_col == "bp_fecha_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_motivo_retiro_paciente'])) ? $this->New_label['bp_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          if ($Cada_col == "bp_motivo_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_observacion_motivo_retiro_paciente'])) ? $this->New_label['bp_observacion_motivo_retiro_paciente'] : "OBSERVACION MOTIVO RETIRO PACIENTE"; 
          if ($Cada_col == "bp_observacion_motivo_retiro_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_identificacion_paciente'])) ? $this->New_label['bp_identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          if ($Cada_col == "bp_identificacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_nombre_paciente'])) ? $this->New_label['bp_nombre_paciente'] : "NOMBRE PACIENTE"; 
          if ($Cada_col == "bp_nombre_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_apellido_paciente'])) ? $this->New_label['bp_apellido_paciente'] : "APELLIDO PACIENTE"; 
          if ($Cada_col == "bp_apellido_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_telefono_paciente'])) ? $this->New_label['bp_telefono_paciente'] : "TELEFONO PACIENTE"; 
          if ($Cada_col == "bp_telefono_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_telefono2_paciente'])) ? $this->New_label['bp_telefono2_paciente'] : "TELEFONO2 PACIENTE"; 
          if ($Cada_col == "bp_telefono2_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_telefono3_paciente'])) ? $this->New_label['bp_telefono3_paciente'] : "TELEFONO3 PACIENTE"; 
          if ($Cada_col == "bp_telefono3_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_correo_paciente'])) ? $this->New_label['bp_correo_paciente'] : "CORREO PACIENTE"; 
          if ($Cada_col == "bp_correo_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_direccion_paciente'])) ? $this->New_label['bp_direccion_paciente'] : "DIRECCION PACIENTE"; 
          if ($Cada_col == "bp_direccion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_barrio_paciente'])) ? $this->New_label['bp_barrio_paciente'] : "BARRIO PACIENTE"; 
          if ($Cada_col == "bp_barrio_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_departamento_paciente'])) ? $this->New_label['bp_departamento_paciente'] : "DEPARTAMENTO PACIENTE"; 
          if ($Cada_col == "bp_departamento_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_ciudad_paciente'])) ? $this->New_label['bp_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          if ($Cada_col == "bp_ciudad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_genero_paciente'])) ? $this->New_label['bp_genero_paciente'] : "GENERO PACIENTE"; 
          if ($Cada_col == "bp_genero_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_fecha_nacimineto_paciente'])) ? $this->New_label['bp_fecha_nacimineto_paciente'] : "FECHA NACIMINETO PACIENTE"; 
          if ($Cada_col == "bp_fecha_nacimineto_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_edad_paciente'])) ? $this->New_label['bp_edad_paciente'] : "EDAD PACIENTE"; 
          if ($Cada_col == "bp_edad_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_acudiente_paciente'])) ? $this->New_label['bp_acudiente_paciente'] : "ACUDIENTE PACIENTE"; 
          if ($Cada_col == "bp_acudiente_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_telefono_acudiente_paciente'])) ? $this->New_label['bp_telefono_acudiente_paciente'] : "TELEFONO ACUDIENTE PACIENTE"; 
          if ($Cada_col == "bp_telefono_acudiente_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_codigo_xofigo'])) ? $this->New_label['bp_codigo_xofigo'] : "CODIGO XOFIGO"; 
          if ($Cada_col == "bp_codigo_xofigo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_status_paciente'])) ? $this->New_label['bp_status_paciente'] : "STATUS PACIENTE"; 
          if ($Cada_col == "bp_status_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_id_ultima_gestion'])) ? $this->New_label['bp_id_ultima_gestion'] : "ID ULTIMA GESTION"; 
          if ($Cada_col == "bp_id_ultima_gestion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bp_usuario_creacion'])) ? $this->New_label['bp_usuario_creacion'] : "USUARIO CREACION"; 
          if ($Cada_col == "bp_usuario_creacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_id_tratamiento'])) ? $this->New_label['bt_id_tratamiento'] : "ID TRATAMIENTO"; 
          if ($Cada_col == "bt_id_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_producto_tratamiento'])) ? $this->New_label['bt_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          if ($Cada_col == "bt_producto_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_nombre_referencia'])) ? $this->New_label['bt_nombre_referencia'] : "NOMBRE REFERENCIA"; 
          if ($Cada_col == "bt_nombre_referencia" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_clasificacion_patologica_tratamiento'])) ? $this->New_label['bt_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          if ($Cada_col == "bt_clasificacion_patologica_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_tratamiento_previo'])) ? $this->New_label['bt_tratamiento_previo'] : "TRATAMIENTO PREVIO"; 
          if ($Cada_col == "bt_tratamiento_previo" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_consentimiento_tratamiento'])) ? $this->New_label['bt_consentimiento_tratamiento'] : "CONSENTIMIENTO TRATAMIENTO"; 
          if ($Cada_col == "bt_consentimiento_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_fecha_inicio_terapia_tratamiento'])) ? $this->New_label['bt_fecha_inicio_terapia_tratamiento'] : "FECHA INICIO TERAPIA TRATAMIENTO"; 
          if ($Cada_col == "bt_fecha_inicio_terapia_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_regimen_tratamiento'])) ? $this->New_label['bt_regimen_tratamiento'] : "REGIMEN TRATAMIENTO"; 
          if ($Cada_col == "bt_regimen_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_asegurador_tratamiento'])) ? $this->New_label['bt_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          if ($Cada_col == "bt_asegurador_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_operador_logistico_tratamiento'])) ? $this->New_label['bt_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          if ($Cada_col == "bt_operador_logistico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_punto_entrega'])) ? $this->New_label['bt_punto_entrega'] : "PUNTO ENTREGA"; 
          if ($Cada_col == "bt_punto_entrega" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_fecha_ultima_reclamacion_tratamiento'])) ? $this->New_label['bt_fecha_ultima_reclamacion_tratamiento'] : "FECHA ULTIMA RECLAMACION TRATAMIENTO"; 
          if ($Cada_col == "bt_fecha_ultima_reclamacion_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_otros_operadores_tratamiento'])) ? $this->New_label['bt_otros_operadores_tratamiento'] : "OTROS OPERADORES TRATAMIENTO"; 
          if ($Cada_col == "bt_otros_operadores_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_medios_adquisicion_tratamiento'])) ? $this->New_label['bt_medios_adquisicion_tratamiento'] : "MEDIOS ADQUISICION TRATAMIENTO"; 
          if ($Cada_col == "bt_medios_adquisicion_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_ips_atiende_tratamiento'])) ? $this->New_label['bt_ips_atiende_tratamiento'] : "IPS ATIENDE TRATAMIENTO"; 
          if ($Cada_col == "bt_ips_atiende_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_medico_tratamiento'])) ? $this->New_label['bt_medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          if ($Cada_col == "bt_medico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_especialidad_tratamiento'])) ? $this->New_label['bt_especialidad_tratamiento'] : "ESPECIALIDAD TRATAMIENTO"; 
          if ($Cada_col == "bt_especialidad_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_paramedico_tratamiento'])) ? $this->New_label['bt_paramedico_tratamiento'] : "PARAMEDICO TRATAMIENTO"; 
          if ($Cada_col == "bt_paramedico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_zona_atencion_paramedico_tratamiento'])) ? $this->New_label['bt_zona_atencion_paramedico_tratamiento'] : "ZONA ATENCION PARAMEDICO TRATAMIENTO"; 
          if ($Cada_col == "bt_zona_atencion_paramedico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_ciudad_base_paramedico_tratamiento'])) ? $this->New_label['bt_ciudad_base_paramedico_tratamiento'] : "CIUDAD BASE PARAMEDICO TRATAMIENTO"; 
          if ($Cada_col == "bt_ciudad_base_paramedico_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_notas_adjuntos_tratamiento'])) ? $this->New_label['bt_notas_adjuntos_tratamiento'] : "NOTAS ADJUNTOS TRATAMIENTO"; 
          if ($Cada_col == "bt_notas_adjuntos_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
          $SC_Label = (isset($this->New_label['bt_dosis_tratamiento'])) ? $this->New_label['bt_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          if ($Cada_col == "bt_dosis_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
             if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
              $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $SC_Label);
              $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getFont()->setBold(true);
              $this->Nm_ActiveSheet->getColumnDimension($this->calc_cell($this->Xls_col))->setAutoSize(true);
              $this->Xls_col++;
          }
      } 
      while (!$rs->EOF)
      {
         $this->Xls_col = 0;
         $this->Xls_row++;
         $this->bp_id_paciente = $rs->fields[0] ;  
         $this->bp_id_paciente = (string)$this->bp_id_paciente;
         $this->bp_estado_paciente = $rs->fields[1] ;  
         $this->bp_fecha_activacion_paciente = $rs->fields[2] ;  
         $this->bp_fecha_retiro_paciente = $rs->fields[3] ;  
         $this->bp_motivo_retiro_paciente = $rs->fields[4] ;  
         $this->bp_observacion_motivo_retiro_paciente = $rs->fields[5] ;  
         $this->bp_identificacion_paciente = $rs->fields[6] ;  
         $this->bp_nombre_paciente = $rs->fields[7] ;  
         $this->bp_apellido_paciente = $rs->fields[8] ;  
         $this->bp_telefono_paciente = $rs->fields[9] ;  
         $this->bp_telefono2_paciente = $rs->fields[10] ;  
         $this->bp_telefono3_paciente = $rs->fields[11] ;  
         $this->bp_correo_paciente = $rs->fields[12] ;  
         $this->bp_direccion_paciente = $rs->fields[13] ;  
         $this->bp_barrio_paciente = $rs->fields[14] ;  
         $this->bp_departamento_paciente = $rs->fields[15] ;  
         $this->bp_ciudad_paciente = $rs->fields[16] ;  
         $this->bp_genero_paciente = $rs->fields[17] ;  
         $this->bp_fecha_nacimineto_paciente = $rs->fields[18] ;  
         $this->bp_edad_paciente = $rs->fields[19] ;  
         $this->bp_edad_paciente = (string)$this->bp_edad_paciente;
         $this->bp_acudiente_paciente = $rs->fields[20] ;  
         $this->bp_telefono_acudiente_paciente = $rs->fields[21] ;  
         $this->bp_codigo_xofigo = $rs->fields[22] ;  
         $this->bp_codigo_xofigo = (string)$this->bp_codigo_xofigo;
         $this->bp_status_paciente = $rs->fields[23] ;  
         $this->bp_id_ultima_gestion = $rs->fields[24] ;  
         $this->bp_id_ultima_gestion = (string)$this->bp_id_ultima_gestion;
         $this->bp_usuario_creacion = $rs->fields[25] ;  
         $this->bt_id_tratamiento = $rs->fields[26] ;  
         $this->bt_id_tratamiento = (string)$this->bt_id_tratamiento;
         $this->bt_producto_tratamiento = $rs->fields[27] ;  
         $this->bt_nombre_referencia = $rs->fields[28] ;  
         $this->bt_clasificacion_patologica_tratamiento = $rs->fields[29] ;  
         $this->bt_tratamiento_previo = $rs->fields[30] ;  
         $this->bt_consentimiento_tratamiento = $rs->fields[31] ;  
         $this->bt_fecha_inicio_terapia_tratamiento = $rs->fields[32] ;  
         $this->bt_regimen_tratamiento = $rs->fields[33] ;  
         $this->bt_asegurador_tratamiento = $rs->fields[34] ;  
         $this->bt_operador_logistico_tratamiento = $rs->fields[35] ;  
         $this->bt_punto_entrega = $rs->fields[36] ;  
         $this->bt_fecha_ultima_reclamacion_tratamiento = $rs->fields[37] ;  
         $this->bt_otros_operadores_tratamiento = $rs->fields[38] ;  
         $this->bt_medios_adquisicion_tratamiento = $rs->fields[39] ;  
         $this->bt_ips_atiende_tratamiento = $rs->fields[40] ;  
         $this->bt_medico_tratamiento = $rs->fields[41] ;  
         $this->bt_especialidad_tratamiento = $rs->fields[42] ;  
         $this->bt_paramedico_tratamiento = $rs->fields[43] ;  
         $this->bt_zona_atencion_paramedico_tratamiento = $rs->fields[44] ;  
         $this->bt_ciudad_base_paramedico_tratamiento = $rs->fields[45] ;  
         $this->bt_notas_adjuntos_tratamiento = $rs->fields[46] ;  
         $this->bt_dosis_tratamiento = $rs->fields[47] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         if (isset($this->NM_Row_din[$this->Xls_row]))
         { 
             $this->Nm_ActiveSheet->getRowDimension($this->Xls_row)->setRowHeight($this->NM_Row_din[$this->Xls_row]);
         } 
         $rs->MoveNext();
      }
      if (isset($this->NM_Col_din))
      { 
          foreach ($this->NM_Col_din as $col => $width)
          { 
              $this->Nm_ActiveSheet->getColumnDimension($col)->setWidth($width / 5);
          } 
      } 
      $rs->Close();
      $objWriter = new PHPExcel_Writer_Excel5($this->Xls_dados);
      $objWriter->save($this->Xls_f);
   }
   //----- bp_id_paciente
   function NM_export_bp_id_paciente()
   {
         $this->bp_id_paciente = html_entity_decode($this->bp_id_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_id_paciente = strip_tags($this->bp_id_paciente);
         if (!NM_is_utf8($this->bp_id_paciente))
         {
             $this->bp_id_paciente = sc_convert_encoding($this->bp_id_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_id_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_estado_paciente
   function NM_export_bp_estado_paciente()
   {
         $this->bp_estado_paciente = html_entity_decode($this->bp_estado_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_estado_paciente = strip_tags($this->bp_estado_paciente);
         if (!NM_is_utf8($this->bp_estado_paciente))
         {
             $this->bp_estado_paciente = sc_convert_encoding($this->bp_estado_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_estado_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_fecha_activacion_paciente
   function NM_export_bp_fecha_activacion_paciente()
   {
         $this->bp_fecha_activacion_paciente = html_entity_decode($this->bp_fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_fecha_activacion_paciente = strip_tags($this->bp_fecha_activacion_paciente);
         if (!NM_is_utf8($this->bp_fecha_activacion_paciente))
         {
             $this->bp_fecha_activacion_paciente = sc_convert_encoding($this->bp_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_fecha_activacion_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_fecha_retiro_paciente
   function NM_export_bp_fecha_retiro_paciente()
   {
         $this->bp_fecha_retiro_paciente = html_entity_decode($this->bp_fecha_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_fecha_retiro_paciente = strip_tags($this->bp_fecha_retiro_paciente);
         if (!NM_is_utf8($this->bp_fecha_retiro_paciente))
         {
             $this->bp_fecha_retiro_paciente = sc_convert_encoding($this->bp_fecha_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_fecha_retiro_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_motivo_retiro_paciente
   function NM_export_bp_motivo_retiro_paciente()
   {
         $this->bp_motivo_retiro_paciente = html_entity_decode($this->bp_motivo_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_motivo_retiro_paciente = strip_tags($this->bp_motivo_retiro_paciente);
         if (!NM_is_utf8($this->bp_motivo_retiro_paciente))
         {
             $this->bp_motivo_retiro_paciente = sc_convert_encoding($this->bp_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_motivo_retiro_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_observacion_motivo_retiro_paciente
   function NM_export_bp_observacion_motivo_retiro_paciente()
   {
         $this->bp_observacion_motivo_retiro_paciente = html_entity_decode($this->bp_observacion_motivo_retiro_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_observacion_motivo_retiro_paciente = strip_tags($this->bp_observacion_motivo_retiro_paciente);
         if (!NM_is_utf8($this->bp_observacion_motivo_retiro_paciente))
         {
             $this->bp_observacion_motivo_retiro_paciente = sc_convert_encoding($this->bp_observacion_motivo_retiro_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_observacion_motivo_retiro_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_identificacion_paciente
   function NM_export_bp_identificacion_paciente()
   {
         $this->bp_identificacion_paciente = html_entity_decode($this->bp_identificacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_identificacion_paciente = strip_tags($this->bp_identificacion_paciente);
         if (!NM_is_utf8($this->bp_identificacion_paciente))
         {
             $this->bp_identificacion_paciente = sc_convert_encoding($this->bp_identificacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_identificacion_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_nombre_paciente
   function NM_export_bp_nombre_paciente()
   {
         $this->bp_nombre_paciente = html_entity_decode($this->bp_nombre_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_nombre_paciente = strip_tags($this->bp_nombre_paciente);
         if (!NM_is_utf8($this->bp_nombre_paciente))
         {
             $this->bp_nombre_paciente = sc_convert_encoding($this->bp_nombre_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_nombre_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_apellido_paciente
   function NM_export_bp_apellido_paciente()
   {
         $this->bp_apellido_paciente = html_entity_decode($this->bp_apellido_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_apellido_paciente = strip_tags($this->bp_apellido_paciente);
         if (!NM_is_utf8($this->bp_apellido_paciente))
         {
             $this->bp_apellido_paciente = sc_convert_encoding($this->bp_apellido_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_apellido_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_telefono_paciente
   function NM_export_bp_telefono_paciente()
   {
         $this->bp_telefono_paciente = html_entity_decode($this->bp_telefono_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_telefono_paciente = strip_tags($this->bp_telefono_paciente);
         if (!NM_is_utf8($this->bp_telefono_paciente))
         {
             $this->bp_telefono_paciente = sc_convert_encoding($this->bp_telefono_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_telefono_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_telefono2_paciente
   function NM_export_bp_telefono2_paciente()
   {
         $this->bp_telefono2_paciente = html_entity_decode($this->bp_telefono2_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_telefono2_paciente = strip_tags($this->bp_telefono2_paciente);
         if (!NM_is_utf8($this->bp_telefono2_paciente))
         {
             $this->bp_telefono2_paciente = sc_convert_encoding($this->bp_telefono2_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_telefono2_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_telefono3_paciente
   function NM_export_bp_telefono3_paciente()
   {
         $this->bp_telefono3_paciente = html_entity_decode($this->bp_telefono3_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_telefono3_paciente = strip_tags($this->bp_telefono3_paciente);
         if (!NM_is_utf8($this->bp_telefono3_paciente))
         {
             $this->bp_telefono3_paciente = sc_convert_encoding($this->bp_telefono3_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_telefono3_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_correo_paciente
   function NM_export_bp_correo_paciente()
   {
         $this->bp_correo_paciente = html_entity_decode($this->bp_correo_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_correo_paciente = strip_tags($this->bp_correo_paciente);
         if (!NM_is_utf8($this->bp_correo_paciente))
         {
             $this->bp_correo_paciente = sc_convert_encoding($this->bp_correo_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_correo_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_direccion_paciente
   function NM_export_bp_direccion_paciente()
   {
         $this->bp_direccion_paciente = html_entity_decode($this->bp_direccion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_direccion_paciente = strip_tags($this->bp_direccion_paciente);
         if (!NM_is_utf8($this->bp_direccion_paciente))
         {
             $this->bp_direccion_paciente = sc_convert_encoding($this->bp_direccion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_direccion_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_barrio_paciente
   function NM_export_bp_barrio_paciente()
   {
         $this->bp_barrio_paciente = html_entity_decode($this->bp_barrio_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_barrio_paciente = strip_tags($this->bp_barrio_paciente);
         if (!NM_is_utf8($this->bp_barrio_paciente))
         {
             $this->bp_barrio_paciente = sc_convert_encoding($this->bp_barrio_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_barrio_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_departamento_paciente
   function NM_export_bp_departamento_paciente()
   {
         $this->bp_departamento_paciente = html_entity_decode($this->bp_departamento_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_departamento_paciente = strip_tags($this->bp_departamento_paciente);
         if (!NM_is_utf8($this->bp_departamento_paciente))
         {
             $this->bp_departamento_paciente = sc_convert_encoding($this->bp_departamento_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_departamento_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_ciudad_paciente
   function NM_export_bp_ciudad_paciente()
   {
         $this->bp_ciudad_paciente = html_entity_decode($this->bp_ciudad_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_ciudad_paciente = strip_tags($this->bp_ciudad_paciente);
         if (!NM_is_utf8($this->bp_ciudad_paciente))
         {
             $this->bp_ciudad_paciente = sc_convert_encoding($this->bp_ciudad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_ciudad_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_genero_paciente
   function NM_export_bp_genero_paciente()
   {
         $this->bp_genero_paciente = html_entity_decode($this->bp_genero_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_genero_paciente = strip_tags($this->bp_genero_paciente);
         if (!NM_is_utf8($this->bp_genero_paciente))
         {
             $this->bp_genero_paciente = sc_convert_encoding($this->bp_genero_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_genero_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_fecha_nacimineto_paciente
   function NM_export_bp_fecha_nacimineto_paciente()
   {
         $this->bp_fecha_nacimineto_paciente = html_entity_decode($this->bp_fecha_nacimineto_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_fecha_nacimineto_paciente = strip_tags($this->bp_fecha_nacimineto_paciente);
         if (!NM_is_utf8($this->bp_fecha_nacimineto_paciente))
         {
             $this->bp_fecha_nacimineto_paciente = sc_convert_encoding($this->bp_fecha_nacimineto_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_fecha_nacimineto_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_edad_paciente
   function NM_export_bp_edad_paciente()
   {
         if (!NM_is_utf8($this->bp_edad_paciente))
         {
             $this->bp_edad_paciente = sc_convert_encoding($this->bp_edad_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
         if (is_numeric($this->bp_edad_paciente))
         {
             $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getNumberFormat()->setFormatCode('#,##0');
         }
         $this->Nm_ActiveSheet->setCellValue($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_edad_paciente);
         $this->Xls_col++;
   }
   //----- bp_acudiente_paciente
   function NM_export_bp_acudiente_paciente()
   {
         $this->bp_acudiente_paciente = html_entity_decode($this->bp_acudiente_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_acudiente_paciente = strip_tags($this->bp_acudiente_paciente);
         if (!NM_is_utf8($this->bp_acudiente_paciente))
         {
             $this->bp_acudiente_paciente = sc_convert_encoding($this->bp_acudiente_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_acudiente_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_telefono_acudiente_paciente
   function NM_export_bp_telefono_acudiente_paciente()
   {
         $this->bp_telefono_acudiente_paciente = html_entity_decode($this->bp_telefono_acudiente_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_telefono_acudiente_paciente = strip_tags($this->bp_telefono_acudiente_paciente);
         if (!NM_is_utf8($this->bp_telefono_acudiente_paciente))
         {
             $this->bp_telefono_acudiente_paciente = sc_convert_encoding($this->bp_telefono_acudiente_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_telefono_acudiente_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_codigo_xofigo
   function NM_export_bp_codigo_xofigo()
   {
         $this->bp_codigo_xofigo = html_entity_decode($this->bp_codigo_xofigo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_codigo_xofigo = strip_tags($this->bp_codigo_xofigo);
         if (!NM_is_utf8($this->bp_codigo_xofigo))
         {
             $this->bp_codigo_xofigo = sc_convert_encoding($this->bp_codigo_xofigo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_codigo_xofigo, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_status_paciente
   function NM_export_bp_status_paciente()
   {
         $this->bp_status_paciente = html_entity_decode($this->bp_status_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_status_paciente = strip_tags($this->bp_status_paciente);
         if (!NM_is_utf8($this->bp_status_paciente))
         {
             $this->bp_status_paciente = sc_convert_encoding($this->bp_status_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_status_paciente, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_id_ultima_gestion
   function NM_export_bp_id_ultima_gestion()
   {
         $this->bp_id_ultima_gestion = html_entity_decode($this->bp_id_ultima_gestion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_id_ultima_gestion = strip_tags($this->bp_id_ultima_gestion);
         if (!NM_is_utf8($this->bp_id_ultima_gestion))
         {
             $this->bp_id_ultima_gestion = sc_convert_encoding($this->bp_id_ultima_gestion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_id_ultima_gestion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bp_usuario_creacion
   function NM_export_bp_usuario_creacion()
   {
         $this->bp_usuario_creacion = html_entity_decode($this->bp_usuario_creacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bp_usuario_creacion = strip_tags($this->bp_usuario_creacion);
         if (!NM_is_utf8($this->bp_usuario_creacion))
         {
             $this->bp_usuario_creacion = sc_convert_encoding($this->bp_usuario_creacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bp_usuario_creacion, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_id_tratamiento
   function NM_export_bt_id_tratamiento()
   {
         $this->bt_id_tratamiento = html_entity_decode($this->bt_id_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_id_tratamiento = strip_tags($this->bt_id_tratamiento);
         if (!NM_is_utf8($this->bt_id_tratamiento))
         {
             $this->bt_id_tratamiento = sc_convert_encoding($this->bt_id_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_id_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_producto_tratamiento
   function NM_export_bt_producto_tratamiento()
   {
         $this->bt_producto_tratamiento = html_entity_decode($this->bt_producto_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_producto_tratamiento = strip_tags($this->bt_producto_tratamiento);
         if (!NM_is_utf8($this->bt_producto_tratamiento))
         {
             $this->bt_producto_tratamiento = sc_convert_encoding($this->bt_producto_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_producto_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_nombre_referencia
   function NM_export_bt_nombre_referencia()
   {
         $this->bt_nombre_referencia = html_entity_decode($this->bt_nombre_referencia, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_nombre_referencia = strip_tags($this->bt_nombre_referencia);
         if (!NM_is_utf8($this->bt_nombre_referencia))
         {
             $this->bt_nombre_referencia = sc_convert_encoding($this->bt_nombre_referencia, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_nombre_referencia, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_clasificacion_patologica_tratamiento
   function NM_export_bt_clasificacion_patologica_tratamiento()
   {
         $this->bt_clasificacion_patologica_tratamiento = html_entity_decode($this->bt_clasificacion_patologica_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_clasificacion_patologica_tratamiento = strip_tags($this->bt_clasificacion_patologica_tratamiento);
         if (!NM_is_utf8($this->bt_clasificacion_patologica_tratamiento))
         {
             $this->bt_clasificacion_patologica_tratamiento = sc_convert_encoding($this->bt_clasificacion_patologica_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_clasificacion_patologica_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_tratamiento_previo
   function NM_export_bt_tratamiento_previo()
   {
         $this->bt_tratamiento_previo = html_entity_decode($this->bt_tratamiento_previo, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_tratamiento_previo = strip_tags($this->bt_tratamiento_previo);
         if (!NM_is_utf8($this->bt_tratamiento_previo))
         {
             $this->bt_tratamiento_previo = sc_convert_encoding($this->bt_tratamiento_previo, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_tratamiento_previo, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_consentimiento_tratamiento
   function NM_export_bt_consentimiento_tratamiento()
   {
         $this->bt_consentimiento_tratamiento = html_entity_decode($this->bt_consentimiento_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_consentimiento_tratamiento = strip_tags($this->bt_consentimiento_tratamiento);
         if (!NM_is_utf8($this->bt_consentimiento_tratamiento))
         {
             $this->bt_consentimiento_tratamiento = sc_convert_encoding($this->bt_consentimiento_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_consentimiento_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_fecha_inicio_terapia_tratamiento
   function NM_export_bt_fecha_inicio_terapia_tratamiento()
   {
         $this->bt_fecha_inicio_terapia_tratamiento = html_entity_decode($this->bt_fecha_inicio_terapia_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_fecha_inicio_terapia_tratamiento = strip_tags($this->bt_fecha_inicio_terapia_tratamiento);
         if (!NM_is_utf8($this->bt_fecha_inicio_terapia_tratamiento))
         {
             $this->bt_fecha_inicio_terapia_tratamiento = sc_convert_encoding($this->bt_fecha_inicio_terapia_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_fecha_inicio_terapia_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_regimen_tratamiento
   function NM_export_bt_regimen_tratamiento()
   {
         $this->bt_regimen_tratamiento = html_entity_decode($this->bt_regimen_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_regimen_tratamiento = strip_tags($this->bt_regimen_tratamiento);
         if (!NM_is_utf8($this->bt_regimen_tratamiento))
         {
             $this->bt_regimen_tratamiento = sc_convert_encoding($this->bt_regimen_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_regimen_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_asegurador_tratamiento
   function NM_export_bt_asegurador_tratamiento()
   {
         $this->bt_asegurador_tratamiento = html_entity_decode($this->bt_asegurador_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_asegurador_tratamiento = strip_tags($this->bt_asegurador_tratamiento);
         if (!NM_is_utf8($this->bt_asegurador_tratamiento))
         {
             $this->bt_asegurador_tratamiento = sc_convert_encoding($this->bt_asegurador_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_asegurador_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_operador_logistico_tratamiento
   function NM_export_bt_operador_logistico_tratamiento()
   {
         $this->bt_operador_logistico_tratamiento = html_entity_decode($this->bt_operador_logistico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_operador_logistico_tratamiento = strip_tags($this->bt_operador_logistico_tratamiento);
         if (!NM_is_utf8($this->bt_operador_logistico_tratamiento))
         {
             $this->bt_operador_logistico_tratamiento = sc_convert_encoding($this->bt_operador_logistico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_operador_logistico_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_punto_entrega
   function NM_export_bt_punto_entrega()
   {
         $this->bt_punto_entrega = html_entity_decode($this->bt_punto_entrega, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_punto_entrega = strip_tags($this->bt_punto_entrega);
         if (!NM_is_utf8($this->bt_punto_entrega))
         {
             $this->bt_punto_entrega = sc_convert_encoding($this->bt_punto_entrega, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_punto_entrega, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_fecha_ultima_reclamacion_tratamiento
   function NM_export_bt_fecha_ultima_reclamacion_tratamiento()
   {
         $this->bt_fecha_ultima_reclamacion_tratamiento = html_entity_decode($this->bt_fecha_ultima_reclamacion_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_fecha_ultima_reclamacion_tratamiento = strip_tags($this->bt_fecha_ultima_reclamacion_tratamiento);
         if (!NM_is_utf8($this->bt_fecha_ultima_reclamacion_tratamiento))
         {
             $this->bt_fecha_ultima_reclamacion_tratamiento = sc_convert_encoding($this->bt_fecha_ultima_reclamacion_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_fecha_ultima_reclamacion_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_otros_operadores_tratamiento
   function NM_export_bt_otros_operadores_tratamiento()
   {
         $this->bt_otros_operadores_tratamiento = html_entity_decode($this->bt_otros_operadores_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_otros_operadores_tratamiento = strip_tags($this->bt_otros_operadores_tratamiento);
         if (!NM_is_utf8($this->bt_otros_operadores_tratamiento))
         {
             $this->bt_otros_operadores_tratamiento = sc_convert_encoding($this->bt_otros_operadores_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_otros_operadores_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_medios_adquisicion_tratamiento
   function NM_export_bt_medios_adquisicion_tratamiento()
   {
         $this->bt_medios_adquisicion_tratamiento = html_entity_decode($this->bt_medios_adquisicion_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_medios_adquisicion_tratamiento = strip_tags($this->bt_medios_adquisicion_tratamiento);
         if (!NM_is_utf8($this->bt_medios_adquisicion_tratamiento))
         {
             $this->bt_medios_adquisicion_tratamiento = sc_convert_encoding($this->bt_medios_adquisicion_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_medios_adquisicion_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_ips_atiende_tratamiento
   function NM_export_bt_ips_atiende_tratamiento()
   {
         $this->bt_ips_atiende_tratamiento = html_entity_decode($this->bt_ips_atiende_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_ips_atiende_tratamiento = strip_tags($this->bt_ips_atiende_tratamiento);
         if (!NM_is_utf8($this->bt_ips_atiende_tratamiento))
         {
             $this->bt_ips_atiende_tratamiento = sc_convert_encoding($this->bt_ips_atiende_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_ips_atiende_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_medico_tratamiento
   function NM_export_bt_medico_tratamiento()
   {
         $this->bt_medico_tratamiento = html_entity_decode($this->bt_medico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_medico_tratamiento = strip_tags($this->bt_medico_tratamiento);
         if (!NM_is_utf8($this->bt_medico_tratamiento))
         {
             $this->bt_medico_tratamiento = sc_convert_encoding($this->bt_medico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_medico_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_especialidad_tratamiento
   function NM_export_bt_especialidad_tratamiento()
   {
         $this->bt_especialidad_tratamiento = html_entity_decode($this->bt_especialidad_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_especialidad_tratamiento = strip_tags($this->bt_especialidad_tratamiento);
         if (!NM_is_utf8($this->bt_especialidad_tratamiento))
         {
             $this->bt_especialidad_tratamiento = sc_convert_encoding($this->bt_especialidad_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_especialidad_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_paramedico_tratamiento
   function NM_export_bt_paramedico_tratamiento()
   {
         $this->bt_paramedico_tratamiento = html_entity_decode($this->bt_paramedico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_paramedico_tratamiento = strip_tags($this->bt_paramedico_tratamiento);
         if (!NM_is_utf8($this->bt_paramedico_tratamiento))
         {
             $this->bt_paramedico_tratamiento = sc_convert_encoding($this->bt_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_paramedico_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_zona_atencion_paramedico_tratamiento
   function NM_export_bt_zona_atencion_paramedico_tratamiento()
   {
         $this->bt_zona_atencion_paramedico_tratamiento = html_entity_decode($this->bt_zona_atencion_paramedico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_zona_atencion_paramedico_tratamiento = strip_tags($this->bt_zona_atencion_paramedico_tratamiento);
         if (!NM_is_utf8($this->bt_zona_atencion_paramedico_tratamiento))
         {
             $this->bt_zona_atencion_paramedico_tratamiento = sc_convert_encoding($this->bt_zona_atencion_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_zona_atencion_paramedico_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_ciudad_base_paramedico_tratamiento
   function NM_export_bt_ciudad_base_paramedico_tratamiento()
   {
         $this->bt_ciudad_base_paramedico_tratamiento = html_entity_decode($this->bt_ciudad_base_paramedico_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_ciudad_base_paramedico_tratamiento = strip_tags($this->bt_ciudad_base_paramedico_tratamiento);
         if (!NM_is_utf8($this->bt_ciudad_base_paramedico_tratamiento))
         {
             $this->bt_ciudad_base_paramedico_tratamiento = sc_convert_encoding($this->bt_ciudad_base_paramedico_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_ciudad_base_paramedico_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_notas_adjuntos_tratamiento
   function NM_export_bt_notas_adjuntos_tratamiento()
   {
         $this->bt_notas_adjuntos_tratamiento = html_entity_decode($this->bt_notas_adjuntos_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_notas_adjuntos_tratamiento = strip_tags($this->bt_notas_adjuntos_tratamiento);
         if (!NM_is_utf8($this->bt_notas_adjuntos_tratamiento))
         {
             $this->bt_notas_adjuntos_tratamiento = sc_convert_encoding($this->bt_notas_adjuntos_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_notas_adjuntos_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }
   //----- bt_dosis_tratamiento
   function NM_export_bt_dosis_tratamiento()
   {
         $this->bt_dosis_tratamiento = html_entity_decode($this->bt_dosis_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->bt_dosis_tratamiento = strip_tags($this->bt_dosis_tratamiento);
         if (!NM_is_utf8($this->bt_dosis_tratamiento))
         {
             $this->bt_dosis_tratamiento = sc_convert_encoding($this->bt_dosis_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->Nm_ActiveSheet->getStyle($this->calc_cell($this->Xls_col) . $this->Xls_row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
         $this->Nm_ActiveSheet->setCellValueExplicit($this->calc_cell($this->Xls_col) . $this->Xls_row, $this->bt_dosis_tratamiento, PHPExcel_Cell_DataType::TYPE_STRING);
         $this->Xls_col++;
   }

   function calc_cell($col)
   {
       $arr_alfa = array("","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
       $val_ret = "";
       $result = $col + 1;
       while ($result > 26)
       {
           $cel      = $result % 26;
           $result   = $result / 26;
           if ($cel == 0)
           {
               $cel    = 26;
               $result--;
           }
           $val_ret = $arr_alfa[$cel] . $val_ret;
       }
       $val_ret = $arr_alfa[$result] . $val_ret;
       return $val_ret;
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['xls_file']);
      if (is_file($this->Xls_f))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['xls_file'] = $this->Xls_f;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>Listado Pacientes :: Excel</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">XLS</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="Listado_pacientes_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="Listado_pacientes"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
