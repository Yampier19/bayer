<?php
//--- 
class Listado_pacientes_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $nmgp_botoes; 
   var $nm_location;
   var $bp_id_paciente;
   var $bp_estado_paciente;
   var $bp_fecha_activacion_paciente;
   var $bp_fecha_retiro_paciente;
   var $bp_motivo_retiro_paciente;
   var $bp_observacion_motivo_retiro_paciente;
   var $bp_identificacion_paciente;
   var $bp_nombre_paciente;
   var $bp_apellido_paciente;
   var $bp_telefono_paciente;
   var $bp_telefono2_paciente;
   var $bp_telefono3_paciente;
   var $bp_correo_paciente;
   var $bp_direccion_paciente;
   var $bp_barrio_paciente;
   var $bp_departamento_paciente;
   var $bp_ciudad_paciente;
   var $bp_genero_paciente;
   var $bp_fecha_nacimineto_paciente;
   var $bp_edad_paciente;
   var $bp_acudiente_paciente;
   var $bp_telefono_acudiente_paciente;
   var $bp_codigo_xofigo;
   var $bp_status_paciente;
   var $bp_id_ultima_gestion;
   var $bp_usuario_creacion;
   var $bt_id_tratamiento;
   var $bt_producto_tratamiento;
   var $bt_nombre_referencia;
   var $bt_clasificacion_patologica_tratamiento;
   var $bt_tratamiento_previo;
   var $bt_consentimiento_tratamiento;
   var $bt_fecha_inicio_terapia_tratamiento;
   var $bt_regimen_tratamiento;
   var $bt_asegurador_tratamiento;
   var $bt_operador_logistico_tratamiento;
   var $bt_punto_entrega;
   var $bt_fecha_ultima_reclamacion_tratamiento;
   var $bt_otros_operadores_tratamiento;
   var $bt_medios_adquisicion_tratamiento;
   var $bt_ips_atiende_tratamiento;
   var $bt_medico_tratamiento;
   var $bt_especialidad_tratamiento;
   var $bt_paramedico_tratamiento;
   var $bt_zona_atencion_paramedico_tratamiento;
   var $bt_ciudad_base_paramedico_tratamiento;
   var $bt_notas_adjuntos_tratamiento;
   var $bt_dosis_tratamiento;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    $this->nmgp_botoes['det_pdf'] = "on";
    $this->nmgp_botoes['det_print'] = "on";
    $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
    if (isset($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['btn_display']))
    {
        foreach ($_SESSION['scriptcase']['sc_apl_conf']['Listado_pacientes']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
        {
            $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
        }
    }
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['campos_busca']))
    { 
        $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['campos_busca'];
        if ($_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        $this->bp_id_paciente = $Busca_temp['bp_id_paciente']; 
        $tmp_pos = strpos($this->bp_id_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->bp_id_paciente = substr($this->bp_id_paciente, 0, $tmp_pos);
        }
        $this->bp_id_paciente_2 = $Busca_temp['bp_id_paciente_input_2']; 
        $this->bp_estado_paciente = $Busca_temp['bp_estado_paciente']; 
        $tmp_pos = strpos($this->bp_estado_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->bp_estado_paciente = substr($this->bp_estado_paciente, 0, $tmp_pos);
        }
        $this->bp_status_paciente = $Busca_temp['bp_status_paciente']; 
        $tmp_pos = strpos($this->bp_status_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->bp_status_paciente = substr($this->bp_status_paciente, 0, $tmp_pos);
        }
        $this->bp_fecha_activacion_paciente = $Busca_temp['bp_fecha_activacion_paciente']; 
        $tmp_pos = strpos($this->bp_fecha_activacion_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->bp_fecha_activacion_paciente = substr($this->bp_fecha_activacion_paciente, 0, $tmp_pos);
        }
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("es_es");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
    $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['seq_dir'] = 0; 
    $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['sub_dir'] = array(); 
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   else 
   { 
       $nmgp_select = "SELECT bp.ID_PACIENTE as bp_id_paciente, bp.ESTADO_PACIENTE as bp_estado_paciente, bp.STATUS_PACIENTE as bp_status_paciente, bp.FECHA_ACTIVACION_PACIENTE as bp_fecha_activacion_paciente, bp.FECHA_RETIRO_PACIENTE as bp_fecha_retiro_paciente, bp.MOTIVO_RETIRO_PACIENTE as bp_motivo_retiro_paciente, bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE as cmp_maior_30_1, bp.IDENTIFICACION_PACIENTE as bp_identificacion_paciente, bp.NOMBRE_PACIENTE as bp_nombre_paciente, bp.APELLIDO_PACIENTE as bp_apellido_paciente, bp.TELEFONO_PACIENTE as bp_telefono_paciente, bp.TELEFONO2_PACIENTE as bp_telefono2_paciente, bp.TELEFONO3_PACIENTE as bp_telefono3_paciente, bp.CORREO_PACIENTE as bp_correo_paciente, bp.DIRECCION_PACIENTE as bp_direccion_paciente, bp.BARRIO_PACIENTE as bp_barrio_paciente, bp.DEPARTAMENTO_PACIENTE as bp_departamento_paciente, bp.CIUDAD_PACIENTE as bp_ciudad_paciente, bp.GENERO_PACIENTE as bp_genero_paciente, bp.FECHA_NACIMINETO_PACIENTE as bp_fecha_nacimineto_paciente, bp.EDAD_PACIENTE as bp_edad_paciente, bp.ACUDIENTE_PACIENTE as bp_acudiente_paciente, bp.TELEFONO_ACUDIENTE_PACIENTE as bp_telefono_acudiente_paciente, bp.CODIGO_XOFIGO as bp_codigo_xofigo, bp.ID_ULTIMA_GESTION as bp_id_ultima_gestion, bp.USUARIO_CREACION as bp_usuario_creacion, bt.ID_TRATAMIENTO as bt_id_tratamiento, bt.PRODUCTO_TRATAMIENTO as bt_producto_tratamiento, bt.NOMBRE_REFERENCIA as bt_nombre_referencia, bt.DOSIS_TRATAMIENTO as bt_dosis_tratamiento, bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_2, bt.TRATAMIENTO_PREVIO as bt_tratamiento_previo, bt.CONSENTIMIENTO_TRATAMIENTO as bt_consentimiento_tratamiento, bt.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_3, bt.REGIMEN_TRATAMIENTO as bt_regimen_tratamiento, bt.ASEGURADOR_TRATAMIENTO as bt_asegurador_tratamiento, bt.OPERADOR_LOGISTICO_TRATAMIENTO as cmp_maior_30_4, bt.PUNTO_ENTREGA as bt_punto_entrega, bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO as cmp_maior_30_5, bt.OTROS_OPERADORES_TRATAMIENTO as cmp_maior_30_6, bt.MEDIOS_ADQUISICION_TRATAMIENTO as cmp_maior_30_7, bt.IPS_ATIENDE_TRATAMIENTO as bt_ips_atiende_tratamiento, bt.MEDICO_TRATAMIENTO as bt_medico_tratamiento, bt.ESPECIALIDAD_TRATAMIENTO as bt_especialidad_tratamiento, bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO as cmp_maior_30_8, bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO as cmp_maior_30_9, bt.NOTAS_ADJUNTOS_TRATAMIENTO as bt_notas_adjuntos_tratamiento from " . $this->Ini->nm_tabela; 
   } 
   $parms_det = explode("*PDet*", $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  bp.ID_PACIENTE = $parms_det[0] and bp.ESTADO_PACIENTE = '$parms_det[1]' and bp.FECHA_ACTIVACION_PACIENTE = '$parms_det[2]' and bp.FECHA_RETIRO_PACIENTE = '$parms_det[3]' and bp.MOTIVO_RETIRO_PACIENTE = '$parms_det[4]' and bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE = '$parms_det[5]' and bp.IDENTIFICACION_PACIENTE = '$parms_det[6]' and bp.NOMBRE_PACIENTE = '$parms_det[7]' and bp.APELLIDO_PACIENTE = '$parms_det[8]' and bp.TELEFONO_PACIENTE = '$parms_det[9]' and bp.TELEFONO2_PACIENTE = '$parms_det[10]' and bp.TELEFONO3_PACIENTE = '$parms_det[11]' and bp.CORREO_PACIENTE = '$parms_det[12]' and bp.DIRECCION_PACIENTE = '$parms_det[13]' and bp.BARRIO_PACIENTE = '$parms_det[14]' and bp.DEPARTAMENTO_PACIENTE = '$parms_det[15]' and bp.CIUDAD_PACIENTE = '$parms_det[16]' and bp.GENERO_PACIENTE = '$parms_det[17]' and bp.FECHA_NACIMINETO_PACIENTE = '$parms_det[18]' and bp.EDAD_PACIENTE = $parms_det[19] and bp.ACUDIENTE_PACIENTE = '$parms_det[20]' and bp.TELEFONO_ACUDIENTE_PACIENTE = '$parms_det[21]' and bp.CODIGO_XOFIGO = $parms_det[22] and bp.STATUS_PACIENTE = '$parms_det[23]' and bp.ID_ULTIMA_GESTION = $parms_det[24] and bp.USUARIO_CREACION = '$parms_det[25]' and bt.ID_TRATAMIENTO = $parms_det[26] and bt.PRODUCTO_TRATAMIENTO = '$parms_det[27]' and bt.NOMBRE_REFERENCIA = '$parms_det[28]' and bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[29]' and bt.TRATAMIENTO_PREVIO = '$parms_det[30]' and bt.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[31]' and bt.FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[32]' and bt.REGIMEN_TRATAMIENTO = '$parms_det[33]' and bt.ASEGURADOR_TRATAMIENTO = '$parms_det[34]' and bt.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[35]' and bt.PUNTO_ENTREGA = '$parms_det[36]' and bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[37]' and bt.OTROS_OPERADORES_TRATAMIENTO = '$parms_det[38]' and bt.MEDIOS_ADQUISICION_TRATAMIENTO = '$parms_det[39]' and bt.IPS_ATIENDE_TRATAMIENTO = '$parms_det[40]' and bt.MEDICO_TRATAMIENTO = '$parms_det[41]' and bt.ESPECIALIDAD_TRATAMIENTO = '$parms_det[42]' and bt.PARAMEDICO_TRATAMIENTO = '$parms_det[43]' and bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO = '$parms_det[44]' and bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO = '$parms_det[45]' and bt.NOTAS_ADJUNTOS_TRATAMIENTO = '$parms_det[46]' and bt.DOSIS_TRATAMIENTO = '$parms_det[47]'" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  bp.ID_PACIENTE = $parms_det[0] and bp.ESTADO_PACIENTE = '$parms_det[1]' and bp.FECHA_ACTIVACION_PACIENTE = '$parms_det[2]' and bp.FECHA_RETIRO_PACIENTE = '$parms_det[3]' and bp.MOTIVO_RETIRO_PACIENTE = '$parms_det[4]' and bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE = '$parms_det[5]' and bp.IDENTIFICACION_PACIENTE = '$parms_det[6]' and bp.NOMBRE_PACIENTE = '$parms_det[7]' and bp.APELLIDO_PACIENTE = '$parms_det[8]' and bp.TELEFONO_PACIENTE = '$parms_det[9]' and bp.TELEFONO2_PACIENTE = '$parms_det[10]' and bp.TELEFONO3_PACIENTE = '$parms_det[11]' and bp.CORREO_PACIENTE = '$parms_det[12]' and bp.DIRECCION_PACIENTE = '$parms_det[13]' and bp.BARRIO_PACIENTE = '$parms_det[14]' and bp.DEPARTAMENTO_PACIENTE = '$parms_det[15]' and bp.CIUDAD_PACIENTE = '$parms_det[16]' and bp.GENERO_PACIENTE = '$parms_det[17]' and bp.FECHA_NACIMINETO_PACIENTE = '$parms_det[18]' and bp.EDAD_PACIENTE = $parms_det[19] and bp.ACUDIENTE_PACIENTE = '$parms_det[20]' and bp.TELEFONO_ACUDIENTE_PACIENTE = '$parms_det[21]' and bp.CODIGO_XOFIGO = $parms_det[22] and bp.STATUS_PACIENTE = '$parms_det[23]' and bp.ID_ULTIMA_GESTION = $parms_det[24] and bp.USUARIO_CREACION = '$parms_det[25]' and bt.ID_TRATAMIENTO = $parms_det[26] and bt.PRODUCTO_TRATAMIENTO = '$parms_det[27]' and bt.NOMBRE_REFERENCIA = '$parms_det[28]' and bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[29]' and bt.TRATAMIENTO_PREVIO = '$parms_det[30]' and bt.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[31]' and bt.FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[32]' and bt.REGIMEN_TRATAMIENTO = '$parms_det[33]' and bt.ASEGURADOR_TRATAMIENTO = '$parms_det[34]' and bt.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[35]' and bt.PUNTO_ENTREGA = '$parms_det[36]' and bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[37]' and bt.OTROS_OPERADORES_TRATAMIENTO = '$parms_det[38]' and bt.MEDIOS_ADQUISICION_TRATAMIENTO = '$parms_det[39]' and bt.IPS_ATIENDE_TRATAMIENTO = '$parms_det[40]' and bt.MEDICO_TRATAMIENTO = '$parms_det[41]' and bt.ESPECIALIDAD_TRATAMIENTO = '$parms_det[42]' and bt.PARAMEDICO_TRATAMIENTO = '$parms_det[43]' and bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO = '$parms_det[44]' and bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO = '$parms_det[45]' and bt.NOTAS_ADJUNTOS_TRATAMIENTO = '$parms_det[46]' and bt.DOSIS_TRATAMIENTO = '$parms_det[47]'" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  bp.ID_PACIENTE = $parms_det[0] and bp.ESTADO_PACIENTE = '$parms_det[1]' and bp.FECHA_ACTIVACION_PACIENTE = '$parms_det[2]' and bp.FECHA_RETIRO_PACIENTE = '$parms_det[3]' and bp.MOTIVO_RETIRO_PACIENTE = '$parms_det[4]' and bp.OBSERVACION_MOTIVO_RETIRO_PACIENTE = '$parms_det[5]' and bp.IDENTIFICACION_PACIENTE = '$parms_det[6]' and bp.NOMBRE_PACIENTE = '$parms_det[7]' and bp.APELLIDO_PACIENTE = '$parms_det[8]' and bp.TELEFONO_PACIENTE = '$parms_det[9]' and bp.TELEFONO2_PACIENTE = '$parms_det[10]' and bp.TELEFONO3_PACIENTE = '$parms_det[11]' and bp.CORREO_PACIENTE = '$parms_det[12]' and bp.DIRECCION_PACIENTE = '$parms_det[13]' and bp.BARRIO_PACIENTE = '$parms_det[14]' and bp.DEPARTAMENTO_PACIENTE = '$parms_det[15]' and bp.CIUDAD_PACIENTE = '$parms_det[16]' and bp.GENERO_PACIENTE = '$parms_det[17]' and bp.FECHA_NACIMINETO_PACIENTE = '$parms_det[18]' and bp.EDAD_PACIENTE = $parms_det[19] and bp.ACUDIENTE_PACIENTE = '$parms_det[20]' and bp.TELEFONO_ACUDIENTE_PACIENTE = '$parms_det[21]' and bp.CODIGO_XOFIGO = $parms_det[22] and bp.STATUS_PACIENTE = '$parms_det[23]' and bp.ID_ULTIMA_GESTION = $parms_det[24] and bp.USUARIO_CREACION = '$parms_det[25]' and bt.ID_TRATAMIENTO = $parms_det[26] and bt.PRODUCTO_TRATAMIENTO = '$parms_det[27]' and bt.NOMBRE_REFERENCIA = '$parms_det[28]' and bt.CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[29]' and bt.TRATAMIENTO_PREVIO = '$parms_det[30]' and bt.CONSENTIMIENTO_TRATAMIENTO = '$parms_det[31]' and bt.FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[32]' and bt.REGIMEN_TRATAMIENTO = '$parms_det[33]' and bt.ASEGURADOR_TRATAMIENTO = '$parms_det[34]' and bt.OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[35]' and bt.PUNTO_ENTREGA = '$parms_det[36]' and bt.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[37]' and bt.OTROS_OPERADORES_TRATAMIENTO = '$parms_det[38]' and bt.MEDIOS_ADQUISICION_TRATAMIENTO = '$parms_det[39]' and bt.IPS_ATIENDE_TRATAMIENTO = '$parms_det[40]' and bt.MEDICO_TRATAMIENTO = '$parms_det[41]' and bt.ESPECIALIDAD_TRATAMIENTO = '$parms_det[42]' and bt.PARAMEDICO_TRATAMIENTO = '$parms_det[43]' and bt.ZONA_ATENCION_PARAMEDICO_TRATAMIENTO = '$parms_det[44]' and bt.CIUDAD_BASE_PARAMEDICO_TRATAMIENTO = '$parms_det[45]' and bt.NOTAS_ADJUNTOS_TRATAMIENTO = '$parms_det[46]' and bt.DOSIS_TRATAMIENTO = '$parms_det[47]'" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->bp_id_paciente = $rs->fields[0] ;  
   $this->bp_id_paciente = (string)$this->bp_id_paciente;
   $this->bp_estado_paciente = $rs->fields[1] ;  
   $this->bp_status_paciente = $rs->fields[2] ;  
   $this->bp_fecha_activacion_paciente = $rs->fields[3] ;  
   $this->bp_fecha_retiro_paciente = $rs->fields[4] ;  
   $this->bp_motivo_retiro_paciente = $rs->fields[5] ;  
   $this->bp_observacion_motivo_retiro_paciente = $rs->fields[6] ;  
   $this->bp_identificacion_paciente = $rs->fields[7] ;  
   $this->bp_nombre_paciente = $rs->fields[8] ;  
   $this->bp_apellido_paciente = $rs->fields[9] ;  
   $this->bp_telefono_paciente = $rs->fields[10] ;  
   $this->bp_telefono2_paciente = $rs->fields[11] ;  
   $this->bp_telefono3_paciente = $rs->fields[12] ;  
   $this->bp_correo_paciente = $rs->fields[13] ;  
   $this->bp_direccion_paciente = $rs->fields[14] ;  
   $this->bp_barrio_paciente = $rs->fields[15] ;  
   $this->bp_departamento_paciente = $rs->fields[16] ;  
   $this->bp_ciudad_paciente = $rs->fields[17] ;  
   $this->bp_genero_paciente = $rs->fields[18] ;  
   $this->bp_fecha_nacimineto_paciente = $rs->fields[19] ;  
   $this->bp_edad_paciente = $rs->fields[20] ;  
   $this->bp_edad_paciente = (string)$this->bp_edad_paciente;
   $this->bp_acudiente_paciente = $rs->fields[21] ;  
   $this->bp_telefono_acudiente_paciente = $rs->fields[22] ;  
   $this->bp_codigo_xofigo = $rs->fields[23] ;  
   $this->bp_codigo_xofigo = (string)$this->bp_codigo_xofigo;
   $this->bp_id_ultima_gestion = $rs->fields[24] ;  
   $this->bp_id_ultima_gestion = (string)$this->bp_id_ultima_gestion;
   $this->bp_usuario_creacion = $rs->fields[25] ;  
   $this->bt_id_tratamiento = $rs->fields[26] ;  
   $this->bt_id_tratamiento = (string)$this->bt_id_tratamiento;
   $this->bt_producto_tratamiento = $rs->fields[27] ;  
   $this->bt_nombre_referencia = $rs->fields[28] ;  
   $this->bt_dosis_tratamiento = $rs->fields[29] ;  
   $this->bt_clasificacion_patologica_tratamiento = $rs->fields[30] ;  
   $this->bt_tratamiento_previo = $rs->fields[31] ;  
   $this->bt_consentimiento_tratamiento = $rs->fields[32] ;  
   $this->bt_fecha_inicio_terapia_tratamiento = $rs->fields[33] ;  
   $this->bt_regimen_tratamiento = $rs->fields[34] ;  
   $this->bt_asegurador_tratamiento = $rs->fields[35] ;  
   $this->bt_operador_logistico_tratamiento = $rs->fields[36] ;  
   $this->bt_punto_entrega = $rs->fields[37] ;  
   $this->bt_fecha_ultima_reclamacion_tratamiento = $rs->fields[38] ;  
   $this->bt_otros_operadores_tratamiento = $rs->fields[39] ;  
   $this->bt_medios_adquisicion_tratamiento = $rs->fields[40] ;  
   $this->bt_ips_atiende_tratamiento = $rs->fields[41] ;  
   $this->bt_medico_tratamiento = $rs->fields[42] ;  
   $this->bt_especialidad_tratamiento = $rs->fields[43] ;  
   $this->bt_zona_atencion_paramedico_tratamiento = $rs->fields[44] ;  
   $this->bt_ciudad_base_paramedico_tratamiento = $rs->fields[45] ;  
   $this->bt_notas_adjuntos_tratamiento = $rs->fields[46] ;  
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['cmp_acum']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['cmp_acum']))
   {
       $parms_acum = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['cmp_acum']);
       foreach ($parms_acum as $cada_par)
       {
          $cada_val = explode("=", $cada_par);
          $this->$cada_val[0] = $cada_val[1];
       }
   }
//--- 
   $nm_saida->saida("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
   $nm_saida->saida("            \"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">\r\n");
   $nm_saida->saida("<html" . $_SESSION['scriptcase']['reg_conf']['html_dir'] . ">\r\n");
   $nm_saida->saida("<HEAD>\r\n");
   $nm_saida->saida("   <TITLE>Listado Pacientes</TITLE>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "\" />\r\n");
   $nm_saida->saida(" <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Last-Modified\" content=\"" . gmdate("D, d M Y H:i:s") . " GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"no-store, no-cache, must-revalidate\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"post-check=0, pre-check=0\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n");
   if ($_SESSION['scriptcase']['proc_mobile'])
   {
       $nm_saida->saida(" <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" />\r\n");
   }

   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery/js/jquery.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/malsup-blockui/jquery.blockUI.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\">var sc_pathToTB = '" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/';</script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox-compressed.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"../_lib/lib/js/jquery.scInput.js\"></script>\r\n");
   $nm_saida->saida(" <link rel=\"stylesheet\" href=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />\r\n");
   if (($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['det_print'] == "print" && strtoupper($nmgp_cor_print) == "PB") || $nmgp_tipo_pdf == "pb")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
   else
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "Listado_pacientes/Listado_pacientes_det_" . strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) . ".css\" />\r\n");
   if (!$_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['pdf_det'] && $_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['det_print'] != "print")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/buttons/" . $this->Ini->Str_btn_css . "\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema_dir'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
   }
   $nm_saida->saida("</HEAD>\r\n");
   $nm_saida->saida("  <body class=\"scGridPage\">\r\n");
   $nm_saida->saida("  " . $this->Ini->Ajax_result_set . "\r\n");
   $nm_saida->saida("<table border=0 align=\"center\" valign=\"top\" ><tr><td style=\"padding: 0px\"><div class=\"scGridBorder\"><table width='100%' cellspacing=0 cellpadding=0><tr><td>\r\n");
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<style>\r\n");
   $nm_saida->saida("#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 \r\n");
   $nm_saida->saida("#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}\r\n");
   $nm_saida->saida("</style>\r\n");
   $nm_saida->saida("<div style=\"width: 100%\">\r\n");
   $nm_saida->saida(" <div class=\"scGridHeader\" style=\"height:11px; display: block; border-width:0px; \"></div>\r\n");
   $nm_saida->saida(" <div style=\"height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block\">\r\n");
   $nm_saida->saida(" 	<table style=\"width:100%; border-collapse:collapse; padding:0;\">\r\n");
   $nm_saida->saida("    	<tr>\r\n");
   $nm_saida->saida("        	<td id=\"lin1_col1\" class=\"scGridHeaderFont\"><span>Listado Pacientes</span></td>\r\n");
   $nm_saida->saida("            <td id=\"lin1_col2\" class=\"scGridHeaderFont\"><span></span></td>\r\n");
   $nm_saida->saida("        </tr>\r\n");
   $nm_saida->saida("    </table>		 \r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("</div>\r\n");
   $nm_saida->saida("  </TD>\r\n");
   $nm_saida->saida(" </TR>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       if ($this->nmgp_botoes['det_pdf'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bpdf", "", "", "Dpdf_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "Listado_pacientes/Listado_pacientes_config_pdf.php?nm_opc=pdf_det&nm_target=0&nm_cor=cor&papel=1&orientacao=1&largura=1200&conf_larg=S&conf_fonte=10&language=es&conf_socor=S&KeepThis=false&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       if ($this->nmgp_botoes['det_print'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bprint", "", "", "Dprint_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "Listado_pacientes/Listado_pacientes_config_print.php?nm_opc=detalhe&nm_cor=AM&language=es&KeepThis=true&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       $Cod_Btn = nmButtonOutput($this->arr_buttons, "bvoltar", "document.F3.submit()", "document.F3.submit()", "sc_b_sai_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
       $nm_saida->saida("           $Cod_Btn \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\"  align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_id_paciente'])) ? $this->New_label['bp_id_paciente'] : "ID PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_id_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_id_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_id_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_estado_paciente'])) ? $this->New_label['bp_estado_paciente'] : "ESTADO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_estado_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_estado_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_estado_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_status_paciente'])) ? $this->New_label['bp_status_paciente'] : "STATUS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_status_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_status_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_status_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_fecha_activacion_paciente'])) ? $this->New_label['bp_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_fecha_activacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_fecha_activacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_fecha_activacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_fecha_retiro_paciente'])) ? $this->New_label['bp_fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_fecha_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_fecha_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_fecha_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_motivo_retiro_paciente'])) ? $this->New_label['bp_motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_motivo_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_motivo_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_motivo_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_observacion_motivo_retiro_paciente'])) ? $this->New_label['bp_observacion_motivo_retiro_paciente'] : "OBSERVACION MOTIVO RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_observacion_motivo_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_observacion_motivo_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_observacion_motivo_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_identificacion_paciente'])) ? $this->New_label['bp_identificacion_paciente'] : "IDENTIFICACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_identificacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_identificacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_identificacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_nombre_paciente'])) ? $this->New_label['bp_nombre_paciente'] : "NOMBRE PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_nombre_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_nombre_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_nombre_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_apellido_paciente'])) ? $this->New_label['bp_apellido_paciente'] : "APELLIDO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_apellido_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_apellido_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_apellido_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_telefono_paciente'])) ? $this->New_label['bp_telefono_paciente'] : "TELEFONO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_telefono_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_telefono_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_telefono_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_telefono2_paciente'])) ? $this->New_label['bp_telefono2_paciente'] : "TELEFONO2 PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_telefono2_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_telefono2_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_telefono2_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_telefono3_paciente'])) ? $this->New_label['bp_telefono3_paciente'] : "TELEFONO3 PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_telefono3_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_telefono3_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_telefono3_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_correo_paciente'])) ? $this->New_label['bp_correo_paciente'] : "CORREO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_correo_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_correo_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_correo_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_direccion_paciente'])) ? $this->New_label['bp_direccion_paciente'] : "DIRECCION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_direccion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_direccion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_direccion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_barrio_paciente'])) ? $this->New_label['bp_barrio_paciente'] : "BARRIO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_barrio_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_barrio_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_barrio_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_departamento_paciente'])) ? $this->New_label['bp_departamento_paciente'] : "DEPARTAMENTO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_departamento_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_departamento_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_departamento_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_ciudad_paciente'])) ? $this->New_label['bp_ciudad_paciente'] : "CIUDAD PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_ciudad_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_ciudad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_ciudad_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_genero_paciente'])) ? $this->New_label['bp_genero_paciente'] : "GENERO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_genero_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_genero_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_genero_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_fecha_nacimineto_paciente'])) ? $this->New_label['bp_fecha_nacimineto_paciente'] : "FECHA NACIMINETO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_fecha_nacimineto_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_fecha_nacimineto_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_fecha_nacimineto_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_edad_paciente'])) ? $this->New_label['bp_edad_paciente'] : "EDAD PACIENTE"; 
          $conteudo = trim(NM_encode_input(sc_strip_script($this->bp_edad_paciente))); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else    
          { 
              nmgp_Form_Num_Val($conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_edad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_edad_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_acudiente_paciente'])) ? $this->New_label['bp_acudiente_paciente'] : "ACUDIENTE PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_acudiente_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_acudiente_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_acudiente_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_telefono_acudiente_paciente'])) ? $this->New_label['bp_telefono_acudiente_paciente'] : "TELEFONO ACUDIENTE PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->bp_telefono_acudiente_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_telefono_acudiente_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_telefono_acudiente_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_codigo_xofigo'])) ? $this->New_label['bp_codigo_xofigo'] : "CODIGO XOFIGO"; 
          $conteudo = trim(sc_strip_script($this->bp_codigo_xofigo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_codigo_xofigo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_codigo_xofigo_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_id_ultima_gestion'])) ? $this->New_label['bp_id_ultima_gestion'] : "ID ULTIMA GESTION"; 
          $conteudo = trim(sc_strip_script($this->bp_id_ultima_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_id_ultima_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bp_id_ultima_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bp_usuario_creacion'])) ? $this->New_label['bp_usuario_creacion'] : "USUARIO CREACION"; 
          $conteudo = trim(sc_strip_script($this->bp_usuario_creacion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bp_usuario_creacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bp_usuario_creacion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_id_tratamiento'])) ? $this->New_label['bt_id_tratamiento'] : "ID TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_id_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_id_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_id_tratamiento_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_producto_tratamiento'])) ? $this->New_label['bt_producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_producto_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_producto_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_producto_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_nombre_referencia'])) ? $this->New_label['bt_nombre_referencia'] : "NOMBRE REFERENCIA"; 
          $conteudo = trim(sc_strip_script($this->bt_nombre_referencia)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_nombre_referencia_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_nombre_referencia_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_dosis_tratamiento'])) ? $this->New_label['bt_dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_dosis_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_dosis_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_dosis_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_clasificacion_patologica_tratamiento'])) ? $this->New_label['bt_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_clasificacion_patologica_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_clasificacion_patologica_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_clasificacion_patologica_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_tratamiento_previo'])) ? $this->New_label['bt_tratamiento_previo'] : "TRATAMIENTO PREVIO"; 
          $conteudo = trim(sc_strip_script($this->bt_tratamiento_previo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_tratamiento_previo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_tratamiento_previo_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_consentimiento_tratamiento'])) ? $this->New_label['bt_consentimiento_tratamiento'] : "CONSENTIMIENTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_consentimiento_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_consentimiento_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_consentimiento_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_fecha_inicio_terapia_tratamiento'])) ? $this->New_label['bt_fecha_inicio_terapia_tratamiento'] : "FECHA INICIO TERAPIA TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_fecha_inicio_terapia_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_fecha_inicio_terapia_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_fecha_inicio_terapia_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_regimen_tratamiento'])) ? $this->New_label['bt_regimen_tratamiento'] : "REGIMEN TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_regimen_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_regimen_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_regimen_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_asegurador_tratamiento'])) ? $this->New_label['bt_asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_asegurador_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_asegurador_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_asegurador_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_operador_logistico_tratamiento'])) ? $this->New_label['bt_operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_operador_logistico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_operador_logistico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_operador_logistico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_punto_entrega'])) ? $this->New_label['bt_punto_entrega'] : "PUNTO ENTREGA"; 
          $conteudo = trim(sc_strip_script($this->bt_punto_entrega)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_punto_entrega_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_punto_entrega_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_fecha_ultima_reclamacion_tratamiento'])) ? $this->New_label['bt_fecha_ultima_reclamacion_tratamiento'] : "FECHA ULTIMA RECLAMACION TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_fecha_ultima_reclamacion_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_fecha_ultima_reclamacion_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_fecha_ultima_reclamacion_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_otros_operadores_tratamiento'])) ? $this->New_label['bt_otros_operadores_tratamiento'] : "OTROS OPERADORES TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_otros_operadores_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_otros_operadores_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_otros_operadores_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_medios_adquisicion_tratamiento'])) ? $this->New_label['bt_medios_adquisicion_tratamiento'] : "MEDIOS ADQUISICION TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_medios_adquisicion_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_medios_adquisicion_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_medios_adquisicion_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_ips_atiende_tratamiento'])) ? $this->New_label['bt_ips_atiende_tratamiento'] : "IPS ATIENDE TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_ips_atiende_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_ips_atiende_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_ips_atiende_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_medico_tratamiento'])) ? $this->New_label['bt_medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_medico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_medico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_medico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_especialidad_tratamiento'])) ? $this->New_label['bt_especialidad_tratamiento'] : "ESPECIALIDAD TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_especialidad_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_especialidad_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_especialidad_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_zona_atencion_paramedico_tratamiento'])) ? $this->New_label['bt_zona_atencion_paramedico_tratamiento'] : "ZONA ATENCION PARAMEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_zona_atencion_paramedico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_zona_atencion_paramedico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_zona_atencion_paramedico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_ciudad_base_paramedico_tratamiento'])) ? $this->New_label['bt_ciudad_base_paramedico_tratamiento'] : "CIUDAD BASE PARAMEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_ciudad_base_paramedico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_ciudad_base_paramedico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_bt_ciudad_base_paramedico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['bt_notas_adjuntos_tratamiento'])) ? $this->New_label['bt_notas_adjuntos_tratamiento'] : "NOTAS ADJUNTOS TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->bt_notas_adjuntos_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_bt_notas_adjuntos_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_bt_notas_adjuntos_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['Listado_pacientes']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
//--- 
//--- 
   $nm_saida->saida("<form name=\"F3\" method=post\r\n");
   $nm_saida->saida("                  target=\"_self\"\r\n");
   $nm_saida->saida("                  action=\"./\">\r\n");
   $nm_saida->saida("<input type=hidden name=\"nmgp_opcao\" value=\"igual\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_init\" value=\"" . NM_encode_input($this->Ini->sc_page) . "\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_session\" value=\"" . NM_encode_input(session_id()) . "\"/>\r\n");
   $nm_saida->saida("</form>\r\n");
   $nm_saida->saida("<script language=JavaScript>\r\n");
   $nm_saida->saida("   function nm_mostra_doc(campo1, campo2, campo3)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       NovaJanela = window.open (\"Listado_pacientes_doc.php?script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&nm_cod_doc=\" + campo1 + \"&nm_nome_doc=\" + campo2 + \"&nm_cod_apl=\" + campo3, \"ScriptCase\", \"resizable\");\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_move(x, y, z, p, g) \r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("      window.location = \"" . $this->Ini->path_link . "Listado_pacientes/index.php?nmgp_opcao=pdf_det&nmgp_tipo_pdf=\" + z + \"&nmgp_parms_pdf=\" + p +  \"&nmgp_graf_pdf=\" + g + \"&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "\";\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_print_conf(tp, cor)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       window.open('" . $this->Ini->path_link . "Listado_pacientes/Listado_pacientes_iframe_prt.php?path_botoes=" . $this->Ini->path_botoes . "&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&opcao=det_print&cor_print=' + cor,'','location=no,menubar,resizable,scrollbars,status=no,toolbar');\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("</script>\r\n");
   $nm_saida->saida("</body>\r\n");
   $nm_saida->saida("</html>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
