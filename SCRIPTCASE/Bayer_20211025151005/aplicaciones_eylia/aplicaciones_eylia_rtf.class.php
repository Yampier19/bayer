<?php

class aplicaciones_eylia_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function aplicaciones_eylia_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_aplicaciones_eylia";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "aplicaciones_eylia.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['aplicaciones_eylia']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['aplicaciones_eylia']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['aplicaciones_eylia']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->ae_id_bayer_aplicaciones = $Busca_temp['ae_id_bayer_aplicaciones']; 
          $tmp_pos = strpos($this->ae_id_bayer_aplicaciones, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ae_id_bayer_aplicaciones = substr($this->ae_id_bayer_aplicaciones, 0, $tmp_pos);
          }
          $this->ae_id_bayer_aplicaciones_2 = $Busca_temp['ae_id_bayer_aplicaciones_input_2']; 
          $this->ae_numero_ojos = $Busca_temp['ae_numero_ojos']; 
          $tmp_pos = strpos($this->ae_numero_ojos, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ae_numero_ojos = substr($this->ae_numero_ojos, 0, $tmp_pos);
          }
          $this->ae_fecha_aplicacion = $Busca_temp['ae_fecha_aplicacion']; 
          $tmp_pos = strpos($this->ae_fecha_aplicacion, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ae_fecha_aplicacion = substr($this->ae_fecha_aplicacion, 0, $tmp_pos);
          }
          $this->ae_fecha_aplicacion_2 = $Busca_temp['ae_fecha_aplicacion_input_2']; 
          $this->ae_fecha_registro = $Busca_temp['ae_fecha_registro']; 
          $tmp_pos = strpos($this->ae_fecha_registro, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ae_fecha_registro = substr($this->ae_fecha_registro, 0, $tmp_pos);
          }
          $this->ae_fecha_registro_2 = $Busca_temp['ae_fecha_registro_input_2']; 
          $this->ae_id_paciente_fk = $Busca_temp['ae_id_paciente_fk']; 
          $tmp_pos = strpos($this->ae_id_paciente_fk, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ae_id_paciente_fk = substr($this->ae_id_paciente_fk, 0, $tmp_pos);
          }
          $this->ae_id_paciente_fk_2 = $Busca_temp['ae_id_paciente_fk_input_2']; 
          $this->ae_id_usuario_fk = $Busca_temp['ae_id_usuario_fk']; 
          $tmp_pos = strpos($this->ae_id_usuario_fk, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ae_id_usuario_fk = substr($this->ae_id_usuario_fk, 0, $tmp_pos);
          }
          $this->ae_id_usuario_fk_2 = $Busca_temp['ae_id_usuario_fk_input_2']; 
          $this->t_clasificacion_patologica_tratamiento = $Busca_temp['t_clasificacion_patologica_tratamiento']; 
          $tmp_pos = strpos($this->t_clasificacion_patologica_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_clasificacion_patologica_tratamiento = substr($this->t_clasificacion_patologica_tratamiento, 0, $tmp_pos);
          }
          $this->p_fecha_activacion_paciente = $Busca_temp['p_fecha_activacion_paciente']; 
          $tmp_pos = strpos($this->p_fecha_activacion_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->p_fecha_activacion_paciente = substr($this->p_fecha_activacion_paciente, 0, $tmp_pos);
          }
          $this->t_fecha_inicio_terapia_tratamiento = $Busca_temp['t_fecha_inicio_terapia_tratamiento']; 
          $tmp_pos = strpos($this->t_fecha_inicio_terapia_tratamiento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->t_fecha_inicio_terapia_tratamiento = substr($this->t_fecha_inicio_terapia_tratamiento, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT ae.ID_BAYER_APLICACIONES as ae_id_bayer_aplicaciones, ae.NUMERO_OJOS as ae_numero_ojos, str_replace (convert(char(10),ae.FECHA_APLICACION,102), '.', '-') + ' ' + convert(char(8),ae.FECHA_APLICACION,20) as ae_fecha_aplicacion, ae.FECHA_REGISTRO as ae_fecha_registro, ae.ID_PACIENTE_FK as ae_id_paciente_fk, ae.ID_USUARIO_FK as ae_id_usuario_fk, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_2 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT ae.ID_BAYER_APLICACIONES as ae_id_bayer_aplicaciones, ae.NUMERO_OJOS as ae_numero_ojos, ae.FECHA_APLICACION as ae_fecha_aplicacion, ae.FECHA_REGISTRO as ae_fecha_registro, ae.ID_PACIENTE_FK as ae_id_paciente_fk, ae.ID_USUARIO_FK as ae_id_usuario_fk, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_2 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT ae.ID_BAYER_APLICACIONES as ae_id_bayer_aplicaciones, ae.NUMERO_OJOS as ae_numero_ojos, convert(char(23),ae.FECHA_APLICACION,121) as ae_fecha_aplicacion, ae.FECHA_REGISTRO as ae_fecha_registro, ae.ID_PACIENTE_FK as ae_id_paciente_fk, ae.ID_USUARIO_FK as ae_id_usuario_fk, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_2 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT ae.ID_BAYER_APLICACIONES as ae_id_bayer_aplicaciones, ae.NUMERO_OJOS as ae_numero_ojos, ae.FECHA_APLICACION as ae_fecha_aplicacion, TO_DATE(TO_CHAR(ae.FECHA_REGISTRO, 'yyyy-mm-dd hh24:mi:ss'), 'yyyy-mm-dd hh24:mi:ss') as ae_fecha_registro, ae.ID_PACIENTE_FK as ae_id_paciente_fk, ae.ID_USUARIO_FK as ae_id_usuario_fk, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_2 from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT ae.ID_BAYER_APLICACIONES as ae_id_bayer_aplicaciones, ae.NUMERO_OJOS as ae_numero_ojos, EXTEND(ae.FECHA_APLICACION, YEAR TO DAY) as ae_fecha_aplicacion, ae.FECHA_REGISTRO as ae_fecha_registro, ae.ID_PACIENTE_FK as ae_id_paciente_fk, ae.ID_USUARIO_FK as ae_id_usuario_fk, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_2 from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT ae.ID_BAYER_APLICACIONES as ae_id_bayer_aplicaciones, ae.NUMERO_OJOS as ae_numero_ojos, ae.FECHA_APLICACION as ae_fecha_aplicacion, ae.FECHA_REGISTRO as ae_fecha_registro, ae.ID_PACIENTE_FK as ae_id_paciente_fk, ae.ID_USUARIO_FK as ae_id_usuario_fk, t.CLASIFICACION_PATOLOGICA_TRATAMIENTO as cmp_maior_30_1, p.FECHA_ACTIVACION_PACIENTE as p_fecha_activacion_paciente, t.FECHA_INICIO_TERAPIA_TRATAMIENTO as cmp_maior_30_2 from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['ae_id_bayer_aplicaciones'])) ? $this->New_label['ae_id_bayer_aplicaciones'] : "ID BAYER APLICACIONES"; 
          if ($Cada_col == "ae_id_bayer_aplicaciones" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ae_numero_ojos'])) ? $this->New_label['ae_numero_ojos'] : "NUMERO OJOS"; 
          if ($Cada_col == "ae_numero_ojos" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ae_fecha_aplicacion'])) ? $this->New_label['ae_fecha_aplicacion'] : "FECHA APLICACION"; 
          if ($Cada_col == "ae_fecha_aplicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ae_fecha_registro'])) ? $this->New_label['ae_fecha_registro'] : "FECHA REGISTRO"; 
          if ($Cada_col == "ae_fecha_registro" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ae_id_paciente_fk'])) ? $this->New_label['ae_id_paciente_fk'] : "ID PACIENTE FK"; 
          if ($Cada_col == "ae_id_paciente_fk" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ae_id_usuario_fk'])) ? $this->New_label['ae_id_usuario_fk'] : "ID USUARIO FK"; 
          if ($Cada_col == "ae_id_usuario_fk" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_clasificacion_patologica_tratamiento'])) ? $this->New_label['t_clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          if ($Cada_col == "t_clasificacion_patologica_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['p_fecha_activacion_paciente'])) ? $this->New_label['p_fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          if ($Cada_col == "p_fecha_activacion_paciente" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['t_fecha_inicio_terapia_tratamiento'])) ? $this->New_label['t_fecha_inicio_terapia_tratamiento'] : "FECHA INICIO TERAPIA TRATAMIENTO"; 
          if ($Cada_col == "t_fecha_inicio_terapia_tratamiento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->ae_id_bayer_aplicaciones = $rs->fields[0] ;  
         $this->ae_id_bayer_aplicaciones = (string)$this->ae_id_bayer_aplicaciones;
         $this->ae_numero_ojos = $rs->fields[1] ;  
         $this->ae_fecha_aplicacion = $rs->fields[2] ;  
         $this->ae_fecha_registro = $rs->fields[3] ;  
         $this->ae_id_paciente_fk = $rs->fields[4] ;  
         $this->ae_id_paciente_fk = (string)$this->ae_id_paciente_fk;
         $this->ae_id_usuario_fk = $rs->fields[5] ;  
         $this->ae_id_usuario_fk = (string)$this->ae_id_usuario_fk;
         $this->t_clasificacion_patologica_tratamiento = $rs->fields[6] ;  
         $this->p_fecha_activacion_paciente = $rs->fields[7] ;  
         $this->t_fecha_inicio_terapia_tratamiento = $rs->fields[8] ;  
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- ae_id_bayer_aplicaciones
   function NM_export_ae_id_bayer_aplicaciones()
   {
         $this->ae_id_bayer_aplicaciones = html_entity_decode($this->ae_id_bayer_aplicaciones, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ae_id_bayer_aplicaciones = strip_tags($this->ae_id_bayer_aplicaciones);
         if (!NM_is_utf8($this->ae_id_bayer_aplicaciones))
         {
             $this->ae_id_bayer_aplicaciones = sc_convert_encoding($this->ae_id_bayer_aplicaciones, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ae_id_bayer_aplicaciones = str_replace('<', '&lt;', $this->ae_id_bayer_aplicaciones);
         $this->ae_id_bayer_aplicaciones = str_replace('>', '&gt;', $this->ae_id_bayer_aplicaciones);
         $this->Texto_tag .= "<td>" . $this->ae_id_bayer_aplicaciones . "</td>\r\n";
   }
   //----- ae_numero_ojos
   function NM_export_ae_numero_ojos()
   {
         $this->ae_numero_ojos = html_entity_decode($this->ae_numero_ojos, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ae_numero_ojos = strip_tags($this->ae_numero_ojos);
         if (!NM_is_utf8($this->ae_numero_ojos))
         {
             $this->ae_numero_ojos = sc_convert_encoding($this->ae_numero_ojos, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ae_numero_ojos = str_replace('<', '&lt;', $this->ae_numero_ojos);
         $this->ae_numero_ojos = str_replace('>', '&gt;', $this->ae_numero_ojos);
         $this->Texto_tag .= "<td>" . $this->ae_numero_ojos . "</td>\r\n";
   }
   //----- ae_fecha_aplicacion
   function NM_export_ae_fecha_aplicacion()
   {
         $conteudo_x = $this->ae_fecha_aplicacion;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->ae_fecha_aplicacion, "YYYY-MM-DD");
             $this->ae_fecha_aplicacion = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
         } 
         if (!NM_is_utf8($this->ae_fecha_aplicacion))
         {
             $this->ae_fecha_aplicacion = sc_convert_encoding($this->ae_fecha_aplicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ae_fecha_aplicacion = str_replace('<', '&lt;', $this->ae_fecha_aplicacion);
         $this->ae_fecha_aplicacion = str_replace('>', '&gt;', $this->ae_fecha_aplicacion);
         $this->Texto_tag .= "<td>" . $this->ae_fecha_aplicacion . "</td>\r\n";
   }
   //----- ae_fecha_registro
   function NM_export_ae_fecha_registro()
   {
         if (substr($this->ae_fecha_registro, 10, 1) == "-") 
         { 
             $this->ae_fecha_registro = substr($this->ae_fecha_registro, 0, 10) . " " . substr($this->ae_fecha_registro, 11);
         } 
         if (substr($this->ae_fecha_registro, 13, 1) == ".") 
         { 
            $this->ae_fecha_registro = substr($this->ae_fecha_registro, 0, 13) . ":" . substr($this->ae_fecha_registro, 14, 2) . ":" . substr($this->ae_fecha_registro, 17);
         } 
         $conteudo_x = $this->ae_fecha_registro;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD HH:II:SS");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->ae_fecha_registro, "YYYY-MM-DD HH:II:SS");
             $this->ae_fecha_registro = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DH", "ddmmaaaa;hhiiss"));
         } 
         if (!NM_is_utf8($this->ae_fecha_registro))
         {
             $this->ae_fecha_registro = sc_convert_encoding($this->ae_fecha_registro, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ae_fecha_registro = str_replace('<', '&lt;', $this->ae_fecha_registro);
         $this->ae_fecha_registro = str_replace('>', '&gt;', $this->ae_fecha_registro);
         $this->Texto_tag .= "<td>" . $this->ae_fecha_registro . "</td>\r\n";
   }
   //----- ae_id_paciente_fk
   function NM_export_ae_id_paciente_fk()
   {
         $this->ae_id_paciente_fk = html_entity_decode($this->ae_id_paciente_fk, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ae_id_paciente_fk = strip_tags($this->ae_id_paciente_fk);
         if (!NM_is_utf8($this->ae_id_paciente_fk))
         {
             $this->ae_id_paciente_fk = sc_convert_encoding($this->ae_id_paciente_fk, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ae_id_paciente_fk = str_replace('<', '&lt;', $this->ae_id_paciente_fk);
         $this->ae_id_paciente_fk = str_replace('>', '&gt;', $this->ae_id_paciente_fk);
         $this->Texto_tag .= "<td>" . $this->ae_id_paciente_fk . "</td>\r\n";
   }
   //----- ae_id_usuario_fk
   function NM_export_ae_id_usuario_fk()
   {
         $this->ae_id_usuario_fk = html_entity_decode($this->ae_id_usuario_fk, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ae_id_usuario_fk = strip_tags($this->ae_id_usuario_fk);
         if (!NM_is_utf8($this->ae_id_usuario_fk))
         {
             $this->ae_id_usuario_fk = sc_convert_encoding($this->ae_id_usuario_fk, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ae_id_usuario_fk = str_replace('<', '&lt;', $this->ae_id_usuario_fk);
         $this->ae_id_usuario_fk = str_replace('>', '&gt;', $this->ae_id_usuario_fk);
         $this->Texto_tag .= "<td>" . $this->ae_id_usuario_fk . "</td>\r\n";
   }
   //----- t_clasificacion_patologica_tratamiento
   function NM_export_t_clasificacion_patologica_tratamiento()
   {
         $this->t_clasificacion_patologica_tratamiento = html_entity_decode($this->t_clasificacion_patologica_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_clasificacion_patologica_tratamiento = strip_tags($this->t_clasificacion_patologica_tratamiento);
         if (!NM_is_utf8($this->t_clasificacion_patologica_tratamiento))
         {
             $this->t_clasificacion_patologica_tratamiento = sc_convert_encoding($this->t_clasificacion_patologica_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_clasificacion_patologica_tratamiento = str_replace('<', '&lt;', $this->t_clasificacion_patologica_tratamiento);
         $this->t_clasificacion_patologica_tratamiento = str_replace('>', '&gt;', $this->t_clasificacion_patologica_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_clasificacion_patologica_tratamiento . "</td>\r\n";
   }
   //----- p_fecha_activacion_paciente
   function NM_export_p_fecha_activacion_paciente()
   {
         $this->p_fecha_activacion_paciente = html_entity_decode($this->p_fecha_activacion_paciente, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->p_fecha_activacion_paciente = strip_tags($this->p_fecha_activacion_paciente);
         if (!NM_is_utf8($this->p_fecha_activacion_paciente))
         {
             $this->p_fecha_activacion_paciente = sc_convert_encoding($this->p_fecha_activacion_paciente, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->p_fecha_activacion_paciente = str_replace('<', '&lt;', $this->p_fecha_activacion_paciente);
         $this->p_fecha_activacion_paciente = str_replace('>', '&gt;', $this->p_fecha_activacion_paciente);
         $this->Texto_tag .= "<td>" . $this->p_fecha_activacion_paciente . "</td>\r\n";
   }
   //----- t_fecha_inicio_terapia_tratamiento
   function NM_export_t_fecha_inicio_terapia_tratamiento()
   {
         $this->t_fecha_inicio_terapia_tratamiento = html_entity_decode($this->t_fecha_inicio_terapia_tratamiento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->t_fecha_inicio_terapia_tratamiento = strip_tags($this->t_fecha_inicio_terapia_tratamiento);
         if (!NM_is_utf8($this->t_fecha_inicio_terapia_tratamiento))
         {
             $this->t_fecha_inicio_terapia_tratamiento = sc_convert_encoding($this->t_fecha_inicio_terapia_tratamiento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->t_fecha_inicio_terapia_tratamiento = str_replace('<', '&lt;', $this->t_fecha_inicio_terapia_tratamiento);
         $this->t_fecha_inicio_terapia_tratamiento = str_replace('>', '&gt;', $this->t_fecha_inicio_terapia_tratamiento);
         $this->Texto_tag .= "<td>" . $this->t_fecha_inicio_terapia_tratamiento . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['aplicaciones_eylia'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE>APLICACIONES EYLIA :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="aplicaciones_eylia_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="aplicaciones_eylia"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
