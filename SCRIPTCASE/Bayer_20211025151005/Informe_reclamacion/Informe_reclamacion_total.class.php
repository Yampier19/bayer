<?php

class Informe_reclamacion_total
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;

   var $nm_data;

   //----- 
   function Informe_reclamacion_total($sc_page)
   {
      $this->sc_page = $sc_page;
      $this->nm_data = new nm_data("es");
      if (isset($_SESSION['sc_session'][$this->sc_page]['Informe_reclamacion']['campos_busca']) && !empty($_SESSION['sc_session'][$this->sc_page]['Informe_reclamacion']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_paciente = $Busca_temp['id_paciente']; 
          $tmp_pos = strpos($this->id_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
          }
          $id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
          $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
          $this->departamento_paciente = $Busca_temp['departamento_paciente']; 
          $tmp_pos = strpos($this->departamento_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->departamento_paciente = substr($this->departamento_paciente, 0, $tmp_pos);
          }
          $this->ciudad_paciente = $Busca_temp['ciudad_paciente']; 
          $tmp_pos = strpos($this->ciudad_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->ciudad_paciente = substr($this->ciudad_paciente, 0, $tmp_pos);
          }
          $this->estado_paciente = $Busca_temp['estado_paciente']; 
          $tmp_pos = strpos($this->estado_paciente, "##@@");
          if ($tmp_pos !== false)
          {
              $this->estado_paciente = substr($this->estado_paciente, 0, $tmp_pos);
          }
      } 
   }

   //---- 
   function quebra_geral()
   {
      global $nada, $nm_lang ;
      if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['contr_total_geral'] == "OK") 
      { 
          return; 
      } 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['tot_geral'] = array() ;  
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
          $nm_comando = "select count(*) from (SELECT    bayer_pacientes.ID_PACIENTE,    bayer_pacientes.DEPARTAMENTO_PACIENTE,    bayer_pacientes.CIUDAD_PACIENTE,    bayer_pacientes.ESTADO_PACIENTE,    bayer_pacientes.STATUS_PACIENTE,    bayer_pacientes.FECHA_ACTIVACION_PACIENTE,    bayer_pacientes.FECHA_RETIRO_PACIENTE,    bayer_pacientes.MOTIVO_RETIRO_PACIENTE,    bayer_pacientes.CODIGO_XOFIGO,    bayer_pacientes.ID_ULTIMA_GESTION,     bayer_tratamiento.ID_TRATAMIENTO,    bayer_tratamiento.PRODUCTO_TRATAMIENTO,    bayer_tratamiento.NOMBRE_REFERENCIA,    bayer_tratamiento.DOSIS_TRATAMIENTO,    bayer_tratamiento.FECHA_INICIO_TERAPIA_TRATAMIENTO,    bayer_tratamiento.ASEGURADOR_TRATAMIENTO,     bayer_tratamiento.OPERADOR_LOGISTICO_TRATAMIENTO,    bayer_tratamiento.PUNTO_ENTREGA,        bayer_tratamiento.IPS_ATIENDE_TRATAMIENTO,    bayer_tratamiento.REGIMEN_TRATAMIENTO,    bayer_tratamiento.MEDICO_TRATAMIENTO,    bayer_tratamiento.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,               bayer_tratamiento.CLASIFICACION_PATOLOGICA_TRATAMIENTO,    bayer_tratamiento.TRATAMIENTO_PREVIO,    bayer_tratamiento.ID_PACIENTE_FK,     bayer_gestiones.ID_GESTION,    bayer_gestiones.LOGRO_COMUNICACION_GESTION,    bayer_gestiones.CAUSA_NO_RECLAMACION_GESTION,    bayer_gestiones.RECLAMO_GESTION,    bayer_gestiones.FECHA_RECLAMACION_GESTION,    bayer_gestiones.FECHA_COMUNICACION,    bayer_gestiones.FECHA_PROGRAMADA_GESTION,    bayer_gestiones.AUTOR_GESTION,    bayer_gestiones.DESCRIPCION_COMUNICACION_GESTION,    bayer_gestiones.FECHA_MEDICAMENTO_HASTA,       bayer_gestiones.NUMERO_CAJAS,    bayer_gestiones.PAAP,    bayer_gestiones.SUB_PAAP,    bayer_gestiones.BARRERA,    bayer_gestiones.FECHA_INI_PAAP,    bayer_gestiones.FECHA_FIN_PAAP   FROM bayer_pacientes     INNER JOIN bayer_gestiones ON  bayer_pacientes.ID_PACIENTE = bayer_gestiones.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento ON bayer_pacientes.ID_PACIENTE = bayer_tratamiento.ID_PACIENTE_FK  WHERE bayer_gestiones.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp " . $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['where_pesq']; 
      } 
      else 
      { 
          $nm_comando = "select count(*) from (SELECT    bayer_pacientes.ID_PACIENTE,    bayer_pacientes.DEPARTAMENTO_PACIENTE,    bayer_pacientes.CIUDAD_PACIENTE,    bayer_pacientes.ESTADO_PACIENTE,    bayer_pacientes.STATUS_PACIENTE,    bayer_pacientes.FECHA_ACTIVACION_PACIENTE,    bayer_pacientes.FECHA_RETIRO_PACIENTE,    bayer_pacientes.MOTIVO_RETIRO_PACIENTE,    bayer_pacientes.CODIGO_XOFIGO,    bayer_pacientes.ID_ULTIMA_GESTION,     bayer_tratamiento.ID_TRATAMIENTO,    bayer_tratamiento.PRODUCTO_TRATAMIENTO,    bayer_tratamiento.NOMBRE_REFERENCIA,    bayer_tratamiento.DOSIS_TRATAMIENTO,    bayer_tratamiento.FECHA_INICIO_TERAPIA_TRATAMIENTO,    bayer_tratamiento.ASEGURADOR_TRATAMIENTO,     bayer_tratamiento.OPERADOR_LOGISTICO_TRATAMIENTO,    bayer_tratamiento.PUNTO_ENTREGA,        bayer_tratamiento.IPS_ATIENDE_TRATAMIENTO,    bayer_tratamiento.REGIMEN_TRATAMIENTO,    bayer_tratamiento.MEDICO_TRATAMIENTO,    bayer_tratamiento.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,               bayer_tratamiento.CLASIFICACION_PATOLOGICA_TRATAMIENTO,    bayer_tratamiento.TRATAMIENTO_PREVIO,    bayer_tratamiento.ID_PACIENTE_FK,     bayer_gestiones.ID_GESTION,    bayer_gestiones.LOGRO_COMUNICACION_GESTION,    bayer_gestiones.CAUSA_NO_RECLAMACION_GESTION,    bayer_gestiones.RECLAMO_GESTION,    bayer_gestiones.FECHA_RECLAMACION_GESTION,    bayer_gestiones.FECHA_COMUNICACION,    bayer_gestiones.FECHA_PROGRAMADA_GESTION,    bayer_gestiones.AUTOR_GESTION,    bayer_gestiones.DESCRIPCION_COMUNICACION_GESTION,    bayer_gestiones.FECHA_MEDICAMENTO_HASTA,       bayer_gestiones.NUMERO_CAJAS,    bayer_gestiones.PAAP,    bayer_gestiones.SUB_PAAP,    bayer_gestiones.BARRERA,    bayer_gestiones.FECHA_INI_PAAP,    bayer_gestiones.FECHA_FIN_PAAP   FROM bayer_pacientes     INNER JOIN bayer_gestiones ON  bayer_pacientes.ID_PACIENTE = bayer_gestiones.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento ON bayer_pacientes.ID_PACIENTE = bayer_tratamiento.ID_PACIENTE_FK  WHERE bayer_gestiones.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp " . $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['where_pesq']; 
      } 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nm_comando;
      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = '';
      if (!$rt = $this->Db->Execute($nm_comando)) 
      { 
         $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
         exit ; 
      }
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['tot_geral'][0] = "" . $this->Ini->Nm_lang['lang_msgs_totl'] . ""; 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['tot_geral'][1] = $rt->fields[0] ; 
      $rt->Close(); 
      $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['contr_total_geral'] = "OK";
   } 

   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
