<?php
//--- 
class Informe_reclamacion_det
{
   var $Ini;
   var $Erro;
   var $Db;
   var $nm_data;
   var $NM_raiz_img; 
   var $nmgp_botoes; 
   var $nm_location;
   var $id_paciente;
   var $logro_comunicacion_gestion;
   var $fecha_comunicacion;
   var $autor_gestion;
   var $departamento_paciente;
   var $ciudad_paciente;
   var $estado_paciente;
   var $status_paciente;
   var $fecha_activacion_paciente;
   var $fecha_retiro_paciente;
   var $motivo_retiro_paciente;
   var $fecha_inicio_terapia_tratamiento;
   var $codigo_xofigo;
   var $clasificacion_patologica_tratamiento;
   var $producto_tratamiento;
   var $nombre_referencia;
   var $dosis_tratamiento;
   var $fecha_medicamento_hasta;
   var $numero_cajas;
   var $asegurador_tratamiento;
   var $operador_logistico_tratamiento;
   var $punto_entrega;
   var $ips_atiende_tratamiento;
   var $regimen_tratamiento;
   var $medico_tratamiento;
   var $tratamiento_previo;
   var $reclamo_gestion;
   var $fecha_reclamacion_gestion;
   var $causa_no_reclamacion_gestion;
   var $descripcion_comunicacion_gestion;
   var $fecha_programada_gestion;
   var $id_ultima_gestion;
   var $id_tratamiento;
   var $id_paciente_fk;
   var $paap;
   var $sub_paap;
   var $barrera;
   var $fecha_ini_paap;
   var $fecha_fin_paap;
   var $fecha_ultima_reclamacion_tratamiento;
   var $id_gestion;
 function monta_det()
 {
    global 
           $nm_saida, $nm_lang, $nmgp_cor_print, $nmgp_tipo_pdf;
    $this->nmgp_botoes['det_pdf'] = "on";
    $this->nmgp_botoes['det_print'] = "on";
    $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
    if (isset($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion']['btn_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion']['btn_display']))
    {
        foreach ($_SESSION['scriptcase']['sc_apl_conf']['Informe_reclamacion']['btn_display'] as $NM_cada_btn => $NM_cada_opc)
        {
            $this->nmgp_botoes[$NM_cada_btn] = $NM_cada_opc;
        }
    }
    if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['campos_busca']))
    { 
        $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['campos_busca'];
        if ($_SESSION['scriptcase']['charset'] != "UTF-8")
        {
            $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
        }
        $this->id_paciente = $Busca_temp['id_paciente']; 
        $tmp_pos = strpos($this->id_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->id_paciente = substr($this->id_paciente, 0, $tmp_pos);
        }
        $this->id_paciente_2 = $Busca_temp['id_paciente_input_2']; 
        $this->departamento_paciente = $Busca_temp['departamento_paciente']; 
        $tmp_pos = strpos($this->departamento_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->departamento_paciente = substr($this->departamento_paciente, 0, $tmp_pos);
        }
        $this->ciudad_paciente = $Busca_temp['ciudad_paciente']; 
        $tmp_pos = strpos($this->ciudad_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->ciudad_paciente = substr($this->ciudad_paciente, 0, $tmp_pos);
        }
        $this->estado_paciente = $Busca_temp['estado_paciente']; 
        $tmp_pos = strpos($this->estado_paciente, "##@@");
        if ($tmp_pos !== false)
        {
            $this->estado_paciente = substr($this->estado_paciente, 0, $tmp_pos);
        }
    } 
    $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['where_orig'];
    $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['where_pesq'];
    $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['where_pesq_filtro'];
    $this->nm_field_dinamico = array();
    $this->nm_order_dinamico = array();
    $this->nm_data = new nm_data("es_es");
    $this->NM_raiz_img  = ""; 
    $this->sc_proc_grid = false; 
    include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
    $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['seq_dir'] = 0; 
    $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['sub_dir'] = array(); 
   $Str_date = strtolower($_SESSION['scriptcase']['reg_conf']['date_format']);
   $Lim   = strlen($Str_date);
   $Ult   = "";
   $Arr_D = array();
   for ($I = 0; $I < $Lim; $I++)
   {
       $Char = substr($Str_date, $I, 1);
       if ($Char != $Ult)
       {
           $Arr_D[] = $Char;
       }
       $Ult = $Char;
   }
   $Prim = true;
   $Str  = "";
   foreach ($Arr_D as $Cada_d)
   {
       $Str .= (!$Prim) ? $_SESSION['scriptcase']['reg_conf']['date_sep'] : "";
       $Str .= $Cada_d;
       $Prim = false;
   }
   $Str = str_replace("a", "Y", $Str);
   $Str = str_replace("y", "Y", $Str);
   $nm_data_fixa = date($Str); 
   $this->nm_data->SetaData(date("Y/m/d H:i:s"), "YYYY/MM/DD HH:II:SS"); 
   $this->Ini->sc_Include($this->Ini->path_lib_php . "/nm_edit.php", "F", "nmgp_Form_Num_Val") ; 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, CODIGO_XOFIGO, ID_ULTIMA_GESTION, ID_TRATAMIENTO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, FECHA_INICIO_TERAPIA_TRATAMIENTO, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, IPS_ATIENDE_TRATAMIENTO, REGIMEN_TRATAMIENTO, MEDICO_TRATAMIENTO, FECHA_ULTIMA_RECLAMACION_TRATAMIENTO, CLASIFICACION_PATOLOGICA_TRATAMIENTO, TRATAMIENTO_PREVIO, ID_PACIENTE_FK, ID_GESTION, LOGRO_COMUNICACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, FECHA_COMUNICACION, FECHA_PROGRAMADA_GESTION, AUTOR_GESTION, DESCRIPCION_COMUNICACION_GESTION, FECHA_MEDICAMENTO_HASTA, NUMERO_CAJAS, PAAP, SUB_PAAP, BARRERA, FECHA_INI_PAAP, FECHA_FIN_PAAP from (SELECT    bayer_pacientes.ID_PACIENTE,    bayer_pacientes.DEPARTAMENTO_PACIENTE,    bayer_pacientes.CIUDAD_PACIENTE,    bayer_pacientes.ESTADO_PACIENTE,    bayer_pacientes.STATUS_PACIENTE,    bayer_pacientes.FECHA_ACTIVACION_PACIENTE,    bayer_pacientes.FECHA_RETIRO_PACIENTE,    bayer_pacientes.MOTIVO_RETIRO_PACIENTE,    bayer_pacientes.CODIGO_XOFIGO,    bayer_pacientes.ID_ULTIMA_GESTION,     bayer_tratamiento.ID_TRATAMIENTO,    bayer_tratamiento.PRODUCTO_TRATAMIENTO,    bayer_tratamiento.NOMBRE_REFERENCIA,    bayer_tratamiento.DOSIS_TRATAMIENTO,    bayer_tratamiento.FECHA_INICIO_TERAPIA_TRATAMIENTO,    bayer_tratamiento.ASEGURADOR_TRATAMIENTO,     bayer_tratamiento.OPERADOR_LOGISTICO_TRATAMIENTO,    bayer_tratamiento.PUNTO_ENTREGA,        bayer_tratamiento.IPS_ATIENDE_TRATAMIENTO,    bayer_tratamiento.REGIMEN_TRATAMIENTO,    bayer_tratamiento.MEDICO_TRATAMIENTO,    bayer_tratamiento.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,               bayer_tratamiento.CLASIFICACION_PATOLOGICA_TRATAMIENTO,    bayer_tratamiento.TRATAMIENTO_PREVIO,    bayer_tratamiento.ID_PACIENTE_FK,     bayer_gestiones.ID_GESTION,    bayer_gestiones.LOGRO_COMUNICACION_GESTION,    bayer_gestiones.CAUSA_NO_RECLAMACION_GESTION,    bayer_gestiones.RECLAMO_GESTION,    bayer_gestiones.FECHA_RECLAMACION_GESTION,    bayer_gestiones.FECHA_COMUNICACION,    bayer_gestiones.FECHA_PROGRAMADA_GESTION,    bayer_gestiones.AUTOR_GESTION,    bayer_gestiones.DESCRIPCION_COMUNICACION_GESTION,    bayer_gestiones.FECHA_MEDICAMENTO_HASTA,       bayer_gestiones.NUMERO_CAJAS,    bayer_gestiones.PAAP,    bayer_gestiones.SUB_PAAP,    bayer_gestiones.BARRERA,    bayer_gestiones.FECHA_INI_PAAP,    bayer_gestiones.FECHA_FIN_PAAP   FROM bayer_pacientes     INNER JOIN bayer_gestiones ON  bayer_pacientes.ID_PACIENTE = bayer_gestiones.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento ON bayer_pacientes.ID_PACIENTE = bayer_tratamiento.ID_PACIENTE_FK  WHERE bayer_gestiones.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, CODIGO_XOFIGO, ID_ULTIMA_GESTION, ID_TRATAMIENTO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, FECHA_INICIO_TERAPIA_TRATAMIENTO, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, IPS_ATIENDE_TRATAMIENTO, REGIMEN_TRATAMIENTO, MEDICO_TRATAMIENTO, FECHA_ULTIMA_RECLAMACION_TRATAMIENTO, CLASIFICACION_PATOLOGICA_TRATAMIENTO, TRATAMIENTO_PREVIO, ID_PACIENTE_FK, ID_GESTION, LOGRO_COMUNICACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, FECHA_COMUNICACION, FECHA_PROGRAMADA_GESTION, AUTOR_GESTION, DESCRIPCION_COMUNICACION_GESTION, FECHA_MEDICAMENTO_HASTA, NUMERO_CAJAS, PAAP, SUB_PAAP, BARRERA, FECHA_INI_PAAP, FECHA_FIN_PAAP from (SELECT    bayer_pacientes.ID_PACIENTE,    bayer_pacientes.DEPARTAMENTO_PACIENTE,    bayer_pacientes.CIUDAD_PACIENTE,    bayer_pacientes.ESTADO_PACIENTE,    bayer_pacientes.STATUS_PACIENTE,    bayer_pacientes.FECHA_ACTIVACION_PACIENTE,    bayer_pacientes.FECHA_RETIRO_PACIENTE,    bayer_pacientes.MOTIVO_RETIRO_PACIENTE,    bayer_pacientes.CODIGO_XOFIGO,    bayer_pacientes.ID_ULTIMA_GESTION,     bayer_tratamiento.ID_TRATAMIENTO,    bayer_tratamiento.PRODUCTO_TRATAMIENTO,    bayer_tratamiento.NOMBRE_REFERENCIA,    bayer_tratamiento.DOSIS_TRATAMIENTO,    bayer_tratamiento.FECHA_INICIO_TERAPIA_TRATAMIENTO,    bayer_tratamiento.ASEGURADOR_TRATAMIENTO,     bayer_tratamiento.OPERADOR_LOGISTICO_TRATAMIENTO,    bayer_tratamiento.PUNTO_ENTREGA,        bayer_tratamiento.IPS_ATIENDE_TRATAMIENTO,    bayer_tratamiento.REGIMEN_TRATAMIENTO,    bayer_tratamiento.MEDICO_TRATAMIENTO,    bayer_tratamiento.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,               bayer_tratamiento.CLASIFICACION_PATOLOGICA_TRATAMIENTO,    bayer_tratamiento.TRATAMIENTO_PREVIO,    bayer_tratamiento.ID_PACIENTE_FK,     bayer_gestiones.ID_GESTION,    bayer_gestiones.LOGRO_COMUNICACION_GESTION,    bayer_gestiones.CAUSA_NO_RECLAMACION_GESTION,    bayer_gestiones.RECLAMO_GESTION,    bayer_gestiones.FECHA_RECLAMACION_GESTION,    bayer_gestiones.FECHA_COMUNICACION,    bayer_gestiones.FECHA_PROGRAMADA_GESTION,    bayer_gestiones.AUTOR_GESTION,    bayer_gestiones.DESCRIPCION_COMUNICACION_GESTION,    bayer_gestiones.FECHA_MEDICAMENTO_HASTA,       bayer_gestiones.NUMERO_CAJAS,    bayer_gestiones.PAAP,    bayer_gestiones.SUB_PAAP,    bayer_gestiones.BARRERA,    bayer_gestiones.FECHA_INI_PAAP,    bayer_gestiones.FECHA_FIN_PAAP   FROM bayer_pacientes     INNER JOIN bayer_gestiones ON  bayer_pacientes.ID_PACIENTE = bayer_gestiones.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento ON bayer_pacientes.ID_PACIENTE = bayer_tratamiento.ID_PACIENTE_FK  WHERE bayer_gestiones.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, CODIGO_XOFIGO, ID_ULTIMA_GESTION, ID_TRATAMIENTO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, FECHA_INICIO_TERAPIA_TRATAMIENTO, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, IPS_ATIENDE_TRATAMIENTO, REGIMEN_TRATAMIENTO, MEDICO_TRATAMIENTO, FECHA_ULTIMA_RECLAMACION_TRATAMIENTO, CLASIFICACION_PATOLOGICA_TRATAMIENTO, TRATAMIENTO_PREVIO, ID_PACIENTE_FK, ID_GESTION, LOGRO_COMUNICACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, FECHA_COMUNICACION, FECHA_PROGRAMADA_GESTION, AUTOR_GESTION, DESCRIPCION_COMUNICACION_GESTION, FECHA_MEDICAMENTO_HASTA, NUMERO_CAJAS, PAAP, SUB_PAAP, BARRERA, FECHA_INI_PAAP, FECHA_FIN_PAAP from (SELECT    bayer_pacientes.ID_PACIENTE,    bayer_pacientes.DEPARTAMENTO_PACIENTE,    bayer_pacientes.CIUDAD_PACIENTE,    bayer_pacientes.ESTADO_PACIENTE,    bayer_pacientes.STATUS_PACIENTE,    bayer_pacientes.FECHA_ACTIVACION_PACIENTE,    bayer_pacientes.FECHA_RETIRO_PACIENTE,    bayer_pacientes.MOTIVO_RETIRO_PACIENTE,    bayer_pacientes.CODIGO_XOFIGO,    bayer_pacientes.ID_ULTIMA_GESTION,     bayer_tratamiento.ID_TRATAMIENTO,    bayer_tratamiento.PRODUCTO_TRATAMIENTO,    bayer_tratamiento.NOMBRE_REFERENCIA,    bayer_tratamiento.DOSIS_TRATAMIENTO,    bayer_tratamiento.FECHA_INICIO_TERAPIA_TRATAMIENTO,    bayer_tratamiento.ASEGURADOR_TRATAMIENTO,     bayer_tratamiento.OPERADOR_LOGISTICO_TRATAMIENTO,    bayer_tratamiento.PUNTO_ENTREGA,        bayer_tratamiento.IPS_ATIENDE_TRATAMIENTO,    bayer_tratamiento.REGIMEN_TRATAMIENTO,    bayer_tratamiento.MEDICO_TRATAMIENTO,    bayer_tratamiento.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,               bayer_tratamiento.CLASIFICACION_PATOLOGICA_TRATAMIENTO,    bayer_tratamiento.TRATAMIENTO_PREVIO,    bayer_tratamiento.ID_PACIENTE_FK,     bayer_gestiones.ID_GESTION,    bayer_gestiones.LOGRO_COMUNICACION_GESTION,    bayer_gestiones.CAUSA_NO_RECLAMACION_GESTION,    bayer_gestiones.RECLAMO_GESTION,    bayer_gestiones.FECHA_RECLAMACION_GESTION,    bayer_gestiones.FECHA_COMUNICACION,    bayer_gestiones.FECHA_PROGRAMADA_GESTION,    bayer_gestiones.AUTOR_GESTION,    bayer_gestiones.DESCRIPCION_COMUNICACION_GESTION,    bayer_gestiones.FECHA_MEDICAMENTO_HASTA,       bayer_gestiones.NUMERO_CAJAS,    bayer_gestiones.PAAP,    bayer_gestiones.SUB_PAAP,    bayer_gestiones.BARRERA,    bayer_gestiones.FECHA_INI_PAAP,    bayer_gestiones.FECHA_FIN_PAAP   FROM bayer_pacientes     INNER JOIN bayer_gestiones ON  bayer_pacientes.ID_PACIENTE = bayer_gestiones.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento ON bayer_pacientes.ID_PACIENTE = bayer_tratamiento.ID_PACIENTE_FK  WHERE bayer_gestiones.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix)) 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, CODIGO_XOFIGO, ID_ULTIMA_GESTION, ID_TRATAMIENTO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, FECHA_INICIO_TERAPIA_TRATAMIENTO, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, IPS_ATIENDE_TRATAMIENTO, REGIMEN_TRATAMIENTO, MEDICO_TRATAMIENTO, FECHA_ULTIMA_RECLAMACION_TRATAMIENTO, CLASIFICACION_PATOLOGICA_TRATAMIENTO, TRATAMIENTO_PREVIO, ID_PACIENTE_FK, ID_GESTION, LOGRO_COMUNICACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, FECHA_COMUNICACION, FECHA_PROGRAMADA_GESTION, AUTOR_GESTION, LOTOFILE(DESCRIPCION_COMUNICACION_GESTION, '" . $this->Ini->root . $this->Ini->path_imag_temp . "/sc_blob_informix', 'client') as descripcion_comunicacion_gestion, FECHA_MEDICAMENTO_HASTA, NUMERO_CAJAS, PAAP, SUB_PAAP, BARRERA, FECHA_INI_PAAP, FECHA_FIN_PAAP from (SELECT    bayer_pacientes.ID_PACIENTE,    bayer_pacientes.DEPARTAMENTO_PACIENTE,    bayer_pacientes.CIUDAD_PACIENTE,    bayer_pacientes.ESTADO_PACIENTE,    bayer_pacientes.STATUS_PACIENTE,    bayer_pacientes.FECHA_ACTIVACION_PACIENTE,    bayer_pacientes.FECHA_RETIRO_PACIENTE,    bayer_pacientes.MOTIVO_RETIRO_PACIENTE,    bayer_pacientes.CODIGO_XOFIGO,    bayer_pacientes.ID_ULTIMA_GESTION,     bayer_tratamiento.ID_TRATAMIENTO,    bayer_tratamiento.PRODUCTO_TRATAMIENTO,    bayer_tratamiento.NOMBRE_REFERENCIA,    bayer_tratamiento.DOSIS_TRATAMIENTO,    bayer_tratamiento.FECHA_INICIO_TERAPIA_TRATAMIENTO,    bayer_tratamiento.ASEGURADOR_TRATAMIENTO,     bayer_tratamiento.OPERADOR_LOGISTICO_TRATAMIENTO,    bayer_tratamiento.PUNTO_ENTREGA,        bayer_tratamiento.IPS_ATIENDE_TRATAMIENTO,    bayer_tratamiento.REGIMEN_TRATAMIENTO,    bayer_tratamiento.MEDICO_TRATAMIENTO,    bayer_tratamiento.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,               bayer_tratamiento.CLASIFICACION_PATOLOGICA_TRATAMIENTO,    bayer_tratamiento.TRATAMIENTO_PREVIO,    bayer_tratamiento.ID_PACIENTE_FK,     bayer_gestiones.ID_GESTION,    bayer_gestiones.LOGRO_COMUNICACION_GESTION,    bayer_gestiones.CAUSA_NO_RECLAMACION_GESTION,    bayer_gestiones.RECLAMO_GESTION,    bayer_gestiones.FECHA_RECLAMACION_GESTION,    bayer_gestiones.FECHA_COMUNICACION,    bayer_gestiones.FECHA_PROGRAMADA_GESTION,    bayer_gestiones.AUTOR_GESTION,    bayer_gestiones.DESCRIPCION_COMUNICACION_GESTION,    bayer_gestiones.FECHA_MEDICAMENTO_HASTA,       bayer_gestiones.NUMERO_CAJAS,    bayer_gestiones.PAAP,    bayer_gestiones.SUB_PAAP,    bayer_gestiones.BARRERA,    bayer_gestiones.FECHA_INI_PAAP,    bayer_gestiones.FECHA_FIN_PAAP   FROM bayer_pacientes     INNER JOIN bayer_gestiones ON  bayer_pacientes.ID_PACIENTE = bayer_gestiones.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento ON bayer_pacientes.ID_PACIENTE = bayer_tratamiento.ID_PACIENTE_FK  WHERE bayer_gestiones.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   else 
   { 
       $nmgp_select = "SELECT ID_PACIENTE, DEPARTAMENTO_PACIENTE, CIUDAD_PACIENTE, ESTADO_PACIENTE, STATUS_PACIENTE, FECHA_ACTIVACION_PACIENTE, FECHA_RETIRO_PACIENTE, MOTIVO_RETIRO_PACIENTE, CODIGO_XOFIGO, ID_ULTIMA_GESTION, ID_TRATAMIENTO, PRODUCTO_TRATAMIENTO, NOMBRE_REFERENCIA, DOSIS_TRATAMIENTO, FECHA_INICIO_TERAPIA_TRATAMIENTO, ASEGURADOR_TRATAMIENTO, OPERADOR_LOGISTICO_TRATAMIENTO, PUNTO_ENTREGA, IPS_ATIENDE_TRATAMIENTO, REGIMEN_TRATAMIENTO, MEDICO_TRATAMIENTO, FECHA_ULTIMA_RECLAMACION_TRATAMIENTO, CLASIFICACION_PATOLOGICA_TRATAMIENTO, TRATAMIENTO_PREVIO, ID_PACIENTE_FK, ID_GESTION, LOGRO_COMUNICACION_GESTION, CAUSA_NO_RECLAMACION_GESTION, RECLAMO_GESTION, FECHA_RECLAMACION_GESTION, FECHA_COMUNICACION, FECHA_PROGRAMADA_GESTION, AUTOR_GESTION, DESCRIPCION_COMUNICACION_GESTION, FECHA_MEDICAMENTO_HASTA, NUMERO_CAJAS, PAAP, SUB_PAAP, BARRERA, FECHA_INI_PAAP, FECHA_FIN_PAAP from (SELECT    bayer_pacientes.ID_PACIENTE,    bayer_pacientes.DEPARTAMENTO_PACIENTE,    bayer_pacientes.CIUDAD_PACIENTE,    bayer_pacientes.ESTADO_PACIENTE,    bayer_pacientes.STATUS_PACIENTE,    bayer_pacientes.FECHA_ACTIVACION_PACIENTE,    bayer_pacientes.FECHA_RETIRO_PACIENTE,    bayer_pacientes.MOTIVO_RETIRO_PACIENTE,    bayer_pacientes.CODIGO_XOFIGO,    bayer_pacientes.ID_ULTIMA_GESTION,     bayer_tratamiento.ID_TRATAMIENTO,    bayer_tratamiento.PRODUCTO_TRATAMIENTO,    bayer_tratamiento.NOMBRE_REFERENCIA,    bayer_tratamiento.DOSIS_TRATAMIENTO,    bayer_tratamiento.FECHA_INICIO_TERAPIA_TRATAMIENTO,    bayer_tratamiento.ASEGURADOR_TRATAMIENTO,     bayer_tratamiento.OPERADOR_LOGISTICO_TRATAMIENTO,    bayer_tratamiento.PUNTO_ENTREGA,        bayer_tratamiento.IPS_ATIENDE_TRATAMIENTO,    bayer_tratamiento.REGIMEN_TRATAMIENTO,    bayer_tratamiento.MEDICO_TRATAMIENTO,    bayer_tratamiento.FECHA_ULTIMA_RECLAMACION_TRATAMIENTO,               bayer_tratamiento.CLASIFICACION_PATOLOGICA_TRATAMIENTO,    bayer_tratamiento.TRATAMIENTO_PREVIO,    bayer_tratamiento.ID_PACIENTE_FK,     bayer_gestiones.ID_GESTION,    bayer_gestiones.LOGRO_COMUNICACION_GESTION,    bayer_gestiones.CAUSA_NO_RECLAMACION_GESTION,    bayer_gestiones.RECLAMO_GESTION,    bayer_gestiones.FECHA_RECLAMACION_GESTION,    bayer_gestiones.FECHA_COMUNICACION,    bayer_gestiones.FECHA_PROGRAMADA_GESTION,    bayer_gestiones.AUTOR_GESTION,    bayer_gestiones.DESCRIPCION_COMUNICACION_GESTION,    bayer_gestiones.FECHA_MEDICAMENTO_HASTA,       bayer_gestiones.NUMERO_CAJAS,    bayer_gestiones.PAAP,    bayer_gestiones.SUB_PAAP,    bayer_gestiones.BARRERA,    bayer_gestiones.FECHA_INI_PAAP,    bayer_gestiones.FECHA_FIN_PAAP   FROM bayer_pacientes     INNER JOIN bayer_gestiones ON  bayer_pacientes.ID_PACIENTE = bayer_gestiones.ID_PACIENTE_FK2    INNER JOIN bayer_tratamiento ON bayer_pacientes.ID_PACIENTE = bayer_tratamiento.ID_PACIENTE_FK  WHERE bayer_gestiones.FECHA_COMUNICACION = (SELECT  FECHA_COMUNICACION FROM bayer_gestiones WHERE ID_PACIENTE_FK2=ID_PACIENTE ORDER BY FECHA_COMUNICACION DESC LIMIT 1)) nm_sel_esp"; 
   } 
   $parms_det = explode("*PDet*", $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['chave_det']) ; 
   foreach ($parms_det as $key => $cada_par)
   {
       $parms_det[$key] = $this->Db->qstr($parms_det[$key]);
       $parms_det[$key] = substr($parms_det[$key], 1, strlen($parms_det[$key]) - 2);
   } 
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_access))
   {
       $nmgp_select .= " where  ID_PACIENTE = $parms_det[0] and LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and FECHA_COMUNICACION = '$parms_det[2]' and AUTOR_GESTION = '$parms_det[3]' and DEPARTAMENTO_PACIENTE = '$parms_det[4]' and CIUDAD_PACIENTE = '$parms_det[5]' and ESTADO_PACIENTE = '$parms_det[6]' and STATUS_PACIENTE = '$parms_det[7]' and FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and FECHA_RETIRO_PACIENTE = '$parms_det[9]' and MOTIVO_RETIRO_PACIENTE = '$parms_det[10]' and FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[11]' and CODIGO_XOFIGO = $parms_det[12] and CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[13]' and PRODUCTO_TRATAMIENTO = '$parms_det[14]' and NOMBRE_REFERENCIA = '$parms_det[15]' and DOSIS_TRATAMIENTO = '$parms_det[16]' and FECHA_MEDICAMENTO_HASTA = '$parms_det[17]' and NUMERO_CAJAS = '$parms_det[18]' and ASEGURADOR_TRATAMIENTO = '$parms_det[19]' and OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[20]' and PUNTO_ENTREGA = '$parms_det[21]' and IPS_ATIENDE_TRATAMIENTO = '$parms_det[22]' and REGIMEN_TRATAMIENTO = '$parms_det[23]' and MEDICO_TRATAMIENTO = '$parms_det[24]' and TRATAMIENTO_PREVIO = '$parms_det[25]' and RECLAMO_GESTION = '$parms_det[26]' and FECHA_RECLAMACION_GESTION = '$parms_det[27]' and CAUSA_NO_RECLAMACION_GESTION = '$parms_det[28]' and DESCRIPCION_COMUNICACION_GESTION = '$parms_det[29]' and FECHA_PROGRAMADA_GESTION = '$parms_det[30]' and ID_ULTIMA_GESTION = $parms_det[31] and ID_TRATAMIENTO = $parms_det[32] and ID_PACIENTE_FK = $parms_det[33] and PAAP = '$parms_det[34]' and SUB_PAAP = '$parms_det[35]' and BARRERA = '$parms_det[36]' and FECHA_INI_PAAP = '$parms_det[37]' and FECHA_FIN_PAAP = '$parms_det[38]' and FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[39]' and ID_GESTION = $parms_det[40]" ;  
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
   {
       $nmgp_select .= " where  ID_PACIENTE = $parms_det[0] and LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and FECHA_COMUNICACION = '$parms_det[2]' and AUTOR_GESTION = '$parms_det[3]' and DEPARTAMENTO_PACIENTE = '$parms_det[4]' and CIUDAD_PACIENTE = '$parms_det[5]' and ESTADO_PACIENTE = '$parms_det[6]' and STATUS_PACIENTE = '$parms_det[7]' and FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and FECHA_RETIRO_PACIENTE = '$parms_det[9]' and MOTIVO_RETIRO_PACIENTE = '$parms_det[10]' and FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[11]' and CODIGO_XOFIGO = $parms_det[12] and CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[13]' and PRODUCTO_TRATAMIENTO = '$parms_det[14]' and NOMBRE_REFERENCIA = '$parms_det[15]' and DOSIS_TRATAMIENTO = '$parms_det[16]' and FECHA_MEDICAMENTO_HASTA = '$parms_det[17]' and NUMERO_CAJAS = '$parms_det[18]' and ASEGURADOR_TRATAMIENTO = '$parms_det[19]' and OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[20]' and PUNTO_ENTREGA = '$parms_det[21]' and IPS_ATIENDE_TRATAMIENTO = '$parms_det[22]' and REGIMEN_TRATAMIENTO = '$parms_det[23]' and MEDICO_TRATAMIENTO = '$parms_det[24]' and TRATAMIENTO_PREVIO = '$parms_det[25]' and RECLAMO_GESTION = '$parms_det[26]' and FECHA_RECLAMACION_GESTION = '$parms_det[27]' and CAUSA_NO_RECLAMACION_GESTION = '$parms_det[28]' and DESCRIPCION_COMUNICACION_GESTION = '$parms_det[29]' and FECHA_PROGRAMADA_GESTION = '$parms_det[30]' and ID_ULTIMA_GESTION = $parms_det[31] and ID_TRATAMIENTO = $parms_det[32] and ID_PACIENTE_FK = $parms_det[33] and PAAP = '$parms_det[34]' and SUB_PAAP = '$parms_det[35]' and BARRERA = '$parms_det[36]' and FECHA_INI_PAAP = '$parms_det[37]' and FECHA_FIN_PAAP = '$parms_det[38]' and FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[39]' and ID_GESTION = $parms_det[40]" ;  
   } 
   else 
   { 
       $nmgp_select .= " where  ID_PACIENTE = $parms_det[0] and LOGRO_COMUNICACION_GESTION = '$parms_det[1]' and FECHA_COMUNICACION = '$parms_det[2]' and AUTOR_GESTION = '$parms_det[3]' and DEPARTAMENTO_PACIENTE = '$parms_det[4]' and CIUDAD_PACIENTE = '$parms_det[5]' and ESTADO_PACIENTE = '$parms_det[6]' and STATUS_PACIENTE = '$parms_det[7]' and FECHA_ACTIVACION_PACIENTE = '$parms_det[8]' and FECHA_RETIRO_PACIENTE = '$parms_det[9]' and MOTIVO_RETIRO_PACIENTE = '$parms_det[10]' and FECHA_INICIO_TERAPIA_TRATAMIENTO = '$parms_det[11]' and CODIGO_XOFIGO = $parms_det[12] and CLASIFICACION_PATOLOGICA_TRATAMIENTO = '$parms_det[13]' and PRODUCTO_TRATAMIENTO = '$parms_det[14]' and NOMBRE_REFERENCIA = '$parms_det[15]' and DOSIS_TRATAMIENTO = '$parms_det[16]' and FECHA_MEDICAMENTO_HASTA = '$parms_det[17]' and NUMERO_CAJAS = '$parms_det[18]' and ASEGURADOR_TRATAMIENTO = '$parms_det[19]' and OPERADOR_LOGISTICO_TRATAMIENTO = '$parms_det[20]' and PUNTO_ENTREGA = '$parms_det[21]' and IPS_ATIENDE_TRATAMIENTO = '$parms_det[22]' and REGIMEN_TRATAMIENTO = '$parms_det[23]' and MEDICO_TRATAMIENTO = '$parms_det[24]' and TRATAMIENTO_PREVIO = '$parms_det[25]' and RECLAMO_GESTION = '$parms_det[26]' and FECHA_RECLAMACION_GESTION = '$parms_det[27]' and CAUSA_NO_RECLAMACION_GESTION = '$parms_det[28]' and DESCRIPCION_COMUNICACION_GESTION = '$parms_det[29]' and FECHA_PROGRAMADA_GESTION = '$parms_det[30]' and ID_ULTIMA_GESTION = $parms_det[31] and ID_TRATAMIENTO = $parms_det[32] and ID_PACIENTE_FK = $parms_det[33] and PAAP = '$parms_det[34]' and SUB_PAAP = '$parms_det[35]' and BARRERA = '$parms_det[36]' and FECHA_INI_PAAP = '$parms_det[37]' and FECHA_FIN_PAAP = '$parms_det[38]' and FECHA_ULTIMA_RECLAMACION_TRATAMIENTO = '$parms_det[39]' and ID_GESTION = $parms_det[40]" ;  
   } 
   $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
   $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select; 
   $rs = $this->Db->Execute($nmgp_select) ; 
   if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1) 
   { 
       $this->Erro->mensagem (__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg()); 
       exit ; 
   }  
   $this->id_paciente = $rs->fields[0] ;  
   $this->id_paciente = (string)$this->id_paciente;
   $this->departamento_paciente = $rs->fields[1] ;  
   $this->ciudad_paciente = $rs->fields[2] ;  
   $this->estado_paciente = $rs->fields[3] ;  
   $this->status_paciente = $rs->fields[4] ;  
   $this->fecha_activacion_paciente = $rs->fields[5] ;  
   $this->fecha_retiro_paciente = $rs->fields[6] ;  
   $this->motivo_retiro_paciente = $rs->fields[7] ;  
   $this->codigo_xofigo = $rs->fields[8] ;  
   $this->codigo_xofigo = (string)$this->codigo_xofigo;
   $this->id_ultima_gestion = $rs->fields[9] ;  
   $this->id_ultima_gestion = (string)$this->id_ultima_gestion;
   $this->id_tratamiento = $rs->fields[10] ;  
   $this->id_tratamiento = (string)$this->id_tratamiento;
   $this->producto_tratamiento = $rs->fields[11] ;  
   $this->nombre_referencia = $rs->fields[12] ;  
   $this->dosis_tratamiento = $rs->fields[13] ;  
   $this->fecha_inicio_terapia_tratamiento = $rs->fields[14] ;  
   $this->asegurador_tratamiento = $rs->fields[15] ;  
   $this->operador_logistico_tratamiento = $rs->fields[16] ;  
   $this->punto_entrega = $rs->fields[17] ;  
   $this->ips_atiende_tratamiento = $rs->fields[18] ;  
   $this->regimen_tratamiento = $rs->fields[19] ;  
   $this->medico_tratamiento = $rs->fields[20] ;  
   $this->fecha_ultima_reclamacion_tratamiento = $rs->fields[21] ;  
   $this->clasificacion_patologica_tratamiento = $rs->fields[22] ;  
   $this->tratamiento_previo = $rs->fields[23] ;  
   $this->id_paciente_fk = $rs->fields[24] ;  
   $this->id_paciente_fk = (string)$this->id_paciente_fk;
   $this->id_gestion = $rs->fields[25] ;  
   $this->id_gestion = (string)$this->id_gestion;
   $this->logro_comunicacion_gestion = $rs->fields[26] ;  
   $this->causa_no_reclamacion_gestion = $rs->fields[27] ;  
   $this->reclamo_gestion = $rs->fields[28] ;  
   $this->fecha_reclamacion_gestion = $rs->fields[29] ;  
   $this->fecha_comunicacion = $rs->fields[30] ;  
   $this->fecha_programada_gestion = $rs->fields[31] ;  
   $this->autor_gestion = $rs->fields[32] ;  
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
   { 
       $this->descripcion_comunicacion_gestion = "";  
       if (is_file($rs->fields[33])) 
       { 
           $this->descripcion_comunicacion_gestion = file_get_contents($rs->fields[33]);  
       } 
   } 
   elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_ibase))
   { 
       $this->descripcion_comunicacion_gestion = $this->Db->BlobDecode($rs->fields[33]) ;  
   } 
   else
   { 
       $this->descripcion_comunicacion_gestion = $rs->fields[33] ;  
   } 
   $this->fecha_medicamento_hasta = $rs->fields[34] ;  
   $this->numero_cajas = $rs->fields[35] ;  
   $this->paap = $rs->fields[36] ;  
   $this->sub_paap = $rs->fields[37] ;  
   $this->barrera = $rs->fields[38] ;  
   $this->fecha_ini_paap = $rs->fields[39] ;  
   $this->fecha_fin_paap = $rs->fields[40] ;  
   if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_postgres))
   { 
       if (!empty($this->descripcion_comunicacion_gestion))
       { 
           $this->descripcion_comunicacion_gestion = $this->Db->BlobDecode($this->descripcion_comunicacion_gestion, false, true, "BLOB");
       }
   }
   if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['cmp_acum']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['cmp_acum']))
   {
       $parms_acum = explode(";", $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['cmp_acum']);
       foreach ($parms_acum as $cada_par)
       {
          $cada_val = explode("=", $cada_par);
          $this->$cada_val[0] = $cada_val[1];
       }
   }
//--- 
   $nm_saida->saida("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\r\n");
   $nm_saida->saida("            \"http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd\">\r\n");
   $nm_saida->saida("<html" . $_SESSION['scriptcase']['reg_conf']['html_dir'] . ">\r\n");
   $nm_saida->saida("<HEAD>\r\n");
   $nm_saida->saida("   <TITLE>Informe Reclamaciones</TITLE>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Content-Type\" content=\"text/html; charset=" . $_SESSION['scriptcase']['charset_html'] . "\" />\r\n");
   $nm_saida->saida(" <META http-equiv=\"Expires\" content=\"Fri, Jan 01 1900 00:00:00 GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Last-Modified\" content=\"" . gmdate("D, d M Y H:i:s") . " GMT\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"no-store, no-cache, must-revalidate\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Cache-Control\" content=\"post-check=0, pre-check=0\"/>\r\n");
   $nm_saida->saida(" <META http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n");
   if ($_SESSION['scriptcase']['proc_mobile'])
   {
       $nm_saida->saida(" <meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\" />\r\n");
   }

   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery/js/jquery.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/malsup-blockui/jquery.blockUI.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\">var sc_pathToTB = '" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/';</script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox-compressed.js\"></script>\r\n");
   $nm_saida->saida(" <script type=\"text/javascript\" src=\"../_lib/lib/js/jquery.scInput.js\"></script>\r\n");
   $nm_saida->saida(" <link rel=\"stylesheet\" href=\"" . $this->Ini->path_prod . "/third/jquery_plugin/thickbox/thickbox.css\" type=\"text/css\" media=\"screen\" />\r\n");
   if (($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['det_print'] == "print" && strtoupper($nmgp_cor_print) == "PB") || $nmgp_tipo_pdf == "pb")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid_bw" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
   else
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid.css\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "_lib/css/" . $this->Ini->str_schema_all . "_grid" . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".css\" /> \r\n");
   }
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"" . $this->Ini->path_link . "Informe_reclamacion/Informe_reclamacion_det_" . strtolower($_SESSION['scriptcase']['reg_conf']['css_dir']) . ".css\" />\r\n");
   if (!$_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['pdf_det'] && $_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['det_print'] != "print")
   {
       $nm_saida->saida(" <link rel=\"stylesheet\" type=\"text/css\" href=\"../_lib/buttons/" . $this->Ini->Str_btn_css . "\" /> \r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
       $nm_saida->saida(" <link rel=\"stylesheet\" href=\"../_lib/css/" . $_SESSION['scriptcase']['erro']['str_schema_dir'] . "\" type=\"text/css\" media=\"screen\" />\r\n");
   }
   $nm_saida->saida("</HEAD>\r\n");
   $nm_saida->saida("  <body class=\"scGridPage\">\r\n");
   $nm_saida->saida("  " . $this->Ini->Ajax_result_set . "\r\n");
   $nm_saida->saida("<table border=0 align=\"center\" valign=\"top\" ><tr><td style=\"padding: 0px\"><div class=\"scGridBorder\"><table width='100%' cellspacing=0 cellpadding=0><tr><td>\r\n");
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<style>\r\n");
   $nm_saida->saida("#lin1_col1 { padding-left:9px; padding-top:7px;  height:27px; overflow:hidden; text-align:left;}			 \r\n");
   $nm_saida->saida("#lin1_col2 { padding-right:9px; padding-top:7px; height:27px; text-align:right; overflow:hidden;   font-size:12px; font-weight:normal;}\r\n");
   $nm_saida->saida("</style>\r\n");
   $nm_saida->saida("<div style=\"width: 100%\">\r\n");
   $nm_saida->saida(" <div class=\"scGridHeader\" style=\"height:11px; display: block; border-width:0px; \"></div>\r\n");
   $nm_saida->saida(" <div style=\"height:37px; border-width:0px 0px 1px 0px;  border-style: dashed; border-color:#ddd; display: block\">\r\n");
   $nm_saida->saida(" 	<table style=\"width:100%; border-collapse:collapse; padding:0;\">\r\n");
   $nm_saida->saida("    	<tr>\r\n");
   $nm_saida->saida("        	<td id=\"lin1_col1\" class=\"scGridHeaderFont\"><span>Informe Reclamaciones</span></td>\r\n");
   $nm_saida->saida("            <td id=\"lin1_col2\" class=\"scGridHeaderFont\"><span></span></td>\r\n");
   $nm_saida->saida("        </tr>\r\n");
   $nm_saida->saida("    </table>		 \r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("</div>\r\n");
   $nm_saida->saida("  </TD>\r\n");
   $nm_saida->saida(" </TR>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       if ($this->nmgp_botoes['det_pdf'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bpdf", "", "", "Dpdf_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "Informe_reclamacion/Informe_reclamacion_config_pdf.php?nm_opc=pdf_det&nm_target=0&nm_cor=cor&papel=1&orientacao=1&largura=1200&conf_larg=S&conf_fonte=10&language=es&conf_socor=S&KeepThis=false&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       if ($this->nmgp_botoes['det_print'] == "on")
       {
         $Cod_Btn = nmButtonOutput($this->arr_buttons, "bprint", "", "", "Dprint_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "thickbox", "" . $this->Ini->path_link . "Informe_reclamacion/Informe_reclamacion_config_print.php?nm_opc=detalhe&nm_cor=AM&language=es&KeepThis=true&TB_iframe=true&modal=true", "", "only_text", "text_right", "", "", "", "", "", "");
         $nm_saida->saida("           $Cod_Btn \r\n");
       }
       $Cod_Btn = nmButtonOutput($this->arr_buttons, "bvoltar", "document.F3.submit()", "document.F3.submit()", "sc_b_sai_top", "", "", "", "absmiddle", "", "0px", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
       $nm_saida->saida("           $Cod_Btn \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $nm_saida->saida("<tr><td class=\"scGridTabelaTd\">\r\n");
   $nm_saida->saida("<TABLE style=\"padding: 0px; spacing: 0px; border-width: 0px;\"  align=\"center\" valign=\"top\" width=\"100%\">\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id_paciente'])) ? $this->New_label['id_paciente'] : "ID PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->id_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_id_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_id_paciente_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['departamento_paciente'])) ? $this->New_label['departamento_paciente'] : "DEPARTAMENTO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->departamento_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_departamento_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_departamento_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['ciudad_paciente'])) ? $this->New_label['ciudad_paciente'] : "CIUDAD PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->ciudad_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_ciudad_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_ciudad_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['estado_paciente'])) ? $this->New_label['estado_paciente'] : "ESTADO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->estado_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_estado_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_estado_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['status_paciente'])) ? $this->New_label['status_paciente'] : "STATUS PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->status_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_status_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_status_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_activacion_paciente'])) ? $this->New_label['fecha_activacion_paciente'] : "FECHA ACTIVACION PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->fecha_activacion_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_activacion_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_fecha_activacion_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_retiro_paciente'])) ? $this->New_label['fecha_retiro_paciente'] : "FECHA RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->fecha_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['motivo_retiro_paciente'])) ? $this->New_label['motivo_retiro_paciente'] : "MOTIVO RETIRO PACIENTE"; 
          $conteudo = trim(sc_strip_script($this->motivo_retiro_paciente)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_motivo_retiro_paciente_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_motivo_retiro_paciente_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['codigo_xofigo'])) ? $this->New_label['codigo_xofigo'] : "CODIGO XOFIGO"; 
          $conteudo = trim(sc_strip_script($this->codigo_xofigo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_codigo_xofigo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_codigo_xofigo_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id_ultima_gestion'])) ? $this->New_label['id_ultima_gestion'] : "ID ULTIMA GESTION"; 
          $conteudo = trim(sc_strip_script($this->id_ultima_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_id_ultima_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_id_ultima_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id_tratamiento'])) ? $this->New_label['id_tratamiento'] : "ID TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->id_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_id_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_id_tratamiento_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['producto_tratamiento'])) ? $this->New_label['producto_tratamiento'] : "PRODUCTO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->producto_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_producto_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_producto_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['nombre_referencia'])) ? $this->New_label['nombre_referencia'] : "NOMBRE REFERENCIA"; 
          $conteudo = trim(sc_strip_script($this->nombre_referencia)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_nombre_referencia_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_nombre_referencia_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['dosis_tratamiento'])) ? $this->New_label['dosis_tratamiento'] : "DOSIS TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->dosis_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_dosis_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_dosis_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_inicio_terapia_tratamiento'])) ? $this->New_label['fecha_inicio_terapia_tratamiento'] : "FECHA INICIO TERAPIA TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->fecha_inicio_terapia_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_inicio_terapia_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_inicio_terapia_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['asegurador_tratamiento'])) ? $this->New_label['asegurador_tratamiento'] : "ASEGURADOR TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->asegurador_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_asegurador_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_asegurador_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['operador_logistico_tratamiento'])) ? $this->New_label['operador_logistico_tratamiento'] : "OPERADOR LOGISTICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->operador_logistico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_operador_logistico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_operador_logistico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['punto_entrega'])) ? $this->New_label['punto_entrega'] : "PUNTO ENTREGA"; 
          $conteudo = trim(sc_strip_script($this->punto_entrega)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_punto_entrega_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_punto_entrega_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['ips_atiende_tratamiento'])) ? $this->New_label['ips_atiende_tratamiento'] : "IPS"; 
          $conteudo = trim(sc_strip_script($this->ips_atiende_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_ips_atiende_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_ips_atiende_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['regimen_tratamiento'])) ? $this->New_label['regimen_tratamiento'] : "REGIMEN TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->regimen_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_regimen_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_regimen_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['medico_tratamiento'])) ? $this->New_label['medico_tratamiento'] : "MEDICO TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->medico_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_medico_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_medico_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_ultima_reclamacion_tratamiento'])) ? $this->New_label['fecha_ultima_reclamacion_tratamiento'] : "FECHA ULTIMA RECLAMACION TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->fecha_ultima_reclamacion_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_ultima_reclamacion_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_fecha_ultima_reclamacion_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['clasificacion_patologica_tratamiento'])) ? $this->New_label['clasificacion_patologica_tratamiento'] : "CLASIFICACION PATOLOGICA TRATAMIENTO"; 
          $conteudo = trim(sc_strip_script($this->clasificacion_patologica_tratamiento)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_clasificacion_patologica_tratamiento_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_clasificacion_patologica_tratamiento_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['tratamiento_previo'])) ? $this->New_label['tratamiento_previo'] : "TRATAMIENTO PREVIO"; 
          $conteudo = trim(sc_strip_script($this->tratamiento_previo)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_tratamiento_previo_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_tratamiento_previo_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id_paciente_fk'])) ? $this->New_label['id_paciente_fk'] : "ID PACIENTE FK"; 
          $conteudo = trim(sc_strip_script($this->id_paciente_fk)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_id_paciente_fk_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_id_paciente_fk_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['id_gestion'])) ? $this->New_label['id_gestion'] : "ID GESTION"; 
          $conteudo = trim(NM_encode_input(sc_strip_script($this->id_gestion))); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else    
          { 
              nmgp_Form_Num_Val($conteudo, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_id_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_id_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['logro_comunicacion_gestion'])) ? $this->New_label['logro_comunicacion_gestion'] : "LOGRO COMUNICACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->logro_comunicacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_logro_comunicacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_logro_comunicacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['causa_no_reclamacion_gestion'])) ? $this->New_label['causa_no_reclamacion_gestion'] : "CAUSA NO RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->causa_no_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_causa_no_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_causa_no_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['reclamo_gestion'])) ? $this->New_label['reclamo_gestion'] : "RECLAMO GESTION"; 
          $conteudo = trim(sc_strip_script($this->reclamo_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_reclamo_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_reclamo_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_reclamacion_gestion'])) ? $this->New_label['fecha_reclamacion_gestion'] : "FECHA RECLAMACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->fecha_reclamacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_reclamacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_fecha_reclamacion_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_comunicacion'])) ? $this->New_label['fecha_comunicacion'] : "FECHA COMUNICACION"; 
          $conteudo = trim(NM_encode_input(sc_strip_script($this->fecha_comunicacion))); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
          else    
          { 
               $conteudo_x =  $conteudo;
               nm_conv_limpa_dado($conteudo_x, "");
               if (is_numeric($conteudo_x) && $conteudo_x > 0) 
               { 
                   $this->nm_data->SetaData($conteudo, "");
                   $conteudo = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DH", "ddmmaaaa;hhii"));
               } 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_comunicacion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_comunicacion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_programada_gestion'])) ? $this->New_label['fecha_programada_gestion'] : "FECHA PROGRAMADA GESTION"; 
          $conteudo = trim(sc_strip_script($this->fecha_programada_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_programada_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_fecha_programada_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['autor_gestion'])) ? $this->New_label['autor_gestion'] : "AUTOR GESTION"; 
          $conteudo = trim(sc_strip_script($this->autor_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_autor_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_autor_gestion_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['descripcion_comunicacion_gestion'])) ? $this->New_label['descripcion_comunicacion_gestion'] : "DESCRIPCION COMUNICACION GESTION"; 
          $conteudo = trim(sc_strip_script($this->descripcion_comunicacion_gestion)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_descripcion_comunicacion_gestion_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_descripcion_comunicacion_gestion_det_line\"  NOWRAP ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_medicamento_hasta'])) ? $this->New_label['fecha_medicamento_hasta'] : "FECHA MEDICAMENTO HASTA"; 
          $conteudo = trim(sc_strip_script($this->fecha_medicamento_hasta)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_medicamento_hasta_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_medicamento_hasta_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['numero_cajas'])) ? $this->New_label['numero_cajas'] : "NUMERO CAJAS"; 
          $conteudo = trim(sc_strip_script($this->numero_cajas)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_numero_cajas_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_numero_cajas_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['paap'])) ? $this->New_label['paap'] : "PAAP"; 
          $conteudo = trim(sc_strip_script($this->paap)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_paap_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_paap_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['sub_paap'])) ? $this->New_label['sub_paap'] : "SUB PAAP"; 
          $conteudo = trim(sc_strip_script($this->sub_paap)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_sub_paap_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_sub_paap_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['barrera'])) ? $this->New_label['barrera'] : "BARRERA"; 
          $conteudo = trim(sc_strip_script($this->barrera)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_barrera_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_barrera_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_ini_paap'])) ? $this->New_label['fecha_ini_paap'] : "FECHA INI PAAP"; 
          $conteudo = trim(sc_strip_script($this->fecha_ini_paap)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_ini_paap_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldEvenVert css_fecha_ini_paap_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("  <TR class=\"scGridLabel\">\r\n");
          $SC_Label = (isset($this->New_label['fecha_fin_paap'])) ? $this->New_label['fecha_fin_paap'] : "FECHA FIN PAAP"; 
          $conteudo = trim(sc_strip_script($this->fecha_fin_paap)); 
          if ($conteudo === "") 
          { 
              $conteudo = "&nbsp;" ; 
          } 
   $nm_saida->saida("    <TD class=\"scGridLabelFont css_fecha_fin_paap_det_label\"  >" . nl2br($SC_Label) . "</TD>\r\n");
   $nm_saida->saida("    <TD class=\"scGridFieldOddVert css_fecha_fin_paap_det_line\"   ALIGN=\"\" VALIGN=\"\">" . $conteudo . "</TD>\r\n");
   $nm_saida->saida("   \r\n");
   $nm_saida->saida("  </TR>\r\n");
   $nm_saida->saida("</TABLE>\r\n");
   if ($_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['det_print'] != "print" && !$_SESSION['sc_session'][$this->Ini->sc_page]['Informe_reclamacion']['pdf_det']) 
   { 
       $nm_saida->saida("   <tr><td class=\"scGridTabelaTd\">\r\n");
       $nm_saida->saida("    <table width=\"100%\"><tr>\r\n");
       $nm_saida->saida("     <td class=\"scGridToolbar\">\r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"center\" width=\"33%\"> \r\n");
       $nm_saida->saida("         </td> \r\n");
       $nm_saida->saida("          <td class=\"" . $this->css_scGridToolbarPadd . "\" nowrap valign=\"middle\" align=\"right\" width=\"33%\"> \r\n");
       $nm_saida->saida("     </td>\r\n");
       $nm_saida->saida("    </tr></table>\r\n");
       $nm_saida->saida("   </td></tr>\r\n");
   } 
   $rs->Close(); 
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
   $nm_saida->saida(" </div>\r\n");
   $nm_saida->saida("  </td>\r\n");
   $nm_saida->saida(" </tr>\r\n");
   $nm_saida->saida(" </table>\r\n");
//--- 
//--- 
   $nm_saida->saida("<form name=\"F3\" method=post\r\n");
   $nm_saida->saida("                  target=\"_self\"\r\n");
   $nm_saida->saida("                  action=\"./\">\r\n");
   $nm_saida->saida("<input type=hidden name=\"nmgp_opcao\" value=\"igual\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_init\" value=\"" . NM_encode_input($this->Ini->sc_page) . "\"/>\r\n");
   $nm_saida->saida("<input type=hidden name=\"script_case_session\" value=\"" . NM_encode_input(session_id()) . "\"/>\r\n");
   $nm_saida->saida("</form>\r\n");
   $nm_saida->saida("<script language=JavaScript>\r\n");
   $nm_saida->saida("   function nm_mostra_doc(campo1, campo2, campo3)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       NovaJanela = window.open (\"Informe_reclamacion_doc.php?script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&nm_cod_doc=\" + campo1 + \"&nm_nome_doc=\" + campo2 + \"&nm_cod_apl=\" + campo3, \"ScriptCase\", \"resizable\");\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_move(x, y, z, p, g) \r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("      window.location = \"" . $this->Ini->path_link . "Informe_reclamacion/index.php?nmgp_opcao=pdf_det&nmgp_tipo_pdf=\" + z + \"&nmgp_parms_pdf=\" + p +  \"&nmgp_graf_pdf=\" + g + \"&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "\";\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("   function nm_gp_print_conf(tp, cor)\r\n");
   $nm_saida->saida("   {\r\n");
   $nm_saida->saida("       window.open('" . $this->Ini->path_link . "Informe_reclamacion/Informe_reclamacion_iframe_prt.php?path_botoes=" . $this->Ini->path_botoes . "&script_case_init=" . NM_encode_input($this->Ini->sc_page) . "&script_case_session=" . session_id() . "&opcao=det_print&cor_print=' + cor,'','location=no,menubar,resizable,scrollbars,status=no,toolbar');\r\n");
   $nm_saida->saida("   }\r\n");
   $nm_saida->saida("</script>\r\n");
   $nm_saida->saida("</body>\r\n");
   $nm_saida->saida("</html>\r\n");
 }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
}
