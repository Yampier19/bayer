<?php
include('../logica/session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="windows-1252">

    <title>Documento sin t�tulo</title>
    <link rel="stylesheet" href="css/menu.css" />
    <link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
    <link type="text/css" rel="stylesheet" href="css/estilo_form_paciente.css" />
    <link rel="stylesheet" href="../presentacion/css/menu.css" />
</head>
<script src="js/jquery.js"></script>
<script src="../presentacion/js/jquery.js"></script>
<script>
    var height = window.innerHeight - 2;
    var porh = (height * 74 / 100);
    $(document).ready(function() {
        $('#info').css('height', porh);
    });
</script>
<style>
    html {
        background: url(../presentacion/imagenes/FONDO.png) no-repeat fixed center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .circulo {
        width: 100px;
        height: 100px;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        border-radius: 50%;
        background: red;
    }
</style>
<?php
if ($usua == 'YRAMIREZ' || $usua == 'GPARRA') {
?>
    <style>
        .div_menu {
            margin: 0px auto;
            width: 1030px;
            font-family: Tahoma, Geneva, sans-serif;
            background-color: #92c14a;
            margin: auto auto;
            margin-top: 15%;
            padding: 5px;
        }
    </style>
<?php
} elseif ($usua == 'ADMIN') {
?>
    <style>
        .div_menu {
            margin: 0px auto;
            width: 1030px;
            font-family: Tahoma, Geneva, sans-serif;
            background-color: #92c14a;
            margin: auto auto;
            margin-top: 15%;
            padding: 5px;
        }
    </style>
<?php
} else {
?>
    <style>
        .div_menu {
            margin: 0px auto;
            width: 805px;
            font-family: Tahoma, Geneva, sans-serif;
            background-color: #92c14a;
            margin: auto auto;
            margin-top: 15%;
            padding: 5px;
        }
    </style>
<?php
}
?>
<?php
if ($privilegios != '' && $usua != '') {

    $select_opl = mysqli_query($conex, "SELECT * FROM bayer_operador_logistico WHERE ESTADO = 'OUT'");
    $num_total_opl = mysqli_num_rows($select_opl);

    $select_asegurador = mysqli_query($conex, "SELECT * FROM bayer_asegurador WHERE ESTADO = 'OUT'");
    $num_total_asegurador = mysqli_num_rows($select_asegurador);

    $select_ips = mysqli_query($conex, "SELECT * FROM bayer_ips WHERE ESTADO = 'OUT'");
    $num_total_ips = mysqli_num_rows($select_ips);

    $select_medicos = mysqli_query($conex, "SELECT * FROM bayer_listas WHERE ESTADO = 'OUT'");
    $num_total_medicos = mysqli_num_rows($select_medicos);

    $select_puntos = mysqli_query($conex, "SELECT * FROM bayer_puntos_entrega WHERE ESTADO = 'OUT'");
    $num_total_puntos = mysqli_num_rows($select_puntos);

    $num_total_registros = $num_total_opl + $num_total_asegurador + $num_total_ips + $num_total_medicos + $num_total_puntos;
?>

    <body>
        <br>
        <br>
        <div class="body">
            <div class="div_menu" style="margin-top:-20px;">
                <ul>
                    <li><a href="#"><span class="icon-home3"></span>NOVEDADES</a>
                        <ul>
                            <li><a href="../presentacion/novedades_registro.php" target="info"><span class="icon-unlocked"></span> REGISTRO NOVEDAD</a></li>
                            <li><a href="../presentacion/novedades_correo.php" target="info"><span class="icon-unlocked"></span> CONSULTA NOVEDAD</a></li>
                            <li><a href="../informes/grid_bayer_novedades" target="info"><span class="icon-unlocked"></span> REPORTE NOVEDAD</a></li>
                        </ul>
                    </li>
                    <li><a href="../presentacion/form_paciente_nuevo.php" target="info"><span class="icon-paste"></span> PACIENTE NUEVO</a>
                    </li>
                    <li><a href="../presentacion/form_paciente_seguimiento.php" target="info"><span class="icon-user-check"></span> SEGUIMIENTO </a>
                    </li>
                    <li><a href="#"><span class="icon-clipboard"></span> PRODUCTOS</a>
                        <ul>
                            <li><a href="../presentacion/form_registro_material.php" target="info"><span class="icon-unlocked"></span> REGISTRO MATERIAL</a></li>
                            <li><a href="../presentacion/form_inventario.php" target="info"><span class="icon-unlocked"></span> INVENTARIO</a></li>
                        </ul>
                    </li>
                    <li><a href="../presentacion/form_reporte.php" target="info"><span class="icon-user-check"></span> REPORTES </a>
                        <ul>
                            <li>
                                <a href="../SCRIPTCASE/new_scriptcase" target="info"><span class="icon-unlocked"></span> OTROS REPORTES</a>
                            </li>
                        </ul>
                    </li>
                    <?php
                    if ($usua == 'ADMIN' || $usua == 'YRAMIREZ') {
                    ?>
                        <li><a href="#"><span class="icon-unlocked"></span> CONFIGURACION</a>
                            <ul>
                                <li><a href="../presentacion/form_cargar_observacion_fundem.php" target="info"><span class="icon-unlocked"></span> GESTIONES FUNDEM</a></li>
                                <li><a href="../presentacion/form_cambio_contacto_fundem.php" target="info"><span class="icon-unlocked"></span> CAMBIO DE FECHA</a></li>
                                <li><a href="../presentacion/form_asignacion_gestiones.php" target="info"><span class="icon-unlocked"></span> ASIGNACION</a></li>
                                <li><a href="../presentacion/form_usuarios.php" target="info"><span class="icon-unlocked"></span> USUARIOS</a></li>
                                <li><a href="../presentacion/form_cuenta_usuario.php" target="info"><span class="icon-unlocked"></span> MI CUENTA</a></li>
                            </ul>
                        </li>
                    <?php
                    } else {
                    ?>
                        <li><a href="../presentacion/form_cuenta_usuario.php" target="info"><span class="icon-user-check"></span> MI CUENTA </a>
                        </li>
                    <?php
                    }
                    ?>
                    <?php if ($usua == 'ADMIN' || $usua == 'YRAMIREZ' || $usua == 'GPARRA') { ?>
                        <li><a href="#"><span class="icon-unlocked"></span> NOTIFICACIONES <span class="circulo" style="color:#fff;"><?php echo $num_total_registros ?></span></a>
                            <ul>
                                <li><a href="../presentacion/form_listado_operador.php" target="info"><span class="icon-unlocked"></span> HABILITAR OPERADOR LOGISTICO <span class="circulo" style="color:#fff;"><?php echo $num_total_opl ?></span></a></li>
                                <li><a href="../presentacion/form_listado_asegurador.php" target="info"><span class="icon-unlocked"></span> HABILITAR ASEGURADOR <span class="circulo" style="color:#fff;"><?php echo $num_total_asegurador ?></span></a></li>
                                <li><a href="../presentacion/form_listado_ips.php" target="info"><span class="icon-unlocked"></span> HABILITAR IPS <span class="circulo" style="color:#fff;"><?php echo $num_total_ips ?></span></a></li>
                                <li><a href="../presentacion/form_listado_medicos.php" target="info"><span class="icon-unlocked"></span> HABILITAR MEDICOS <span class="circulo" style="color:#fff;"><?php echo $num_total_medicos ?></span></a></li>
                                <li><a href="../presentacion/form_listado_puntos.php" target="info"><span class="icon-unlocked"></span> HABILITAR PUNTOS DE ENTREGA <span class="circulo" style="color:#fff;"><?php echo $num_total_puntos ?></span></a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li class="a1" id="salir"><a href="../logica/cerrar_sesion.php" style="border-right:2px solid transparent;"><span class="icon-exit"></span>SALIR</a></li>
                </ul>
            </div>
        </div>
        <div class="body">
            <iframe style=" padding-top:20px; width:100%;border:1px solid transparent" name="info" id="info" scrolling="auto"></iframe>
        </div>
    </body>
<?php
} else {
?>
    <script type="text/javascript">
        window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
    </script>
<?php
}
?>

</html>