<!DOCTYPE html>
<html>
<head>
  <title>tabla reporte conteo</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" type="text/css" href="css/menu_conteo.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap_menuc.css">
</head>
<body>
  <div class="body">
    <header class="complemento_menu">
      <div class="contenido_principal">
        <div class="div_menuss">
          <ul class="nav nav-pills nav-fill">
            <li class="nav-item"><a class="nav-link" href="reporte_tabla_conteo.php" target="info">Reporte Causal Conteo</a></li>
          </ul>
        </div>
      </div>
    </header>
  </div>
  <div class="body">
    <iframe class="menu_principal" name="info" id="info" scrolling="auto"></iframe>
  </div>
  </div>
</body>
</html>