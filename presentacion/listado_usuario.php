<?php
include('../logica/session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>LISTADO USUARIOS</title>
    <link rel="stylesheet" type="text/css" href="../presentacion/css/estilo_tablas.css" />
</head>
<?php
require('../datos/parse_str.php');
require('../datos/conex.php');
if ($privilegios != '' && $usua != '') {
?>
    <body>
        <?php
        include('../logica/consultas_usuarios.php');
        $url = "../presentacion/listado_usuario.php";
        $num_total = mysqli_num_rows($SELECT_USUARIO_TOTAL);
        if ($num_total > 0) {
        ?>
            <table border="0" bordercolor="#A1A1A1" width="100%" rules="cols">
                <tr>
                    <th class="botones">NOMBRE USUARIO</th>
                    <th class="botones">NOMBRE(S) Y APELLIDO(S)</th>
                    <?PHP
                    if ($privilegios == '1') {
                    ?>
                        <th class="botones">PERFIL</th>
                        <th class="botones">ESTADO</th>
                        <th class="botones">ACCION</th>
                    <?PHP
                    }
                    ?>
                </tr>
                <?PHP
                //Limito la busqueda
                $TAMANO_PAGINA = 10;
                $pagina = false;
                //examino la pagina a mostrar y el inicio del registro a mostrar
                if (isset($_GET["pagina"]))
                    $pagina = $_GET["pagina"];
                if (!$pagina) {
                    $inicio = 0;
                    $pagina = 1;
                } else {
                    $inicio = ($pagina - 1) * $TAMANO_PAGINA;
                }
                //calculo el total de paginas
                $total_paginas = ceil($num_total / $TAMANO_PAGINA);
                //pongo el numero de registros total, el tamaño de pagina y la pagina que se muestra
                /*echo '<h3>Numero de articulos: '.$num_total .'</h3>';
            echo '<h3>En cada pagina se muestra '.$TAMANO_PAGINA.' articulos ordenados por fecha de forma descendente.</h3>';
            echo '<h3>Mostrando la pagina '.$pagina.' de ' .$total_paginas.' paginas.</h3>';*/
                $consulta = "$SELECT_USUARIO " . $inicio . "," . $TAMANO_PAGINA;
                $consulta_ref = mysqli_query($conex, $consulta);
                while ($fila1 = mysqli_fetch_array($consulta_ref)) {
                ?>
                    <tr align="center">
                        <td><?php echo $fila1['USER'] ?></td>
                        <td><?php echo $fila1['NOMBRES'] . ' ' . $fila1['APELLIDOS'] ?></td>
                        <?php
                        if ($privilegios == '1') {
                            if ($fila1['PRIVILEGIOS'] == 1) {
                                $perfil = 'COORDINADOR(A)';
                            }
                            if ($fila1['PRIVILEGIOS'] == 2) {
                                $perfil = 'ASESOR(A)';
                            }
                            if ($fila1['PRIVILEGIOS'] == 3) {
                                $perfil = 'OTRO';
                            }
                            if ($fila1['ESTADO'] == 1) {
                                $ESTADO = 'ACTIVO';
                            }
                            if ($fila1['ESTADO'] == 0) {
                                $ESTADO = 'INACTIVO';
                            }
                        }
                        ?>
                        <td><?php echo $perfil ?></td>
                        <td><?php echo $ESTADO ?></td>
                        <td>
                            <?php
                            if ($privilegios == '1') {
                                $NOM = $fila1['USER'];
                                $ESTA = $ESTADO;
                                $ID = $fila1['ID_USUARIO'];
                                accion($ID, $NOM);
                                estado($ESTA, $ID);
                            }
                            ?>
                        </td>
                    </tr>
                <?php
                }
                ?>
                <tr bgcolor="#FFFFFF" class="titulo" align="center">
                    <td colspan="2" class="botones">Se encontraron Registros <?php echo $num_total; ?></td>
                    <td colspan="8" class="botones">
                        <?php
                        if ($total_paginas > 1) {
                            if ($pagina != 1)
                                echo '<a href="' . $url . '?pagina=' . ($pagina - 1) . '"><img src="../presentacion/imagenes/izq.gif" border="0"></a>';
                            for ($i = 1; $i <= $total_paginas; $i++) {
                                if ($pagina == $i)
                                    //si muestro el indice de la pagina actual, no coloco enlace
                                    echo "<label style='font-size:120%; color:#000;'> $pagina </label>";
                                else
                                    //si el indice no corresponde con la pagina mostrada actualmente,co
                                    //coloco el enlace para ir a esa pagina
                                    echo '  <a href="' . $url . '?pagina=' . $i . '" style="font-size:110%;">' . $i . '</a>  ';
                            }
                            if ($pagina != $total_paginas)
                                echo '<a href="' . $url . '?pagina=' . ($pagina + 1) . '"><img src="../presentacion/imagenes/der.gif" border="0"></a>';
                        }
                        echo '</p>';
                        ?>
                    </td>
                </tr>
            </table>
        <?php
        } else {
        ?>
            <span style="margin-top:1%;">
                <center>
                    <img src="../presentacion/imagenes/advertencia.png" style="width:70px; margin-top:1%;" />
                </center>
            </span>
            <p class="error" style=" width:68.9%; margin:auto auto;">
                <span style="border-left-color:#fff">NO SE ENCUENTRAN REGISTROS CON ESTA INFORMACION.</span>
            </p>
        <?php
        }
        ?>
    </body>
<?php
} else {
?>
    <script type="text/javascript">
        window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
    </script>
<?php
}
?>
</html>